<?php
namespace frontend\tests\fake;

/**
 * Fake request component for mock in tests
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class Request extends \yii\web\Request
{
    /**
     * @var string
     */
    public $fakeUrl;


    /**
     * Returns fake URL
     */
    public function getAbsoluteUrl()
    {
        return $this->fakeUrl;
    }
}
