<?php
namespace frontend\tests\unit\helpers;

use frontend\helpers\ExtendedHtml;
use common\tests\base\unit\TestCase;

/**
 * Test case for [[ExtendedHtml]] helper
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class ExtendedHtmlTest extends TestCase
{
    public function testAjaxLink()
    {
        $expected = '<!--noindex--><a class="ajax-link" href="test-url" rel="nofollow">test</a><!--/noindex-->';
        $actual = ExtendedHtml::ajaxLink('test', 'test-url');
        $this->assertEquals($expected, $actual);

        $expected = '<!--noindex--><a class="test-class ajax-link" href="test-url" rel="nofollow">test</a><!--/noindex-->';
        $actual = ExtendedHtml::ajaxLink('test', 'test-url', [
            'class' => 'test-class'
        ]);
        $this->assertEquals($expected, $actual);
    }

    public function testExternalLink()
    {
        $expected = '<!--noindex--><a href="test-url" rel="nofollow">test</a><!--/noindex-->';
        $actual = ExtendedHtml::externalLink('test', 'test-url');

        $this->assertEquals($expected, $actual);
    }

    public function testWrapInNoIndexTag()
    {
        $expected = '<!--noindex-->test<!--/noindex-->';
        $actual = ExtendedHtml::wrapInNoIndexTag('test');

        $this->assertEquals($expected, $actual);
    }

    public function testCallto()
    {
        $expected = '<a href="tel:7 77 777 7777">7 77 777 7777</a>';
        $actual = ExtendedHtml::callto('7 77 777 7777');
        $this->assertEquals($expected, $actual);

        $expected = '<a href="tel:7 77 777 7777">Call to us</a>';
        $actual = ExtendedHtml::callto('7 77 777 7777', 'Call to us');
        $this->assertEquals($expected, $actual);
    }
}
