<?php
namespace frontend\tests\unit\widgets\microdata;

use common\tests\base\unit\TestCase;
use frontend\tests\fake\FakeController;
use frontend\widgets\microdata\WebsiteMicrodataWidget;
use Yii;

/**
 * Test case for [[WebsiteMicrodataWidget]]
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class WebsiteMicrodataWidgetTest extends TestCase
{
    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();
        Yii::$app->controller = new FakeController('default', Yii::$app);
        Yii::$app->homeUrl = './codecept?r=default';
    }

    public function testWithoutSearchData()
    {
        $widget = new WebsiteMicrodataWidget();

        $expected = '<script type="application/ld+json">'
            . '{"@context":"http://schema.org","@type":"WebSite",'
            . '"url":"http://localhost' . Yii::$app->homeUrl . '"}'
            .'</script>';
        $actual = $widget->run();

        $this->assertEquals($expected, $actual);
    }

    public function testWithSearchData()
    {
        $widget = new WebsiteMicrodataWidget([
            'searchConfig' => [
                'target' => 'test',
                'query-input' => 'test'
            ],
        ]);

        $expected = '<script type="application/ld+json">'
            . '{"@context":"http://schema.org","@type":"WebSite",'
            . '"url":"http://localhost' . Yii::$app->homeUrl . '"'
            . ',"potentialAction":{"type":"SearchAction","target":"test","query-input":"test"}}'
            .'</script>';
        $actual = $widget->run();

        $this->assertEquals($expected, $actual);
    }
}
