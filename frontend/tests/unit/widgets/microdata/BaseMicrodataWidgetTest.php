<?php
namespace frontend\tests\unit\widgets\microdata;

use common\tests\base\unit\TestCase;
use frontend\widgets\microdata\BaseMicrodataWidget;

/**
 * Test case for [[BaseMicrodataWidget]]
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class BaseMicrodataWidgetTest extends TestCase
{
    public function testRun()
    {
        $data = ['widget' => 'test'];
        $widget = new BaseMicrodataWidget(['rawMicrodataData' => $data]);

        $expected = '<script type="application/ld+json">{"widget":"test"}</script>';
        $actual = $widget->run();
        $this->assertEquals($expected, $actual);

        $widget->rawMicrodataData = [];
        $this->assertNull($widget->run());
    }
}
