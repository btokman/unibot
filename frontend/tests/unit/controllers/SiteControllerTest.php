<?php
namespace frontend\tests\unit\controllers;

use common\tests\base\unit\DbTestCase;
use common\tests\fixtures\RobotsFixture;
use frontend\controllers\SiteController;

/**
 * Test case for [[SiteController]]
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class SiteControllerTest extends DbTestCase
{
    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'robots' => RobotsFixture::className(),
        ];
    }

    public function testActionRobots()
    {
        $c = new SiteController('test', null);

        $expected = 'User agent: *'
                . "\nDisallow:"
                . "\nSitemap: http://localhost./codecept?r=sitemap%2Fdefault%2Findex";
        $actual = $c->actionRobots();

        $this->assertEquals($expected, $actual);
    }
}
