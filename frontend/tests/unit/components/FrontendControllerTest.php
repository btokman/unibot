<?php
namespace frontend\tests\unit\components;

use common\tests\base\unit\TestCase;
use frontend\components\FrontendController;
use frontend\tests\fake\Request as FakeRequest;
use Yii;
use yii\base\Action;
use yii\web\Request;
use yii\web\Response;

/**
 * Test case for [[FrontendController]] component
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class FrontendControllerTest extends TestCase
{
    /**
     * @var FrontendController
     */
    private $_controller;
    /**
     * @var Action
     */
    private $_action;


    /**
     * @inheritdoc
     */
    protected function registerDependencies()
    {
        Yii::$container->set(
            Request::className(),
            FakeRequest::className()
        );
    }

    /**
     * @inheritdoc
     */
    protected function _before()
    {
        $this->_controller = new FrontendController('test', null);
        $this->_action = new Action('test', $this->_controller);
    }

    public function testRedirectFromWww()
    {
        Yii::$app->getRequest()->fakeUrl = 'http://www.test.dev/';
        $response = $this->_controller->beforeAction($this->_action);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals($response->getStatusCode(), 301);
        $this->assertEquals($response->getHeaders()['location'], 'http://test.dev/');
    }

    public function testRedirectFromIndexPhp()
    {
        Yii::$app->getRequest()->fakeUrl = 'http://localhost/index.php';
        $response = $this->_controller->beforeAction($this->_action);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals($response->getStatusCode(), 302);
        $this->assertEquals($response->getHeaders()['location'], 'http://localhost/');
    }
}
