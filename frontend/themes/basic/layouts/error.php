<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/static/favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= \frontend\widgets\microdata\BreadcrumbsMicrodataWidget::widget(['homePageTitle' => 'Home page title text here']) ?>
    <?= \frontend\widgets\microdata\WebsiteMicrodataWidget::widget() ?>
    <?= Yii::$app->config->get('endHead') ?>
</head>
<body>
<?php $this->beginBody() ?>
<?= $this->render('_svg') ?>
<?= $this->render('_preloader') ?>
<?= \frontend\widgets\common\HeaderWidget::widget() ?>
<main class="content" data-page="404">
    <?= $content ?>
</main>
<div class="modal modalit" id="orderBot" aria-hidden="true">
    <div class="dialog">
        <div class="modal__close" data-modal-btn="dismiss">
            <svg class="icon icon-close">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-close"></use>
            </svg>
            <svg class="icon icon-menu-close">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-menu-close"></use>
            </svg>
        </div>
        <?= \frontend\widgets\common\FormWidget::widget() ?>
    </div>
</div>
<?php $this->endBody() ?>
<?= Yii::$app->config->get('endBody') ?>
</body>
</html>
<?php $this->endPage() ?>
