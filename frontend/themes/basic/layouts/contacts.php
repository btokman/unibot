<?php

use yii\helpers\Html;


/* @var $this \yii\web\View */
/* @var $content string */
$asset = new \frontend\assets\ContactsAsset();

if (\frontend\helpers\UserAgent::isGooglePageSpeed()) {
    $css = $asset->css[0] ?? false;
    if ($css) $css = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/' . $css);
    $asset->css = [];
}

$css = $css ?? '';

$asset->register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/static/favicon.ico">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MZH86W8');</script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" async defer>
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter48176444 = new Ya.Metrika({
                        id: 48176444,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://d31j93rd8oukbv.cloudfront.net/metrika/watch_ua.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/48176444" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= \frontend\widgets\microdata\BreadcrumbsMicrodataWidget::widget(['homePageTitle' => 'Home page title text here']) ?>
    <?= \frontend\widgets\microdata\WebsiteMicrodataWidget::widget() ?>
    <?= Yii::$app->config->get('endHead') ?>
    <style>
        .is-article .breadcrumbs ul li {
            color: #fff;
        }
    </style>
</head>
<body class="is-sub <?= $this->context->bodyClass ?? '' ?>" style="cursor: default">
<?php $this->beginBody() ?>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZH86W8"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- /Yandex.Metrika counter -->
<?= $this->render('_svg') ?>
<?= $this->render('_preloader') ?>
<?= \frontend\widgets\common\HeaderWidget::widget(['isHome' => false]) ?>
<main class="content" data-page="<?= $this->context->page ?>">
    <div class="breadcrumbs">
        <div class="breadcrumbs__container container container_md">
            <?= \yii\widgets\Breadcrumbs::widget([
                'homeLink' => [
                    'label' => Yii::$app->config->get(\common\models\HomePage::PAGE_NAME),
                    'url' => Yii::$app->homeUrl,
                ],
                'options' => ['class' => ''],
                'links' => \frontend\helpers\ExtendedHtml::limitBreadCrumbsCharsets($this->params['breadcrumbs'] ?? [])
            ]) ?>
        </div>
    </div>
    <?= $content ?>
</main>
<?= \frontend\widgets\common\FooterWidget::widget(['big' => false]) ?>
<?php $this->endBody() ?>
<?= Yii::$app->config->get('endBody') ?>
</body>
<script>
    $(document).on("submit", "form", function (t) {
        t.preventDefault()
    }),
    $(document).on("beforeSubmit", "form", function (t) {
        t.preventDefault();
        var e = new FormData(this);
        jQuery.ajax({
            cache: !1,
            type: "POST",
            dataType: "json",
            data: e,
            processData: !1,
            contentType: !1,
            success: function (t) {
                window.PublicAPI.ContactsSuccess()
            },
            url: this.action
        })
    }), $(document).on("click", ".blog__btn-wrap", function (t) {
        t.preventDefault();
        var e = $(".blog__list-item").length, n = window.location.href + "?offset=" + e;
        jQuery.ajax({
            cache: !1, type: "GET", dataType: "json", processData: !1, contentType: !1, success: function (t) {
                $(".blog__list").append(t.data), t.showBtn || $(".blog__btn-wrap").hide()
                window.PublicAPI.refreshBlog();

            }, url: n
        })
    }), $(document).on("click", ".article-content__share-soc-icon-wrap > a", function (t) {
        t.preventDefault();
        var e = $(this).attr("href");
        return window.open(e, "targetWindow", "toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,               width=600,height=250"), !1
    });
</script>
</html>
<?php $this->endPage() ?>
