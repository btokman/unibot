<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

$asset = new AppAsset();

if (\frontend\helpers\UserAgent::isGooglePageSpeed()) {
    $css = $asset->css[0] ?? false;
    if ($css) $css = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/' . $css);
    $asset->css = [];
}

$css = $css ?? '';

$asset->register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/static/favicon.ico">
    <?php if (!\frontend\helpers\UserAgent::isGooglePageSpeed()) : ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-MZH86W8');</script>
    <?php endif; ?>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" async defer>
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter48176444 = new Ya.Metrika({
                        id: 48176444,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://d31j93rd8oukbv.cloudfront.net/metrika/watch_ua.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/48176444" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <style><?= $css ?></style>
    <style>.chat__content-chat-bot span{background-repeat:no-repeat!important;font-size:inherit;height:1em;width:1em;min-height:20px;min-width:20px;display:inline-block;margin:-.1em .1em .1em;line-height:1;vertical-align:middle}</style>
    <?php $this->head() ?>
    <?= \frontend\widgets\microdata\BreadcrumbsMicrodataWidget::widget(['homePageTitle' => 'Home page title text here']) ?>
    <?= \frontend\widgets\microdata\WebsiteMicrodataWidget::widget() ?>
    <?= Yii::$app->config->get('endHead') ?>
</head>
<body style="cursor: default">
<?php $this->beginBody() ?>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZH86W8"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<?= $this->render('_svg') ?>
<?= $this->render('_preloader') ?>
<?= \frontend\widgets\common\HeaderWidget::widget() ?>
<main class="content" data-page="<?= $this->context->page ?>">
    <?= $content ?>
</main>

<?= \frontend\widgets\common\FooterWidget::widget() ?>
<div class="modal modalit" id="orderBot" aria-hidden="true">
    <div class="dialog">
        <div class="modal__close" data-modal-btn="dismiss">
            <svg class="icon icon-close">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-close"></use>
            </svg>
            <svg class="icon icon-menu-close">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-menu-close"></use>
            </svg>
        </div>
        <?= \frontend\widgets\common\FormWidget::widget() ?>
    </div>
</div>
<?php $this->endBody() ?>
<?= Yii::$app->config->get('endBody') ?>
</body>
<script>
    $(document).on("submit", ".modal__form", function (t) {
        t.preventDefault()
    }), $(document).on("beforeSubmit", ".modal__form", function (t) {
        t.preventDefault();
        var o = new FormData(this);
        jQuery.ajax({
            cache: !1,
            type: "POST",
            dataType: "json",
            data: o,
            processData: !1,
            contentType: !1,
            success: function (t) {
                window.PublicAPI.Modal.success("orderBot")
            },
            url: this.action
        })
    }), $(".chat__content-controls-form").on("submit", function (t) {
        var o = $(".chat__content-controls-form-input").val();
        if (!(!o.length > 2)) {
            var n = $(this).attr("action") + "?email=" + o;
            jQuery.ajax({cache: !1, processData: !1, contentType: !1, url: n})
        }
    });
</script>
</html>
<?php $this->endPage() ?>
