<?php
/**
 * @var $model \common\models\NotFoundPage
 */

use metalguardian\fileProcessor\helpers\FPM;

?>

<div class="b-404">
    <div class="b-404__bg">
        <img src="<?= FPM::originalSrc($model->image->file_id ?? '') ?>" alt="image">
        <img src="<?= FPM::originalSrc($model->mobileImage->file_id ?? '') ?>" alt="image">
    </div>
    <div class="b-404__content">
        <p class="b-404__content-text"><?= $model->pageLabel ?></p>
        <div class="b-404__content-btn">
            <a class="btn btn_default" href="<?= \yii\helpers\Url::home() ?>">
                <span class="btn__text"><?= $model->buttonLabel ?></span>
                <div class="btn__bg">
                    <div class="btn__bg-left">
                        <div class="btn__bg-left-dot"></div>
                        <div class="btn__bg-left-dot"></div>
                        <div class="btn__bg-left-dot"></div>
                    </div>
                    <div class="btn__bg-right">
                        <div class="btn__bg-right-dot"></div>
                        <div class="btn__bg-right-dot"></div>
                        <div class="btn__bg-right-dot"></div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
