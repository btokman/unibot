<?php

namespace frontend\controllers;

use common\models\NotFoundPage;
use frontend\components\MetaRegister;
use frontend\modules\socialShareContent\components\SocialShareContentRegister;
use yii\web\Controller;

/**
 * Error controller
 */
class ErrorController extends Controller
{
    public function actionError()
    {
        $this->layout = 'error';

        $model = (new NotFoundPage())->get();

        SocialShareContentRegister::register($model);
        MetaRegister::registerSeo($model);

        return $this->render('error', ['model' => $model]);
    }
}
