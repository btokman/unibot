<?php

namespace frontend\controllers;

use common\models\BotEmails;
use common\models\FormSettings;
use common\models\Requests;
use common\models\Robots;
use frontend\components\FrontendController;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends FrontendController
{

    public function actionForm()
    {
        $model = new Requests();
        $model->load(\Yii::$app->request->post());

        $model->save(false);

        return 1;
    }

    public function actionEmail()
    {
        $model = new BotEmails();

        $email = Yii::$app->request->get('email');

        $model->email = $email;

        $model->save();

        return true;
    }

    /**
     * Generates robots.txt file
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionRobots()
    {
        /* @var Robots $robots */
        $robots = Robots::find()->one();

        if (!$robots) {
            throw new NotFoundHttpException();
        }

        $this->layout = false;

        $response = Yii::$app->getResponse();
        $response->format = Response::FORMAT_RAW;
        $headers = $response->headers;
        $headers->add('Content-Type', 'text/plain');

        $text = $robots->text;
        $text .= "\nSitemap: " . Url::to(['/sitemap/default/index'], true);

        return $this->renderContent($text);
    }
}
