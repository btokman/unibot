const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
var workingDir = '../../web/uploads/**/*';
var destinationDir = '../../web/uploads';

gulp.task('compress-image', function () {
    gulp.src(workingDir)
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({plugins: [{removeViewBox: true}]})
        ], {
            verbose: true
        }))
        .pipe(gulp.dest(destinationDir))
});
