<?php
/*
Yii command:
yii asset tools/gulp/assets-config.php config/assets-prod.php
*/

Yii::setAlias('@webroot', str_replace('\\', '/', dirname(__FILE__, 3)) . '/web');
Yii::setAlias('@web', '/');

return [
    'jsCompressor' => 'gulp compress-js --gulpfile frontend/tools/gulp/gulpfile.js --src {from} --dist {to}',
    'cssCompressor' => 'gulp compress-css --gulpfile frontend/tools/gulp/gulpfile.js --src {from} --dist {to}',
    'bundles' => [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\widgets\ActiveFormAsset',
        'yii\validators\ValidationAsset',
        'frontend\assets\AppAsset',
    ],
    'targets' => [
        'all' => [
            'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/compress',
            'baseUrl' => '@web/compress',
            'js' => 'all-{hash}.js',
            'css' => 'all-{hash}.css',
        ],
    ],
    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets',
    ],
];
