<?php

namespace frontend\helpers;

use Yii;

/**
 * User agent helper.
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class UserAgent
{
    /**
     * Check whether user agent is a Google Page Speed Insights.
     * @see https://stackoverflow.com/questions/29162881
     *
     * @return bool
     */
    public static function isGooglePageSpeed()
    {
        $userAgent = Yii::$app->getRequest()->getUserAgent();
        return ($userAgent !== null) && (stripos($userAgent, 'Speed Insights') !== false);
    }
}
