<?php
/**
 * @author walter
 */

namespace frontend\helpers;

use yii\helpers\BaseHtml;

/**
 * Class ExtendedHtml
 * @package frontend\helpers
 */
class ExtendedHtml extends BaseHtml
{
    /**
     * Generates ajax hyperlink tag
     *
     * @param string $text
     * @param array|string|null $url
     * @param array $options
     * @return string
     */
    public static function ajaxLink($text, $url = null, $options = [])
    {
        self::addCssClass($options, 'ajax-link');
        $options['rel'] = 'nofollow';
        $link = parent::a($text, $url, $options);
        return self::wrapInNoindexTag($link);
    }

    /**
     * Generates a hyperlink tag to another sites
     *
     * @param string $text
     * @param array|string|null $url
     * @param array $options
     * @return string
     */
    public static function externalLink($text, $url = null, $options = [])
    {
        $options['rel'] = 'nofollow';
        $link = parent::a($text, $url, $options);
        return self::wrapInNoindexTag($link);
    }

    /**
     * Returns HTML wrapped in to no index block
     *
     * @param string $html
     * @return string
     */
    public static function wrapInNoIndexTag($html)
    {
        return '<!--noindex-->' . $html . '<!--/noindex-->';
    }

    /**
     * Generates a callto hyperlink
     *
     * @param string $tel
     * @param null|string $text
     * @param array $options
     * @return string
     */
    public static function callto($tel, $text = null, $options = [])
    {
        return self::a(
            ($text === null ? $tel : $text),
            "tel:$tel",
            $options
        );
    }

    const DEFAULT_LIMIT = 50;

    /**
     * @param array $data
     * @param int $limit
     * @return array
     */
    public static function limitBreadCrumbsCharsets(array $data, int $limit = 0): array
    {
        $returnData = [];
        if (!$limit) $limit = self::DEFAULT_LIMIT;

        foreach ($data as $item) {
            $itemLabel = $item['label'] ?? $item;

            if (is_array($itemLabel)) return $data;

            if (strlen($itemLabel) < $limit) {
                $returnData[] = $item;
                continue;
            }

            $label = mb_substr($itemLabel, 0, $limit) . '...';

            if (is_array($item)) {
                $item['label'] = $label;
                $returnData[] = $item;
                continue;
            }

            $returnData[] = $label;

        }

        return $returnData;
    }

}