<?php
/**
 * @author walter
 */

namespace frontend\components;

/**
 * Class Redirect
 *
 * @package frontend\components
 */
class Redirect
{
    /**
     * @var string
     */
    protected $currentUrl;

    public function __construct()
    {
        $this->currentUrl = $this->getCurrentUrl();
    }

    public function make()
    {
        $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
        if (!$isAjax) {
            $this->toLowerCaseRedirect();
            $this->removeSlashFromEnd();
            $this->usersRedirects();
        }
    }

    protected function toLowerCaseRedirect()
    {
        $urlWithoutParams = substr(
            $this->currentUrl,
            0,
            strpos($this->currentUrl, '?') ?: strlen($this->currentUrl)
        );
        $camelCaseInUrl = preg_match('|[A-Z]|', $urlWithoutParams);
        $isUploadsRequest = strpos($this->currentUrl, '/uploads/') !== FALSE;
        $isPressKitRequest = strpos($this->currentUrl, '/press-kit/') !== FALSE;
        if ($camelCaseInUrl && !$isUploadsRequest && !$isPressKitRequest) {
            $this->doRedirect(strtolower($this->currentUrl));
        }
    }

    protected function removeSlashFromEnd()
    {
        if ($this->getShortUrl() != '/' && substr($this->currentUrl, -1) == '/') {
            $this->doRedirect(substr($this->currentUrl, 0, -1));
        }
    }

    protected function usersRedirects()
    {
        $to = $this->urlInUsersRedirects();
        if ($to) {
            $this->doRedirect($to);
        }
    }

    protected function urlInUsersRedirects()
    {
        $toUrl = $this->getUrlFromCache();
        if (!$toUrl) {
            $toUrl = $this->setUrlToCache();
        }
        if ($toUrl) {
            return $toUrl;
        }
        return false;
    }

    protected function getCurrentUrl()
    {
        return "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    protected function getShortUrl()
    {
        return "$_SERVER[REQUEST_URI]";
    }

    protected function doRedirect($toUrl)
    {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location:{$toUrl}");
        exit();
    }

    protected function getUrlFromCache()
    {
        $cache = new \Cache([
            'path' => __DIR__ . '/../runtime/redirectCache/',
            'name' => 'default',
            'extension' => '.cache',
        ]);
        return $cache->retrieve($this->currentUrl);
    }

    protected function setUrlToCache()
    {
        $url = $this->findUrlFromDB();
        $cache = new \Cache([
            'path' => __DIR__ . '/../runtime/redirectCache/',
            'name' => 'default',
            'extension' => '.cache',
        ]);
        $cache->store($this->currentUrl, $url);
        return $url;
    }

    protected function findUrlFromDB()
    {
        $config = $this->getYiiAppConfig();
        $db = $config['components']['db'];
        $dbh = new \PDO($db['dsn'], $db['username'], $db['password']);
        $from = $this->currentUrl;
        $query = "SELECT `to` FROM {$this->getTableName()} WHERE `from` LIKE :from"
            . " AND `is_active`=1 ORDER BY `updated_at` DESC LIMIT 1;";
        $statement = $dbh->prepare($query);
        $statement->execute([':from' => '%' . $from]);
        $res = $statement->fetch();
        $to = isset($res['to']) ? $res['to'] : null;

        if (!is_null($to)) {
            return $to;
        }
        return false;
    }

    protected function getTableName()
    {
        $tableName = 'redirects';
        if (isset($db['tablePrefix'])) {
            $tableName = $db['tablePrefix'] . $tableName;
        }
        return $tableName;
    }

    protected function getYiiAppConfig()
    {
        return array_merge(
            require(__DIR__ . '/../../common/config/main.php'),
            require(__DIR__ . '/../../common/config/main-local.php')
        );
    }

}
