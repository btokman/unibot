<?php
namespace frontend\components;

use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use notgosu\yii2\modules\metaTag\models\MetaTag;
use notgosu\yii2\modules\metaTag\models\MetaTagContent;
use Yii;

/**
 * Class MetaRegister
 * @package common\components
 */
class MetaRegister extends MetaTagRegister
{
    const RIGHT_TEXT = 5;
    const LEFT_TEXT = 6;

    public static function registerSeo($model, $isStatic = true, $language = null)
    {
        $seoText = [];
        !$isStatic ? $metaTags = MetaTagContent::find()
            ->where([MetaTagContent::tableName() . '.model_id' => $model->id])
            ->andWhere([MetaTagContent::tableName() . '.model_name' => $model->formName()])
            ->joinWith(['metaTag']) :
            $metaTags = MetaTagContent::find()
                ->andWhere([MetaTagContent::tableName() . '.model_name' => $model->formName()])
                ->joinWith(['metaTag']);

        $metaTags->andWhere([MetaTag::tableName() . '.is_active' => 1]);

        if (!is_string($language)) {
            $language = Yii::$app->language;
        }
        $metaTags->andWhere([MetaTagContent::tableName() . '.language' => $language]);

        /** @var MetaTagContent[] $metaTags */
        $metaTags = $metaTags->all();

        foreach ($metaTags as $metaTag) {
            $content = $metaTag->content;
            if (!empty($content)) {
                if (strtolower($metaTag->metaTag->name) === MetaTag::META_TITLE_NAME) {
                    Yii::$app->getView()->title = $content;
                } else {
                    if ($metaTag->metaTag->name) {
                        Yii::$app->view->registerMetaTag(
                            [
                                ($metaTag->metaTag->is_http_equiv ? 'http-equiv' : 'name') => $metaTag->metaTag->name,
                                'content' => $content,
                            ],
                            $metaTag->metaTag->name
                        );
                    }
                }
            }
        }

        return $seoText;
    }
}