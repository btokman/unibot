<?php

namespace frontend\modules\news\controllers;


use common\models\News;
use common\models\NewsPage;
use common\models\NewsToTags;
use common\models\Tags;
use frontend\components\FrontendController;
use frontend\components\MetaRegister;
use frontend\modules\socialShareContent\components\SocialShareContentRegister;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Class DefaultController
 * @package frontend\modules\news\controllers
 */
class DefaultController extends FrontendController
{

    public $layout = '//contacts';

    public $page = 'blog';

    /**
     * @param bool $tag
     * @return string
     */
    public function actionIndex($tag = false)
    {
        $model = (new NewsPage())->get();

        $offset = \Yii::$app->request->get('offset');

        SocialShareContentRegister::register($model);
        MetaRegister::registerSeo($model);

        $models = News::getElements(News::PER_PAGE + 1, $offset);

        if ($tag) {
            $tag = Tags::findOne(['alias' => $tag]);

            if ($tag) $models = NewsToTags::getNewsByTagId($tag->id);
        }

        $showBtn = true;

        if (count($models) <= News::PER_PAGE) {
            $showBtn = false;
        }

        $models = array_slice($models, 0, News::PER_PAGE);
        if (\Yii::$app->request->isAjax) {
            $data = $this->renderAjax('_item_preview', ['models' => array_slice($models, 0, 3)]);

            return Json::encode([
                'data' => $data,
                'showBtn' => count($models) > 4
            ]);
        }

        return $this->render('index', ['model' => $model, 'models' => $models, 'btn' => $showBtn]);
    }

    /**
     * @param $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionItem($alias)
    {
        $tag = \Yii::$app->request->get('tag');

        if ($tag) return $this->actionIndex($tag);

        $model = News::findWithCheckForTranslations()->andWhere(['alias' => $alias])->one();

        if (!$model && News::findOne(['alias' => $alias])) return $this->redirect(['/blog']);

        if (!$model) throw new NotFoundHttpException();

        $this->page = 'article';
        $this->bodyClass = 'is-article';

        SocialShareContentRegister::register($model);
        MetaRegister::register($model);

        $pageModel = (new NewsPage())->get();

        return $this->render('item', ['model' => $model, 'pageModel' => $pageModel]);
    }
}
