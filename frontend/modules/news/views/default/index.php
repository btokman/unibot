<?php
/**
 * @var $model \common\models\NewsPage
 * @var $models \common\models\News[]
 * @var $btn boolean
 */
$this->params['breadcrumbs'][] = $model->pageLabel;
?>
<section class="blog">
    <div class="blog__container container container_md">
        <h1 class="blog__title"><?= $model->pageSubLabel ?></h1>
        <div class="blog__list">
            <?php foreach ($models as $key => $item) : ?>
                <div class="blog__list-item">
                    <div class="card card_hash <?= $key === 0 ? 'card_large' : '' ?>">
                        <a class="card__image" href="<?= $item->url ?>">
                            <img class="card__image-img"
                                 src="<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($item->previewImage->file_id ?? '') ?>"
                                 alt='<?= $item->label ?>'
                                 title=""/>
                            <svg class="card__image-svg" viewBox="0 0 300 219" preserveAspectRatio="none">
                                <path d="M-2,-2v223h304V-2H-2z M271.8,169.7c-46.3,39.6-168.4,77.1-231.1,16.5C-22.1,125.6,0.3,79.1,45.9,50C91.6,20.9,179.8-29,242,25.1C304.2,79.2,318.2,130.1,271.8,169.7z"></path>
                            </svg>
                        </a>
                        <h3 class="card__title">
                            <a href="<?= $item->url ?>">
                                <?= $item->label ?>
                            </a>
                        </h3>
                        <div class="card__text">
                            <p><?= $item->short_description ?></p>
                        </div>
                        <a class="card__more" href="<?= $item->url ?>">
                            <div class="card__more-dot"></div>
                            <div class="card__more-dot"></div>
                            <div class="card__more-dot"></div>
                            <div class="card__more-dot"></div>
                            <div class="card__more-dot"></div>
                            <div class="card__more-dot"></div>
                            <div class="card__more-dot"></div>
                            <div class="card__more-circle"></div>
                        </a>
                        <?php if (count($item->tags) > 0) : ?>
                            <div class="card__hash">
                                <?php foreach ($item->tags as $tag) : ?>
                                    <a href="<?= $tag->url ?>"><?= $tag->label ?></a>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php if (count($models) >= \common\models\News::PER_PAGE && $btn) : ?>
            <div class="blog__btn-wrap">
                <a class="btn btn_white" rel="nofollow" href="<?= \yii\helpers\Url::current() ?>">
                    <span class="btn__text"><?= $model->readMoreButton ?></span>
                    <div class="btn__bg">
                        <div class="btn__bg-left">
                            <div class="btn__bg-left-dot"></div>
                            <div class="btn__bg-left-dot"></div>
                            <div class="btn__bg-left-dot"></div>
                        </div>
                        <div class="btn__bg-right">
                            <div class="btn__bg-right-dot"></div>
                            <div class="btn__bg-right-dot"></div>
                            <div class="btn__bg-right-dot"></div>
                        </div>
                    </div>
                </a>
            </div>
        <?php endif; ?>
    </div>
</section>