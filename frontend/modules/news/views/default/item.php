<?php
/**
 * @var $model \common\models\News
 * @var $pageModel \common\models\NewsPage
 */

$this->params['breadcrumbs'][] = ['label' => $pageModel->pageLabel, 'url' => $pageModel::getUrl()];
$this->params['breadcrumbs'][] = $model->label;
?>

<section class="article-header">
    <div class="article-header__bg">
        <img src="<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($model->previewImage->file_id ?? '') ?>"
             alt="<?= $model->label ?>">
        <div class="article-header__bg-mask">
        </div>
    </div>
    <div class="article-header__container container container_md">
        <h1 class="article-header__title"><?= $model->label ?></h1>
        <p><?= $model->short_description ?></p>
    </div>
</section>
<section class="article-content">
    <div class="article-content__container container container_md">
        <div class="article">
            <?php foreach ($model->content as $builder) :
                ?>
                <?php if ($builder instanceof \common\models\builder\ImageBuilderModel) : ?>
                <div class="article__image">
                    <img src="<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($builder->langImage->file_id ?? '') ?>"
                         alt="image">
                </div>
            <?php else : ?>
                <?= Yii::$app->language === \common\helpers\LanguageHelper::getDefaultLanguage()->code ?
                    $builder->text : $builder->{'text_' . Yii::$app->language} ?>
            <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <div class="article-content__hash">
            <?php foreach ($model->tags as $tag) : ?>
                <a href="<?= $tag->url ?>">#<?= $tag->label ?></a>
            <?php endforeach; ?>
        </div>
        <div class="article-content__share">
            <div class="article-content__share-in">
                <p class="article-content__share-text"><?= $pageModel->shareBtn ?></p>
                <div class="article-content__share-soc">
                    <div class="article-content__share-soc-icon-wrap">
                        <a class="soc-icon soc-icon_default article-content__share-soc-icon"
                           href="https://www.facebook.com/sharer/sharer.php?u=<?= $model->getUrl(true) ?>">
                            <svg class="icon icon-fb">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-fb"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="article-content__share-soc-icon-wrap">
                        <a class="soc-icon soc-icon_default article-content__share-soc-icon"
                           href="https://twitter.com/intent/tweet?text=<?= $model->getUrl(true) ?>">
                            <svg class="icon icon-twitter">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-twitter"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="article-content__share-soc-icon-wrap">
                        <a class="soc-icon soc-icon_default article-content__share-soc-icon"
                           href="https://telegram.me/share/url?url=<?= $model->getUrl(true) ?>">
                            <svg class="icon icon-telegram">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-telegram"></use>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
