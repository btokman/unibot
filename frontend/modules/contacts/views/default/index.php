<?php
/**
 * @var $model \common\models\ContactsPage
 * @var $footer \common\models\FooterSettings
 */

$this->params['breadcrumbs'][] = $model->pageLabel;
?>
<section class="contacts">
    <div class="contacts__container container container_md">
        <h1 class="contacts__title"><?= $model->pageLabel ?></h1>
        <div class="contacts__row">
            <div class="contacts__col contacts__col_left">
                <div class="contacts__list">
                    <div class="contacts__list-col">
                        <div class="contacts__list-phone contacts__list-text">
                            <p><?= $footer->phoneLabel ?></p>
                            <a href="tel: <?= $footer->phone ?>">
                                <?= $footer->phone ?></a>
                        </div>
                        <div class="contacts__list-mail contacts__list-text">
                            <p><?= $footer->emailLabel ?></p>
                            <a href="mailto: <?= $footer->email ?>"><?= $footer->email ?></a>
                        </div>
                    </div>
                    <div class="contacts__list-col">
                        <div class="contacts__list-address contacts__list-text">
                            <?= $footer->adress ?>
                        </div>
                        <div class="contacts__list-soc-text contacts__list-text">
                            <p>
                                <?= $footer->socialLabel ?>
                            </p>
                        </div>
                        <div class="contacts__list-soc contacts__list-text">
                            <div class="contacts__list-soc-icon-wrap">
                                <a class="soc-icon soc-icon_default contacts__list-soc-icon" target="_blank"
                                   href="<?= $footer->faceboockSocialLink ?>">
                                    <svg class="icon icon-fb">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-fb"></use>
                                    </svg>
                                </a>
                            </div>
                            <?php if ($footer->telegramSocialLink) : ?>
                                <div class="contacts__list-soc-icon-wrap">
                                    <a class="soc-icon soc-icon_default contacts__list-soc-icon" target="_blank"
                                       href="<?= $footer->telegramSocialLink ?>">
                                        <svg class="icon icon-linkedin">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-linkedin"></use>
                                        </svg>
                                    </a>
                                </div>
                            <?php endif; ?>
                            <?php if ($footer->instagramSocialLink) : ?>
                                <div class="contacts__list-soc-icon-wrap">
                                    <a class="soc-icon soc-icon_default contacts__list-soc-icon" target="_blank"
                                       href="<?= $footer->instagramSocialLink ?>">
                                        <svg class="icon icon-twitter-pink">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-twitter-pink"></use>
                                        </svg>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contacts__col contacts__col_right">
                <div class="contacts__map-wrap">
                    <div class="contacts__map-mask">
                        <div class="contacts__map-mask-jelly">
                            <img src="/static/img/map-jelly.svg" alt="image">
                        </div>
                        <div class="contacts__map-mask-mouse">
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewbox="0 0 598.8 438.1">
                                <path fill="transparent"
                                      d="M0,0v438.1h598.8V0H0z M518.3,327.8c-83.7,71.6-304.1,139.4-417.4,29.8s-73-193.6,9.5-246.2c82.5-52.6,241.7-142.7,354.1-45S602,256.2,518.3,327.8z"></path>
                            </svg>
                        </div>
                    </div>
                    <div class="contacts__map" data-map-center="<?= $model->lat . ',' . $model->lng ?>"
                         data-marker-path="/static/img/map-marker.svg"></div>
                </div>
            </div>
        </div>
    </div>
</section>
