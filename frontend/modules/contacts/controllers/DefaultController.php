<?php

namespace frontend\modules\contacts\controllers;

use common\models\ContactsPage;
use common\models\FooterSettings;
use frontend\components\FrontendController;
use frontend\components\MetaRegister;
use frontend\modules\socialShareContent\components\SocialShareContentRegister;


/**
 * Default controller for the `contacts` module
 */
class DefaultController extends FrontendController
{
    public $layout = '//contacts';

    public $page = 'contacts';

    public function actionIndex()
    {
        $model = (new ContactsPage())->get();

        SocialShareContentRegister::register($model);
        MetaRegister::registerSeo($model);

        $footer = (new FooterSettings())->get();

        return $this->render('index', ['model' => $model, 'footer' => $footer]);
    }
}
