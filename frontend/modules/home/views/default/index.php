<?php
/**
 * @var $model \common\models\HomePage
 */

use metalguardian\fileProcessor\helpers\FPM;

?>
<!-- Navigation-->
<div class="navigation">
    <div class="navigation__current"></div>
    <div class="navigation__total"></div>
</div>
<!-- Navigation-btn-->
<div class="navigation-btn">
    <div class="navigation-btn__dot"></div>
    <div class="navigation-btn__dot"></div>
    <div class="navigation-btn__dot"></div>
    <div class="navigation-btn__dot"></div>
    <div class="navigation-btn__dot"></div>
    <div class="navigation-btn__dot"></div>
    <div class="navigation-btn__dot"></div>
    <div class="navigation-btn__circle"></div>
</div>
<section class="screen" id="screen" data-scroll-section="data-scroll-section">
    <div class="screen__content">
        <div class="screen__container container">
            <div class="screen__content-in">
                <h1 class="screen__title"><?= $model->pageLabel ?></h1>
                <p class="screen__text">
                    <?= $model->pageDescription ?>
                </p>
                <div class="screen__btn-wrap">
                    <a class="btn btn_default screen__btn js-chat-init" href="#!">
                        <span class="btn__text btn_bot_init"><?= $model->botButtonLabel ?></span>
                        <div class="btn__bg">
                            <div class="btn__bg-left">
                                <div class="btn__bg-left-dot"></div>
                                <div class="btn__bg-left-dot"></div>
                                <div class="btn__bg-left-dot"></div>
                            </div>
                            <div class="btn__bg-right">
                                <div class="btn__bg-right-dot"></div>
                                <div class="btn__bg-right-dot"></div>
                                <div class="btn__bg-right-dot"></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="screen__canvas">
        <svg class="screen__svg is-hidden">
            <path id="path-screen-1"
                  d="M7.9,21.4c7-8.1,11.4-9.1,25-15S89.2-9.6,96.5,21.2c7.4,31,11.1,76.3-17.6,81.3c-28.7,5-65-11.6-74-32S0.9,29.6,7.9,21.4z"></path>
            <path id="path-screen-2"
                  d="M26.829,69.534 C10.804,97.396 5.776,174.763 15.688,232.656 C25.600,290.549 53.135,349.188 76.995,347.954 C100.856,346.720 120.227,312.557 141.229,281.224 C162.232,249.893 216.211,181.984 285.876,163.581 C355.541,145.179 316.377,77.483 296.598,51.462 C270.570,17.221 214.893,5.394 148.096,6.155 C86.101,6.861 42.854,41.673 26.829,69.534 Z"></path>
            <path id="path-screen-3"
                  d="M109.2,8.2c18,10,47.7,39.2,49.4,68.4c1,16.8-18.4,56.6-72,68s-91.7-13.1-86-43c5.6-29.5,3.2-73.8,23-83C53.2,4.8,77.4-9.4,109.2,8.2z"></path>
            <path id="path-screen-4"
                  d="M9.9,48.7c8.5-11.1,16.2-19.8,27-33s29.6-21.3,49-11s54,54.1,25,88s-39.3,27.5-48,26s-59.3-25.3-62-39S1.4,59.8,9.9,48.7z"></path>
            <path id="path-screen-5"
                  d="M422.000,35.000 C401.288,17.180 363.156,5.151 309.438,45.911 C266.942,78.155 228.426,171.862 149.588,168.745 C57.253,165.096 5.979,217.322 11.362,269.880 C18.438,338.973 86.443,350.634 109.626,351.501 C132.769,352.366 153.070,343.415 175.564,331.527 C198.059,319.640 222.387,286.262 269.476,285.589 C316.564,284.915 333.312,331.871 356.394,348.505 C412.115,388.658 476.521,323.555 452.000,224.000 C426.739,121.443 470.266,76.525 422.000,35.000 Z"></path>
        </svg>
    </div>
</section>
<section class="how" id="how" data-scroll-section="data-scroll-section">
    <div class="chat how__chat" id="chat">
        <div class="chat__container container">
            <div class="chat__icon-bot">
                <img src="/static/img/bot.svg" alt="image">
            </div>
            <div class="chat__close-btn">
                <div class="chat__close-icon">
                    <svg class="icon icon-close">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-close"></use>
                    </svg>
                    <svg class="icon icon-menu-close">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-menu-close"></use>
                    </svg>
                </div>
            </div>
            <div class="chat__content">
                <div class="chat__content-chat"></div>
                <div class="chat__content-controls"></div>
            </div>
        </div>
        <!-- settings-->
        <div class="chat__data-bot-msg is-hidden">
            <div class="chat__content-chat-bot">
                <div class="chat__content-chat-bot-inner">
                    <div class="chat__content-chat-bot-dot"></div>
                    <div class="chat__content-chat-bot-dot"></div>
                    <div class="chat__content-chat-bot-dot"></div>
                </div>
            </div>
            <div class="chat__content-chat-human">
                <div class="chat__content-chat-human-inner"></div>
            </div>
            <div class="chat__content-controls-btn-wrap">
                <a class="btn btn_default chat__content-controls-btn" href="#!">
                    <span class="btn__text"></span>
                    <div class="btn__bg">
                        <div class="btn__bg">
                            <div class="btn__bg-left">
                                <div class="btn__bg-left-dot"></div>
                                <div class="btn__bg-left-dot"></div>
                                <div class="btn__bg-left-dot"></div>
                            </div>
                            <div class="btn__bg-right">
                                <div class="btn__bg-right-dot"></div>
                                <div class="btn__bg-right-dot"></div>
                                <div class="btn__bg-right-dot"></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <form class="chat__content-controls-form" action="<?= \common\models\BotEmails::getFormUrl() ?>"
                  novalidate="novalidate">
                <div class="form-group">
                    <input class="chat__content-controls-form-input" type="email"
                           placeholder="<?= Yii::$app->config->get(\common\models\BotSettings::EMAIL_PLACEHOLDER) ?>"/>
                    <button class="chat__content-controls-form-submit" type="submit">
                        <svg class="icon icon-submit">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-submit"></use>
                        </svg>
                    </button>
                </div>
            </form>
        </div>
        <?= \frontend\widgets\common\BotWidget::widget() ?>
    </div>
    <div class="how__gradient">
        <div class="how__gradient-el"></div>
        <div class="how__gradient-el"></div>
        <div class="how__gradient-el"></div>
    </div>
    <div class="how__round-bottom">
        <svg viewBox="0 0 1282 81">
            <path d="M0,81h641.4C320.9,81,0,0,0,0V81z"/>
            <path d="M1282,0c0,0-319.6,81-639.6,81H1282v-81H1282z"/>
        </svg>
    </div>
    <svg class="is-hidden">
        <path id="path-how-1"
              d="M69.005,75.918 C139.703,30.693 249.171,-46.600 345.529,37.443 C441.887,121.484 490.431,200.368 418.726,261.891 C347.022,323.412 158.055,381.676 60.871,287.542 C-36.312,193.401 -1.694,121.143 69.005,75.918 Z"></path>
        <path id="path-how-2"
              d="M0.588,207.597 C-9.308,66.038 112.951,9.894 230.659,1.316 C348.367,-7.258 381.256,127.830 395.583,213.821 C409.910,299.812 326.068,315.336 209.426,364.212 C85.675,416.067 5.806,282.272 0.588,207.597 Z"></path>
        <path id="path-how-3"
              d="M410.350,223.687 C370.485,291.710 312.406,404.086 184.810,355.603 C57.209,307.123 -25.449,247.280 7.115,163.116 C39.674,78.951 177.722,-42.183 311.128,15.800 C444.539,73.783 450.220,155.664 410.350,223.687 Z"></path>
        <path id="path-how-4"
              d="M402.712,141.186 C399.770,211.571 402.080,325.228 260.569,327.526 C119.058,329.819 13.275,307.490 1.480,226.041 C-10.314,144.591 56.643,-3.299 208.217,0.413 C359.790,4.126 405.654,70.799 402.712,141.186 Z"></path>
        <path id="path-how-5"
              d="M417.330,185.645 C403.089,298.233 292.018,389.774 163.208,386.925 C39.261,384.185 12.136,246.594 1.805,155.998 C-8.526,65.402 80.277,53.743 205.314,9.039 C337.970,-38.390 426.531,112.901 417.330,185.645 Z"></path>
        <path id="path-how-6"
              d="M57.080,106.431 C143.768,78.887 167.116,-9.028 299.453,0.756 C442.448,11.325 454.420,172.418 427.577,247.408 C388.906,355.439 196.964,394.817 86.711,321.656 C-23.544,248.488 -23.202,131.941 57.080,106.431 Z"></path>
    </svg>


    <div class="how__container container">
        <h2 class="how__title"><?= $model->howItWorkLabel ?></h2>
        <p class="how__text">
            <?= $model->howItWorkDescription ?>
        </p>
        <div class="how__soc">
            <div class="how__soc-icon-wrap">
                <div class="soc-icon soc-icon_default how__soc-icon">
                    <svg class="icon icon-facebook">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
                    </svg>
                </div>
            </div>
            <div class="how__soc-icon-wrap">
                <div class="soc-icon soc-icon_default how__soc-icon">
                    <svg class="icon icon-telegram">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-telegram"></use>
                    </svg>
                </div>
            </div>
            <div class="how__soc-icon-wrap">
                <div class="soc-icon soc-icon_default how__soc-icon">
                    <svg class="icon icon-viber">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-viber"></use>
                    </svg>
                </div>
            </div>
            <div class="how__soc-icon-wrap">
                <div class="soc-icon soc-icon_default how__soc-icon">
                    <svg class="icon icon-skype">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-skype"></use>
                    </svg>
                </div>
            </div>
            <div class="how__soc-icon-wrap">
                <div class="soc-icon soc-icon_default how__soc-icon">
                    <svg class="icon icon-slack">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-slack"></use>
                    </svg>
                </div>
            </div>
            <div class="how__soc-icon-wrap">
                <div class="soc-icon soc-icon_default how__soc-icon">
                    <svg class="icon icon-insta">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-insta"></use>
                    </svg>
                </div>
            </div>
        </div>
        <div class="how__history">
            <div class="how__history-start"></div>
            <div class="how__history-line">
                <svg viewBox="0 0 604 1923">
                    <path d="M305,2c0,0,0.2,56.3-1,76c-1.7,28.8-12,67.7-52,91c-55.7,32.4-128,35-128,35S-47.5,311.9,96,355s269.1-55.9,412,65c142.9,120.9-44.5,309.3-298,299c-251.8-10.2-258.9,186.4-69,240c119.5,33.8,145.3-32.7,273-24s176.5,114.7,185,174s-104.5,220.1-319,244s-264.5,157-127,209s294.9-56.2,349,87c41.8,110.7-8,273-8,273"
                    />
                </svg>
            </div>
            <?php foreach ($model->howItWorksItems as $key => $item) : ?>
                <div class="how__history-el how__history-el_<?= ++$key ?>">
                    <div class="how__history-canvas"></div>
                    <div class="how__history-number">
                        <svg class="icon icon-0<?= $key ?>">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-0<?= $key ?>"></use>
                        </svg>
                    </div>
                    <div class="how__history-content">
                        <h3 class="how__history-title"><?= $item->label ?></h3>
                        <p class="how__history-text">
                            <?= $item->description ?>
                        </p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<section class="loc" id="loc" data-scroll-section="data-scroll-section">
    <div class="loc__container container">
        <h2 class="loc__title"><?= $model->whereItWorkLabel ?></h2>
        <div class="loc__slider-wrap">
            <div class="loc__slider-counter">
                <div class="loc__slider-counter-current"></div>
                <div class="loc__slider-counter-total"></div>
            </div>
            <div class="loc__slider-btn">
                <a class="btn btn_default js-modal" href="#!" data-target="#orderBot">
                    <span class="btn__text"><?= $model->whereItWorkButtonLabel ?></span>
                    <div class="btn__bg">
                        <div class="btn__bg-left">
                            <div class="btn__bg-left-dot"></div>
                            <div class="btn__bg-left-dot"></div>
                            <div class="btn__bg-left-dot"></div>
                        </div>
                        <div class="btn__bg-right">
                            <div class="btn__bg-right-dot"></div>
                            <div class="btn__bg-right-dot"></div>
                            <div class="btn__bg-right-dot"></div>
                        </div>
                    </div>
                </a>
            </div>
            <?php if (count($model->whereItWorksItems) > 0) : ?>
                <div class="loc__slider">
                    <?php foreach ($model->whereItWorksItems as $item) : ?>
                        <div class="loc__slider-item">
                            <div class="loc__slider-row">
                                <div class="loc__slider-left">
                                    <h3 class="loc__slider-title"><?= $item->label ?></h3>
                                    <p class="loc__slider-text">
                                        <?= $item->description ?>
                                    </p>
                                </div>
                                <div class="loc__slider-right">
                                    <div class="loc__slider-image">
                                        <img class="loc__slider-img"
                                             src="<?= FPM::originalSrc($item->image->file_id ?? '') ?>"
                                             alt="image"
                                             title=""/>
                                        <svg class="loc__slider-image-svg" viewBox="0 0 498 434"
                                             preserveAspectRatio="none">
                                            <path d="M0,0v434h498V0H0z M274,433.1C151.8,442.1,63,386.2,28,330.8C-23.5,249,9.6,91.5,21,69.1S69.8-18.4,154.1,5c84.7,23.5,48.6,11.8,182.8,4.1s153.5,109.5,153.5,109.5C520.4,304.1,464.2,419,274,433.1z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php if (count($model->advantegesItems) > 0) : ?>
    <section class="advantage" id="advantage" data-scroll-section="data-scroll-section">
        <div class="advantage__container container">
            <h2 class="advantage__title"><?= $model->benefitsLabel ?></h2>
            <div class="advantage__list-counter">
                <div class="advantage__list-counter-current"></div>
                <div class="advantage__list-counter-total"></div>
            </div>
            <div class="advantage__list">
                <?php foreach ($model->advantegesItems as $key => $item) : ?>
                    <div class="advantage__list-item">
                        <div class="advantage__list-item-row">
                            <div class="advantage__list-item-left">
                                <svg class="icon icon-advantage-1">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xlink:href="#icon-advantage-<?= ++$key ?>"></use>
                                </svg>

                            </div>
                            <div class="advantage__list-item-right">
                                <h3 class="advantage__list-item-title"><?= $item->label ?></h3>
                            </div>
                        </div>
                        <p class="advantage__list-item-text">
                            <?= $item->description ?>
                        </p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if (count($model->cases) > 0) : ?>
    <section class="example" id="example" data-scroll-section="data-scroll-section">
        <div class="example__container container">
            <h2 class="example__title"><?= $model->casesLabel ?></h2>
            <div class="example__slider-wrap">
                <div class="example__slider-counter">
                    <div class="example__slider-counter-current"></div>
                    <div class="example__slider-counter-total"></div>
                </div>
                <div class="example__slider-btn">
                    <a class="btn btn_default js-modal" href="#!" data-target="#orderBot">
                        <span class="btn__text btn__modal"><?= $model->casesBtn ?></span>
                        <div class="btn__bg">
                            <div class="btn__bg-left">
                                <div class="btn__bg-left-dot"></div>
                                <div class="btn__bg-left-dot"></div>
                                <div class="btn__bg-left-dot"></div>
                            </div>
                            <div class="btn__bg-right">
                                <div class="btn__bg-right-dot"></div>
                                <div class="btn__bg-right-dot"></div>
                                <div class="btn__bg-right-dot"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="example__slider">
                    <?php foreach ($model->cases as $item) : ?>
                        <div class="example__slider-item">
                            <div class="example__slider-row">
                                <div class="example__slider-left">
                                    <div class="example__slider-image">
                                        <img class="example__slider-img"
                                             src="<?= FPM::originalSrc($item->image->file_id ?? '') ?>"
                                             alt="image"
                                             title=""/>
                                        <svg class="example__slider-image-svg" viewBox="0 0 393 373.7"
                                             preserveAspectRatio="none">
                                            <path d="M393,179.6v5.2C393,183.1,393,181.3,393,179.6z"></path>
                                            <path d="M333.6,340c-39.4,28.4-144.5,51.2-246,15C-17.4,317.5-13,184.3,23.3,120.5C59.7,56.3,50.1,0.2,113.6,0c63.8-0.2,87.8,16.4,186.5,8.9c57.2-4.3,92.4,82.6,93,170.7V0H0v373.7h393V184.8C392.7,247.9,374.3,310.7,333.6,340z"></path>
                                        </svg>
                                    </div>
                                    <?php if (count($item->links) > 0) : ?>
                                        <div class="example__slider-chat">
                                            <p class="example__slider-chat-text" href="#!" target="_blank"
                                               data-text-mobile="<?= $model->casesLinkLabel ?>"><?= $model->casesLinkLabel ?></p>
                                            <?php foreach ($item->links as $link) : ?>
                                                <a class="example__slider-chat-icon" href="<?= $link->link ?>"
                                                   target="_blank">
                                                    <?= file_get_contents($_SERVER['DOCUMENT_ROOT'] . FPM::originalSrc($link->image->file_id ?? '')) ?>
                                                </a>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="example__slider-right">
                                    <h3 class="example__slider-title"><?= $item->label ?></h3>
                                    <div class="example__slider-text">
                                        <?= $item->description ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if (count($model->newsItems) > 0) : ?>
    <section class="top" id="top" data-scroll-section="data-scroll-section">
        <div class="top__container container">
            <h2 class="top__title" data-text-mobile="<?= $model->newsLabel ?>">
                <?= $model->newsLabel ?>
            </h2>
            <div class="top__list-counter">
                <div class="top__list-counter-current"></div>
                <div class="top__list-counter-total"></div>
            </div>
            <div class="top__list">
                <?php foreach ($model->newsItems as $item) :
                    /** @var $item \common\models\News */
                    ?>
                    <div class="top__list-item">
                        <div class="card">
                            <a class="card__image" href="<?= $item->url ?>">
                                <img class="card__image-img"
                                     src="<?= FPM::originalSrc($item->previewImage->file_id ?? '') ?>" alt="image"
                                     title=""/>
                                <svg class="card__image-svg" viewBox="0 0 300 219" preserveAspectRatio="none">
                                    <path d="M-2,-2v223h304V-2H-2z M271.8,169.7c-46.3,39.6-168.4,77.1-231.1,16.5C-22.1,125.6,0.3,79.1,45.9,50C91.6,20.9,179.8-29,242,25.1C304.2,79.2,318.2,130.1,271.8,169.7z"></path>
                                </svg>
                            </a>
                            <div class="card__title">
                                <a href="<?= $item->url ?>">
                                    <?= $item->label ?>
                                </a>
                            </div>
                            <div class="card__text">
                                <p><?= $item->short_description ?></p>
                            </div>
                            <a class="card__more" href="<?= $item->url ?>">
                                <div class="card__more-dot"></div>
                                <div class="card__more-dot"></div>
                                <div class="card__more-dot"></div>
                                <div class="card__more-dot"></div>
                                <div class="card__more-dot"></div>
                                <div class="card__more-dot"></div>
                                <div class="card__more-dot"></div>
                                <div class="card__more-circle"></div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
            <div class="top__btn-wrap">
                <a class="btn btn_white" href="<?= \common\models\NewsPage::getUrl() ?>">
                    <span class="btn__text"><?= $model->newsButtonLabel ?></span>
                    <div class="btn__bg">
                        <div class="btn__bg-left">
                            <div class="btn__bg-left-dot"></div>
                            <div class="btn__bg-left-dot"></div>
                            <div class="btn__bg-left-dot"></div>
                        </div>
                        <div class="btn__bg-right">
                            <div class="btn__bg-right-dot"></div>
                            <div class="btn__bg-right-dot"></div>
                            <div class="btn__bg-right-dot"></div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
<?php endif; ?>
