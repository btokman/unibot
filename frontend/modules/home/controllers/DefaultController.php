<?php

namespace frontend\modules\home\controllers;

use common\models\HomePage;
use frontend\components\FrontendController;
use frontend\components\MetaRegister;
use frontend\modules\socialShareContent\components\SocialShareContentRegister;

/**
 * Class DefaultController
 * @package frontend\modules\home\controllers
 */
class DefaultController extends FrontendController
{
    /**
     * Renders the main page
     * @return string
     */
    public function actionIndex()
    {
        $model = (new HomePage())->get();

        SocialShareContentRegister::register($model);
        MetaRegister::registerSeo($model);

        return $this->render('index', ['model' => $model]);
    }
}
