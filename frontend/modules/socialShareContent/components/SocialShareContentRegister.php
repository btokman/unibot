<?php
namespace frontend\modules\socialShareContent\components;

use common\models\SocialShareContent;
use frontend\modules\socialShareContent\widgets\openGraphMetaTags\Widget as OpenGraphMetaTagsWidget;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Class SocialShareContentRegister
 *
 * @package frontend\modules\socialShareContent\components
 */
class SocialShareContentRegister
{
    /**
     * @param Model $model
     */
    public static function register(Model $model)
    {
        /** @var SocialShareContent $socialShareContent */
        $socialShareContent = SocialShareContent::find()
            ->where(['model_name' => $model->formName()])
            ->andWhere(['model_id' => $model->id])
            ->one();
        
        if ($socialShareContent) {
            OpenGraphMetaTagsWidget::widget([
                'title' => $socialShareContent->title,
                'url' => Url::to(Url::current(['_pjax' => null]), true),
                'description' => strip_tags($socialShareContent->description),
                'image' => Yii::$app->request->hostInfo . FPM::originalSrc(
                        $socialShareContent->image_id ?? Yii::$app->config->get('default_share_image')
                    ),
            ]);
        }
    }
}
