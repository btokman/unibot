<?php

namespace frontend\modules\socialShareContent;

use common\components\interfaces\CoreModuleFrontendInterface;

class Module extends \yii\base\Module implements CoreModuleFrontendInterface
{
    public $controllerNamespace = 'frontend\modules\socialShareContent\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
