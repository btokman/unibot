<?php

namespace frontend\assets;

use yii\web\AssetBundle;


class YiiAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
      'js/yii.js',
      'js/form.js',
      'js/validate.js'
    ];
    public $depends = [
      'yii\web\JqueryAsset'
    ];
}
