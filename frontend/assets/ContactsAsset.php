<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class ContactsAsset
 * @package frontend\assets
 */
class ContactsAsset extends AssetBundle
{
    public function init()
    {
        $this->js[] = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDk6mzEzumjkj6ppV83VxGuI5gbM-csMHc&language=' . \Yii::$app->language;
        $this->js[] = 'static/js/index.min.js';
        parent::init();
    }

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'static/css/index.min.css',
    ];
}
