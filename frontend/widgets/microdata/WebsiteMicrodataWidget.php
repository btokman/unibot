<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 18.05.17
 * Time: 17:37
 */

namespace frontend\widgets\microdata;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use Yii;

class WebsiteMicrodataWidget extends BaseMicrodataWidget
{
    /**
     * Example: [
     *      'target' => Url::toRoute(['/search/search/index'], true) . '?search={search_term_string}',
     *      'query-input' => 'required name=search_term_string',
     * ]
     * @var array
     */
    public $searchConfig;

    public function run()
    {
        if (Url::home(true) != Url::current([], true)) {
            return null;
        }

        $data = [
            '@context' => 'http://schema.org',
            '@type' => 'WebSite',
            'url' => Url::home(true),
        ];

        if (isset($this->searchConfig)) {
            $data['potentialAction'] = [
                'type' => 'SearchAction',
            ];
            $data['potentialAction'] = ArrayHelper::merge($data['potentialAction'], $this->searchConfig);
        }

        return $this->prepareData($data);
    }
}