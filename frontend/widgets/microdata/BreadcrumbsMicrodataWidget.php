<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 18.05.17
 * Time: 12:45
 */

namespace frontend\widgets\microdata;


use yii\base\Exception;
use Yii;
use yii\helpers\Url;

class BreadcrumbsMicrodataWidget extends BaseMicrodataWidget
{
    public $homePageTitle;

    /**
     * @throws Exception
     */
    public function init()
    {
        parent::init();

        if (!isset($this->homePageTitle)) {
            throw new Exception('$homePageTitle must be set!');
        }
    }

    /**
     * @return string
     */
    public function run()
    {
        $breadcrumbs = $this->view->params['breadcrumbs'] ?? [];
        if (empty($breadcrumbs)) {
            return null;
        }

        $data = $this->collectData($breadcrumbs);

        return $this->prepareData($data);
    }

    /**
     * @param array $breadcrumbs
     * @return array
     */
    protected function collectData($breadcrumbs)
    {
        $data = [
            '@context' => 'http://schema.org',
            '@type' => 'BreadcrumbList',
        ];
        $data['itemListElement'][] = [
            '@type' => 'ListItem',
            'position' => 1,
            'item' => [
                '@id' => Url::home(true),
                'name' => $this->homePageTitle,
            ]
        ];
        foreach ($breadcrumbs as $index => $breadcrumb) {
            $url = isset($breadcrumb['url'])
                ? Yii::$app->request->getHostName() . $breadcrumb['url']
                : null;
            $url = (is_string($breadcrumb))
                ? Yii::$app->request->absoluteUrl
                : $url;
            $label = null;
            if (isset($breadcrumb['label'])) {
                $label = $breadcrumb['label'];
            } elseif (is_string($breadcrumb)) {
                $label = $breadcrumb;
            }
            if (isset($url) && isset($label)) {
                $data['itemListElement'][] = [
                    '@type' => 'ListItem',
                    'position' => $index + 2,
                    'item' => [
                        '@id' => $url,
                        'name' => $label,
                    ]
                ];
            }
        }

        return $data;
    }
}