<?php

namespace frontend\widgets\common;

use common\helpers\LanguageHelper;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class LanguageChecker
 */
class LanguageChecker extends Widget
{
    protected $items = [];

    public $models;

    public $defaultLang;

    public function init()
    {
        parent::init();
        $models = LanguageHelper::getLanguageModels();

        foreach ($models as $model) {
            if ($model->code === Yii::$app->language) {
                $this->defaultLang = $model;
                continue;
            }
            $this->models[] = $model;
        }
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('_lang', ['models' => $this->models, 'defaultLang' => $this->defaultLang]);
    }

}
