<?php
/**
 * @var $data \common\models\FooterSettings
 * @var $menu \common\models\MenuElements[]
 */
?>
<footer class="footer footer_contacts" id="footer" data-scroll-section="data-scroll-section">
    <div class="footer__mobile-top js-to-top">
        <svg class="icon icon-mobile-top">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-mobile-top"></use>
        </svg>
    </div>
    <div class="footer__round-contacts-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1280 76.6">
            <path d="M0,0c0,0,391.7,76.6,789.7,76.6c248,0,490.3-66.6,490.3-66.6V0H0z"/>
        </svg>
    </div>
    <div class="footer__callback">
        <div class="footer__callback-logo">
            <img src="/static/img/logo-footer.png" alt="image">
        </div>
        <?= \frontend\widgets\common\ContactsFormWidget::widget() ?>
    </div>
    <div class="footer__container container">
        <div class="footer__bot">
            <a class="footer__bot-logo" href="/">
                <img class="footer__bot-logo-img" src="/static/img/logo.svg" alt="image" title=""/>
            </a>
            <div class="footer__bot-right">
                <nav class="footer__bot-nav">
                    <ul>
                        <?php foreach ($menu as $item) : ?>
                            <li>
                                <a href="<?= strpos($item->route, '#') === 0 ? '/' . $item->route : $item->route ?>"><?= $item->label ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
            </div>
        </div>
        <p class="footer__copy"><?= $data->copyrightLabel ?></p>
        <div class="footer__load">
            <?php if ($data->termsFile) : ?>
                <a class="footer__load-link"
                   href="<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($data->termsFile->file_id ?? '') ?>"><?= $data->termsLabel ?></a>
            <?php endif; ?>
            <?php if ($data->privacyFile) : ?>
                <a class="footer__load-link"
                   href="<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($data->privacyFile->file_id ?? '') ?>"><?= $data->privacyLabel ?></a>
            <?php endif; ?>
        </div>
    </div>
</footer>