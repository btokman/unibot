<?php
/**
 * @var $data \common\models\FooterSettings
 * @var $menu \common\models\MenuElements[]
 */
?>
<footer class="footer" id="footer" data-scroll-section="data-scroll-section">
    <div data-line></div>
    <div class="footer__map-image">
        <img src="/static/img/footer-map.png" alt="image">
    </div>
    <div class="footer__mobile-top js-to-top">
        <svg class="icon icon-mobile-top">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-mobile-top"></use>
        </svg>
    </div>
    <div class="footer__round-top">
        <svg viewBox="0 0 1282 81">
            <path d="M0,81h641.4C320.9,81,0,0,0,0V81z"/>
            <path d="M1282,0c0,0-319.6,81-639.6,81H1282v-81H1282z"/>
        </svg>
    </div>
    <div class="footer__container container">
        <div class="footer__title"><?= $data->contactlsLabel ?></div>
        <div class="footer__list">
            <div class="footer__list-col">
                <div class="footer__list-phone footer__list-text">
                    <p><?= $data->phoneLabel ?></p>
                    <a href="tel: <?= $data->phone ?>"><?= $data->phone ?></a>
                </div>
                <div class="footer__list-mail footer__list-text">
                    <p><?= $data->emailLabel ?></p>
                    <a href="mailto: <?= $data->email ?>"><?= $data->email ?></a>
                </div>
            </div>
            <div class="footer__list-col">
                <div class="footer__list-address footer__list-text">
                    <?= $data->adress ?>
                </div>
                <div class="footer__list-soc-text footer__list-text">
                    <p>
                        <?= $data->socialLabel ?>
                    </p>
                </div>
                <div class="footer__list-soc footer__list-text">

                    <div class="footer__list-soc-icon-wrap">
                        <a class="soc-icon soc-icon_default footer__list-soc-icon" target="_blank"
                           href="<?= $data->faceboockSocialLink ?>">
                            <svg class="icon icon-fb">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-fb"></use>
                            </svg>
                        </a>
                    </div>
                    <?php if ($data->telegramSocialLink) : ?>
                        <div class="footer__list-soc-icon-wrap">
                            <a class="soc-icon soc-icon_default footer__list-soc-icon" target="_blank"
                               href="<?= $data->telegramSocialLink ?>">
                                <svg class="icon icon-linkedin">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-linkedin"></use>
                                </svg>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php if ($data->instagramSocialLink) : ?>
                        <div class="footer__list-soc-icon-wrap">
                            <a class="soc-icon soc-icon_default footer__list-soc-icon" target="_blank"
                               href="<?= $data->instagramSocialLink ?>">
                                <svg class="icon icon-twitter-pink">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xlink:href="#icon-twitter-pink"></use>
                                </svg>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>

            </div>

        </div>
        <div class="footer__bot">
            <a class="footer__bot-logo" href="#!">
                <img class="footer__bot-logo-img" src="/static/img/logo.svg" alt="image" title=""/>
            </a>
            <div class="footer__bot-right">
                <nav class="footer__bot-nav">
                    <ul>
                        <?php foreach ($menu as $item) : ?>
                            <li>
                                <a href="<?= strpos($item->route, '#') === 0 ? '/' . $item->route : $item->route ?>"><?= $item->label ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
            </div>
        </div>
        <p class="footer__copy"><?= $data->copyrightLabel ?></p>
        <div class="footer__load">
            <?php if ($data->termsFile) : ?>
                <a class="footer__load-link" target="_blank"
                   href="<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($data->termsFile->file_id ?? '') ?>"><?= $data->termsLabel ?></a>
            <?php endif; ?>
            <?php if ($data->privacyFile) : ?>
                <a class="footer__load-link" target="_blank"
                   href="<?= \metalguardian\fileProcessor\helpers\FPM::originalSrc($data->privacyFile->file_id ?? '') ?>"><?= $data->privacyLabel ?></a>
            <?php endif; ?>
        </div>
    </div>
</footer>