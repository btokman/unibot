<?php
/**
 * @var $data \common\models\FormSettings
 * @var $model \common\models\Requests
 */
\frontend\assets\YiiAsset::register($this);
?>
<div class="content">
    <p class="modal__title"><?= $data->formLabel ?></p>
    <?php $form = \yii\widgets\ActiveForm::begin([
        'action' => \common\models\FormSettings::getFormUrl(),
        'options' => [
            'class' => 'form modal__form request__form',
            'autocomplete' => 'off'
        ],
        'fieldConfig' => [
            'errorOptions' => ['class' => 'help-block'],
            'options' => [
                'class' => 'form__row form__row_item'
            ]
        ]
    ]); ?>
    <div class="form__row form__row_half">
        <div class="form__row">
            <?= $form->field($model, "name")->textInput([
                'class' => 'form__input form__input_text',
                'placeholder' => $data->namePlaceholder
            ])->label(false); ?>
            <?= $form->field($model, "phone")->textInput([
                'class' => 'form__input form__input_text',
                'placeholder' => $data->phonePlaceholder
            ])->label(false); ?>
            <?= $form->field($model, "email")->textInput([
                'class' => 'form__input form__input_text',
                'placeholder' => $data->emailPlaceholder
            ])->label(false); ?>
        </div>
        <div class="form__row">
            <?= $form->field($model, "message", ['options' => ['class' => 'form__row form__row_item form__row_full-height']])->textarea([
                'class' => 'form__textarea',
                'placeholder' => $data->messagePlaceholder
            ])->label(false); ?>
        </div>
    </div>
    <div class="form__row form__row_simple form__row_center">
        <button class="btn btn_default form__submit" type="submit">
            <div class="btn__text"><?= $data->buttonLabel ?></div>
            <div class="btn__bg">
                <div class="btn__bg-left">
                    <div class="btn__bg-left-dot"></div>
                    <div class="btn__bg-left-dot"></div>
                    <div class="btn__bg-left-dot"></div>
                </div>
                <div class="btn__bg-right">
                    <div class="btn__bg-right-dot"></div>
                    <div class="btn__bg-right-dot"></div>
                    <div class="btn__bg-right-dot"></div>
                </div>
            </div>
        </button>
    </div>
    <?php \yii\widgets\ActiveForm::end() ?>
</div>
<div class="content-show">
    <p><?= $data->successMessage ?></p>
</div>