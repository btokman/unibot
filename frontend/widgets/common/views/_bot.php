<?php
/**
 * @var $questions \common\models\BotQuestions[]
 * @var $data \common\models\BotSettings
 */
?>
<div class="chat__data is-hidden">
    <div class="chat__data-bot">
        <?php foreach ($questions as $key => $item) : ?>
            <div data-bot="<?= $item->id ?>">
                <?= $item->label ?>
            </div>
        <?php endforeach; ?>
        <div data-bot="last">
            <p><?= $data->lasMessage ?></p>
        </div>
        <div data-bot="submit">
            <p><?= $data->successMessage ?></p>
        </div>
        <div data-bot="rejected">
            <p><?= $data->errorMessage ?></p>
        </div>
        <div data-bot="stop">
            <p><?= $data->stopMessage ?></p>
        </div>
    </div>
    <div class="chat__data-btn">
        <div data-btn="rejected">
            <p data-btn-event-target="stop"><?= $data->stomBtn ?></p>
            <p data-btn-event-target="last"><?= $data->mailBtn ?></p>
        </div>
        <?php foreach ($questions as $key => $item) : ?>
            <div data-btn="<?= $item->id ?>">
                <?php foreach ($item->answers as $answer) :
                    /** @var $answer \common\models\BotAnswer */
                    ?>
                    <p data-btn-event-target="<?=
                    $answer->is_last ? 'last' : $answer->next_question
                    ?>"><?= $answer->label ?></p>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
