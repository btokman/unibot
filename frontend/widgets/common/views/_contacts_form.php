<?php
/**
 * @var $data \common\models\FormSettings
 * @var $model \common\models\Requests
 */
\frontend\assets\YiiAsset::register($this);
?>
<div class="footer__callback-content">
    <div class="footer__callback-container container container_md">
        <h3 class="footer__callback-title"><?= \common\models\ContactsPage::getFormLabel() ?></h3>
        <?php $form = \yii\widgets\ActiveForm::begin([
            'action' => \common\models\FormSettings::getFormUrl(),
            'options' => [
                'class' => 'form form_contacts footer__callback-form',
                'autocomplete' => 'off'
            ],
            'fieldConfig' => [
                'errorOptions' => ['class' => 'help-block'],
                'options' => [
                    'class' => 'form__row form__row_item'
                ]
            ]
        ]); ?>
        <div class="form__row form__row_half">
            <div class="form__row">
                <?= $form->field($model, "name")->textInput([
                    'class' => 'form__input form__input_text',
                    'placeholder' => $data->namePlaceholder
                ])->label(false); ?>
                <?= $form->field($model, "phone")->textInput([
                    'class' => 'form__input form__input_text',
                    'placeholder' => $data->phonePlaceholder
                ])->label(false); ?>
                <?= $form->field($model, "email")->textInput([
                    'class' => 'form__input form__input_text',
                    'placeholder' => $data->emailPlaceholder
                ])->label(false); ?>
            </div>
            <div class="form__row">
                <?= $form->field($model, "message", ['options' => ['class' => 'form__row form__row_item form__row_full-height']])->textarea([
                    'class' => 'form__textarea',
                    'placeholder' => $data->messagePlaceholder
                ])->label(false); ?>
            </div>
        </div>
        <div class="form__row form__row_simple">
            <button class="btn btn_default form__submit" type="submit">
                <div class="btn__text"><?= $data->buttonLabel ?></div>
                <div class="btn__bg">
                    <div class="btn__bg-left">
                        <div class="btn__bg-left-dot"></div>
                        <div class="btn__bg-left-dot"></div>
                        <div class="btn__bg-left-dot"></div>
                    </div>
                    <div class="btn__bg-right">
                        <div class="btn__bg-right-dot"></div>
                        <div class="btn__bg-right-dot"></div>
                        <div class="btn__bg-right-dot"></div>
                    </div>
                </div>
            </button>
        </div>
        <?php \yii\widgets\ActiveForm::end() ?>
    </div>
</div>
<div class="footer__callback-content-show">
    <div class="footer__container container container_md">
        <p><?= \common\models\ContactsPage::getFormThanksMessage() ?></p>
    </div>
</div>