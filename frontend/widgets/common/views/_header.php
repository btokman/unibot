<?php
/**
 * @var $menu \common\models\MenuElements[]
 * @var $isHome boolean
 */

?>
<header class="header">
    <div class="header__container container">
        <a class="header__logo" href="<?= \yii\helpers\Url::home() ?>">
            <img class="header__logo-img" src="/static/img/logo.svg" alt="image" title=""/>
            <img class="header__logo-img" src="/static/img/logo-dark.svg" alt="image" title=""/>
        </a>
        <div class="header__right">
            <nav class="header__nav">
                <ul>
                    <?php foreach ($menu as $item) : ?>
                        <li>
                            <a href="<?= strpos($item->route, '#') === 0 ? '/' . $item->route : $item->route ?>"><?= $item->label ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </nav>
            <a class="header__chat" target="_blank"
               href="<?= Yii::$app->config->get(\common\models\FooterSettings::HEADER_LINK) ?>">
            <span class="header__chat-icon">
              <svg class="icon icon-facebook">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
              </svg>
            </span>
            </a>
            <div class="header__lang">
                <?= \frontend\widgets\common\LanguageChecker::widget() ?>
            </div>
            <div class="header__menu-btn">
                <svg class="icon icon-menu-open">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-menu-open"></use>
                </svg>
                <svg class="icon icon-menu-close">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-menu-close"></use>
                </svg>
            </div>

            <div class="header__mobile-chat-btn">
                <a href="#!" target="_blank"><?= Yii::$app->config->get(\common\models\FooterSettings::BOT_LABEL) ?></a>
            </div>

        </div>
    </div>
</header>