<?php
/**
 * @var $models \common\models\Language[]
 * @var $defaultLang \common\models\Language[]
 */

if (!$models || empty($models)) $models = [];

?>
<ul>
    <li>
        <a href="#"><?= $defaultLang->label ?></a>
        <ul>
            <?php foreach ($models as $model) : ?>
                <li>
                    <a href="<?= \yii\helpers\Url::current(['language' => $model->code]) ?>"><?= $model->label ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </li>
</ul>