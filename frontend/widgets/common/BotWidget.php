<?php

namespace frontend\widgets\common;

use common\models\BotAnswer;
use common\models\BotQuestions;
use common\models\BotSettings;
use yii\base\Widget;

/**
 * Class BotWidget
 * @package frontend\widgets\common
 */
class BotWidget extends Widget
{
    /** @var BotQuestions[] */
    public $questions;
    /** @var BotSettings */
    public $data;

    public function init()
    {
        $questions = BotQuestions::getElements();

        $this->data = (new BotSettings())->get();

        $ids = [];

        foreach ($questions as $question) {
            $this->questions[] = $question;
            foreach ($question->answers as $answer) {
                /** @var $answer BotAnswer */
                if (!$answer->question) continue;

                $this->questions[] = $answer->question;
            }
        }

        $this->questions = array_filter($this->questions, function ($obj) {
            static $idList = array();
            if (in_array($obj->id, $idList)) return false;

            $idList [] = $obj->id;
            return true;
        });

        parent::init();
    }

    public function run()
    {
        return $this->render('_bot', ['questions' => $this->questions, 'data' => $this->data]);
    }
}