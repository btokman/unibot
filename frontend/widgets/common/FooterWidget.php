<?php

namespace frontend\widgets\common;

use common\models\FooterSettings;
use common\models\MenuElements;
use yii\base\Widget;

/**
 * Class FooterWidget
 * @package frontend\widgets\common
 */
class FooterWidget extends Widget
{
    /** @var FooterSettings */
    private $data;
    /** @var MenuElements[] */
    private $menu;

    /** @var bool footer type */
    public $big = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->data = (new FooterSettings())->get();
        $this->menu = MenuElements::getElements();
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        $view = $this->big ? '_footer' : '_footer_small';

        return $this->render($view, ['data' => $this->data, 'menu' => $this->menu]);
    }
}