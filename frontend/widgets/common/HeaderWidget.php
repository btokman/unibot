<?php

namespace frontend\widgets\common;

use common\models\MenuElements;
use yii\base\Widget;

/**
 * Class HeaderWidget
 * @package frontend\widgets\common
 */
class HeaderWidget extends Widget
{
	/** @var MenuElements[] */
	private $menu;

	public $isHome = true;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		$this->menu = MenuElements::getElements();
		parent::init();
	}

	/**
	 * @return string
	 */
	public function run()
	{
		return $this->render('_header', ['menu' => $this->menu, 'isHome' => $this->isHome]);
	}
}