<?php

namespace frontend\widgets\common;

use common\models\FormSettings;
use common\models\Requests;
use yii\base\Widget;

/**
 * Class FormWidget
 * @package frontend\widgets\common
 */
class FormWidget extends Widget
{
    /** @var FormSettings */
    private $data;
    /** @var Requests */
    private $model;

    public function init()
    {
        $this->data = (new FormSettings())->get();
        $this->model = new Requests();
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('_form', ['model' => $this->model, 'data' => $this->data]);
    }
}