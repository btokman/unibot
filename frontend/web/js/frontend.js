/**
 * Created by walter on 17.12.15.
 */

function parseResponse(response) {
    if (response.replaces instanceof Array) {
        for (var i = 0, ilen = response.replaces.length; i < ilen; i++) {
            $(response.replaces[i].what).replaceWith(response.replaces[i].data);
        }
    }
    if (response.append instanceof Array) {
        for (i = 0, ilen = response.append.length; i < ilen; i++) {
            $(response.append[i].what).append(response.append[i].data);
        }
    }
    if (response.content instanceof Array) {
        for (i = 0, ilen = response.content.length; i < ilen; i++) {
            $(response.content[i].what).html(response.content[i].data);
        }
    }
    if (response.js) {
        $("body").append(response.js);
    }
    if (response.refresh) {
        window.location.reload(true);
    }
    if (response.redirect) {
        window.location.href = response.redirect;
    }
}

function executeAjaxRequest(url, data, type, successCallback, completeCallback) {
    var csrfParam = $('meta[name="csrf-param"]').attr('content');
    var csrfToken = $('meta[name="csrf-token"]').attr('content');

    var postData = {};
    if (!type) {
        type = 'GET';
        postData[csrfParam] = csrfToken;
    }
    postData = data ? $.extend(postData, data) : postData;

    jQuery.ajax({
        'cache': false,
        'type': type,
        'dataType': 'json',
        'data': postData,
        'success': successCallback ? successCallback : function (response) {
                parseResponse(response);
            },
        'error': function (response) {
            alert(response.responseText);
        },
        'beforeSend': function () {
        },
        'complete': completeCallback ? completeCallback : function () {
            },
        'url': url
    });
}

$(function () {
    $(document).on('click', 'a.ajax-link', function (event) {
        event.preventDefault();
        var that = this;
        if ($(that).data('confirm') && !confirm($(that).data('confirm'))) {
            return false;
        }
        executeAjaxRequest(that.href, $(that).data('params'))
    });

    $(document).on('submit', '.ajax-form', function (event) {
        event.preventDefault();
        var that = this;
        var formData = new FormData(that);
        jQuery.ajax({
            'cache': false,
            'type': 'POST',
            'dataType': 'json',
            'data': formData,
            'processData': false,
            'contentType': false,
            'success': function (response) {
                parseResponse(response);
            },
            'error': function (response) {
                alert(response.responseText);
            },
            'beforeSend': function () {
            },
            'complete': function () {
            },
            'url': that.action
        });
    });

    $(document).on("click", ".form-submit", function (e) {
        e.preventDefault();
        var form = $(this).parents('form');
        if (form.length) {
            form.submit();
        } else {
            var formId = $(this).data('id');
            $('#' + formId).submit();
        }
    });
});
