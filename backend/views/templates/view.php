<?php

use yii\helpers\Html;
use \backend\modules\seo\models\Robots;

/* @var $this yii\web\View */
/* @var $model \backend\components\BackendModel */

$this->title = $model->hasAttribute('label') && $model->label ? $model->label : $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $model->getTitle()), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary border-black">
    <div class="box-header">
        <h1 class="box-title"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="box-body">
        <p>
            <?php if ($model->showUpdateButton()): ?>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-flat bg-black btn-primary']) ?>
            <?php endif; ?>
            <?php if ($model->showDeleteButton()): ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-flat btn-danger bg-black',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif; ?>
            <?php if ($model->showCreateButton()): ?>
                <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn bg-black btn-success']) ?>

            <?php endif; ?>
        </p>

        <?= \backend\components\LanguageDetailView::widget([
            'model' => $model,
            'attributes' => $model->getColumns('view'),
        ]) ?>
    </div>

</div>
