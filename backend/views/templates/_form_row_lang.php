<?php
/**
 * @var $form FormBuilder
 * @var $model ActiveRecord
 * @var $attribute string
 * @var $element array
 */
use backend\components\FormBuilder;
use common\components\model\ActiveRecord;

if (!empty($translationModels) && $model->isTranslateAttribute($attribute)) :?>
    <div class="nav-tabs-custom navy tab-primary">
        <ul class="nav nav-tabs  language_tabs">
            <label class="switch">
                <input type="checkbox">
                <div class="swiper"></div>
            </label>
            <li class="active">
                <a href="#tab-lang-content_<?= $attribute ?>"
                   data-toggle="tab">    <?= $model->getAttributeLabel($attribute) ?></a>
            </li>
            <?php foreach ($translationModels as $languageModel) : ?>
                <li>
                    <a href="#tab-lang-content_<?= $languageModel->language . '_' . $attribute ?>"
                       data-toggle="tab"><?= $languageModel->language ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="tab-content">
        <div id="tab-lang-content_<?= $attribute ?>" class="tab-pane active">
            <?= $form->renderField($model, $attribute, $element)->label($model->getAttributeLabel($attribute), ['class' => 'hidden_label control-label']); ?>
            <?= $form->renderUploadedFile($model, $attribute, $element); ?>
        </div>

        <?php foreach ($translationModels as $languageModel) : ?>
            <div id="tab-lang-content_<?= $languageModel->language . '_' . $attribute ?>" class="tab-pane">
                <?= $form->renderField($languageModel, '[' . $languageModel->language . ']' . $attribute, $element)->label($languageModel->getAttributeLabel($attribute), ['class' => 'hidden_label control-label']); ?>
                <?= $form->renderUploadedFile($languageModel, $attribute, $element, $languageModel->language); ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->renderField($model, $attribute, $element) ?>
            <?= $form->renderUploadedFile($model, $attribute, $element) ?>
        </div>
    </div>
<?php endif; ?>
