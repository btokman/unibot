<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \backend\components\ModifiedDataColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\components\BackendModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->getTitle();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary border-black">
    <div class="box-header">
        <h1 class="box-title"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="box-body">
        <?php if($searchModel->showCreateButton()): ?>
            <p>
                <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'create btn btn-flat btn-primary black ']) ?>
            </p>
        <?php endif; ?>
        <?= GridView::widget([
            'options' => [
                'class' => 'grid-view table-responsive',
            ],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $searchModel->getColumns('index'),
            'tableOptions' => ['class' => 'table table-striped'],
            'dataColumnClass' => ModifiedDataColumn::className()
        ]); ?>

    </div>

</div>
