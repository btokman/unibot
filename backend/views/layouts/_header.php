<?php
use yii\helpers\Html;


?>

<header class="main-header">
    <!-- Logo -->
    <a href="<?= \Yii::$app->homeUrl ?>" class="logo">
        <?= $this->render('_logo') ?>
    </a>

    <!-- Header Navbar: style can be found in header.less -->

    <nav class="navbar navbar-static-top visible-sm visible-xs" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
    </nav>
</header>
