<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header"><?= Yii::t('app', 'Main') ?></li>
        </ul>
			<?php if (!\Yii::$app->user->isGuest): ?>
				<?= \backend\widgets\CustomMenu::widget([
					'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
					'items' => require(Yii::getAlias('@backend') . '/config/menu-items.php')
				]); ?>
			<?php endif; ?>
        <ul class="sidebar-menu">
            <li class="header"><?= Yii::t('app', 'Layout') ?></li>
        </ul>
			<?php if (!\Yii::$app->user->isGuest): ?>
				<?= \backend\widgets\CustomMenu::widget([
					'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
					'items' => require(Yii::getAlias('@backend') . '/config/layout-menu.php')
				]); ?>
			<?php endif; ?>
        <ul class="sidebar-menu">
            <li class="header"><?= Yii::t('app', 'Requests') ?></li>
        </ul>
        <?php if (!\Yii::$app->user->isGuest): ?>
            <?= \backend\widgets\CustomMenu::widget([
                'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
                'items' => require(Yii::getAlias('@backend') . '/config/requests-items.php')
            ]); ?>
        <?php endif; ?>
        <ul class="sidebar-menu">
            <li class="header"><?= Yii::t('app', 'Settings') ?></li>
        </ul>
			<?php if (!\Yii::$app->user->isGuest): ?>
				<?= \backend\widgets\CustomMenu::widget([
					'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
					'items' => require(Yii::getAlias('@backend') . '/config/settings-items.php')
				]); ?>
			<?php endif; ?>
    </section>
    <!-- /.sidebar -->
</aside>