<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

$this->title = \Yii::t('app', 'Unibot admin panel');

AppAsset::register($this);
dmstr\web\AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="sidebar-mini skin-green fixed ready">
<?php $this->beginBody() ?>
<div class="wrap">
    <!-- Header-->
    <?= $this->render('_header') ?>
    <!-- Sidebar -->
    <?= $this->render('_sidebar') ?>
    <!-- Right side column. Contains the navbar and content of the page -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->

        <section class="content-header">
            <?php if (\yii\helpers\Url::current() != '/site/index')  : ?>
                <h1>
                    <small><?= $this->title ?></small>
                </h1>
            <?php endif; ?>
            <?= Breadcrumbs::widget([
                'links' => $this->params['breadcrumbs'] ?? [],
            ]) ?>
        </section>

        <!-- Main content -->
        <section class="content">

            <?= $content ?>
        </section>
        <!-- /.content -->
    </div>

    <div class="modal modal-hidden fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <div class="popup" data-popup-id="remote" tabindex="-1" role="dialog">
        <div class="popup__container">
            <div class="popup__close"><span></span><span></span></div>
            <div class="popup__content"></div>
        </div>
    </div>
    <!-- /.content-wrapper -->

</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
