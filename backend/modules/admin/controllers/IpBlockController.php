<?php

namespace backend\modules\admin\controllers;

use backend\components\BackendController;
use backend\modules\admin\models\IpBlock;

/**
 * IpBlockController implements the CRUD actions for IpBlock model.
 */
class IpBlockController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return IpBlock::className();
    }
}
