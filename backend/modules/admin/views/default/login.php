<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $block \backend\modules\admin\models\IpBlock */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="login-box">
    <div class="login-logo">
        <a href="/"><b>Unibot</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>



        <?php if ($model->isVerifyRobotRequired) : ?>
            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                'captchaAction' => \yii\helpers\Url::to(['/site/captcha']),
                'template' => '{image}<br/><br/>{input}',
                'options' => ['class' => 'form-control', 'autocomplete' => 'off',],
            ]) ?>
        <?php endif; ?>
        <div class="row">
            <div class="col-xs-8">
                <?php if (Yii::$app->user->enableAutoLogin) : ?>
                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                <?php endif; ?>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

</div>
