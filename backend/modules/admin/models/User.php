<?php

namespace backend\modules\admin\models;

use backend\components\BackendModel;
use backend\modules\admin\widgets\authLog\AuthLogWidget;
use common\models\AuthAssignment;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class User
 *
 * @property AuthAssignment $authAdmin
 * @property AuthAssignment $authModerator
 */
class User extends \common\models\User implements BackendModel
{
    protected $admin;

    protected $moderator;

    public $new_password;

    public $repeat_password;

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_NEW_USER => ['username', 'email', 'new_password', 'repeat_password'],
            self::SCENARIO_UPDATE_USER => ['username', 'email', 'new_password', 'repeat_password'],
        ]);
    }

    public function rules()
    {
        $rules = ArrayHelper::merge(parent::rules(), [
            [
                'new_password',
                'compare',
                'compareAttribute' => 'repeat_password'
            ],
            [
                'email',
                'unique',
                'targetClass' => User::className(),
                'message' => 'This email address has already been taken.',
                'on' => self::SCENARIO_NEW_USER,
            ],
            [
                ['new_password', 'repeat_password'],
                'required',
                'on' => self::SCENARIO_NEW_USER,
            ],
            [
                ['auth_key', 'password_hash', 'password_reset_token'],
                'safe',
            ]
        ]);

        return $rules;
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('app', 'User');
    }

    /**
     * Has search form on index template page
     *
     * @return bool
     */
    public function hasSearch()
    {
        return false;
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    [
                        'attribute' => 'id',
                        'headerOptions' => [
                            'width' => '30px',
                        ],
                    ],
                    'username',
                    'email:email',
                    'block_at:datetime',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'username',
                    'email:email',
                    'block_at:datetime',
                ];
                break;
        }
        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new UserSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'form-set' => [
                \help\bt('Main') => [
                    'username' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'email' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                        ],
                    ],
                    'new_password' => [
                        'type' => ActiveFormBuilder::INPUT_PASSWORD,
                        'options' => [
                            'maxlength' => true,
                            'autocomplete' => 'off',
                        ],
                    ],
                    'repeat_password' => [
                        'type' => ActiveFormBuilder::INPUT_PASSWORD,
                        'options' => [
                            'maxlength' => true,
                            'autocomplete' => 'off',
                        ],
                    ],
                ],
                \help\bt('Auth Log') => [
                    'authLogsTable' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => AuthLogWidget::className(),
                    ],
                ],
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->new_password) {
            $this->auth_key = Yii::$app->security->generateRandomString();
            $this->password_hash = Yii::$app->security->generatePasswordHash($this->new_password);
            $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $auth = Yii::$app->authManager;
        if (!$auth->checkAccess($this->id, User::ROLE_ADMIN)) {
            $userRole = $auth->getRole(User::ROLE_ADMIN);
            $auth->assign($userRole, $this->id);
        }

        $admin = $auth->getRole(User::ROLE_ADMIN);
        if ($this->admin === '0') {
            if ($this->id !== Yii::$app->user->getId()) {
                $auth->revoke($admin, $this->id);
            }
        } elseif ($this->admin === '1') {
            $auth->assign($admin, $this->id);
        }
    }

    public function getAuthAdmin()
    {
        return $this->hasOne(AuthAssignment::className(),
            ['user_id' => 'id'])->andWhere(['item_name' => User::ROLE_ADMIN]);
    }

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return !!$this->authAdmin;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    public function getAuthModerator()
    {
        return null; // Заглушка на всякий пожарный
    }

    /**
     * @return mixed
     */
    public function getModerator()
    {
        return !!$this->authModerator;
    }

    /**
     * @param mixed $moderator
     */
    public function setModerator($moderator)
    {
        $this->moderator = $moderator;
    }

    /**
     * @return array
     */
    public static function getModeratorList()
    {
        $all = User::find()
            ->select(['id', 'username', 'email'])
            ->innerJoinWith(['authModerator'])
            ->asArray()
            ->all();

        return ArrayHelper::map($all, 'id', function ($item) {
            return $item['username'] . ' (' . $item['email'] . ')';
        });
    }

    public static function getLogoutUrl()
    {
        return Url::toRoute(['/admin/default/logout']);
    }
}
