<?php

namespace backend\modules\builder\controllers;

use backend\components\BackendController;
use backend\modules\builder\models\BuilderPreview;

/**
 * BuilderPreviewController implements the CRUD actions for BuilderPreview model.
 */
class BuilderPreviewController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BuilderPreview::className();
    }
}
