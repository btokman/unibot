<?php

namespace backend\modules\builder\controllers;

use backend\components\BackendController;
use backend\modules\builder\models\BuilderWidget;

/**
 * BuilderWidgetController implements the CRUD actions for BuilderWidget model.
 */
class BuilderWidgetController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BuilderWidget::className();
    }
}
