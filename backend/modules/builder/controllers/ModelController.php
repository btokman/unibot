<?php
namespace backend\modules\builder\controllers;

use backend\modules\builder\models\BuilderPreview;
use backend\modules\builder\Module;
use backend\modules\builder\widgets\previewWidget\PreviewWidget;
use common\components\BuilderModel;
use common\components\model\ActiveRecord;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class ModelController
 *
 * @package backend\modules\builder\controllers
 */
class ModelController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [\common\models\User::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    public function actionGet()
    {
        $model = \Yii::$app->request->post('model');
        $id = \Yii::$app->request->post('id');
        $attribute = \Yii::$app->request->post('attribute');
        $widget = \Yii::$app->request->post('widget');
        $key = \Yii::$app->request->post('key');

        if ($id) {
            /** @var ActiveRecord $class */
            $class = new $model;

            $model = $class::findOne($id);
        } else {
            $model = new $model;
        }

        /** @var BuilderModel $widget */
        $widget = new $widget;

        $widget->target_attribute = $attribute;
        $widget->sign = \Yii::$app->security->generateRandomString();

        $widget->setIsNewRecord(true);

        return $this->renderAjax('get', [
            'model' => $model,
            'id' => $model->getPrimaryKey(),
            'attribute' => $attribute,
            'widget' => $widget,
            'key' => $key,
        ]);
    }

    public function actionPreviewCache()
    {
        $tmpData = \Yii::$app->request->post('data');

        parse_str($tmpData, $data);

        $hash = \Yii::$app->security->generateRandomString();

        $model = new BuilderPreview();
        $model->hash = $hash;
        $model->data = Json::encode($data);
        $model->save();

        return Json::encode(['link' => Url::toRoute(['/builder/model/preview', 'hash' => $hash], true)]);
    }

    public function actionPreview($hash)
    {
        $this->layout = '@backend/modules/builder/views/layouts/builder';

        /** @var BuilderPreview $model */
        $model = BuilderPreview::find()->where(['hash' => $hash])->one();

        if (!$model) {
            throw new NotFoundHttpException();
        }

        $data = Json::decode($model->data);

        /** @var Module $module */
        $module = \Yii::$app->getModule('builder');
        $widgets = $module->models;

        $models = [];

        foreach ($widgets as $widget) {
            $data_s = ArrayHelper::getValue($data, $widget->formName(), []);

            foreach ($data_s as $key => $tmpData) {
                /** @var BuilderModel $model */
                $model = new $widget;
                $model->setAttributes($tmpData);
                $model->position = $key;

                if ($tmpData) {
                    $models[$key] = $model;
                }
            }
        }

        ksort($models);

        $widgets = PreviewWidget::widget(['widgets' => $models]);

        return $this->render('preview', ['widgets' => $widgets]);
    }
}
