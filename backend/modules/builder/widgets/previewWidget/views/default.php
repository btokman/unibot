<?php
/**
 * @var \yii\web\View $this
 */
use backend\modules\builder\widgets\previewWidget\PreviewAssets;

PreviewAssets::register($this);
