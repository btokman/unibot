<?php
/**
 * @var \yii\web\View $this
 *
 * @var \common\models\builder\ImageBuilderModel $model
 */
use yii\helpers\Html;
use yii\helpers\Url;
use metalguardian\fileProcessor\helpers\FPM;
use metalguardian\fileProcessor\models\File;

/** @var File $image */
$image = null;

if ($model->image) {
    $image = array_shift($model->image);
}
?>
<?php if ($image): ?>
    <?= FPM::image($image->id, 'builder', 'preview') ?>
<?php endif; ?>
