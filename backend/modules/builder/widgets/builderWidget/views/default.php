<?php
/**
 * @var \yii\web\View $this
 * @var \common\components\model\ActiveRecord $model
 * @var string $attribute
 * @var string $element
 * @var \backend\components\FormBuilder $form
 * @var [] $models
 * @var \backend\modules\builder\Module $module
 */

use backend\modules\builder\widgets\builderWidget\BuilderFieldAssets;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$selectorClass = ($module->enablePreview) ? 'col-lg-6' : 'col-lg-9';

BuilderFieldAssets::register($this);
?>
<div class="builder-widgets">
    <div class="row">
        <div class="<?= $selectorClass ?>">
            <div class="form-group">
                <?= Html::dropDownList('widget', null, $models,
                    ['class' => 'form-control add-content-builder-list']) ?>
            </div>
        </div>

        <div class="col-lg-3">
            <?= Html::button("<i class=\"glyphicon glyphicon-plus\"></i>&nbsp;" . \help\bt('Add'), [
                'type' => 'button',
                'class' => 'btn btn-success add-content-builder',
                'data' => [
                    'href' => Url::toRoute('/builder/model/get'),
                    'params' => [
                        Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                        'form' => $form::className(),
                        'model' => $model::className(),
                        'id' => $model->getPrimaryKey(),
                        'attribute' => $attribute,
                    ],
                    'key' => count($model->$attribute),
                ],
            ]) ?>
        </div>

        <?php if ($module->enablePreview): ?>
            <div class="col-lg-3">
                <?= Html::button("<i class=\"glyphicon glyphicon-eye-open\"></i>&nbsp;" . \help\bt('Preview'), [
                    'type' => 'button',
                    'class' => 'btn btn-success preview-content-builder',
                    'data' => [
                        'href' => Url::toRoute('/builder/model/preview-cache'),
                        'params' => [
                            Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                        ],
                        'attribute' => $attribute,
                    ],
                ]) ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="row place-row">
        <?php
        $items = [];

        if ($model->$attribute) {
            $model->$attribute = ArrayHelper::map($model->$attribute, 'position', function ($data) {
                return $data;
            });

            ArrayHelper::multisort($model->$attribute, 'position');

            foreach ($model->$attribute as $key => $widget) {
                $items[] = $this->render('@backend/modules/builder/views/model/get', [
                    'model' => $model,
                    'id' => $model->getPrimaryKey(),
                    'attribute' => $attribute,
                    'widget' => $widget,
                    'key' => $key,
                ]);
            }
        }

        echo implode("\n", $items);
        ?>
    </div>
</div>
