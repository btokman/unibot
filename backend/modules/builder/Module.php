<?php

namespace backend\modules\builder;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\builder\controllers';

    public $enablePreview = false;

    /**
     * @var \common\components\BuilderModel[]
     */
    public $models = [];

    public function init()
    {
        parent::init();
        // custom initialization code goes here

        foreach ($this->models as $key => $model) {
            $this->models[$key] = new $model;
        }
    }
}
