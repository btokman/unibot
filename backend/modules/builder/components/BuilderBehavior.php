<?php
namespace backend\modules\builder\components;

use backend\components\Model;
use backend\modules\builder\models\BuilderWidget;
use backend\modules\builder\Module;
use backend\modules\content\models\widgets\ImageModelWidget;
use common\components\BuilderModel;
use common\components\model\ActiveQuery;
use common\components\model\ActiveRecord;
use common\models\builder\ImageBuilderModel;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;

/**
 * Class BuilderBehavior
 *
 * @package backend\modules\builder\components
 */
class BuilderBehavior extends Behavior
{
    public $attributes = [];

    public $widgets = [];

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'validateContent',
            ActiveRecord::EVENT_AFTER_FIND => 'findContent',
            ActiveRecord::EVENT_AFTER_INSERT => 'saveContent',
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveContent',
        ];
    }

    public function validateContent()
    {
        foreach ($this->attributes as $attribute) {
            /** @var BuilderModel[] $models */
            $models = [];

            /** @var Module $module */
            $module = \Yii::$app->getModule('builder');
            $widgets = $module->models;

            if (!count($widgets)) {
                $widgets = $this->combineWidgets();
            }

            foreach ($widgets as $widget) {
                $datas = \Yii::$app->request->post($widget->formName(), []);

                foreach ($datas as $key => $data) {
                    /** @var BuilderModel $model */
                    $model = new $widget;
                    $model->setAttributes($data);

                    if($model->id) {
                        $model->setIsNewRecord(false);
                    } else {
                        $model->setIsNewRecord(true);
                    }

                    $model->position = $key;

                    if ($data) {
                        $models[] = $model;
                    }
                }
            }

            Model::validateMultiple($models);

            foreach ($models as $model) {
                $errors = $model->getErrors();

                foreach ($errors as $label => $tmpError) {
                    $label = $model->formName() . "[{$model->position}][{$label}]";

                    foreach ($tmpError as $error) {
                        $this->owner->addError($label, $error);
                    }
                }
            }

            $this->owner->$attribute = $models;
        }
    }

    /**
     *
     */
    public function findContent()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;
        $className = $owner->formName();
        $id = $owner->getPrimaryKey();

        $locales = \common\helpers\LanguageHelper::getApplicationLanguages();
        $defaultLocale = \common\helpers\LanguageHelper::getDefaultLanguage();
        $defaultLocale = \yii\helpers\ArrayHelper::getValue($defaultLocale, 'code');

        foreach ($this->attributes as $attribute) {
            /** @var ActiveQuery $query */
            $query = BuilderWidget::find()
                ->where([
                    'target_class' => $className,
                    'target_id' => $id,
                    'target_attribute' => $attribute
                ])
                ->orderBy(['position' => SORT_ASC, 'id' => SORT_ASC]);

            /** @var BuilderWidget[] $widgets */
            $widgets = $query->all();

            $tmpWidgets = [];

            foreach ($widgets as $widget) {
                /** @var BuilderModel $widgetModel */
                $widgetModel = new $widget->widget_class;

                $tmpAttributes = [];

                foreach ($widget->builderWidgetAttributes as $builderWidgetAttribute) {
                    $tmpAttributes[$builderWidgetAttribute->attribute] = $builderWidgetAttribute->value;
                }

                $tmpAttributes['id'] = $widget->id;
                $tmpAttributes['position'] = $widget->position;
                $tmpAttributes['target_attribute'] = $widget->target_attribute;
                $tmpAttributes['sign'] = \Yii::$app->security->generateRandomString();

                $widgetModel->setIsNewRecord(false);

                $widgetModel->setAttributes($tmpAttributes);

                $tmpWidgets[] = $widgetModel;
            }

            $this->owner->$attribute = $tmpWidgets;
        }
    }

    /**
     * @return bool
     */
    public function saveContent()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;

        $locales = \common\helpers\LanguageHelper::getApplicationLanguages();
        $defaultLocale = \common\helpers\LanguageHelper::getDefaultLanguage();
        $defaultLocale = \yii\helpers\ArrayHelper::getValue($defaultLocale, 'code');

        /** @var BuilderModel[] $models */
        $models = [];

        /** @var Module $module */
        $module = \Yii::$app->getModule('builder');
        $widgets = $module->models;

        if (!count($widgets)) {
            $widgets = $this->combineWidgets();
        }

        foreach ($widgets as $widget) {
            $datas = \Yii::$app->request->post($widget->formName(), []);

            foreach ($datas as $key => $data) {
                /** @var BuilderModel $model */
                $model = new $widget;
                $model->setAttributes($data);
                $model->position = $key;

                if ($data) {
                    $models[] = $model;
                }
            }
        }

        $valid = Model::validateMultiple($models);

        if ($valid) {
            $ids = [];

            foreach ($models as $model) {
                $ids[$model->target_attribute][] = $model->save($owner);
            }

            if (count($ids)) {
                foreach ($ids as $attribute => $tmpIds) {
                    /** @var ActiveQuery $query */
                    $query = BuilderWidget::find();

                    $query->andWhere(['target_class' => $owner->formName()]);
                    $query->andWhere(['target_id' => $owner->getPrimaryKey()]);
                    $query->andWhere(['target_attribute' => $attribute]);

                    $query->andWhere(['not in', 'id', $tmpIds]);

                    /** @var BuilderWidget[] $models */
                    $models = $query->all();

                    foreach ($models as $model) {
                        $model->delete();
                    }
                }
            } else {
                /** @var ActiveQuery $query */
                $query = BuilderWidget::find();

                $query->andWhere(['target_class' => $owner->formName()]);
                $query->andWhere(['target_id' => $owner->getPrimaryKey()]);

                /** @var BuilderWidget[] $models */
                $models = $query->all();

                foreach ($models as $model) {
                    $model->delete();
                }
            }
        }

        return $valid;
    }

    /**
     * @return array
     */
    protected function combineWidgets()
    {
        $widgets = [];

        foreach ($this->widgets as $widget) {
            $widgets[] = new $widget;
        }

        return $widgets;
    }
}
