<?php

namespace backend\modules\builder\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%builder_widget_attribute}}".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $widget_id
 * @property string $attribute
 * @property string $value
 *
 * @property BuilderWidget $widget
 * @property BuilderWidgetAttributeTranslation[] $translations
 */
class BuilderWidgetAttribute extends ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%builder_widget_attribute}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['widget_id', 'attribute'], 'required'],
            [['widget_id'], 'integer'],
            [['value'], 'string'],
            [['attribute'], 'string', 'max' => 255],
            [['widget_id'], 'exist', 'targetClass' => \common\models\BuilderWidget::className(), 'targetAttribute' => 'id'],
                ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'widget_id' => 'Widget ID',
            'attribute' => 'Attribute',
            'value' => 'Value',
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'value',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidget()
    {
        return $this->hasOne(BuilderWidget::className(), ['id' => 'widget_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BuilderWidgetAttributeTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('builder/attribute', 'Builder Widget Attribute');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'widget_id',
                    'attribute',
                    // 'value:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'widget_id',
                    'attribute',
                    [
                        'attribute' => 'value',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new BuilderWidgetAttributeSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'widget_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\BuilderWidget::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'attribute' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'value' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'value',
                ]
            ],
        ];

        return $config;
    }


}
