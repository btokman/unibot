<?php

namespace backend\modules\builder\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BuilderWidgetSearch represents the model behind the search form about `BuilderWidget`.
 */
class BuilderWidgetSearch extends BuilderWidget
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'target_id', 'position'], 'integer'],
            [['target_class', 'target_sign', 'target_attribute', 'widget_class'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BuilderWidgetSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'target_id' => $this->target_id,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'target_class', $this->target_class])
            ->andFilterWhere(['like', 'target_sign', $this->target_sign])
            ->andFilterWhere(['like', 'target_attribute', $this->target_attribute])
            ->andFilterWhere(['like', 'widget_class', $this->widget_class]);

        return $dataProvider;
    }
}
