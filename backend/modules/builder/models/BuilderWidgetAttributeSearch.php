<?php

namespace backend\modules\builder\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BuilderWidgetAttributeSearch represents the model behind the search form about `BuilderWidgetAttribute`.
 */
class BuilderWidgetAttributeSearch extends BuilderWidgetAttribute
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'widget_id'], 'integer'],
            [['attribute', 'value'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BuilderWidgetAttributeSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'widget_id' => $this->widget_id,
        ]);

        $query->andFilterWhere(['like', 'attribute', $this->attribute])
            ->andFilterWhere(['like', 'value', $this->value]);

        return $dataProvider;
    }
}
