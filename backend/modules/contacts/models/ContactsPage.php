<?php

namespace backend\modules\contacts\models;

use backend\modules\socialShareContent\behavior\SocialShareContentBehavior;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\ContactsPage as CommonContactsPage;
/**
* Class ContactsPage*/
class ContactsPage extends ConfigurationModel
{
    public $showAsConfig = false;

    public $placeId;
    public $map;
    /**
    * Title of the form
    *
    * @return string
    */
    public function getTitle()
    {
        return Yii::t('back/contacts', 'Page Settings');
    }

    /**
    * @return array
    */
    public function getFormRules()
    {
        return [
        ];
    }

    /**
    * @return array
    */
    public function getFormTypes()
    {
        return [
            CommonContactsPage::PAGE_LABEL => Configuration::TYPE_STRING,
            CommonContactsPage::LAT => Configuration::TYPE_STRING,
            CommonContactsPage::LNG => Configuration::TYPE_STRING,
            CommonContactsPage::FILLED_VALUE => Configuration::TYPE_STRING,
            CommonContactsPage::FORM_LABEL => Configuration::TYPE_STRING,
            CommonContactsPage::THANKS_MESSAGE => Configuration::TYPE_STRING,
        ];
    }

    /**
    * @return array
    */
    public function getFormDescriptions()
    {
        return [
            CommonContactsPage::PAGE_LABEL => Yii::t('back/contacts', 'Page label'),
            CommonContactsPage::LAT => Yii::t('back/contacts', 'Lat'),
            CommonContactsPage::LNG => Yii::t('back/contacts', 'Lng'),
            CommonContactsPage::FILLED_VALUE => Yii::t('back/contacts', 'Filled value'),
            CommonContactsPage::FORM_LABEL => Yii::t('back/contacts', 'Forn label'),
            CommonContactsPage::THANKS_MESSAGE => Yii::t('back/contacts', 'Thanks message'),

        ];
    }

    /**
    * @return array
    */
    public function getFormHints()
    {
        return [
            CommonContactsPage::PAGE_LABEL => '',
            CommonContactsPage::LAT => '',
            CommonContactsPage::LNG => '',
            CommonContactsPage::FILLED_VALUE => '',
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            CommonContactsPage::PAGE_LABEL,
            CommonContactsPage::FORM_LABEL,
            CommonContactsPage::THANKS_MESSAGE,
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'socialShareContent' => SocialShareContentBehavior::class
        ]);
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
                    CommonContactsPage::PAGE_LABEL,
                    'placeId' => [
                        'label' => false,
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'options' => [
                            'id' => 'placeId'
                        ]
                    ],
                    'map' => [
                        'label' => false,
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => '<div style="height: 500px" id="map"></div>'
                    ],
                    CommonContactsPage::LAT,
                    CommonContactsPage::LNG,
                    CommonContactsPage::FILLED_VALUE,
                    CommonContactsPage::FORM_LABEL,
                    CommonContactsPage::THANKS_MESSAGE,

                ],
            ]
        ];

        return $config;
    }
}
