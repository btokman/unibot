<?php

namespace backend\modules\news\models;

use backend\components\FileRequireValidator;
use backend\modules\builder\components\BuilderBehavior;
use backend\modules\builder\models\BuilderConfig;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\news\models\related\NewsToTags;
use backend\modules\news\models\related\Tags;
use backend\components\ManyToManyProductBehavior;
use backend\helpers\Select2Helper;
use backend\modules\socialShareContent\behavior\SocialShareContentBehavior;
use common\helpers\LanguageHelper;
use common\models\EntityToFile;
use metalguardian\fileProcessor\helpers\FPM;
use notgosu\yii2\modules\metaTag\models\MetaTag;
use notgosu\yii2\modules\metaTag\models\MetaTagContent;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use common\models\News as CommonModel;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property string $short_description
 * @property integer $position
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property NewsTranslation[] $translations
 */
class News extends ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    public $image;
    public $sign;

    public $tags;
    public $tagsValue;

    public $content;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'alias', 'short_description'], 'required'],
            [['position', 'published'], 'integer'],
            [['label', 'alias'], 'string', 'max' => 255],
            [['short_description'], 'string', 'max' => 455],
            [['position'], 'default', 'value' => 0],
            [['published'], 'default', 'value' => 1],
            [['image'],
                FileRequireValidator::className(),
                'saveAttribute' => CommonModel::IMAGE,
                'skipOnEmpty' => false
            ],
            [['sign', 'tags'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/news', 'ID'),
            'label' => Yii::t('back/news', 'Label'),
            'alias' => Yii::t('back/news', 'Alias'),
            'short_description' => Yii::t('back/news', 'Short Description'),
            'position' => Yii::t('back/news', 'Position'),
            'published' => Yii::t('back/news', 'Published'),
            'image' => Yii::t('back/news', 'Preview image'),
            'tags' => Yii::t('back/news', 'Tags')
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'short_description',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'manyToManyProduct' => [
                'class' => ManyToManyProductBehavior::className(),
                'currentObj' => $this,
                'currentRelatedColumn' => 'news_id',
                'manyToManyConfig' => [
                    'answers' => [
                        'attribute' => 'tags',
                        'valueAttribute' => 'tagsValue',
                        'relatedColumn' => 'tag_id',
                        'linkTableName' => NewsToTags::tableName(),
                        'relatedObjTableName' => Tags::tableName(),
                        'isStringReturn' => false
                    ],
                ]
            ],
            'content' => [
                'class' => BuilderBehavior::className(),
                'attributes' => ['content'],
                'widgets' => [
                    'common\models\builder\ContentBuilderModel',
                    'common\models\builder\ImageBuilderModel',
                ],
            ],
            'socialShareContent' => SocialShareContentBehavior::class
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(NewsTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('app', 'News');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'alias',
                    [
                        'attribute' => 'image',
                        'value' => function ($model) {
                            $src = FPM::originalSrc($model->mainImage->file_id ?? null);
                            return '<img src=' . $src . ' > ';
                        },
                        'format' => 'raw',
                    ],
                    'position',
                    'published:boolean',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'alias',
                    'short_description',
                    'position',
                    'published:boolean',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new NewsSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                Yii::t('back/news', 'Main') => [
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                            'class' => $this->isNewRecord ? 's_name form-control' : 'form-control'
                        ],
                    ],
                    'alias' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                            'class' => $this->isNewRecord ? 's_alias form-control' : 'form-control'
                        ],
                    ],
                    'image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'image',
                            'saveAttribute' => CommonModel::IMAGE,
                            'allowedFileExtensions' => ['png', 'jpg'],
                            'multiple' => false,

                        ])
                    ],
                    'short_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'options' => [
                            'maxlength' => true,

                        ],
                    ],
                    'tags' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'label' => $this->getAttributeLabel('tags'),
                        'value' => Select2Helper::getAjaxSelect2Widget(
                            $this,
                            'tags',
                            self::getTagsUrl(),
                            $this->tagsValue,
                            [
                                'multiple' => true,
                            ]
                        )
                    ],
                    'position' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false
                    ]
                ],
                \help\bt('Content') => [
                    'content' => BuilderConfig::config($this, 'content', [
                        'models' => [
                            'common\models\builder\ContentBuilderModel',
                            'common\models\builder\ImageBuilderModel',
                        ],
                    ]),
                ],
            ]
        ];

        return $config;
    }

    /**
     * @inheritdoc
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);

        $metaTagList = MetaTag::find()
            ->where(['is_active' => 1])
            ->orderBy(['position' => SORT_DESC])
            ->all();

        $metaTags = MetaTagContent::find()
            ->where([MetaTagContent::tableName() . '.model_name' => $this->formName()])
            ->andWhere([MetaTagContent::tableName() . '.model_id' => $this->id])
            ->joinWith(['metaTag'])
            ->orderBy([MetaTag::tableName() . '.position' => SORT_DESC])
            ->indexBy(function ($row) {
                return $row->meta_tag_id . $row->language;
            })
            ->all();

        foreach ($metaTagList as $tag) {
            foreach (LanguageHelper::getApplicationLanguages() as $language) {
                if ($tag->name === 'title') {
                    $metaTags[$tag->id . $language]['content'] = $this->label . ' | Unibot';
                    foreach ($this->translations as $model) {
                        if ($model->language === $language) {
                            $metaTags[$tag->id . $language]['content'] = $model->label . ' | Unibot';
                        }
                    }

                }
            }
        }

        foreach ($metaTags as $tag) {
            $tag->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getMainImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['attribute' => CommonModel::IMAGE]);
    }

    /**
     * @return string
     */
    public static function getTagsUrl()
    {
        return '/news/tags/get-select-items';
    }


}
