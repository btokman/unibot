<?php

namespace backend\modules\news\models;

use Yii;

/**
* This is the model class for table "{{%news_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $short_description
*/
class NewsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%news_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/news', 'Label') . ' [' . $this->language . ']',
            'short_description' => Yii::t('back/news', 'Short Description') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
            [['short_description'], 'string', 'max' => 455],
         ];
    }
}
