<?php

namespace backend\modules\news\models;

use backend\modules\socialShareContent\behavior\SocialShareContentBehavior;
use Yii;
use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\NewsPage as CommonNewsPage;
/**
* Class NewsPage*/
class NewsPage extends ConfigurationModel
{
    public $showAsConfig = false;
    /**
    * Title of the form
    *
    * @return string
    */
    public function getTitle()
    {
        return Yii::t('back/news', 'Page Settings');
    }

    /**
    * @return array
    */
    public function getFormRules()
    {
        return [
            [CommonNewsPage::PAGE_LABEL, 'required'],
            [CommonNewsPage::PAGE_SUB_LABEL, 'required'],
            [CommonNewsPage::READ_MORE_BUTTON, 'required'],
            [CommonNewsPage::SHARE_BUTTON, 'required'],
        ];
    }

    /**
    * @return array
    */
    public function getFormTypes()
    {
        return [
            CommonNewsPage::PAGE_LABEL => Configuration::TYPE_STRING,
            CommonNewsPage::PAGE_SUB_LABEL => Configuration::TYPE_STRING,
            CommonNewsPage::READ_MORE_BUTTON => Configuration::TYPE_STRING,
            CommonNewsPage::SHARE_BUTTON => Configuration::TYPE_STRING,
        ];
    }

    /**
    * @return array
    */
    public function getFormDescriptions()
    {
        return [
            CommonNewsPage::PAGE_LABEL => Yii::t('back/news', 'Page label'),
            CommonNewsPage::PAGE_SUB_LABEL => Yii::t('back/news', 'Page sub label'),
            CommonNewsPage::READ_MORE_BUTTON => Yii::t('back/news', 'Read more button'),
            CommonNewsPage::SHARE_BUTTON => Yii::t('back/news', 'Share button label'),
        ];
    }

    /**
    * @return array
    */
    public function getFormHints()
    {
        return [
            CommonNewsPage::PAGE_LABEL => '',
            CommonNewsPage::PAGE_SUB_LABEL => '',
            CommonNewsPage::READ_MORE_BUTTON => '',
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            CommonNewsPage::PAGE_LABEL,
            CommonNewsPage::PAGE_SUB_LABEL,
            CommonNewsPage::READ_MORE_BUTTON,
            CommonNewsPage::SHARE_BUTTON,
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'socialShareContent' => SocialShareContentBehavior::class
        ]);
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
                    CommonNewsPage::PAGE_LABEL,
                    CommonNewsPage::PAGE_SUB_LABEL,
                    CommonNewsPage::READ_MORE_BUTTON,
                    CommonNewsPage::SHARE_BUTTON,
                ],
            ]
        ];

        return $config;
    }
}
