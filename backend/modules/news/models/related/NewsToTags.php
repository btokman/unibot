<?php

namespace backend\modules\news\models\related;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%news_to_tags}}".
 *
 * @property integer $id
 * @property integer $news_id
 * @property integer $tag_id
 */
class NewsToTags extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_to_tags}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'tag_id'], 'required'],
            [['news_id', 'tag_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/news', 'ID'),
            'news_id' => Yii::t('back/news', 'News ID'),
            'tag_id' => Yii::t('back/news', 'Tag ID'),
        ];
    }

    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'News To Tags');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'news_id',
                    'tag_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'news_id',
                    'tag_id',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new NewsToTagsSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'news_id' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'tag_id' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }


}
