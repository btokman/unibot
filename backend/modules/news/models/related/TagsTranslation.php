<?php

namespace backend\modules\news\models\related;

use Yii;

/**
* This is the model class for table "{{%tags_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class TagsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%tags_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/news', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
         ];
    }
}
