<?php

namespace backend\modules\news\controllers;

use backend\components\BackendController;
use backend\modules\news\models\related\Tags;
use yii\db\Query;
use yii\web\Response;

/**
 * TagsController implements the CRUD actions for Tags model.
 */
class TagsController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Tags::className();
    }


    public function actionGetSelectItems($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($q)) {
            $q = '';
        }

        $query = new Query();
        $query->select('id, label AS text')
            ->from(Tags::tableName())
            ->where(['like', 'label', $q])->limit(5);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);

        if ($id > 0) {
            /**
             * @var $answer Tags
             */
            $tags = Tags::find($id);
            if ($tags) {
                $out['results'] = ['id' => $id, 'text' => $tags->label];
            }
        }
        return $out;
    }
}
