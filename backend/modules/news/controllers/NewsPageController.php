<?php

namespace backend\modules\news\controllers;

use backend\modules\configuration\components\ConfigurationController;
use backend\modules\news\models\NewsPage;

/**
 * NewsPageController implements the CRUD actions for NewsPage model.
 */
class NewsPageController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return NewsPage::className();
    }
}
