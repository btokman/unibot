<?php

namespace backend\modules\news\controllers;

use backend\components\BackendController;
use backend\modules\news\models\related\NewsToTags;

/**
 * NewsToTagsController implements the CRUD actions for NewsToTags model.
 */
class NewsToTagsController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return NewsToTags::className();
    }
}
