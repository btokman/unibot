<?php
/**
 * @var \yii\db\ActiveRecord $model
 * @var \yii\widgets\ActiveForm $form
 */
use backend\components\Translateable;
use yii\helpers\Html;

?>
<?php foreach ($model->formConfig as $attribute => $element) : ?>
    <div class="nav-tabs-custom navy tab-primary">
        <ul class="nav nav-tabs  language_tabs">
            <label class="switch">
                <input type="checkbox">
                <div class="swiper"></div>
            </label>
            <li class="active">
                <a href="#tab-lang-content_<?= $attribute ?>"
                   data-toggle="tab">    <?= $model->getAttributeLabel($attribute) ?></a>
            </li>
            <?php if ($model instanceof Translateable && $model->isTranslateAttribute($attribute)) : ?>
                <?php foreach ($model->getTranslationModels() as $languageModel) : ?>
                    <li>
                        <a href="#tab-lang-content_<?= $languageModel->language . '_' . $attribute ?>"
                           data-toggle="tab"><?= $languageModel->language ?></a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
    <div class="tab-content">
        <div id="tab-lang-content_<?= $attribute ?>" class="tab-pane active">
            <?= $form->renderField($model, $attribute, $element)->label($model->getAttributeLabel($attribute), ['class' => 'hidden_label control-label']); ?>
            <?= $form->renderUploadedFile($model, $attribute, $element); ?>
        </div>
        <?php if ($model instanceof Translateable && $model->isTranslateAttribute($attribute)) : ?>
            <?php foreach ($model->getTranslationModels() as $languageModel) : ?>
                <div id="tab-lang-content_<?= $languageModel->language . '_' . $attribute ?>" class="tab-pane">
                    <?= $form->renderField($languageModel, '[' . $languageModel->language . ']' . $attribute, $element)->label(false) ?>
                    <?= $form->renderUploadedFile($languageModel, $attribute, $element, $languageModel->language) ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
<?php endforeach; ?>