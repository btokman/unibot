<?php

namespace backend\modules\home\controllers;

use backend\components\BackendController;
use backend\modules\home\models\related\CasesLinks;

/**
 * CasesLinksController implements the CRUD actions for CasesLinks model.
 */
class CasesLinksController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return CasesLinks::className();
    }
}
