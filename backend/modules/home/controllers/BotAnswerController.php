<?php

namespace backend\modules\home\controllers;

use backend\components\BackendController;
use backend\modules\home\models\BotAnswer;
use yii\db\Query;
use yii\web\Response;

/**
 * BotAnswerController implements the CRUD actions for BotAnswer model.
 */
class BotAnswerController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BotAnswer::className();
    }

	public function actionGetSelectItems($q = null, $id = null)
	{
		\Yii::$app->response->format = Response::FORMAT_JSON;
		$out = ['results' => ['id' => '', 'text' => '']];

		if (is_null($q)) {
			$q = '';
		}

		$query = new Query();
		$query->select('id, label AS text')
			->from(BotAnswer::tableName())
			->where(['like', 'label', $q])->limit(5);
		$command = $query->createCommand();
		$data = $command->queryAll();
		$out['results'] = array_values($data);

		if ($id > 0) {
			/**
			 * @var $answer BotAnswer
			 */
			$answer = BotAnswer::find($id);
			if ($answer) {
				$out['results'] = ['id' => $id, 'text' => $answer->answer];
			}
		}
		return $out;
	}
}
