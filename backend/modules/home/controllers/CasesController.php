<?php

namespace backend\modules\home\controllers;

use backend\components\BackendController;
use backend\modules\home\models\related\Cases;

/**
 * CasesController implements the CRUD actions for Cases model.
 */
class CasesController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Cases::className();
    }
}
