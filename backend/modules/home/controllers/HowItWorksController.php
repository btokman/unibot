<?php

namespace backend\modules\home\controllers;

use backend\components\BackendController;
use backend\modules\home\models\related\HowItWorks;

/**
 * HowItWorksController implements the CRUD actions for HowItWorks model.
 */
class HowItWorksController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return HowItWorks::className();
    }
}
