<?php

namespace backend\modules\home\controllers;

use backend\modules\configuration\components\ConfigurationController;
use backend\modules\home\models\BotSettings;

/**
 * BotSettingsController implements the CRUD actions for BotSettings model.
 */
class BotSettingsController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BotSettings::className();
    }
}
