<?php

namespace backend\modules\home\controllers;

use backend\components\BackendController;
use backend\modules\home\models\related\WhereItWorks;

/**
 * WhereItWorksController implements the CRUD actions for WhereItWorks model.
 */
class WhereItWorksController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return WhereItWorks::className();
    }
}
