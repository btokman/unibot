<?php

namespace backend\modules\home\controllers;

use backend\components\BackendController;
use backend\modules\home\models\related\Emails;

/**
 * EmailsController implements the CRUD actions for Emails model.
 */
class EmailsController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Emails::className();
    }
}
