<?php

namespace backend\modules\home\controllers;

use backend\components\BackendController;
use backend\modules\home\models\BotQuestions;

/**
 * BotQuestionsController implements the CRUD actions for BotQuestions model.
 */
class BotQuestionsController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BotQuestions::className();
    }
}
