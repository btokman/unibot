<?php

namespace backend\modules\home\controllers;

use backend\components\BackendController;
use backend\modules\home\models\related\Benefits;

/**
 * BenefitsController implements the CRUD actions for Benefits model.
 */
class BenefitsController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Benefits::className();
    }
}
