<?php

namespace backend\modules\home\controllers;

use backend\modules\configuration\components\ConfigurationController;
use backend\modules\home\models\HomePage;

/**
 * HomePageController implements the CRUD actions for HomePage model.
 */
class HomePageController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return HomePage::className();
    }
}
