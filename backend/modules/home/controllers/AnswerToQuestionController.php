<?php

namespace backend\modules\home\controllers;

use backend\components\BackendController;
use backend\modules\home\models\AnswerToQuestion;

/**
 * AnswerToQuestionController implements the CRUD actions for AnswerToQuestion model.
 */
class AnswerToQuestionController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return AnswerToQuestion::className();
    }
}
