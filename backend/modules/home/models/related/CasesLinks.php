<?php

namespace backend\modules\home\models\related;

use backend\components\FileRequireValidator;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\models\CasesLinks as CommonModel;

/**
 * This is the model class for table "{{%cases_links}}".
 *
 * @property integer $id
 * @property string $link
 * @property integer $position
 */
class CasesLinks extends ActiveRecord implements BackendModel
{
    public $image;

    public $sign;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cases_links}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link'], 'required'],
            [['position'], 'integer'],
            [['link'], 'string', 'max' => 455],
            [['link'], 'url', 'defaultScheme' => 'http'],
            [['position'], 'default', 'value' => 0],
            [['image'],
                FileRequireValidator::className(),
                'saveAttribute' => CommonModel::IMAGE,
                'skipOnEmpty' => false
            ],
            ['sign', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/cases', 'ID'),
            'link' => Yii::t('back/cases', 'Link'),
            'position' => Yii::t('back/cases', 'Position'),
            'image' => Yii::t('back/cases', 'Icon')
        ];
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('app', 'Cases Links');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'link:url',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'link:url',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new CasesLinksSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'link' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'image' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'image',
                    'saveAttribute' => CommonModel::IMAGE,
                    'allowedFileExtensions' => ['png', 'svg'],
                    'multiple' => false,

                ])
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false
            ]
        ];

        return $config;
    }

    /**
     * @inheritdoc
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }


}
