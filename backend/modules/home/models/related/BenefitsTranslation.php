<?php

namespace backend\modules\home\models\related;

use Yii;

/**
* This is the model class for table "{{%benefits_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class BenefitsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%benefits_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/home', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('back/home', 'Description') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 455],
         ];
    }
}
