<?php

namespace backend\modules\home\models\related;

use backend\components\FileRequireValidator;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use common\models\Cases as CommonModel;

/**
 * This is the model class for table "{{%cases}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property integer $position
 *
 * @property CasesTranslation[] $translations
 */
class Cases extends ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;


    public $image;

    public $sign;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cases}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'description'], 'required'],
            [['description'], 'string'],
            [['position'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['position'], 'default', 'value' => 0],
            [['image'],
                FileRequireValidator::className(),
                'saveAttribute' => CommonModel::IMAGE,
                'skipOnEmpty' => false
            ],
            ['sign', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/home', 'ID'),
            'label' => Yii::t('back/home', 'Label'),
            'description' => Yii::t('back/home', 'Description'),
            'position' => Yii::t('back/home', 'Position'),
            'image' => Yii::t('back/home', 'Image')
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(CasesTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('app', 'Cases');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    // 'description:ntext',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new CasesSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'image' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'image',
                    'saveAttribute' => CommonModel::IMAGE,
                    'allowedFileExtensions' => ['png', 'jpg'],
                    'multiple' => false,

                ])
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'description',
                ]
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false
            ],
            $this->getRelatedFormConfig()['links']
        ];

        return $config;
    }

    /**
     * @inheritdoc
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [
            'links' => [
                'relation' => 'links'
            ]
        ];
    }

    /**
     * @return $this
     */
    public function getLinks()
    {
        return $this->hasMany(CasesLinks::class, ['model_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }
}
