<?php

namespace backend\modules\home\models;

use backend\modules\home\models\related\Emails;
use Yii;
use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\BotSettings as CommonBotSettings;

/**
 * Class BotSettings*/
class BotSettings extends ConfigurationModel
{
    public $showAsConfig = false;

    /**
     * Title of the form
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/home', 'Settings');
    }

    /**
     * @return array
     */
    public function getFormRules()
    {
        return [
            [CommonBotSettings::LAS_MESSAGE, 'required'],
            [CommonBotSettings::SUCCESS_MESSAGE, 'required'],
            [CommonBotSettings::EMAIL_PLACEHOLDER, 'required'],
            [CommonBotSettings::EMAIL_ERROR_MESSAGE, 'required'],
            [CommonBotSettings::STOP_BTN, 'required'],
            [CommonBotSettings::MAIL_BTN, 'required'],
            [CommonBotSettings::STOP_MESSAGE, 'required'],
        ];
    }

    /**
     * @return array
     */
    public function getFormTypes()
    {
        return [
            CommonBotSettings::LAS_MESSAGE => Configuration::TYPE_STRING,
            CommonBotSettings::SUCCESS_MESSAGE => Configuration::TYPE_STRING,
            CommonBotSettings::EMAIL_PLACEHOLDER => Configuration::TYPE_STRING,
            CommonBotSettings::EMAIL_ERROR_MESSAGE => Configuration::TYPE_STRING,
            CommonBotSettings::STOP_BTN => Configuration::TYPE_STRING,
            CommonBotSettings::MAIL_BTN => Configuration::TYPE_STRING,
            CommonBotSettings::STOP_MESSAGE => Configuration::TYPE_STRING,
        ];
    }

    /**
     * @return array
     */
    public function getFormDescriptions()
    {
        return [
            CommonBotSettings::LAS_MESSAGE => Yii::t('back/home', 'Last message'),
            CommonBotSettings::SUCCESS_MESSAGE => Yii::t('back/home', 'Success message'),
            CommonBotSettings::EMAIL_PLACEHOLDER => Yii::t('back/home', 'Email placeholder'),
            CommonBotSettings::EMAIL_ERROR_MESSAGE => Yii::t('back/home', 'Email error message'),
            CommonBotSettings::STOP_BTN => Yii::t('back/home', 'Stop button'),
            CommonBotSettings::MAIL_BTN => Yii::t('back/home', 'Mail btn'),
            CommonBotSettings::STOP_MESSAGE => Yii::t('back/home', 'Stop message'),
        ];
    }

    /**
     * @return array
     */
    public function getFormHints()
    {
        return [
            CommonBotSettings::LAS_MESSAGE => '',
            CommonBotSettings::SUCCESS_MESSAGE => '',
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            CommonBotSettings::LAS_MESSAGE,
            CommonBotSettings::SUCCESS_MESSAGE,
            CommonBotSettings::EMAIL_PLACEHOLDER,
            CommonBotSettings::EMAIL_ERROR_MESSAGE,
            CommonBotSettings::STOP_BTN,
            CommonBotSettings::MAIL_BTN,
            CommonBotSettings::STOP_MESSAGE
        ];
    }


    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
                    CommonBotSettings::LAS_MESSAGE,
                    CommonBotSettings::SUCCESS_MESSAGE,
                    CommonBotSettings::STOP_MESSAGE,
                    CommonBotSettings::EMAIL_ERROR_MESSAGE,
                    CommonBotSettings::EMAIL_PLACEHOLDER,
                    CommonBotSettings::STOP_BTN,
                    CommonBotSettings::MAIL_BTN,
                    $this->getRelatedFormConfig()['emails']
                ],
            ]
        ];

        return $config;
    }

    /**
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [
            'emails' => [
                'relation' => 'emails'
            ],
        ];
    }

    /**
     * @return $this
     */
    public function getEmails()
    {
        return $this->hasMany(Emails::class, ['page_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }
}
