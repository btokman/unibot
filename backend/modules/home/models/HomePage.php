<?php

namespace backend\modules\home\models;

use backend\modules\home\models\related\Benefits;
use backend\modules\home\models\related\HowItWorks;
use backend\modules\home\models\related\WhereItWorks;
use backend\modules\socialShareContent\behavior\SocialShareContentBehavior;
use common\models\News;
use Yii;
use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\HomePage as CommonHomePage;
use yii\helpers\Json;

/**
 * Class HomePage*/
class HomePage extends ConfigurationModel
{
    public $showAsConfig = false;

    const dropDowns = [CommonHomePage::NEWS];

    /**
     * Title of the form
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/home', 'Page Settings');
    }

    /**
     * @return array
     */
    public function getFormRules()
    {
        return [
            [CommonHomePage::PAGE_LABEL, 'required'],
            [CommonHomePage::PAGE_DESCRIPTION, 'required'],
            [CommonHomePage::BOT_BUTTON_LABEL, 'required'],
            [CommonHomePage::HOW_IT_WORK_LABEL, 'required'],
            [CommonHomePage::HOW_IT_WORK_DESCRIPTION, 'required'],
            [CommonHomePage::WHERE_IT_WORK_LABEL, 'required'],
            [CommonHomePage::WHERE_IT_WORK_BUTTON_LABEL, 'required'],
            [CommonHomePage::BENEFITS_LABEL, 'required'],
            [CommonHomePage::CASES_LABEL, 'required'],
            [CommonHomePage::CASES_LINK_LABEL, 'required'],
            [CommonHomePage::CASES_TELEGRAM_LINK, 'required'],
            [CommonHomePage::CASES_MESSANGER_LINK, 'required'],
            [CommonHomePage::NEWS_LABEL, 'required'],
            [CommonHomePage::NEWS_BUTTON_LABEL, 'required'],
            [CommonHomePage::CASES_BUTTON_LABEL, 'required'],
            [CommonHomePage::PAGE_NAME, 'required'],
        ];
    }

    /**
     * @return array
     */
    public function getFormTypes()
    {
        return [
            CommonHomePage::PAGE_LABEL => Configuration::TYPE_STRING,
            CommonHomePage::PAGE_DESCRIPTION => Configuration::TYPE_TEXT,
            CommonHomePage::BOT_BUTTON_LABEL => Configuration::TYPE_STRING,
            CommonHomePage::HOW_IT_WORK_LABEL => Configuration::TYPE_STRING,
            CommonHomePage::HOW_IT_WORK_DESCRIPTION => Configuration::TYPE_TEXT,
            CommonHomePage::WHERE_IT_WORK_LABEL => Configuration::TYPE_STRING,
            CommonHomePage::WHERE_IT_WORK_BUTTON_LABEL => Configuration::TYPE_STRING,
            CommonHomePage::BENEFITS_LABEL => Configuration::TYPE_STRING,
            CommonHomePage::CASES_LABEL => Configuration::TYPE_STRING,
            CommonHomePage::CASES_LINK_LABEL => Configuration::TYPE_STRING,
            CommonHomePage::CASES_TELEGRAM_LINK => Configuration::TYPE_STRING,
            CommonHomePage::CASES_MESSANGER_LINK => Configuration::TYPE_STRING,
            CommonHomePage::NEWS_LABEL => Configuration::TYPE_STRING,
            CommonHomePage::NEWS_BUTTON_LABEL => Configuration::TYPE_STRING,
            CommonHomePage::NEWS =>  Configuration::TYPE_MULTI_SELECT,
            CommonHomePage::CASES_BUTTON_LABEL => Configuration::TYPE_STRING,
            CommonHomePage::PAGE_NAME => Configuration::TYPE_STRING
        ];
    }

    /**
     * @return array
     */
    public function getFormDescriptions()
    {
        return [
            CommonHomePage::PAGE_LABEL => Yii::t('back/home', 'Page label'),
            CommonHomePage::PAGE_DESCRIPTION => Yii::t('back/home', 'Page description'),
            CommonHomePage::BOT_BUTTON_LABEL => Yii::t('back/home', 'Bot button label'),
            CommonHomePage::HOW_IT_WORK_LABEL => Yii::t('back/home', 'How it work label'),
            CommonHomePage::HOW_IT_WORK_DESCRIPTION => Yii::t('back/home', 'How it work description'),
            CommonHomePage::WHERE_IT_WORK_LABEL => Yii::t('back/home', 'Where it work label'),
            CommonHomePage::WHERE_IT_WORK_BUTTON_LABEL => Yii::t('back/home', 'Where it work button label'),
            CommonHomePage::BENEFITS_LABEL => Yii::t('back/home', 'Benefits label'),
            CommonHomePage::CASES_LABEL => Yii::t('back/home', 'Cases label'),
            CommonHomePage::CASES_LINK_LABEL => Yii::t('back/home', 'Cases link label'),
            CommonHomePage::CASES_TELEGRAM_LINK => Yii::t('back/home', 'Cases telegram link'),
            CommonHomePage::CASES_MESSANGER_LINK => Yii::t('back/home', 'Cases messanger link'),
            CommonHomePage::NEWS_LABEL => Yii::t('back/home', 'News label'),
            CommonHomePage::NEWS_BUTTON_LABEL => Yii::t('back/home', 'News button label'),
            CommonHomePage::NEWS => Yii::t('back/home', 'News'),
            CommonHomePage::CASES_BUTTON_LABEL => Yii::t('back/home', 'Cases button label'),
            CommonHomePage::PAGE_NAME => Yii::t('back/home', 'Page name'),
        ];
    }

    /**
     * @return array
     */
    public function getFormHints()
    {
        return [
            CommonHomePage::PAGE_LABEL => '',
            CommonHomePage::PAGE_DESCRIPTION => '',
            CommonHomePage::BOT_BUTTON_LABEL => '',
            CommonHomePage::HOW_IT_WORK_LABEL => '',
            CommonHomePage::HOW_IT_WORK_DESCRIPTION => '',
            CommonHomePage::WHERE_IT_WORK_LABEL => '',
            CommonHomePage::WHERE_IT_WORK_BUTTON_LABEL => '',
            CommonHomePage::BENEFITS_LABEL => '',
            CommonHomePage::CASES_LABEL => '',
            CommonHomePage::CASES_LINK_LABEL => '',
            CommonHomePage::CASES_TELEGRAM_LINK => '',
            CommonHomePage::CASES_MESSANGER_LINK => '',
            CommonHomePage::NEWS_LABEL => '',
            CommonHomePage::NEWS_BUTTON_LABEL => '',
            CommonHomePage::NEWS => '',
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            CommonHomePage::PAGE_LABEL,
            CommonHomePage::PAGE_DESCRIPTION,
            CommonHomePage::BOT_BUTTON_LABEL,
            CommonHomePage::HOW_IT_WORK_LABEL,
            CommonHomePage::HOW_IT_WORK_DESCRIPTION,
            CommonHomePage::WHERE_IT_WORK_LABEL,
            CommonHomePage::WHERE_IT_WORK_BUTTON_LABEL,
            CommonHomePage::BENEFITS_LABEL,
            CommonHomePage::CASES_LABEL,
            CommonHomePage::CASES_LINK_LABEL,
            CommonHomePage::CASES_TELEGRAM_LINK,
            CommonHomePage::CASES_MESSANGER_LINK,
            CommonHomePage::NEWS_LABEL,
            CommonHomePage::NEWS_BUTTON_LABEL,
            CommonHomePage::CASES_BUTTON_LABEL,
            CommonHomePage::PAGE_NAME
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'socialShareContent' => SocialShareContentBehavior::class
        ]);
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                Yii::t('back/home', 'Main') => [
                    CommonHomePage::PAGE_NAME,
                    CommonHomePage::PAGE_LABEL,
                    CommonHomePage::PAGE_DESCRIPTION,
                    CommonHomePage::BOT_BUTTON_LABEL,
                    CommonHomePage::NEWS_LABEL,
                    CommonHomePage::NEWS_BUTTON_LABEL,
                    CommonHomePage::NEWS,
                ],
                Yii::t('back/home', 'How it works') => [
                    CommonHomePage::HOW_IT_WORK_LABEL,
                    CommonHomePage::HOW_IT_WORK_DESCRIPTION,
                    $this->getRelatedFormConfig()['works']
                ],
                Yii::t('back/home', 'Where it works') => [
                    CommonHomePage::WHERE_IT_WORK_LABEL,
                    CommonHomePage::WHERE_IT_WORK_BUTTON_LABEL,
                    $this->getRelatedFormConfig()['where']
                ],
                Yii::t('back/home', 'Benefits') => [
                    CommonHomePage::BENEFITS_LABEL,
                    $this->getRelatedFormConfig()['benefits']
                ],
                Yii::t('back/home', 'Cases') => [
                    CommonHomePage::CASES_LABEL,
                    CommonHomePage::CASES_LINK_LABEL,
                    CommonHomePage::CASES_BUTTON_LABEL,
                ]
            ]
        ];

        return $config;
    }

    /**
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [
            'works' => [
                'relation' => 'works'
            ],
            'where' => [
                'relation' => 'where'
            ],
            'benefits' => [
                'relation' => 'benefits'
            ],
        ];
    }

    /**
     * @return $this
     */
    public function getWorks()
    {
        return $this->hasMany(HowItWorks::class, ['page_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return $this
     */
    public function getWhere()
    {
        return $this->hasMany(WhereItWorks::class, ['page_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return $this
     */
    public function getBenefits()
    {
        return $this->hasMany(Benefits::class, ['page_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave()
    {
        $data = $this->getModels();
        foreach ($data as $key => $model) {
            if (in_array($model->id, self::dropDowns, true)) {
                $model->value = Json::encode($model->value);
                $data[$key] = $model;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if (\Yii::$app->controller->action->id !== 'view') {
            foreach ($this->models as $key => $model) {
                if (in_array($model->id, self::dropDowns, true)) {
                    $model->value = Json::decode($model->value);
                    $data[$key] = $model;
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getListItems()
    {
        return [
            CommonHomePage::NEWS => News::getItems('id', 'label'),

        ];
    }

    /**
     * @return array
     */
    public function getFieldsOptions()
    {
        return [
            CommonHomePage::NEWS => [
                'multiple' => true,
                'pluginOptions' => [
                    'maximumSelectionLength' => 3,
                ],
            ],
        ];
    }
}
