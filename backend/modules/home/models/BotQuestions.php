<?php

namespace backend\modules\home\models;

use backend\components\ManyToManyProductBehavior;
use backend\helpers\Select2Helper;
use dosamigos\tinymce\TinyMce;
use froala\froalaeditor\FroalaEditorWidget;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%bot_questions}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $position
 *
 * @property BotQuestionsTranslation[] $translations
 */
class BotQuestions extends ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    public $answersIds;
    public $answersIdsValue;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bot_questions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['position'], 'integer'],
            [['label'], 'string', 'max' => 455],
            [['position'], 'default', 'value' => 0],
            [['answersIds'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/home', 'ID'),
            'label' => Yii::t('back/home', 'Question'),
            'position' => Yii::t('back/home', 'Position'),
            'answersIds' => Yii::t('back/home', 'Answers')
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'manyToManyProduct' => [
                'class' => ManyToManyProductBehavior::className(),
                'currentObj' => $this,
                'currentRelatedColumn' => 'question_id',
                'manyToManyConfig' => [
                    'answers' => [
                        'attribute' => 'answersIds',
                        'valueAttribute' => 'answersIdsValue',
                        'relatedColumn' => 'answer_id',
                        'linkTableName' => AnswerToQuestion::tableName(),
                        'relatedObjTableName' => BotAnswer::tableName(),
                        'isStringReturn' => false
                    ],
                ]

            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BotQuestionsTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('app', 'Bot Questions');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'label',
                        'value' => function ($model) {
                            return strip_tags($model->label);
                        },
                        'format' => 'raw',
                    ],
                    // 'id',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'label',
                        'value' => function ($model) {
                            return strip_tags($model->label);
                        },
                        'format' => 'raw',
                    ],
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new BotQuestionsSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => FroalaEditorWidget::className(),
                'options' => [
                    'model' => $this,
                    'clientOptions' => [
                        'height' => 200,
                        'toolbarButtons' => ['emoticons', 'fullscreen', 'bold', 'italic', 'underline', 'insertLink', '|', 'paragraphFormat'],

                    ],
                ]
            ],
            'answersIds' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'label' => $this->getAttributeLabel('answersIds'),
                'value' => Select2Helper::getAjaxSelect2Widget(
                    $this,
                    'answersIds',
                    self::getAnswersUrl(),
                    $this->answersIdsValue,
                    [
                        'multiple' => true,
                    ]
                )
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }


    /**
     * @return string
     */
    public static function getAnswersUrl()
    {
        return '/home/bot-answer/get-select-items';
    }


}
