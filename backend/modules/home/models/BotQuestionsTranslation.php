<?php

namespace backend\modules\home\models;

use Yii;

/**
* This is the model class for table "{{%bot_questions_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $question
*/
class BotQuestionsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%bot_questions_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/home', 'Question') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 455],
         ];
    }
}
