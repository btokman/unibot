<?php

namespace backend\modules\home\models;

use Yii;

/**
* This is the model class for table "{{%bot_answer_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class BotAnswerTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%bot_answer_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/home', 'Answer') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 455],
         ];
    }
}
