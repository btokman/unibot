<?php

namespace backend\modules\home;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\home\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
