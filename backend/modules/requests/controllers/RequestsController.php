<?php

namespace backend\modules\requests\controllers;

use backend\components\BackendController;
use backend\modules\requests\models\related\Requests;

/**
 * RequestsController implements the CRUD actions for Requests model.
 */
class RequestsController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Requests::className();
    }
}
