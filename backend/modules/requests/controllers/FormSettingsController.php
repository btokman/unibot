<?php

namespace backend\modules\requests\controllers;

use backend\modules\configuration\components\ConfigurationController;
use backend\modules\requests\models\FormSettings;

/**
 * FormSettingsController implements the CRUD actions for FormSettings model.
 */
class FormSettingsController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return FormSettings::className();
    }
}
