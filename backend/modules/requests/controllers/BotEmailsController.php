<?php

namespace backend\modules\requests\controllers;

use backend\components\BackendController;
use backend\modules\requests\models\related\BotEmails;

/**
 * BotEmailsController implements the CRUD actions for BotEmails model.
 */
class BotEmailsController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BotEmails::className();
    }
}
