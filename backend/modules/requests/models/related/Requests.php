<?php

namespace backend\modules\requests\models\related;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%requests}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $message
 */
class Requests extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%requests}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => Yii::t('requests', 'Name require error')],
            [['phone'], 'required', 'message' => Yii::t('requests', 'Phone require error')],
            [['email'], 'required', 'message' => Yii::t('requests', 'Email require error')],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
            [['message'], 'string', 'max' => 455],
            ['email', 'email', 'message' => Yii::t('requests', 'Email validation error')],
            [['phone'], 'match', 'pattern' => '^(?:00|\+)[0-9]{4}-?[0-9]{7}$', 'message' => Yii::t('requests', 'Phone validation error')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/requests', 'ID'),
            'name' => Yii::t('back/requests', 'Name'),
            'phone' => Yii::t('back/requests', 'Phone'),
            'email' => Yii::t('back/requests', 'Email'),
            'message' => Yii::t('back/requests', 'Message'),
        ];
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('app', 'Requests');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'name',
                    'phone',
                    'date',
                    'email:email',
                    'message',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'name',
                    'phone',
                    'date',
                    'email:email',
                    'message',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new RequestsSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'name' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'phone' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'email' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'message' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
        ];

        return $config;
    }


}
