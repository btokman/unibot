<?php

namespace backend\modules\requests\models;

use Yii;
use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\FormSettings as CommonFormSettings;

/**
 * Class FormSettings*/
class FormSettings extends ConfigurationModel
{
    public $showAsConfig = true;

    /**
     * Title of the form
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/requests', 'Form Settings');
    }

    /**
     * @return array
     */
    public function getFormRules()
    {
        return [
            [CommonFormSettings::FORM_LABEL, 'required'],
            [CommonFormSettings::NAME_PLACEHOLDER, 'required'],
            [CommonFormSettings::PHONE_PLACEHOLDER, 'required'],
            [CommonFormSettings::EMAIL_PLACEHOLDER, 'required'],
            [CommonFormSettings::MESSAGE_PLACEHOLDER, 'required'],
            [CommonFormSettings::BUTTON_LABEL, 'required'],
            [CommonFormSettings::SUCCESS_MESSAGE, 'required'],
            [CommonFormSettings::NAME_ERROR, 'required'],
            [CommonFormSettings::EMAIL_ERROR, 'required'],
            [CommonFormSettings::EMAIL_VALIDATION_ERROR, 'required'],
            [CommonFormSettings::PHONE_EROROR, 'required'],
            [CommonFormSettings::PHONE_VALIDATION_ERROR, 'required'],
        ];
    }

    /**
     * @return array
     */
    public function getFormTypes()
    {
        return [
            CommonFormSettings::FORM_LABEL => Configuration::TYPE_STRING,
            CommonFormSettings::NAME_PLACEHOLDER => Configuration::TYPE_STRING,
            CommonFormSettings::PHONE_PLACEHOLDER => Configuration::TYPE_STRING,
            CommonFormSettings::EMAIL_PLACEHOLDER => Configuration::TYPE_STRING,
            CommonFormSettings::MESSAGE_PLACEHOLDER => Configuration::TYPE_STRING,
            CommonFormSettings::BUTTON_LABEL => Configuration::TYPE_STRING,
            CommonFormSettings::SUCCESS_MESSAGE => Configuration::TYPE_STRING,
            CommonFormSettings::NAME_ERROR => Configuration::TYPE_STRING,
            CommonFormSettings::EMAIL_ERROR => Configuration::TYPE_STRING,
            CommonFormSettings::EMAIL_VALIDATION_ERROR => Configuration::TYPE_STRING,
            CommonFormSettings::PHONE_EROROR => Configuration::TYPE_STRING,
            CommonFormSettings::PHONE_VALIDATION_ERROR => Configuration::TYPE_STRING,
        ];
    }

    /**
     * @return array
     */
    public function getFormDescriptions()
    {
        return [
            CommonFormSettings::FORM_LABEL => Yii::t('back/requests', 'Form label'),
            CommonFormSettings::NAME_PLACEHOLDER => Yii::t('back/requests', 'Name placeholder'),
            CommonFormSettings::PHONE_PLACEHOLDER => Yii::t('back/requests', 'Phone placeholder'),
            CommonFormSettings::EMAIL_PLACEHOLDER => Yii::t('back/requests', 'Email placeholder'),
            CommonFormSettings::MESSAGE_PLACEHOLDER => Yii::t('back/requests', 'Message placeholder'),
            CommonFormSettings::BUTTON_LABEL => Yii::t('back/requests', 'Button label'),
            CommonFormSettings::SUCCESS_MESSAGE => Yii::t('back/requests', 'Success message'),
            CommonFormSettings::NAME_ERROR => Yii::t('back/requests', 'Name requeire error'),
            CommonFormSettings::EMAIL_ERROR => Yii::t('back/requests', 'Email requeire error'),
            CommonFormSettings::EMAIL_VALIDATION_ERROR => Yii::t('back/requests', 'Email validation error'),
            CommonFormSettings::PHONE_EROROR => Yii::t('back/requests', 'Phone requeire error'),
            CommonFormSettings::PHONE_VALIDATION_ERROR => Yii::t('back/requests', 'Phone validation error'),

        ];
    }

    /**
     * @return array
     */
    public function getFormHints()
    {
        return [
            CommonFormSettings::FORM_LABEL => '',
            CommonFormSettings::NAME_PLACEHOLDER => '',
            CommonFormSettings::PHONE_PLACEHOLDER => '',
            CommonFormSettings::EMAIL_PLACEHOLDER => '',
            CommonFormSettings::MESSAGE_PLACEHOLDER => '',
            CommonFormSettings::BUTTON_LABEL => '',
            CommonFormSettings::SUCCESS_MESSAGE => '',
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            CommonFormSettings::FORM_LABEL,
            CommonFormSettings::NAME_PLACEHOLDER,
            CommonFormSettings::PHONE_PLACEHOLDER,
            CommonFormSettings::EMAIL_PLACEHOLDER,
            CommonFormSettings::MESSAGE_PLACEHOLDER,
            CommonFormSettings::BUTTON_LABEL,
            CommonFormSettings::SUCCESS_MESSAGE,
            CommonFormSettings::NAME_ERROR,
            CommonFormSettings::EMAIL_ERROR,
            CommonFormSettings::EMAIL_VALIDATION_ERROR,
            CommonFormSettings::PHONE_EROROR,
            CommonFormSettings::PHONE_VALIDATION_ERROR,
        ];
    }


    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
                    CommonFormSettings::FORM_LABEL,
                    CommonFormSettings::NAME_PLACEHOLDER,
                    CommonFormSettings::PHONE_PLACEHOLDER,
                    CommonFormSettings::EMAIL_PLACEHOLDER,
                    CommonFormSettings::MESSAGE_PLACEHOLDER,
                    CommonFormSettings::BUTTON_LABEL,
                    CommonFormSettings::SUCCESS_MESSAGE,
                    CommonFormSettings::NAME_ERROR,
                    CommonFormSettings::EMAIL_ERROR,
                    CommonFormSettings::EMAIL_VALIDATION_ERROR,
                    CommonFormSettings::PHONE_EROROR,
                    CommonFormSettings::PHONE_VALIDATION_ERROR,
                ],
            ]
        ];

        return $config;
    }
}
