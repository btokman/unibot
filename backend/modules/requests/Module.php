<?php

namespace backend\modules\requests;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\requests\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
