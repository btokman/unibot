<?php

namespace backend\modules\common\models;

use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\widgets\FileUploader;
use common\models\EntityToFile;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\FooterSettings as CommonFooterSettings;
/**
* Class FooterSettings*/
class FooterSettings extends ConfigurationModel
{
    public $showAsConfig = false;

    public $privacyFile;
    public $termsFile;
    /**
    * Title of the form
    *
    * @return string
    */
    public function getTitle()
    {
        return Yii::t('back/common', 'Footer settings');
    }

    /**
    * @return array
    */
    public function getFormRules()
    {
        return [
            [CommonFooterSettings::CONTACTLS_LABEL, 'required'],
            [CommonFooterSettings::ADRESS, 'required'],
            [CommonFooterSettings::PHONE_LABEL, 'required'],
            [CommonFooterSettings::PHONE, 'required'],
            [CommonFooterSettings::EMAIL_LABEL, 'required'],
            [CommonFooterSettings::EMAIL, 'required'],
            [CommonFooterSettings::BOT_LABEL, 'required'],
            [CommonFooterSettings::COPYRIGHT, 'required'],
            [CommonFooterSettings::HEADER_LINK, 'required'],
            [CommonFooterSettings::TERMS_LABEL, 'required'],
            [CommonFooterSettings::PRIVACY_FILE, 'required'],
        ];
    }

    /**
    * @return array
    */
    public function getFormTypes()
    {
        return [
            CommonFooterSettings::CONTACTLS_LABEL => Configuration::TYPE_STRING,
            CommonFooterSettings::ADRESS => Configuration::TYPE_HTML,
            CommonFooterSettings::WORK_TIME => Configuration::TYPE_HTML,
            CommonFooterSettings::PHONE_LABEL => Configuration::TYPE_STRING,
            CommonFooterSettings::PHONE => Configuration::TYPE_STRING,
            CommonFooterSettings::EMAIL_LABEL => Configuration::TYPE_STRING,
            CommonFooterSettings::EMAIL => Configuration::TYPE_STRING,
            CommonFooterSettings::SOCIAL_LABEL => Configuration::TYPE_STRING,
            CommonFooterSettings::TELEGRAM_SOCIAL_LINK => Configuration::TYPE_STRING,
            CommonFooterSettings::FACEBOOCK_SOCIAL_LINK => Configuration::TYPE_STRING,
            CommonFooterSettings::INSTAGRAM_SOCIAL_LINK => Configuration::TYPE_STRING,
            CommonFooterSettings::BOT_LABEL => Configuration::TYPE_STRING,
            CommonFooterSettings::COPYRIGHT => Configuration::TYPE_STRING,
            CommonFooterSettings::HEADER_LINK => Configuration::TYPE_STRING,
            CommonFooterSettings::PRIVACY_LABEL => Configuration::TYPE_STRING,
            CommonFooterSettings::TERMS_LABEL => Configuration::TYPE_STRING,
        ];
    }

    /**
    * @return array
    */
    public function getFormDescriptions()
    {
        return [
            CommonFooterSettings::CONTACTLS_LABEL => Yii::t('back/common', 'Contactls label'),
            CommonFooterSettings::ADRESS => Yii::t('back/common', 'Adress'),
            CommonFooterSettings::WORK_TIME => Yii::t('back/common', 'Work time'),
            CommonFooterSettings::PHONE_LABEL => Yii::t('back/common', 'Phone label'),
            CommonFooterSettings::PHONE => Yii::t('back/common', 'Phone'),
            CommonFooterSettings::EMAIL_LABEL => Yii::t('back/common', 'Email label'),
            CommonFooterSettings::EMAIL => Yii::t('back/common', 'Email'),
            CommonFooterSettings::SOCIAL_LABEL => Yii::t('back/common', 'Social label'),
            CommonFooterSettings::TELEGRAM_SOCIAL_LINK => Yii::t('back/common', 'Linkedin social link'),
            CommonFooterSettings::FACEBOOCK_SOCIAL_LINK => Yii::t('back/common', 'Faceboock social link'),
            CommonFooterSettings::INSTAGRAM_SOCIAL_LINK => Yii::t('back/common', 'Twitter social link'),
            CommonFooterSettings::BOT_LABEL => Yii::t('back/common', 'Chat bot label'),
            CommonFooterSettings::COPYRIGHT => Yii::t('back/common', 'Copyright'),
            CommonFooterSettings::HEADER_LINK => Yii::t('back/common', 'Header link'),
            CommonFooterSettings::PRIVACY_LABEL => Yii::t('back/common', 'Privacy label'),
            CommonFooterSettings::TERMS_LABEL => Yii::t('back/common', 'Terms label'),
            'privacyFile' => Yii::t('back/common', 'Privacy file'),
            'termsFile' => Yii::t('back/common', 'Terms file')
        ];
    }

    /**
    * @return array
    */
    public function getFormHints()
    {
        return [
            CommonFooterSettings::CONTACTLS_LABEL => '',
            CommonFooterSettings::ADRESS => '',
            CommonFooterSettings::WORK_TIME => '',
            CommonFooterSettings::PHONE_LABEL => '',
            CommonFooterSettings::PHONE => '',
            CommonFooterSettings::EMAIL_LABEL => '',
            CommonFooterSettings::EMAIL => '',
            CommonFooterSettings::SOCIAL_LABEL => '',
            CommonFooterSettings::TELEGRAM_SOCIAL_LINK => '',
            CommonFooterSettings::FACEBOOCK_SOCIAL_LINK => '',
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            CommonFooterSettings::CONTACTLS_LABEL,
            CommonFooterSettings::ADRESS,
            CommonFooterSettings::WORK_TIME,
            CommonFooterSettings::PHONE_LABEL,
            CommonFooterSettings::EMAIL_LABEL,
            CommonFooterSettings::SOCIAL_LABEL,
            CommonFooterSettings::BOT_LABEL,
            CommonFooterSettings::COPYRIGHT,
            CommonFooterSettings::PRIVACY_LABEL,
            CommonFooterSettings::TERMS_LABEL,
        ];
    }


    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
                    CommonFooterSettings::CONTACTLS_LABEL,
                    CommonFooterSettings::ADRESS,
                    CommonFooterSettings::PHONE_LABEL,
                    CommonFooterSettings::PHONE,
                    CommonFooterSettings::EMAIL_LABEL,
                    CommonFooterSettings::EMAIL,
                    CommonFooterSettings::SOCIAL_LABEL,
                    CommonFooterSettings::TELEGRAM_SOCIAL_LINK,
                    CommonFooterSettings::FACEBOOCK_SOCIAL_LINK,
                    CommonFooterSettings::INSTAGRAM_SOCIAL_LINK,
                    CommonFooterSettings::BOT_LABEL,
                    CommonFooterSettings::COPYRIGHT,
                    CommonFooterSettings::HEADER_LINK,
                    CommonFooterSettings::PRIVACY_LABEL,
                    'privacyFile' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'label' => false,
                        'value' => FileUploader::widget([
                            'model' => $this,
                            'saveAttribute' => CommonFooterSettings::PRIVACY_FILE,
                            'multiLang' => true,
                            'attributeLabel' => Yii::t('back/footer-settings', 'Privacy file'),
                            'allowedFileExtensions' => ['pdf']
                        ])
                    ],
                    CommonFooterSettings::TERMS_LABEL,
                    'termsFile' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'label' => false,
                        'value' => FileUploader::widget([
                            'model' => $this,
                            'saveAttribute' => CommonFooterSettings::TERMS_FILE,
                            'multiLang' => true,
                            'attributeLabel' => Yii::t('back/footer-settings', 'Terms file'),
                            'allowedFileExtensions' => ['pdf']
                        ])
                    ],
                ],
            ]
        ];

        return $config;
    }


}
