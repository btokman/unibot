<?php

namespace backend\modules\common\models;

use Yii;

/**
* This is the model class for table "{{%menu_elements_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class MenuElementsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%menu_elements_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/common', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
         ];
    }
}
