<?php

namespace backend\modules\common\models;

use backend\components\FileRequireValidator;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\socialShareContent\behavior\SocialShareContentBehavior;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\NotFoundPage as CommonNotFoundPage;

/**
 * Class NotFoundPage*/
class NotFoundPage extends ConfigurationModel
{
    public $showAsConfig = false;

    public $image;
    public $mobileImage;

    /**
     * Title of the form
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/notfound', 'Page Settings');
    }

    /**
     * @return array
     */
    public function getFormRules()
    {
        return [
            [CommonNotFoundPage::PAGE_LABEL, 'required'],
            [CommonNotFoundPage::BUTTON_LABEL, 'required'],
        ];
    }

    /**
     * @return array
     */
    public function getFormTypes()
    {
        return [
            CommonNotFoundPage::PAGE_LABEL => Configuration::TYPE_STRING,
            CommonNotFoundPage::BUTTON_LABEL => Configuration::TYPE_STRING,
        ];
    }

    /**
     * @return array
     */
    public function getFormDescriptions()
    {
        return [
            CommonNotFoundPage::PAGE_LABEL => Yii::t('back/notfound', 'Page label'),
            CommonNotFoundPage::BUTTON_LABEL => Yii::t('back/notfound', 'Button label'),
            'image' => Yii::t('back/notfound', 'Image'),
            'mobileImage' => Yii::t('back/notfound', 'Mobile image')
        ];
    }

    /**
     * @return array
     */
    public function getFormHints()
    {
        return [
            CommonNotFoundPage::PAGE_LABEL => '',
            CommonNotFoundPage::BUTTON_LABEL => '',
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            CommonNotFoundPage::PAGE_LABEL,
            CommonNotFoundPage::BUTTON_LABEL,
        ];
    }

    public function rules()
    {
        return [
            [
                'image',
                FileRequireValidator::className(),
                'saveAttribute' => CommonNotFoundPage::IMAGE,
                'skipOnEmpty' => false
            ],
            [
                'mobileImage',
                FileRequireValidator::className(),
                'saveAttribute' => CommonNotFoundPage::MOBILE_IMAGE,
                'skipOnEmpty' => false
            ],
        ];
    }


    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
                    CommonNotFoundPage::PAGE_LABEL,
                    CommonNotFoundPage::BUTTON_LABEL,
                    'image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'image',
                            'adobeSdk' => true,
                            'multiple' => true,
                            'saveAttribute' => CommonNotFoundPage::IMAGE,
                            'allowedFileExtensions' => ['jpg', 'png'],
                        ])
                    ],
                    'mobileImage' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'mobileImage',
                            'adobeSdk' => true,
                            'multiple' => true,
                            'saveAttribute' => CommonNotFoundPage::MOBILE_IMAGE,
                            'allowedFileExtensions' => ['jpg', 'png'],
                        ])
                    ],
                ],
            ]
        ];

        return $config;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'socialShareContent' => SocialShareContentBehavior::class
        ]);
    }
}
