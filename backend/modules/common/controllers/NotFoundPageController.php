<?php

namespace backend\modules\common\controllers;

use backend\modules\configuration\components\ConfigurationController;
use backend\modules\common\models\NotFoundPage;

/**
 * NotFoundPageController implements the CRUD actions for NotFoundPage model.
 */
class NotFoundPageController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return NotFoundPage::className();
    }
}
