<?php

namespace backend\modules\common\controllers;

use backend\components\BackendController;
use backend\modules\common\models\MenuElements;

/**
 * MenuElementsController implements the CRUD actions for MenuElements model.
 */
class MenuElementsController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return MenuElements::className();
    }
}
