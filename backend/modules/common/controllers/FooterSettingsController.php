<?php

namespace backend\modules\common\controllers;

use backend\modules\configuration\components\ConfigurationController;
use backend\modules\common\models\FooterSettings;

/**
 * FooterSettingsController implements the CRUD actions for FooterSettings model.
 */
class FooterSettingsController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return FooterSettings::className();
    }
}
