<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\components\BackendModel */

$this->title = Yii::t('app', 'Update {modelClass}', [
    'modelClass' => $model->getTitle(),
]);
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['view']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="box box-primary">
    <div class="box-header">
        <h1 class="box-title"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="box-body">
        <?= $this->render('//templates/_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
