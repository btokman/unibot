<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\modules\configuration\components\ConfigurationModel */

$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header">
        <h1 class="box-title"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update'], ['class' => 'btn btn-flat bg-black  btn-primary']) ?>
        </p>

        <?php foreach ($model->getModels() as $item) :
                if (is_array($item->type)) {
                    continue;
                }
            ?>
            <br>
            <?= \backend\modules\configuration\components\ConfigurationDetailView::widget([
                'model' => $item,
                'attributes' => $item->getColumns('model'),
            ]); ?>

        <?php endforeach ?>
    </div>

</div>
