<?php
/**
 * @var $form FormBuilder
 * @var $model ConfigurationModel
 * @var $element string
 */
use backend\components\FormBuilder;
use common\components\model\Translateable;
use backend\modules\configuration\components\ConfigurationModel;
use backend\modules\configuration\models\Configurator;
use common\helpers\LanguageHelper;

/** @var Configurator[] $values */
$values = $model->getModels();

$content = null;
foreach ($values as $value) {
    if ($value->id == $element) {
        $language = $value instanceof Translateable ? '[language: ' . LanguageHelper::getCurrent()->code . ']' : null;
        $configuration = $value->getValueFieldConfig();
        $slug = \yii\helpers\Inflector::slug($value->id);

        $configuration['label'] = $model->showAsConfig ?
            $value->description . ' [key: ' . $value->id . ']' . $language
            : $value->description;
        if (!is_array($value->type) && $language && $model->isTranslateAttribute($value->id)) {
            $attribute = '[' . $value->id . ']value';

            ?>
            <div class="nav-tabs-custom tab-primary">
                <ul class="nav nav-tabs language_tabs">
                    <label class="switch">
                        <input type="checkbox">
                        <div class="swiper"></div>
                    </label>
                    <li class="active">
                        <a href="#test-tab_<?= $slug ?>"
                           data-toggle="tab"><?= $value->description ?></a>
                    </li>

                    <?php foreach ($value->getTranslationModels() as $languageModel) { ?>
                        <li>
                            <a href="#test-tab_<?= $languageModel->language . '_' . $slug ?>"
                               data-toggle="tab"><?= $languageModel->language ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="tab-content">
                <div id="test-tab_<?= $slug ?>" class="tab-pane active">
                    <?= $form->renderField($value, $attribute, $configuration)->label($value->description, ['style' => 'display:none;']); ?>
                    <?= $form->renderUploadedFile($value, 'value', $configuration); ?>
                </div>
                <?php foreach ($value->getTranslationModels() as $languageModel) { ?>
                    <div id="test-tab_<?= $languageModel->language . '_' . $slug ?>" class="tab-pane">
                        <?= $form->renderField($languageModel, '[' . $languageModel->language . ']' . $attribute, $configuration)->label($value->description . ' [' . $languageModel->language . ']', ['style' => 'display:none;']); ?>
                        <?= $form->renderUploadedFile($languageModel, 'value', $configuration, $languageModel->language); ?>
                    </div>
                <?php } ?>
            </div>
            <?php
        } else {
            $attribute = '[' . $value->id . ']value';
            $content .= $form->renderField($value, $attribute, $configuration);
            $content .= $form->renderUploadedFile($value, 'value', $configuration);
            echo $content;
        }
    }
}