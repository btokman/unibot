<?php
/**
 * Author: metal
 * Email: metal
 */

namespace backend\modules\configuration\components;

use backend\components\Model;
use backend\modules\configuration\models\Configuration;
use backend\modules\configuration\models\Configurator;
use metalguardian\fileProcessor\helpers\FPM;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Class ConfigurationModel
 *
 * @package backend\modules\configuration\models
 */
abstract class  ConfigurationModel extends Model
{
    public $showAsConfig = false;

    protected $models;

    /**
     * @event triggered after transaction commit
     */
    const EVENT_AFTER_SAVE = 'afterSave';

    const BEFORE_SAVE = 'beforeSave';
    const AFTER_FIND = 'afterFind';

    public function init()
    {
        parent::init();
        $this->on(self::BEFORE_SAVE, [$this, self::BEFORE_SAVE]);
        $this->on(self::AFTER_FIND, [$this, self::AFTER_FIND]);
        $seo = $this->getBehavior('seo');
        if ($seo && $seo instanceof \notgosu\yii2\modules\metaTag\components\MetaTagBehavior) {
            $this->attachValidator();
        }
    }


    /**
     * Before save event
     * @return bool
     */
    public function beforeSave()
    {
        return true;
    }

    /**
     * After find event
     * @return bool
     */
    public function afterFind()
    {
        return true;
    }

    public function getFieldsOptions()
    {
        return [];
    }


    public function getListItems()
    {
        return [];
    }

    /**
     * Save configuration models
     *
     * @return bool
     * @throws \yii\db\Exception
     */
    public function save()
    {
        $this->trigger(self::BEFORE_SAVE);
        $transaction = \Yii::$app->getDb()->beginTransaction();
        $saved = true;

        $models = $this->getModels();

        foreach ($models as $item) {
            if (is_array($item->type)) {
                continue;
            }
            if ($item->type == Configurator::TYPE_FILE) {
                $this->uploadFile($item);
            }

            $saved &= $item->save();
        }

        if (!$saved) {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        $this->trigger(self::EVENT_AFTER_SAVE);

        $seo = $this->getBehavior('seo');
        if ($seo && $seo instanceof \notgosu\yii2\modules\metaTag\components\MetaTagBehavior) {
            $modelName = $this->formName();

            $data = \Yii::$app->request->post($modelName);

            $this->metaTags = ArrayHelper::getValue($data, 'metaTags');

            $this->saveMetaTags();
        }

        return true;
    }

    /**
     * Title of the form
     *
     * @return string
     */
    abstract public function getTitle();

    /**
     * @return Configurator[]
     */
    public function getModels()
    {
        $types = $this->getFormTypes();
        $descriptions = $this->getFormDescriptions();
        $hints = $this->getFormHints();
        $rules = $this->getFormRules();

        if (null === $this->models) {
            $models = [];
            $keys = array_keys($types);
            foreach ($keys as $key) {
                $model = Configurator::findOne($key);
                if (!$model) {
                    // create model if it is not created yet
                    $model = new Configurator();
                    $model->id = $key;
                    $model->preload = 0;
                    $model->published = 1;
                    $model->show = 0;
                }
                $model->description = isset($descriptions[$key]) ? $descriptions[$key] : null;
                $model->hint = isset($hints[$key])  ? $hints[$key] : null;
                $model->type = ArrayHelper::getValue($types, $key, Configurator::TYPE_STRING);
                if (isset($this->getFieldsOptions()[$key])) {
                    $model->options = $this->getFieldsOptions()[$key];
                }
                if (isset($this->getListItems()[$key])) {
                    $model->items = $this->getListItems()[$key];
                }
                foreach ($rules as $rule) {
                    if ($rule[0] == $model->id) {
                        $rule[0] = 'value';
                        $model->rules[] = $rule;
                    }
                }

                $models[$key] = $model;
            }

            $this->models = $models;
            $this->trigger(self::AFTER_FIND);
        }

        $seo = $this->getBehavior('seo');
        if ($seo && $seo instanceof \notgosu\yii2\modules\metaTag\components\MetaTagBehavior) {
            $this->loadMetaTags();
        }

        return $this->models;
    }

    /**
     * Updated method to load configuration language models
     *
     * @inheritdoc
     */
    public static function loadMultiple($models, $data, $formName = null, $key = '')
    {
        if ($formName === null) {
            /* @var $first Model */
            $first = reset($models);
            if ($first === false) {
                return false;
            }
            $formName = $first->formName();
        }

        $success = false;
        foreach ($models as $i => $model) {
            /* @var $model Model */
            if ($formName == '') {
                if (!empty($data[$i])) {
                    $model->load($data[$i], '');
                    $success = true;
                }
            } elseif (!empty($data[$formName][$i])) {
                $model->load($data[$formName][$i], $key);
                $success = true;
            }
        }

        return $success;
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [];

        /*$models = $this->getModels();

        foreach ($models as $model) {
            $config["[{$model->id}]value"] = $model->getValueFieldConfig();
        }*/

        return $config;
    }

    /**
     * @return array
     */
    public function getFormRules()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getFormTypes()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getFormDescriptions()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getFormHints()
    {
        return [];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [];
    }

    /**
     * @return array
     */
    //abstract public function getUpdateUrl();

    /**
     * @param Configuration $item
     */
    protected function uploadFile(Configuration &$item)
    {
        $files = UploadedFile::getInstances($item, $item->id);
        $file = ArrayHelper::getValue($files, 0);

        if ($file) {
            $item->value = FPM::transfer()->saveUploadedFile($file);
        }

        foreach ($item->getTranslationModels() as $languageModel) {
            $files = UploadedFile::getInstances($languageModel, $languageModel->language . '[' . $item->id . ']');
            $file = ArrayHelper::getValue($files, 0);

            if ($file) {
                $languageModel->value = (string)FPM::transfer()->saveUploadedFile($file);
            }
        }
    }

    public function getId()
    {
        return 1;
    }

    public function getIsNewRecord()
    {
        return false;
    }

    /**
     * @param string $attribute
     *
     * @return bool
     */
    public function isTranslateAttribute($attribute)
    {
        $position = strpos($attribute, ']');
        $attributeName = $position > 0 ? substr($attribute, $position + 1) : $attribute;

        return in_array($attributeName, $this->getTranslationAttributes(), true);
    }
}
