<?php
namespace backend\widgets;

use common\helpers\LanguageHelper;
use yii\helpers\ArrayHelper;

/**
 * Class MetaWidget
 */
class MetaWidget extends \yii\base\Widget
{

    /**
     * @var \yii\db\ActiveRecord
     */
    public $model;

    /**
     * @return null|string
     */
    public function run()
    {
        /**
         * @var \notgosu\yii2\modules\metaTag\components\MetaTagBehavior $behavior
         */
        $behavior = $this->model->getBehavior('seo');
        if (!$behavior) {
            return null;
        }

        $languageList = $behavior->languages;

        $resultData = [];
        $firstItem = [];

        foreach ($this->model->metaTags as $i => $data) {
            if ($data->language === LanguageHelper::getDefaultLanguage()->code) {
                $firstItem[$data->language][$i] = $data;
            }
            $resultData[$data->language][$i] = $data;
        }

        $resultData = ArrayHelper::merge($firstItem, $resultData);

        return $this->render('default', [
            'model' => $this->model,
            'languageList' => $languageList,
            'resultData' => $resultData
        ]);
    }
}
