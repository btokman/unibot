<?php
/**
 * Author: Pavel Naumenko
 *
 * @var MetaTagContent[] $metaTagModelList
 * @var \yii\db\ActiveRecord $model
 * @var array $languageList
 * @var array $resultData array
 */
use notgosu\yii2\modules\metaTag\models\MetaTagContent;
use yii\helpers\Html;

?>
<div class="nav-tabs-custom navy tab-primary">
    <ul class="nav nav-tabs  language_tabs">
        <label class="switch">
            <input type="checkbox">
            <div class="swiper"></div>
        </label>
        <?php foreach ($resultData as $lang => $someData) :
            ?>
            <li class="<?= $someData === reset($resultData) ? 'active' : '' ?>">
                <a href="#tab-lang-content_<?= $lang ?>"
                   data-toggle="tab">    <?= strtoupper($lang) ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<div class="tab-content">
    <?php foreach ($resultData as $lang => $arData) : ?>
        <div id="tab-lang-content_<?= $lang ?>" class="tab-pane <?= $arData === reset($resultData) ? 'active' : '' ?>">
            <?php foreach ($arData as $i => $data) {
                echo Html::beginTag('div', ['class' => 'form-group']);

                echo Html::activeLabel(
                    $model,
                    'metaTags[' . $i . '][content]',
                    ['class' => 'control-label', 'label' => ucfirst($data->metaTag->name)]
                );
                echo Html::activeTextInput($model, 'metaTags[' . $i . '][content]', ['class' => 'form-control']);


                echo Html::endTag('div');
            }
            ?>
        </div>
    <?php endforeach; ?>

</div>


