<?php if ($state) :?>
    <div class="nav-tabs-custom navy tab-primary">
        <ul class="nav nav-tabs language_tabs">
            <?= $this->render('_swicher') ?>
            <?php foreach ($attributes as $key => $value): ?>
                <?php if ($key == $locale) : ?>
                    <li class="active">
                        <a href="#test-tab<?= $value ?>"
                           data-toggle="tab"><?= str_replace('_', ' ', $attributeLabel) ?></a>
                    </li>
                <?php else: ?>
                    <li>
                        <a href="#test-tab<?= $value ?>"
                           data-toggle="tab"><?= $key ?></a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>



<div class="tab-content">
    <?php foreach ($attributes as $key => $value): ?>
        <div id="test-tab<?= $value ?>" class=" tab-pane <?= ($key == $locale) ? 'active' : '' ?>">
            <input style="opacity: 0;" type="file" id="<?= $value . '_' . $index ?>" name="<?php

            echo $modelName . '[' . $value . ']';

            ?>" accept="<?= $extensions ?>">
        </div>
    <?php endforeach; ?>
</div>


