<?php
namespace backend\widgets;

use yii\base\Widget;
use kartik\file\FileInputAsset;
use metalguardian\fileProcessor\helpers\FPM;
use common\{
    components\model\ActiveRecord,
    helpers\LanguageHelper,
    models\EntityToFile
};
use yii\helpers\{
    ArrayHelper,
    Url,
    StringHelper,
    Json,
    Html
};

/**
 * Class FileUploader
 * @package backend\widgets\FileUpload
 */
class FileUploader extends Widget
{

    const DELETE_IMAGE_URL = '/imagesUpload/default/delete-image';
    const UPLOAD_IMAGE_URL = '/imagesUpload/default/upload-image';
    /**
     * @var ActiveRecord $model
     */
    public $model;

    /**
     * @var bool Multilang flag
     */
    public $multiLang = false;

    /**
     * @var string label for Tabs
     */

    public $attributeLabel;

    /**
     * @var string saveAttribute
     */
    public $saveAttribute = null;


    public $attribute;

    /**
     * Allowed file extensions
     * @var array
     */
    public $allowedFileExtensions = [];


    public function run()
    {
        FileInputAsset::register($this->view);
        return $this->getPageContent();
    }


    private function getPageContent()
    {
        $existModelImages = $this->collectExistingImages();
        $previewData = $this->getPreviewData($existModelImages);
        $content = $this->getContent();
        $this->getView()->registerJs($this->createScript($previewData['initialPreview'], $previewData['initialPreviewConfig']));
        return $content;
    }


    /**
     * Get preview config
     *
     * @param $existModelImages
     * @return array
     */


    private function getPreviewData($existModelImages)
    {
        $initialPreview = [];
        $initialPreviewConfig = [];
        foreach ($existModelImages as $file) {
            $fileName = $file->file->base_name . '.' . $file->file->extension;
            $initialPreview[$file->attribute] = $this->getPreviewContent($file);
            $url = Url::toRoute(ArrayHelper::merge([self::DELETE_IMAGE_URL], ['id' => $file->id]));
            $initialPreviewConfig[$file->attribute] = [
                'caption' => $fileName,
                'width' => '120px',
                'url' => $url,
                'key' => $file->id,
            ];
        }
        return ['initialPreview' => $initialPreview, 'initialPreviewConfig' => $initialPreviewConfig];
    }


    /**
     * Get html content
     *
     * @return string
     */

    private function getContent()
    {
        $index = $this->model->relModelIndex ?? \Yii::$app->request->post('index') ?? \Yii::$app->request->post('key');
        $defaultLocate = LanguageHelper::getDefaultLanguage()->locale;
        $modelName = StringHelper::basename(($this->model)::className());
        $inputExtensions = $this->getInputExtensions();
        $attributeLabel = $this->attributeLabel ?? $this->saveAttribute;
        $viewData = [
            'state' => $this->multiLang,
            'attributes' => $this->getLangAttributes(),
            'locale' => $defaultLocate,
            'modelName' => $modelName,
            'index' => $index,
            'extensions' => $inputExtensions,
            'attributeLabel' => $attributeLabel];

        if (isset($this->model->sign)) {
            $viewData['sign'] = $this->model->sign;
        }
        return $this->render('_file_input', $viewData);
    }

    /**
     * Get lang attributes for input
     *
     * @return array
     */

    private function getLangAttributes()
    {
        $attributes = [];
        $defaultLocate = LanguageHelper::getDefaultLanguage()->locale;
        $langModels = LanguageHelper::getLanguageModels();
        $attributes[$defaultLocate] = $this->saveAttribute;
        if (!$this->multiLang) return $attributes;
        foreach ($langModels as $languageModel) {
            if ($defaultLocate !== $languageModel->locale) {
                $attributes[$languageModel->locale] = $this->createAttribute($this->saveAttribute, $languageModel->locale);
            }
        }
        return $attributes;
    }

    /**
     * Get string for input accept tag
     *
     * @return string
     */

    private function getInputExtensions()
    {
        $string = '';
        foreach ($this->allowedFileExtensions as $extension) {
            $string .= '.' . $extension;
            if (next($this->allowedFileExtensions) == true) $string .= ",";
        }
        return $string;
    }


    /**
     * Get language keys
     *
     * @return array
     */


    private function getLocaleKeys()
    {
        $defaultLocate = LanguageHelper::getDefaultLanguage()->locale;
        $langModels = LanguageHelper::getLanguageModels();
        $locale = [];
        foreach ($langModels as $languageModel) {
            if ($defaultLocate !== $languageModel->locale) {
                $locale[] = $languageModel->locale;
            }
        }
        return $locale;
    }


    /**
     *  Create JavaScript for file input
     *  http://plugins.krajee.com/file-input
     *
     * @param $initialPreview
     * @param $initialPreviewConfig
     * @return string
     */


    private function createScript($initialPreview, $initialPreviewConfig)
    {
        $index = $this->model->relModelIndex ?? \Yii::$app->request->post('index') ?? \Yii::$app->request->post('key');
        $attributes = $this->getLangAttributes();
        $extension = Json::encode($this->allowedFileExtensions);
        $previewTemplates = $this->getPreviewTemplates();
        $previewSetting = $this->getPreviewSettings();
        $js = '';
        $uploadExtraData = $this->model->isNewRecord
            ? ['sign' => $this->model->sign]
            : ['id' => $this->model->id];
        $uploadExtraData = Json::encode($uploadExtraData);
        foreach ($attributes as $key => $value) {
            $attribute = $index == 'noIndex' ? $value : '[' . $index . ']' . $value;
            $url = Url::toRoute(ArrayHelper::merge([self::UPLOAD_IMAGE_URL], [
                'model_name' => $this->model->className(),
                'attribute' => $attribute,
                'entity_attribute' => $value,
            ]));

            $preview = $initialPreview[$value] ?? '';
            $previewConfig = $initialPreviewConfig[$value] ?? '';
            is_array($preview) ? $preview = Json::encode($preview) : '';
            is_array($previewConfig) ? $previewConfig = Json::encode($previewConfig) : '';
            $selector = $value . '_' . $index;
            $browseLabel = \Yii::t('fileinput', 'Browse') . '&hellip;';
            $uploadLabel = \Yii::t('fileinput', 'Upload');
            $removeLabel = \ Yii::t('fileinput', 'Remove');
            $js .= <<<JS
            $('#$selector').fileinput({
                uploadUrl : '$url',
                dropZoneEnabled : false,
                browseLabel : '$browseLabel',
                uploadLabel : '$uploadLabel',
                removeLabel : '$removeLabel',
                browseClass : 'btn btn-success bg-black btn-flat',
                allowedFileExtensions : $extension,
                initialPreview :[$preview],
                initialPreviewConfig : [$previewConfig] ,
                overwriteInitial:false,
                browseIcon : '<i class="glyphicon glyphicon-picture"></i> ',
                removeClass : "btn btn-danger",
                removeIcon  :'<i class="glyphicon glyphicon-trash text-danger"></i> ',
                uploadClass : 'btn btn-primary btn-flat',
                showRemove: false,   
                uploadExtraData : $uploadExtraData,
                previewSettings : $previewSetting,
                previewTemplates : $previewTemplates,
                allowedPreviewTypes : ['image', 'video'],
                showUploadedThumbs : false
            });
             $('#$selector').on('filebrowse',function(file, previewId, index, reader) {
                var buttonId = file.target.id;
                var filePreview = $('#' + buttonId).parents('.file-input');
                var isImage = filePreview.find('.file-preview-image').length;
                if (isImage) {
                    event.preventDefault();
                    alert('Можно загрузить только одно изображение! Для загрузки нового, удалите первое.');
                }  
              });  
JS;
        }
        return $js;
    }


    /**
     * Create attribute for language file
     *
     * @param $attribute - attribute name
     * @param $locale - locate name
     * @return string
     */


    private function createAttribute($attribute, $locale)
    {
        return $attribute . '_' . $locale;
    }


    /*
     * Get existing model images
     */


    protected function collectExistingImages()
    {
        $query = ['OR'];
        $existModelImages = EntityToFile::find()->where('entity_model_name = :emn', [':emn' => $this->model->formName()]);
        $existModelImages = $this->model->isNewRecord
            ? $existModelImages->andWhere('temp_sign = :ts', [':ts' => $this->model->sign])
            : $existModelImages->andWhere('entity_model_id = :id', [':id' => $this->model->id]);
        if ($this->saveAttribute !== null) {
            $attributes = $this->getLocaleKeys();
            array_push($query, ['attribute' => $this->saveAttribute]);
            foreach ($attributes as $locale) {
                array_push($query, ['attribute' => $this->saveAttribute . '_' . $locale]);
            }
        }

        $existModelImages->andFilterWhere($query);
        return $existModelImages->all();
    }


    /**
     * @param EntityToFile $file
     * @return null|string
     */
    protected function getPreviewContent(EntityToFile $file)
    {
        $imagesExtensions = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'tiff', 'exif'];
        $videoExtensions = ['mp4', 'webm', 'mov', 'ogv', 'ogg', 'mkv'];
        $result = null;
        if (in_array($file->file->extension, $imagesExtensions)) {
            $result = Html::img(FPM::originalSrc($file->file_id), [
                'class' => 'file-preview-image',
                'id' => 'preview-image-' . $file->file_id
            ]);
        } else if (in_array($file->file->extension, $videoExtensions)) {
            $src = Html::tag('source', null, [
                'src' => FPM::originalSrc($file->file_id)
            ]);
            $result = Html::tag('video', $src, [
                'class' => 'file-preview-video',
                'id' => 'preview-video-' . $file->file_id,
                'width' => '200px',
                'controls' => true,
            ]);
        } else {
            $result = Html::tag('i', null, [
                'class' => 'glyphicon glyphicon-file',
                'id' => 'preview-image-' . $file->file_id
            ]);
            $result = Html::tag('div', $result, [
                'class' => 'file-preview-other',
            ]);
        }
        return [$result];
    }

    /**
     * @return string JavaScrip object
     */
    protected function getPreviewSettings()
    {
        return Json::encode([
            'video' => [
                'width' => '200px',
                'height' => 'auto',
            ],
            'image' => [
                'width' => '200px',
                'height' => 'auto',
            ],
            'other' => [
                'width' => '200px',
                'height' => '220px',
            ]
        ]);
    }

    /**
     * @return string JavaScript Object
     */
    protected function getPreviewTemplates()
    {
        return Json::encode([
            'video' => '<div class="file-preview-frame{frameClass}" id="{previewId}" data-fileindex="init_{fileindex}"
                             title="{caption}">
                               <video width="{width}" height="{height}" controls>
                                   <source src="{data}" type="{type}">
                               </video>
                               {footer}
                            </div>',
            'image' => '<div class="file-preview-frame{frameClass}" id="{previewId}" data-fileindex="init_{fileindex}">
                            <img src="{data}" class="file-preview-image" title="{caption}" alt="{caption}" >
                            {footer}
                            </div>',
            'other' => '<div class="file-preview-frame{frameClass}" id="{previewId}" data-fileindex="init_{fileindex}"
                            title="{caption}">
                   <div class="file-preview-other"><i id="preview-image-83" class="glyphicon glyphicon-file"></i></div>
                           {footer}
                            </div>'
        ]);
    }
}
