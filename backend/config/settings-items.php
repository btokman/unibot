<?php
return [
    [
        'label' => Yii::t('back/menu', 'User settings'),
        'icon' => 'glyphicon-cog',
        'items' => require 'right-bar-menu-items.php'
    ],
    [
        'label' => Yii::t('back/menu', 'I18n'),
        'icon' => 'glyphicon-globe',
        'items' => [
            [
                'label' => Yii::t('back/menu', 'Translations'),
                'url' => ['/i18n/default/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'Languages'),
                'url' => ['/language/language/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('back/menu','Logout'),
        'icon' => 'glyphicon-off',
        'url' => ['/admin/default/logout'],
    ]


];