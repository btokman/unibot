<?php

return [
    [
        'label' => Yii::t('back/menu', 'Requests'),
        'items' => [
            [
                'label' => Yii::t('back/menu', 'Settings'),
                'url' => ['/requests/form-settings/update']
            ],
            [
                'label' => Yii::t('back/menu', 'Requests'),
                'url' => ['/requests/requests/index']
            ]
        ],
        'icon' => 'glyphicon-flash'
    ],
    [
        'label' => Yii::t('back/menu', 'Emails'),
        'url' => ['/requests/bot-emails/index'],
        'icon' => 'glyphicon-flash'
    ]
];