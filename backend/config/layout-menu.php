<?php

return [
    [
        'label' => Yii::t('back/menu', 'Menu'),
        'url' => ['/common/menu-elements/index'],
        'icon' => 'glyphicon-option-horizontal'
    ],
    [
        'label' => Yii::t('back/menu', 'Layout'),
        'url' => ['/common/footer-settings/update'],
        'icon' => 'glyphicon-tasks'
    ],
    [
        'label' => Yii::t('back/menu', 'Page 404'),
        'url' => ['/common/not-found-page/update'],
        'icon' => 'glyphicon-exclamation-sign'
    ]
];