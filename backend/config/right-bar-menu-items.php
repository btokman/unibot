<?php

return [
    [
        'label' => 'Robots.txt',
        'url' => ['/seo/robots/index'],
    ],
    [
        'label' => 'Redirects',
        'url' => ['/redirects/redirects/index'],
    ],
];
