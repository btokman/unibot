<?php

return [
	[
		'label' => Yii::t('back/menu', 'Home'),
		'icon' => 'glyphicon-home',
		'url' => ['/home/home-page/update']
	],
    [
        'label' => Yii::t('back/menu', 'Contacts'),
        'icon' => 'glyphicon-envelope',
        'url' => ['/contacts/contacts-page/update']
    ],
    [
        'label' => Yii::t('back/menu', 'Cases'),
        'icon' => 'glyphicon-briefcase',
        'url' => ['/home/cases/index']
    ],
	[
		'label' => Yii::t('back/menu', 'Bot'),
		'icon' => 'glyphicon-education',
		'items' => [
            [
                'label' => Yii::t('back/menu', 'Settings'),
                'url' => ['/home/bot-settings/update']
            ],
			[
				'label' => Yii::t('back/menu', 'Questions'),
				'url' => ['/home/bot-questions/index']
			],
			[
				'label' => Yii::t('back/menu', 'Answers'),
				'url' => ['/home/bot-answer/index']
			]
		]
	],
	[
		'label' => Yii::t('back/menu', 'News'),
		'icon' => 'glyphicon-list-alt',
        'items' => [
            [
                'label' => Yii::t('back/menu', 'Settings'),
                'url' => ['/news/news-page/update']
            ],
            [
                'label' => Yii::t('back/menu', 'Articles'),
                'url' => ['/news/news/index']
            ],
            [
                'label' => Yii::t('back/menu', 'Tags'),
                'url' => ['/news/tags/index']
            ]
        ]
	]


];
