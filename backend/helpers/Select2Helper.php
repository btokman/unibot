<?php

namespace backend\helpers;


use kartik\widgets\Select2;
use yii\web\JsExpression;

/**
 * Class Select2Helper
 * @package backend\helpers
 */
class Select2Helper
{
    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     * @param string $dataUrl
     * @param array $selectedData
     * @param array $options
     * @return string
     */
    public static function getAjaxSelect2Widget(
        \yii\base\Model $model,
        string $attribute,
        string $dataUrl,
        $selectedData = [],
        $options = []
    ) {
        return Select2::widget(
            [
                'model' => $model,
                'attribute' => $attribute,
                'data' => $selectedData,
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 0,
                    'ajax' => [
                        'url' => $dataUrl,
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                        'cache' => true
                    ],
                ],
                'options' => $options
            ]
        );
    }
}