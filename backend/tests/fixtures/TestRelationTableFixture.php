<?php
namespace backend\tests\fixtures;

use yii\test\ActiveFixture;

/**
 * Fixture for [[\tests\fake\models\TestRelationTable]] model
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class TestRelationTableFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'backend\tests\fake\models\TestRelationTable';
    /**
     * @inheritdoc
     */
    public $dataFile = '@data/testRelationTable.php';
}
