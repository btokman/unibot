<?php
namespace backend\tests\fixtures;

use yii\test\ActiveFixture;

/**
 * Fixture for [[\tests\fake\models\TestTable]] model
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class TestTableFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'backend\tests\fake\models\TestTable';
    /**
     * @inheritdoc
     */
    public $dataFile = '@data/testTable.php';
}
