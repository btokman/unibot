<?php
namespace backend\tests\fixtures;

use yii\test\ActiveFixture;

/**
 * Fixture for [[\backend\modules\seo\models\Robots]] model
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class RobotsFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'backend\modules\seo\models\Robots';
    /**
     * @inheritdoc
     */
    public $dataFile = '@data/robots.php';
}
