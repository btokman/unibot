<?php
namespace backend\tests\fixtures;

use yii\test\ActiveFixture;

/**
 * Fixture for [[\backend\modules\configuration\models\Configurator]] model
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class ConfiguratorFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'backend\modules\configuration\models\Configurator';
    /**
     * @inheritdoc
     */
    public $dataFile = '@data/configurator.php';
}
