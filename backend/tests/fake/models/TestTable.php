<?php
namespace backend\tests\fake\models;

use backend\components\TranslateableTrait;
use common\components\model\ActiveRecord;
use creocoder\translateable\TranslateableBehavior;

/**
 * Active record model for `test_table`
 *
 * @property integer $id
 * @property string $test_field
 *
 * @property TestRelationTable[] $thisIsTestRelation
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class TestTable extends ActiveRecord
{
    use TranslateableTrait;

    /**
     * Field for upload behaviour test
     *
     * @var mixed
     */
    public $file;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_table';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['test_field'],
                'translationRelation' => 'thisIsTestRelation'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_field'], 'required'],
            [['test_field'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThisIsTestRelation()
    {
        return $this->hasMany(TestRelationTable::className(), ['rel_table_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function getRelatedFormConfig()
    {
        return [
            [
                'relation' => 'thisIsTestRelation',
            ],
        ];
    }
}
