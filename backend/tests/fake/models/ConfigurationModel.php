<?php
namespace backend\tests\fake\models;

use backend\modules\configuration\models\Configurator;

/**
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class ConfigurationModel extends \backend\modules\configuration\components\ConfigurationModel
{
    const FIELD_TEST = 'test';

    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getFormRules()
    {
        return [
            [self::FIELD_TEST, 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFormTypes()
    {
        return [self::FIELD_TEST => Configurator::TYPE_STRING];
    }

    /**
     * @inheritdoc
     */
    public function getFormDescriptions()
    {
        return [self::FIELD_TEST => 'This is test field'];
    }

    /**
     * @inheritdoc
     */
    public function getFormHints()
    {
        return [self::FIELD_TEST => 'This is test field'];
    }

    /**
     * @inheritdoc
     */
    public static function getTranslationAttributes()
    {
        return [self::FIELD_TEST];
    }
}
