<?php
namespace backend\tests\fake\controllers;

use backend\tests\fake\models\TestTable;

/**
 * Fake backend controller
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class BackendController extends \backend\components\BackendController
{
    /**
     * @inheritdoc
     */
    public function getModelClass()
    {
        return TestTable::className();
    }

    /**
     * @inheritdoc
     */
    public function findModel($id)
    {
        return parent::findModel($id);
    }
}
