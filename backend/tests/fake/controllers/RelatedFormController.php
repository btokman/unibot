<?php
namespace backend\tests\fake\controllers;

use backend\components\RelatedFormTrait;
use backend\tests\fake\models\TestTableRelatedForm;

/**
 * Fake related form action
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class RelatedFormController extends \backend\components\BackendController
{
    use RelatedFormTrait;

    /**
     * @inheritdoc
     */
    public function getModelClass()
    {
        return TestTableRelatedForm::className();
    }
}
