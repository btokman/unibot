<?php
return [
    [
        'id' => 1,
        'rel_table_id' => 1,
        'language' => 'en-US',
        'test_data_field' => 'test english translation'
    ],
    [
        'id' => 2,
        'rel_table_id' => 1,
        'language' => 'uk-UA',
        'test_data_field' => 'test ukrainian translation'
    ],
    [
        'id' => 3,
        'rel_table_id' => 1,
        'language' => 'ru-RU',
        'test_data_field' => 'test russian translation'
    ],
    [
        'id' => 4,
        'rel_table_id' => 2,
        'language' => 'en-US',
        'test_data_field' => 'test 2 english translation'
    ],
    [
        'id' => 5,
        'rel_table_id' => 2,
        'language' => 'uk-UA',
        'test_data_field' => 'test 2 ukrainian translation'
    ],
    [
        'id' => 6,
        'rel_table_id' => 2,
        'language' => 'ru-RU',
        'test_data_field' => 'test 2 russian translation'
    ],
];
