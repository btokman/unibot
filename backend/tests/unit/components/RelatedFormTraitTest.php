<?php
namespace backend\tests\unit\components;

use Yii;
use yii\base\Action;
use yii\web\Response;
use common\tests\base\unit\DbTestCase;
use backend\tests\fake\models\TestTableRelatedForm;
use backend\tests\fake\controllers\RelatedFormController;
use backend\tests\fake\models\TestRelationTable;
use backend\tests\fixtures\TestTableFixture;
use backend\tests\fixtures\TestRelationTableFixture;
use metalguardian\fileProcessor\behaviors\UploadBehavior;

/**
 * Test case for [[backend\components\RelatedFormTrait]]
 * @see \backend\components\RelatedFormTrait
 *
 * @property \backend\tests\UnitTester $tester
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class RelatedFormTraitTest extends DbTestCase
{
    const FLASH_MESSAGE_KEY = 'info';

    /**
     * @var RelatedFormController
     */
    private $controller;
    /**
     * @var TestTableRelatedForm
     */
    private $model;


    /**
     * @inheritdoc
     */
    public function fixtures()
    {
       return [
           'testTable' => ['class' => TestTableFixture::className()],
           'testRelationTable' => ['class' => TestRelationTableFixture::className()],
       ];
    }

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->model = TestTableRelatedForm::findOne(1);
        $this->controller = new RelatedFormController('test', Yii::$app);
        Yii::$app->controller = $this->controller;
        Yii::$app->setViewPath('@tests/fake/views');
    }

    /**
     * Set action for controller
     *
     * @param string $action
     */
    private function setAction($action)
    {
        $this->controller->action = new Action($action, $this->controller);
    }

    /**
     * Test data for mock to POST in update action
     *
     * @return array
     */
    private function getDataForUpdate()
    {
        return [
            'TestTableRelatedForm' => [
                ['id' => 1],
            ],
            'TestRelationTable' => [
                [
                    'rel_table_id' => 1,
                    'test_data_field' => 'test related form',
                    'language' => 'en',
                ],
                [
                    'rel_table_id' => 1,
                    'test_data_field' => 'test related form',
                    'language' => 'uk',
                ],
            ],
        ];
    }

    /**
     * Test data for mock to POST in create action
     *
     * @return array
     */
    private function getDataForCreate()
    {
        return [
            'TestTableRelatedForm' => [
                'test_field' => 'test related form'
            ]
        ];
    }

    public function testSetToNewRecordState()
    {
        $this->tester->readRelatedFormConfig($this->model);
        $this->controller->setToNewRecordState($this->model);

        $this->assertTrue($this->model->getIsNewRecord());
        foreach ($this->model->relModels as $relModels) {
            foreach ($relModels as $relModel) {
                $this->assertTrue($relModel->getIsNewRecord());
            }
        }
    }

    public function testGetRelatedFormActionConfig()
    {
        $expected['thisIsTestRelation'] = ['relation' => 'thisIsTestRelation'];
        $actual = $this->controller->getRelatedFormActionConfig($this->model);

        $this->assertEquals($expected, $actual);
        $this->assertNotNull($this->model->relModels);
    }

    public function testRelatedFormActionSuccessUpdate()
    {
        Yii::$app->getRequest()->setData($this->getDataForUpdate());
        $this->setAction('update');
        $config = $this->tester->readRelatedFormConfig($this->model);

        $response = $this->controller->relatedFormAction($this->model, $config);

        $this->assertInstanceOf(Response::className(), $response);
        $this->assertStringEndsWith('view&id=1', $response->headers['location']);
        $this->assertEquals(
            'Record successfully updated!',
            Yii::$app->getSession()->getFlash(self::FLASH_MESSAGE_KEY)
        );
        $this->tester->seeInDb(TestRelationTable::tableName(), [
            'rel_table_id' => 1,
            'test_data_field' => 'test related form',
            'language' => 'en',
        ]);
        $this->tester->seeInDb(TestRelationTable::tableName(), [
            'rel_table_id' => 1,
            'test_data_field' => 'test related form',
            'language' => 'uk',
        ]);
    }

    public function testRelatedFormActionErrorUpdate()
    {
        $this->setAction('update');
        $config = $this->tester->readRelatedFormConfig($this->model);

        $response = $this->controller->relatedFormAction($this->model, $config);

        $this->assertInternalType('string', $response);
        $this->assertEquals('update', $response);
    }

    public function testRelatedFormActionSuccessCreate()
    {
        $model = new TestTableRelatedForm(['test_field' => 'test related form']);
        Yii::$app->getRequest()->setData($this->getDataForCreate());
        $this->setAction('create');
        $config = $this->tester->readRelatedFormConfig($model);

        $response = $this->controller->relatedFormAction($model, $config);

        $this->assertInstanceOf(Response::className(), $response);
        $this->assertStringEndsWith('view&id=3', $response->headers['location']);
        $this->assertEquals(
            'Record successfully created!',
            Yii::$app->getSession()->getFlash(self::FLASH_MESSAGE_KEY)
        );
        $this->tester->seeInDb(TestTableRelatedForm::tableName(), [
            'id' => 3,
            'test_field' => 'test related form',
        ]);
    }

    public function testRelatedFormActionErrorCreate()
    {
        $this->setAction('create');
        $this->model->setIsNewRecord(true);
        $config = $this->tester->readRelatedFormConfig($this->model);

        $response = $this->controller->relatedFormAction($this->model, $config);

        $this->assertInternalType('string', $response);
        $this->assertEquals('create', $response);
    }

    public function testAttachUploadBehavior()
    {
        $attribute = 'file';
        $extensions = [
            'pdf',
            'doc',
            'docx',
            'xls',
            'xlsx',
            'txt',
            'png',
            'gif',
            'jpg',
            'jpeg'
        ];
        $required = false;
        $config['thisIsTestRelation']['uploadBehavior'][] = [
            'attribute' => $attribute,
            'extensions' => $extensions,
            'required' => $required
        ];

        $models = TestRelationTable::find()->all();
        $this->controller->attachUploadBehavior($config, 'thisIsTestRelation', $models);

        foreach ($models as $index => $model) {
            $expected = new UploadBehavior([
                'attribute' => "[$index]$attribute",
                'validator' => [
                    'extensions' => $extensions
                ],
                'required' => $required,
                'owner' => $model
            ]);
            $actual = $model->getBehavior('file');

            $this->assertEquals($expected, $actual);
        }
    }
}
