<?php
namespace backend\tests\unit\modules\admin\controllers;

use Yii;
use yii\web\Response;
use common\tests\base\unit\DbTestCase;
use backend\components\BackendController;
use backend\modules\seo\models\Robots;
use backend\modules\seo\controllers\RobotsController;
use backend\tests\fixtures\RobotsFixture;

/**
 * Test case for [[\backend\modules\seo\controllers\RobotsController]]
 * @see \backend\modules\seo\controllers\RobotsController
 *
 * @property \backend\tests\UnitTester $tester
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class RobotsControllerTest extends DbTestCase
{
    /**
     * @var RobotsController
     */
    private $controller;


    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            RobotsFixture::className() => ['class' => RobotsFixture::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();
        $this->controller = new RobotsController('robots', Yii::$app);
        Yii::$app->controller = $this->controller;
        Yii::$app->setViewPath('@tests/fake/views');
    }

    /**
     * @inheritdoc
     */
    protected function init()
    {

    }

    public function testInstanceOf()
    {
        $this->assertInstanceOf(BackendController::className(), $this->controller);
    }

    public function testGetModelClass()
    {
        $this->assertEquals(Robots::className(), $this->controller->getModelClass());
    }

    public function testActionIndexCreate()
    {
        $response = $this->controller->actionIndex();

        $this->assertInstanceOf(Response::className(), $response);
        $this->assertStringEndsWith('create', $response->headers['location']);
    }

    public function testActionCreateView()
    {
        $response = $this->controller->actionCreate();
        $this->assertInternalType('string', $response);
    }

    public function testActionCreateSuccess()
    {
        Yii::$app->getRequest()->setData([
            'Robots' => ['text' => 'save test']
        ]);
        $response = $this->controller->actionCreate();

        $this->assertInstanceOf(Response::className(), $response);
        $this->assertStringEndsWith('view&id=1', $response->headers['location']);
        $this->tester->seeInDb(Robots::tableName(), [
            'id' => 1,
            'text' => 'save test'
        ]);
    }

    public function testActionIndexUpdate()
    {
        $this->getFixture(RobotsFixture::className())->load();
        $response = $this->controller->actionIndex();

        $this->assertInstanceOf(Response::className(), $response);
        $this->assertStringEndsWith('update&id=1', $response->headers['location']);
    }

    public function testActionCreateExists()
    {
        $this->getFixture(RobotsFixture::className())->load();
        $response = $this->controller->actionCreate();

        $this->assertInstanceOf(Response::className(), $response);
        $this->assertStringEndsWith('update&id=1', $response->headers['location']);
    }

    public function testActionUpdateError()
    {
        $this->getFixture(RobotsFixture::className())->load();
        $response = $this->controller->actionUpdate(1);
        $this->assertInternalType('string', $response);
    }

    public function testTestActionUpdateSuccess()
    {
        $this->getFixture(RobotsFixture::className())->load();
        Yii::$app->getRequest()->setData([
            'Robots' => ['text' => 'update test']
        ]);
        $response = $this->controller->actionUpdate(1);

        $this->assertInstanceOf(Response::className(), $response);
        $this->assertStringEndsWith('update&id=1', $response->headers['location']);
        $this->tester->seeInDb(Robots::tableName(), [
            'id' => 1,
            'text' => 'update test'
        ]);
    }

    public function testActionDelete()
    {
        $this->expectException('yii\web\NotFoundHttpException');
        $this->controller->actionDelete(null);
    }
}
