<?php
namespace backend\tests\unit\modules\configuration\components;

use common\tests\base\unit\DbTestCase;
use backend\components\Model;
use backend\modules\configuration\models\Configurator;
use backend\tests\fake\models\ConfigurationModel;
use backend\tests\fixtures\ConfiguratorFixture;

/**
 * Test case for [[\backend\modules\configuration\components\ConfigurationModel]]
 * @see \backend\modules\configuration\components\ConfigurationModel
 *
 * @property \backend\tests\UnitTester $tester
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class ConfigurationModelTest extends DbTestCase
{
    /**
     * @var ConfigurationModel
     */
    private $model;


    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'configurator' => ['class' => ConfiguratorFixture::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();
        $this->model = new ConfigurationModel();
    }

    /**
     * @inheritdoc
     */
    protected function init()
    {

    }

    public function testInstanceOf()
    {
        $this->assertInstanceOf(Model::className(), $this->model);
    }

    public function testGetIsNewRecord()
    {
        $this->assertFalse($this->model->getIsNewRecord());
    }

    public function testGetModels()
    {
        $modelKey = 'test';
        $configurator = new Configurator([
            'id' => $modelKey,
            'preload' => 0,
            'published' => 1,
            'show' => 0,
            'description' => 'This is test field',
            'hint' => 'This is test field',
            'type' => Configurator::TYPE_STRING
        ]);
        $configurator->rules[] = ['value', 'required'];

        $expected[$modelKey] = $configurator;
        $actual = $this->model->getModels();

        $this->assertEquals($expected, $actual);
    }

    public function testLoadMultiple()
    {
        $models = [
            new Configurator(['id' => 'configurator-1']),
            new Configurator(['id' => 'configurator-2']),
            new Configurator(['id' => 'configurator-3']),
        ];
        $data['Configurator'] = [
            [
                'value' => 'configurator-1',
                'preload' => 0,
                'published' => 1,
                'show' => 1,
            ],
            [
                'value' => 'configurator-2',
                'preload' => 1,
                'published' => 1,
                'show' => 1,
            ],
            [
                'value' => 'configurator-3',
                'preload' => 0,
                'published' => 1,
                'show' => 0,
            ],
        ];

        $this->assertTrue(ConfigurationModel::loadMultiple($models, $data));
    }

    public function testSave()
    {
        $this->loadFixtures();

        $this->assertTrue($this->model->save());
        $this->tester->seeInDb(Configurator::tableName(), [
            'id' => 'test',
            'value' => 'test data'
        ]);
    }
}
