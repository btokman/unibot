<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules\parts;


use backend\components\gii\exportModules\parts\fileGetters\FilesGetter;
use backend\components\gii\exportModules\parts\fileGetters\ModuleDirectoryFilesGetter;
use Yii;

/**
 * Class BackendPartManager
 *
 * @package backend\components\gii\exportModules\parts\fileGetters
 */
class BackendPartManager extends ModulePartManager
{
    /**
     * @return string
     */
    public function getAppModulePartPath()
    {
        return Yii::getAlias('@backend') . '/modules/' . $this->module->id;
    }

    /**
     * @return string
     */
    public function getCoreModulePartPath()
    {
        return 'backend';
    }

    /**
     * @return FilesGetter
     */
    public function getFilesGetter(): FilesGetter
    {
        return new ModuleDirectoryFilesGetter($this);
    }
}
