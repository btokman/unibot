<?php
namespace backend\components\gii\assets;

use yii\web\AssetBundle;

/**
 * Created by anatolii
 */
class GiiAsset extends AssetBundle
{
    public $sourcePath = '@app/components/gii/assets';
    public $css = [
        'main.css',
    ];
    public $js = [
        'gii.js',
    ];
    public $depends = [
        'yii\gii\GiiAsset',
        'yii\jui\JuiAsset'
    ];
}
