<?php
/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator \backend\components\gii\staticPageModel\Generator */
/* @var $behaviors string[] list of behaviors */

echo "<?php\n";
$modelClassName = $generator->modelClassName;
?>

namespace <?= $generator->ns ?>;

use Yii;
use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\<?= $modelClassName ?> as Common<?= $modelClassName ?>;
<?php if ($generator->imageUploaders): ?>
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use metalguardian\formBuilder\ActiveFormBuilder;
<?php endif; ?>
<?php if ($generator->relationsForRelatedFormWidget): ?>
use yii\db\ActiveQueryInterface;
<?php endif; ?>
/**
* Class <?= $modelClassName ?>
*/
class <?= $modelClassName ?> extends ConfigurationModel
{
<?php foreach ($generator->imageUploaders as $uploader): ?>
    /**
    * Attribute for imageUploader
    */
    public $<?= $uploader['attribute'] ?>;

<?php endforeach; ?>
<?php if ($generator->showAsConfig) : ?>
    public $showAsConfig = true;
<?php else: ?>
    public $showAsConfig = false;
<?php endif; ?>
    /**
    * Title of the form
    *
    * @return string
    */
    public function getTitle()
    {
        return <?= $generator->generateString($generator->title) ?>;
    }

    /**
    * @return array
    */
    public function getFormRules()
    {
        return [
<?php foreach ($generator->keys as $key):
if ($key['isRequired']): ?>
            [Common<?= $modelClassName ?>::<?= $generator->formatToConstant($key['id']) ?>, 'required'],
<?php endif;
if ($key['rule']): ?>
            [Common<?= $modelClassName ?>::<?= $generator->formatToConstant($key['id']) ?>, <?= $key['rule'] ?>],
<?php endif;
endforeach; ?>
        ];
    }

    /**
    * @return array
    */
    public function getFormTypes()
    {
        return [
<?php foreach ($generator->keys as $key):
        if ($constant = $generator->getConstantName($key['type'])): ?>
            Common<?= $modelClassName ?>::<?= $generator->formatToConstant($key['id']) ?> => Configuration::<?= $constant ?>,
<?php endif;
    endforeach; ?>
        ];
    }

    /**
    * @return array
    */
    public function getFormDescriptions()
    {
        return [
<?php foreach ($generator->keys as $key): ?>
            Common<?= $modelClassName ?>::<?= $generator->formatToConstant($key['id']) ?> => <?= $generator->generateString($key['description']) ?>,
<?php endforeach; ?>
<?php foreach ($generator->imageUploaders as $uploader): ?>
            <?= "'{$uploader['attribute']}' => " . $generator->generateString($uploader['attributeLabel']) . ",\n" ?>
<?php endforeach; ?>
        ];
    }

    /**
    * @return array
    */
    public function getFormHints()
    {
        return [
<?php foreach ($generator->keys as $key): ?>
            Common<?= $modelClassName ?>::<?= $generator->formatToConstant($key['id']) ?> => <?= !empty($key['hint']) ? $generator->generateString($key['hint']) : "''" ?>,
<?php endforeach; ?>
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
<?php foreach ($generator->keys as $key): ?>
<?php if($key['isTranslatable']) : ?>
            Common<?= $modelClassName ?>::<?= $generator->formatToConstant($key['id']) ?>,
<?php endif; ?>
<?php endforeach; ?>
        ];
    }

<?php if ($behaviors) : ?>
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            <?= implode(",\n            ", $behaviors) . ",\n" ?>
        ]);
    }
<?php endif; ?>

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
<?php foreach ($generator->keys as $key): ?>
                    Common<?= $modelClassName ?>::<?= $generator->formatToConstant($key['id']) ?>,
<?php endforeach; ?>
<?php foreach ($generator->imageUploaders as $uploader): ?>
                    '<?= $uploader['attribute'] ?>' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => '<?= $uploader['attribute'] ?>',
                            'saveAttribute' => Common<?= $modelClassName ?>::<?= \backend\components\gii\migration\Generator::getSaveAttributeConstantName($uploader['attribute']) ?>,
                            'aspectRatio' => <?= $uploader['aspectRatio'] ?: 'false' ?>, //Пропорция для кропа
                            'multiple' => <?= $uploader['multiple'] ? 'true' : 'false' ?>, //Вкл/выкл множественную загрузку
                        ])
                    ],
<?php endforeach; ?>
                ],
<?php foreach ($generator->relationsForRelatedFormWidget as $relation): ?>
                '<?= $relation['tabName'] ?>' => [
                    $this->getRelatedFormConfig()['<?= $relation['relationName'] ?>']
                ],
<?php endforeach; ?>
            ]
        ];

        return $config;
    }
<?php if ($generator->relationsForRelatedFormWidget): ?>

    /**
    * @return array
    */
    public function getRelatedFormConfig()
    {
        $config = [
<?php foreach ($generator->relationsForRelatedFormWidget as $relation): ?>
            '<?= $relation['relationName'] ?>' => [
                'relation' => '<?= $relation['relationName'] ?>',
            ],
<?php endforeach; ?>
        ];

        return $config;
    }

    /**
    * @return ActiveQueryInterface
    */
    public function getRelationName()
    {
        return $this->hasMany(ModelName::className(), ['foreign_key' => 'id'])->orderBy('position');
    }
<?php endif; ?>
}
