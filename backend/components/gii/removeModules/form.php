<?php
/* @var $this yii\web\View */
use backend\components\gii\assets\GiiAsset;
use backend\components\gii\importModules\Generator;

/* @var $form yii\widgets\ActiveForm */
/* @var $generator Generator */

$asset = GiiAsset::register($this);

echo $form->field($generator, 'selectedModules')->checkboxList($generator->coreModuleNames);
?>
<?php if ($generator->log) { ?>
<pre>
<?php
foreach ($generator->log as $log) {
    echo $log . '<br>';
}
?>
</pre>
<?php } ?>


