<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\importModules;

use backend\components\gii\exportModules\AppModule;
use backend\components\gii\exportModules\parts\BackendPartManager;
use backend\components\gii\exportModules\parts\FrontendPartManager;
use Yii;
use yii\gii\CodeFile;
use yii\helpers\ArrayHelper;


/**
 * This generator generates model for static pages
 */
class Generator extends \backend\components\gii\exportModules\Generator
{
    public function init()
    {
        $dirs = array_filter(glob( Yii::getAlias('@root') . '/core_modules/*'), 'is_dir');
        foreach ($dirs as $item) {
            $array = explode('/core_modules/', $item);
            if (count($array) == 2) {
                $this->coreModuleNames[$array[1]] = $array[1];
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Import Modules';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'This generator imports core modules to application.';
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $files = [];
        foreach ($this->selectedModules as $moduleId) {
            $coreModule = new CoreModule($moduleId);
            $filesForImport = $coreModule->getFilesForImport();
            foreach ($filesForImport as $coreModuleFile => $appModuleFile) {
                $files[] = new CodeFile(
                    $appModuleFile,
                    file_get_contents($coreModuleFile)
                );
            }
        }
        $this->selectedModules = array_combine($this->selectedModules, $this->selectedModules);
        $modulesAfterImport = ArrayHelper::merge(AppModule::getAppModuleIds(), $this->selectedModules);

        return $this->getRenderedFiles($modulesAfterImport, $files);
    }

    /**
     * @param array $modules
     * @param array $files
     *
     * @return array
     */
    public function getRenderedFiles($modules, $files)
    {
        $modulesWithFrontend = $modulesWithBackend = $modulesWithMigrations = $modulesWithMenuItems = $modulesWithUrlRules = [];
        foreach ($modules as $moduleId) {
            $coreModule = new CoreModule($moduleId);
            $backendPath = $coreModule->parts[BackendPartManager::class]->getCoreModulePartFullPath();
            if (is_dir($backendPath)) {
                $modulesWithBackend[] = $moduleId;
                if (is_file($backendPath . '/menu-items/menu-items.php')) {
                    $modulesWithMenuItems[] = $moduleId;
                }
            }
            if (is_dir($backendPath . '/migrations')) {
                $modulesWithMigrations[] = $moduleId;
            }
            $frontendPath = $coreModule->parts[FrontendPartManager::class]->getCoreModulePartFullPath();
            if (is_dir($frontendPath)) {
                $modulesWithFrontend[] = $moduleId;
                if (is_file($frontendPath . '/url-rules/url-rules.php')) {
                    $modulesWithUrlRules[] = $moduleId;
                }
            }
        }
        $files[] = new CodeFile(
            Yii::getAlias('@backend') . '/config/core_modules/menu-items.php',
            $this->render('menu-items.php', ['modules' => $modulesWithMenuItems])
        );
        $files[] = new CodeFile(
            Yii::getAlias('@backend') . '/config/core_modules/modules.php',
            $this->render('modules-backend.php', ['modules' => $modulesWithBackend])
        );
        $files[] = new CodeFile(
            Yii::getAlias('@console') . '/config/core_modules/migration-paths.php',
            $this->render('migration-paths.php', ['modules' => $modulesWithMigrations])
        );
        $files[] = new CodeFile(
            Yii::getAlias('@frontend') . '/config/core_modules/modules.php',
            $this->render('modules-frontend.php', ['modules' => $modulesWithFrontend])
        );
        $files[] = new CodeFile(
            Yii::getAlias('@frontend') . '/config/core_modules/url-rules.php',
            $this->render('url-rules.php', ['modules' => $modulesWithUrlRules])
        );

        return $files;
    }
}
