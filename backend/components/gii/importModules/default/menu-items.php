<?php
/**
 * @var $modules array
 */

echo "<?php\n";
?>

return [
    'label' => Yii::t('back/menu', 'modules'),
    'items' => [
<?php foreach ($modules as $moduleId): ?>
        include(Yii::getAlias('@backend') . '/modules/<?= $moduleId ?>/menu-items/menu-items.php'),
<?php endforeach; ?>
    ],
];
