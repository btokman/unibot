<?php
/**
 * @var $modules array
 */

echo "<?php\n";
?>

return [
<?php foreach ($modules as $moduleId): ?>
    '<?= $moduleId ?>' => [
        'class' => 'backend\modules\<?= $moduleId ?>\Module',
    ],
<?php endforeach; ?>
];
