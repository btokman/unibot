<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 12.03.2017
 * Time: 17:09
 */
namespace backend\components;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ManyToManyProductBehavior extends Behavior
{
    public $manyToManyConfig = [];
    public $currentObj;
    public $currentRelatedColumn;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }


    /**
     * @param $linkTableName
     */
    protected function deleteManyToManyItems($linkTableName)
    {
        \Yii::$app->db->createCommand()
            ->delete($linkTableName, [$this->currentRelatedColumn => $this->currentObj->id])
            ->execute();
    }


    /**
     * @param $linkTableName
     * @param $attribute
     * @param $relatedColumn
     */
    protected function saveManyToManyItems($linkTableName, $attribute, $relatedColumn)
    {
        //if save model not from crud
        if ($this->currentObj->{$attribute} === null){
            return;
        }

        $data = [];
        $this->deleteManyToManyItems($linkTableName);

        //if is json
        $related = $this->currentObj->{$attribute};

        if (!is_array($this->currentObj->{$attribute})) {
            if (strpos($this->currentObj->{$attribute}, '{') !== false) {
                $relatedObj = json_decode($this->currentObj->{$attribute});
                foreach ($relatedObj as $key => $value) {
                    $related[] = $key;
                }
            } else {
                $related = explode(',', $this->currentObj->{$attribute});
            }
        }

        foreach ($related as $relId) {
            if ($relId != '') {
                $data[] = [
                    $this->currentRelatedColumn => $this->currentObj->id,
                    $relatedColumn => $relId,
                ];
            }
        }

        if (!empty($data)) {
            \Yii::$app->db->createCommand()
                ->batchInsert(
                    $linkTableName,
                    [
                        $this->currentRelatedColumn,
                        $relatedColumn,
                    ],
                    $data
                )
                ->execute();
        }
    }


    /**
     * @param $attribute
     * @param $valueAttribute
     * @param $relatedColumn
     * @param $linkTableName
     * @param $relatedObjTableName
     * @param bool $isStringReturn
     */
    protected function loadManyToManyItems(
        $attribute,
        $valueAttribute,
        $relatedColumn,
        $linkTableName,
        $relatedObjTableName,
        $isStringReturn = false
    ) {
        $this->currentObj->{$valueAttribute} = ArrayHelper::map(
            (new Query())
                ->select([$relatedColumn, 'label'])
                ->innerJoin($relatedObjTableName, $relatedColumn . ' = ' . $relatedObjTableName . '.id')
                ->from($linkTableName)
                ->where($linkTableName . '.' . $this->currentRelatedColumn . ' = :pid', [':pid' => $this->currentObj->id])
                ->all(),
            $relatedColumn,
            'label'
        );


        if ($isStringReturn) {
            $this->currentObj->{$attribute} = implode(',', array_keys($this->currentObj->{$valueAttribute}));
        } else {
            $this->currentObj->{$attribute} = array_keys($this->currentObj->{$valueAttribute});
        }
    }

    public function afterFind($event)
    {
        foreach ($this->manyToManyConfig as $key => $value) {
            $this->loadManyToManyItems(
                $value['attribute'],
                $value['valueAttribute'],
                $value['relatedColumn'],
                $value['linkTableName'],
                $value['relatedObjTableName'],
                $value['isStringReturn']
            );
        }
    }


    /**
     * @param $event
     */
    public function afterSave($event)
    {
        foreach ($this->manyToManyConfig as $key => $value) {
            $this->saveManyToManyItems(
                $value['linkTableName'],
                $value['attribute'],
                $value['relatedColumn']
            );
        }
    }
}
