<?php
namespace backend\components;

use common\components\model\ActiveRecord;
use yii\validators\Validator;
use yii\db\Query;
use common\models\base\EntityToFile;
use backend\modules\configuration\components\ConfigurationModel;

/**
 * Class FileRequireValidator
 * @package backend\components
 */
class FileRequireValidator extends Validator
{
    /** @var  string model save attribute */
    public $saveAttribute;

    /**
     * @param ActiveRecord | ConfigurationModel $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $label = $model->getAttributeLabel($attribute);

        if ($model instanceof ConfigurationModel) {
            $this->validateConstructorFile($model, $attribute, $label);
            return;
        }

        $this->validateFile($model, $attribute, $label);
    }

    /**
     * @param ActiveRecord $model
     * @param string $attribute
     * @param string $label
     * @return bool|void
     */
    private function validateFile(ActiveRecord $model, $attribute = '', $label = '')
    {
        $data = (new Query())->select('id')->from(EntityToFile::tableName())
            ->where(['AND',
                ['entity_model_name' => $model->formName()],
                ['attribute' => $this->saveAttribute]]);

        $model->id ? $data->andWhere(['entity_model_id' => $model->id]) :
            $data->andWhere(['temp_sign' => $model->sign]);

        if ($data->one()) return true;

        return $this->addError($model, $attribute, $this->getErrorMessage($label));
    }

    /**
     * @param ConfigurationModel $model
     * @param string $attribute
     * @param string $label
     * @return bool|void
     */
    private function validateConstructorFile(ConfigurationModel $model, $attribute = '', $label = '')
    {
        $data = (new Query())->select('id')->from(EntityToFile::tableName())
            ->where(['AND',
                ['entity_model_name' => $model->formName()],
                ['attribute' => $this->saveAttribute]])
            ->one();

        if (!empty($data)) return true;

        return $this->addError($model, $attribute, $this->getErrorMessage($label));
    }

    /**
     * @param $label
     * @return string
     */
    private function getErrorMessage($label)
    {
        return $label . \Yii::t('app', ' cannot be blank');
    }
}
