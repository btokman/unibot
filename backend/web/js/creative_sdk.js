var $document = $(document);
var _currentImage, _image;
$(function () {
    window.csdkImageEditor = new Aviary.Feather({
        apiKey: window.adobeKey,
        onSave: function (imageID, newURL) {
            _currentImage.src = newURL;
            csdkImageEditor.close();
            var file_id = _image.data('id');
            updateImage(newURL, file_id);
        },
        onError: function (errorObj) {
            alert(errorObj.message)
        }
    });
});


$document.on('click', '.adobesdk', function (e) {
    e.preventDefault();
    var $this = $(this);
    _image = $this.parents('.file-preview-frame').find('img');
    if (_image.length && _image.data('id')) {
        _currentImage = _image[0];

        window.csdkImageEditor.launch({
            image: _currentImage.id,
            url: _currentImage.src
        });
    } else {
        alert('Download image first');
    }

});

function updateImage(newURL, file_id) {
    $.ajax({
        type: 'POST',
        url: '/imagesUpload/default/update-image',
        data: {image: file_id, url: newURL},
        dataType: 'json',
    })
}