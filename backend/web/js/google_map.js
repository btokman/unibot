function initMap() {

    var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13
        }),
        selectorLatName = '#configurator-contactspagelat-value',
        selectorLngName = '#configurator-contactspagelng-value',
        input = document.getElementById('configurator-contactspagefilledvalue-value'),
        types = document.getElementById('type-selector'),
        latSelector = $(selectorLatName).length > 0 ? $(selectorLatName) : $('#configurator-contactspagelat-value'),
        lngSelector = $(selectorLngName).length > 0 ? $(selectorLngName) : $('#configurator-contactspagelng-value'),
        autocomplete = new google.maps.places.Autocomplete(input),
        infowindow = new google.maps.InfoWindow(),
        marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        }),
        address = '',
        oldLat = parseFloat(latSelector.val()),
        oldLng = parseFloat(lngSelector.val());
    if (oldLat && oldLng) {
        var coords = {lat: oldLat, lng: oldLng};
        marker = new google.maps.Marker({
            position: coords,
            map: map
        });
        map.setCenter(coords);
        map.setZoom(17);
    }

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

    autocomplete.bindTo('bounds', map);

    autocomplete.addListener('place_changed', function (event) {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace(),
            lat = place.geometry.location.lat(),
            lng = place.geometry.location.lng(),
            latlng = {lat: lat, lng: lng},
            geocoder = new google.maps.Geocoder(),
            postCodeLabel = 'postal_code',
            countryLabel = "country";

        /* Setting values */

        latSelector.val(lat);
        lngSelector.val(lng);
        $('#placeId').val(place.place_id);


        /*  Setting Marker and Data to infoWindow */

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        marker.setIcon(({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
    });


}

initMap();