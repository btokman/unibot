<?php

use console\components\Migration;

/**
 * Class m170125_113214_create_builder_preview_table migration
 */
class m170125_113214_create_builder_preview_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%builder_preview}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'hash' => $this->string()->notNull(),
                'data' => $this->text()->null(),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
