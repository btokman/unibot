<?php

use console\components\Migration;

/**
 * Class m170117_052859_create_builder_widget_attribute_table_translation migration
 */
class m170117_052859_create_builder_widget_attribute_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%builder_widget_attribute_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%builder_widget_attribute}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'value' => $this->text(),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-b_w_a_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-b_w_a_translation-model_id-b_w_a-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

