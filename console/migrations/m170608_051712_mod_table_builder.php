<?php

use console\components\Migration;

/**
 * Class m170608_051712_mod_table_builder migration
 */
class m170608_051712_mod_table_builder extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%builder_widget}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'widget_visible', $this->boolean()
            ->notNull()->defaultValue(1)->comment("Widget Visible"));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'widget_visible');
    }
}
