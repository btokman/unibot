<?php

use console\components\Migration;

/**
 * Class m170518_081215_create_file_meta_data_table migration
 */
class m170518_081215_create_file_meta_data_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%file_meta_data}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id'      => $this->primaryKey(),
                'file_id' => $this->integer()->unsigned()->notNull()->comment('File'),

                'alt' => $this->string()->defaultValue(null)->comment('Alt'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->createIndex('key-file_id', $this->tableName, 'file_id');

        $this->addForeignKey(
            'fk-file_meta_data-to-fpm_file-id',
            $this->tableName,
            'file_id',
            '{{%fpm_file}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
