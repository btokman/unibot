<?php

use console\components\Migration;

/**
 * Class m170125_121330_create_builder_history_table migration
 */
class m170125_121330_create_builder_history_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%builder_history}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'target_class' => $this->string()->notNull(),
                'target_id' => $this->integer()->null(),
                'target_salt' => $this->string()->null(),
                'target_attribute' => $this->string()->notNull(),
                'hash' => $this->string()->notNull(),
                'data' => $this->text()->null(),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
