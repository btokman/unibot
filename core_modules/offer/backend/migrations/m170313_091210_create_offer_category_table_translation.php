<?php

use console\components\Migration;

/**
 * Class m170313_091210_create_offer_category_table_translation migration
 */
class m170313_091210_create_offer_category_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%offer_category_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%offer_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->null()->defaultValue(null)->comment('Label'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-offer_category_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-offer_category_translation-model_id-offer_category-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

