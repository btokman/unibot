<?php

use console\components\Migration;

/**
 * Class m170313_091158_create_offer_category_table migration
 */
class m170313_091158_create_offer_category_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%offer_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'label' => $this->string()->notNull()->comment('Label'),
                'alias' => $this->string()->notNull()->comment('Alias'),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(true)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
