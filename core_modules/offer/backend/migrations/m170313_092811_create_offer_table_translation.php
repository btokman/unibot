<?php

use console\components\Migration;

/**
 * Class m170313_092811_create_offer_table_translation migration
 */
class m170313_092811_create_offer_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%offer_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%offer}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label'       => $this->string()->defaultValue(null)->comment('Label'),
                'description' => $this->text()->defaultValue(null)->comment('Description'),
                'content'     => $this->text()->defaultValue(null)->comment('Content'),
            ],
            $this->tableOptions
        );


        $this->addPrimaryKey('pk-offer_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-offer_translation-model_id-offer-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

