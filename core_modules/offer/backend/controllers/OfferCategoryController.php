<?php

namespace backend\modules\offer\controllers;

use backend\components\BackendController;
use backend\modules\offer\models\OfferCategory;

/**
 * OfferCategoryController implements the CRUD actions for OfferCategory model.
 */
class OfferCategoryController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return OfferCategory::className();
    }
}
