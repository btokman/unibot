<?php

namespace backend\modules\offer\models;

use backend\components\BackendModel;
use backend\components\ImperaviContent;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\components\model\Translateable;
use common\components\TranslateableTrait;
use common\models\EntityToFile;
use kartik\widgets\DatePicker;
use kartik\widgets\DateTimePicker;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * Class Offer
 *
 * @package backend\modules\offer\models
 *
 * @property OfferCategory[] $categories
 */
class Offer extends \common\models\Offer implements BackendModel, Translateable
{
    use TranslateableTrait;

    // Date formats used for this model
    const DATETIME_FORMAT_FOR_FORM_PAGE = 'dd.MM.yyyy HH:mm';
    const DATE_FORMAT_FOR_INDEX_PAGE = 'dd.MM.yyyy';

    // String representation of timestamp date values
    public $beginDateString;
    public $endDateString;
    public $createdAtString;

    // Attribute for image upload
    public $imageUpload;

    /**
     * Attribute to save categories
     *
     * @var array
     */
    public $categoriesIds;

    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = Yii::$app->getSecurity()->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'alias', 'begin_date', 'end_date', 'created_at'], 'required'],
            [['description', 'content'], 'string'],
            [['published', 'position', 'begin_date', 'end_date', 'created_at'], 'integer'],
            [['label', 'alias'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [
                ['beginDateString', 'endDateString', 'createdAtString'],
                'date',
                'format' => static::DATETIME_FORMAT_FOR_FORM_PAGE,
            ],
            [['categoriesIds'], 'safe'],
            [['sign'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => Yii::t('back/offer', 'ID'),
            'label'             => Yii::t('back/offer', 'Label'),
            'alias'             => Yii::t('back/offer', 'Alias'),
            'description'       => Yii::t('back/offer', 'Description'),
            'content'           => Yii::t('back/offer', 'Content'),
            'published'         => Yii::t('back/offer', 'Published'),
            'position'          => Yii::t('back/offer', 'Position'),
            'beginDateString' => Yii::t('back/offer', 'Begin date'),
            'endDateString'   => Yii::t('back/offer', 'End date'),
            'createdAtString' => Yii::t('back/offer', 'Created date'),
            'imageUpload'       => Yii::t('back/offer', 'Image'),
            'categoriesIds'     => Yii::t('back/offer', 'Categories'),
        ];
    }

    // Commented until css fix
//    public function attributeHints()
//    {
//        return [
//            'beginDateString' => Yii::t('back/offer', 'Leave empty for current time'),
//            'endDateString'   => Yii::t('back/offer', 'Leave empty for current time'),
//            'createdAtString' => Yii::t('back/offer', 'Leave empty for current time'),
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'timestamp' => [
                    'class' => TimestampBehavior::className(),
                ],
            ]
        );
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/offer', 'Offer');
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        // Convert timestamp dates into string format readable for humans
        $this->createdAtString = $this->_convertTimestampToString($this->created_at);
        $this->beginDateString = $this->_convertTimestampToString($this->begin_date);
        $this->endDateString = $this->_convertTimestampToString($this->end_date);

        $this->categoriesIds = ArrayHelper::getColumn($this->categories, 'id');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    'label',
                    [
                        'attribute' => 'categoriesIds',
                        'value'     => function (Offer $model) {
                            return $model->_getCategories();
                        },
                        'filter'    => OfferCategory::getItems(),
                    ],
                    [
                        'attribute' => 'beginDateString',
                        'format'    => 'html',
                        'value'     => function (Offer $model) {
                            return Yii::$app->formatter->asDatetime(
                                $model->begin_date,
                                static::DATE_FORMAT_FOR_INDEX_PAGE
                            );
                        },
                        'filter'    => DatePicker::widget([
                            'model'         => $this,
                            'attribute'     => 'beginDateString',
                            'options'       => [
                                'class' => 'form-control form-control-filters',
                            ],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format'    => 'dd.mm.yyyy',
                            ],
                        ]),
                    ],
                    [
                        'attribute' => 'endDateString',
                        'format'    => 'html',
                        'value'     => function (Offer $model) {
                            return Yii::$app->formatter->asDatetime(
                                $model->end_date,
                                static::DATE_FORMAT_FOR_INDEX_PAGE
                            );
                        },
                        'filter'    => DatePicker::widget([
                            'model'         => $this,
                            'attribute'     => 'endDateString',
                            'options'       => [
                                'class' => 'form-control form-control-filters',
                            ],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format'    => 'dd.mm.yyyy',
                            ],
                        ]),
                    ],
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'alias',
                    [
                        'attribute' => 'categoriesIds',
                        'value'     => $this->_getCategories(),
                    ],
                    [
                        'attribute' => 'description',
                        'format'    => 'html',
                    ],
                    [
                        'attribute' => 'content',
                        'format'    => 'html',
                    ],
                    'createdAtString',
                    'beginDateString',
                    'endDateString',
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new OfferSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'imageUpload'       => [
                'type'  => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model'         => $this,
                    'attribute'     => 'imageUpload',
                    'saveAttribute' => static::OFFER_IMAGE,
                    'multiple'      => false,
                ]),
            ],
            'label'             => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class'     => $this->isNewRecord ? 's_name form-control' : 'form-control',
                ],
            ],
            'alias'             => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class'     => $this->isNewRecord ? 's_alias form-control' : 'form-control',
                ],
            ],
            'categoriesIds'     => [
                'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items'   => OfferCategory::getItems('id', 'label'),
                'options' => [
                    'multiple' => true,
                ],
            ],
            'description'       => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
            ],
            'content'           => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
            ],
            'createdAtString' => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DateTimePicker::className(),
                'options'     => [
                    'pluginOptions' => [
                        'format'         => 'dd.mm.yyyy hh:ii',
                        'todayHighlight' => true,
                        'default'        => time(),
                    ],
                ],
            ],
            'beginDateString' => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DateTimePicker::className(),
                'options'     => [
                    'pluginOptions' => [
                        'format'         => 'dd.mm.yyyy hh:ii',
                        'todayHighlight' => true,
                        'default'        => time(),
                    ],
                ],
            ],
            'endDateString'   => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DateTimePicker::className(),
                'options'     => [
                    'pluginOptions' => [
                        'format'         => 'dd.mm.yyyy hh:ii',
                        'todayHighlight' => true,
                        'default'        => time(),
                    ],
                ],
            ],
            'published'         => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position'          => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'sign'              => [
                'type'    => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }


    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            // Convert string date values into timestamp values
            $this->created_at = $this->_convertFromStringToTimestamp($this->createdAtString);
            $this->begin_date = $this->_convertFromStringToTimestamp($this->beginDateString);
            $this->end_date = $this->_convertFromStringToTimestamp($this->endDateString);

            return true;
        }

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);

        $this->_saveCategories();
    }

    /**
     * Relation to offer categories
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(OfferCategory::className(), ['id' => 'offer_category_id'])
            ->alias('categories')
            ->viaTable(
                OfferToOfferCategory::tableName(),
                ['offer_id' => 'id']
            );
    }

    /**
     * Method save categories of current offer
     */
    private function _saveCategories()
    {
        OfferToOfferCategory::deleteAll(['offer_id' => $this->id]);

        if (is_array($this->categoriesIds) && !empty($this->categoriesIds)) {
            $categories = [];
            foreach ($this->categoriesIds as $categoryId) {
                $categories[] = ['offer_id' => $this->id, 'offer_category_id' => $categoryId];
            }

            OfferToOfferCategory::saveCategories($categories);
        }
    }

    /**
     * Method returns list of categories of current offer separated by $separator value
     *
     * @param string $separator
     *
     * @return string
     */
    private function _getCategories($separator = ', ')
    {
        return implode($separator, ArrayHelper::getColumn($this->categories, 'label'));
    }

    /**
     * Method converts timestamp date value from DB to string representation
     *
     * @param int $timestamp
     *
     * @return string
     */
    private function _convertTimestampToString($timestamp)
    {
        return Yii::$app->getFormatter()->asDatetime(
            $timestamp,
            static::DATETIME_FORMAT_FOR_FORM_PAGE
        );
    }

    /**
     * Method accepts string date representation and converts it to timestamp value
     * If no date value given then method return current time in timestamp
     *
     * @param string | null $date
     *
     * @return int
     */
    private function _convertFromStringToTimestamp($date)
    {
        return $date
            ? Yii::$app->getFormatter()->asTimestamp($date)
            : time();
    }
}
