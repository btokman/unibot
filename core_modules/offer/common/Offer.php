<?php

namespace common\models;

use common\components\model\ActiveRecord;
use creocoder\translateable\TranslateableBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;

/**
 * This is the model class for table "{{%offer}}".
 *
 * @property integer $id
 * @property string  $label
 * @property string  $alias
 * @property string  $description
 * @property string  $content
 * @property integer $published
 * @property integer $position
 * @property integer $begin_date
 * @property integer $end_date
 * @property integer $created_at
 * @property integer $updated_at
 */
class Offer extends ActiveRecord
{
    // Value to save image
    const OFFER_IMAGE = 'offer-image';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer}}';
    }

    /**
     * List of attributes to translate
     *
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return ['label', 'description', 'content'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(OfferTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class'                 => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo'           => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }
}
