<?php

namespace common\models;

use common\components\model\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%offer_category_translation}}".
 *
 * @property integer $model_id
 * @property string  $language
 * @property string  $label
 */
class OfferCategoryTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer_category_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/offer', 'Label') . ' [' . $this->language . ']',
        ];
    }

    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 225],
        ];
    }
}
