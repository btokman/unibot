<?php

namespace common\models;

use common\components\model\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%offer_translation}}".
 *
 * @property integer $model_id
 * @property string  $language
 * @property string  $label
 * @property string  $description
 * @property string  $content
 *
 */
class OfferTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label'       => Yii::t('back/offer', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('back/offer', 'Description') . ' [' . $this->language . ']',
            'content'     => Yii::t('back/offer', 'Content') . ' [' . $this->language . ']',
        ];
    }

    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 225],
            [['description', 'content'], 'string'],
        ];
    }

}
