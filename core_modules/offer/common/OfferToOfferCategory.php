<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%offer_to_offer_category}}".
 *
 * @property integer $offer_id
 * @property integer $offer_category_id
 *
 */
class OfferToOfferCategory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer_to_offer_category}}';
    }
}
