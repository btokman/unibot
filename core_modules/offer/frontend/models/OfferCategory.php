<?php

namespace frontend\modules\offer\models;

use common\components\Translate;
use frontend\modules\offer\components\OfferUrlHelper;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class OfferCategory extends \common\models\OfferCategory
{
    use Translate;

    /**
     * Method returns one published category offer with given alias or null if no offer was found
     *
     * @param string $alias
     *
     * @return OfferCategory | null
     */
    public static function getCategoryByAlias($alias)
    {
        return static::_getSearchQuery()
            ->andWhere(['t.alias' => $alias])
            ->one();
    }

    /**
     * Method returns all offer categories in array ordered by position
     *
     * @return OfferCategory[]
     */
    public static function getCategories()
    {
        return static::_getSearchQuery()
            ->all();
    }

    /**
     * Method return ActiveDataProvider for offer categories with given pageSize and ordered by position
     *
     * @param int $pageSize
     *
     * @return \yii\data\ActiveDataProvider
     */
    public static function getCategoriesDataProvider($pageSize = 10)
    {
        return new ActiveDataProvider([
            'query'      => static::_getSearchQuery(),
            'pagination' => ['pageSize' => $pageSize],
        ]);
    }

    /**
     * Method returns basic query to find published offer categories
     *
     * @return ActiveQuery
     */
    private static function _getSearchQuery()
    {
        return static::find()
            ->alias('t')
            ->where(['t.published' => true])
            ->orderBy(['t.position' => SORT_ASC]);
    }

    /**
     * Method returns url for current offer category
     *
     * @return string
     */
    public function getCategoryUrl()
    {
        return OfferUrlHelper::getOfferCategoryUrl(['categoryAlias' => $this->alias]);
    }
}