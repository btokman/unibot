<?php

return [
    'offer/categories/<categoryAlias:([a-z0-9\-]+)>' => 'offer/offer/view-category',
    'offer/offers/<offerAlias:([a-z0-9\-]+)>'        => 'offer/offer/view-offer',
];
