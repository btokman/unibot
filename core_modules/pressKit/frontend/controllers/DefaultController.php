<?php

namespace frontend\modules\pressKit\controllers;

use common\models\FpmFile;
use common\models\PressKit;
use metalguardian\fileProcessor\helpers\FPM;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `pressKit` module
 */
class DefaultController extends Controller
{
    /**
     * @param $filename
     * @param $extension
     * @throws NotFoundHttpException
     */
    public function actionIndex($filename, $extension)
    {
        $filename = urldecode($filename);
        $extension = urldecode($extension);
        $fileModel = PressKit::findFile($filename, $extension);

        if (!$fileModel) {
            throw new NotFoundHttpException();
        }
        $filepath = mb_substr(urldecode(FPM::originalSrc($fileModel->id)), 1);

        if (file_exists($filepath)) {
            return $this->showFile($filepath, $filename, $extension);
        } else {
            throw new NotFoundHttpException();
        }
    }

    protected function showFile($filepath, $filename, $extension)
    {
        if (in_array($extension, PressKit::EXTENSIONS_IMAGE)) {
            return $this->renderImage($filepath);
        } elseif ($extension == PressKit::EXTENSION_PDF) {
            $this->setPdfHeader($filepath, $filename);
        } else {
            $this->setDownloadHeader($filepath, $filename . '.' . $extension);
        }
        return true;
    }

    protected function renderImage($filepath)
    {
        return $this->render('image', ['filepath' => $filepath]);
    }

    protected function setPdfHeader($filepath, $filename)
    {
        \Yii::$app->response->sendFile($filepath, $filename, ['inline' => true]);
    }

    protected function setDownloadHeader($filepath, $filename)
    {
        \Yii::$app->response->sendFile($filepath, $filename);
    }
}
