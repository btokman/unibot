<?php

use console\components\Migration;

/**
 * Class m170222_100658_create_press_kit_table migration
 */
class m170222_100658_create_press_kit_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%press_kit}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
