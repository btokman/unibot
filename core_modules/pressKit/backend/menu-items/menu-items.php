<?php

return [
    'label' => Yii::t('back/press-kit', 'Press kit'),
    'url' => ['/pressKit/press-kit/index'],
];