<?php

namespace backend\modules\pressKit;

use common\components\interfaces\CoreModuleBackendInterface;

class Module extends \yii\base\Module implements CoreModuleBackendInterface
{
    public $controllerNamespace = 'backend\modules\pressKit\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
