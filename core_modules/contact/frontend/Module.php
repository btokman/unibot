<?php
namespace frontend\modules\contact;

use common\components\interfaces\CoreModuleFrontendInterface;

/**
 * Class Module
 *
 * @package frontend\modules\contact
 */
class Module extends \yii\base\Module implements CoreModuleFrontendInterface
{
    /**
     * @var string
     */
    public $controllerNamespace = 'frontend\modules\contact\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
