<?php
namespace frontend\modules\contact\models;

use common\components\Translate;

/**
 * Class Contact
 *
 * @package frontend\modules\contact\models
 */
class Contact extends \common\models\Contact
{
    use Translate;

    public static function getContacts()
    {
        return static::find()
            ->isPublished()
            ->orderBy(['position' => SORT_ASC])
            ->all();
    }
}
