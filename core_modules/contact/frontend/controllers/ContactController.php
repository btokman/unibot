<?php
namespace frontend\modules\contact\controllers;

use common\models\ContactPage;
use frontend\components\ConfigurationMetaTagRegister;
use frontend\components\FrontendController;
use frontend\modules\contact\models\Contact;

/**
 * Class ContactController
 *
 * @package frontend\modules\contact\controllers
 */
class ContactController extends FrontendController
{
    public function actionIndex()
    {
        $models = Contact::getContacts();

        $page = (new ContactPage())->get();

        ConfigurationMetaTagRegister::register($page);

        return $this->render('index', [
            'models' => $models,
            'page' => $page,
        ]);
    }
}
