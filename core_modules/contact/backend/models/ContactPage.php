<?php

namespace backend\modules\contact\models;

use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\ContactPage as CommonContactPage;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
* Class ContactPage*/
class ContactPage extends ConfigurationModel
{
    public $showAsConfig = false;
    /**
    * Title of the form
    *
    * @return string
    */
    public function getTitle()
    {
        return Yii::t('back/contact', 'Contact page content');;
    }

    /**
    * @return array
    */
    public function getFormRules()
    {
        return [];
    }

    /**
    * @return array
    */
    public function getFormTypes()
    {
        return [
            CommonContactPage::LABEL => Configuration::TYPE_STRING,
            CommonContactPage::CONTENT => Configuration::TYPE_TEXT,
        ];
    }

    /**
    * @return array
    */
    public function getFormDescriptions()
    {
        return [
            CommonContactPage::LABEL => 'Label',
            CommonContactPage::CONTENT => 'Content',
        ];
    }

    /**
    * @return array
    */
    public function getFormHints()
    {
        return [
            CommonContactPage::LABEL => '',
            CommonContactPage::CONTENT => '',
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            CommonContactPage::LABEL,
            CommonContactPage::CONTENT,
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => MetaTagBehavior::className(),
            ],
        ]);
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
                    CommonContactPage::LABEL,
                    CommonContactPage::CONTENT,
                ],
            ]
        ];
        return $config;
    }
}
