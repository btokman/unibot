<?php

namespace common\models;

use common\components\ConfigurationComponent;
use common\models\base\StaticPage;
use Yii;

/**
 * This is class for static page model.
 */
class ContactPage extends StaticPage
{
    const LABEL = 'contactPageLabel';
    const CONTENT = 'contactPageContent';

    public $label;
    public $content;

    /**
    * @return array
    */
    public function getKeys()
    {
        return [
            self::LABEL,
            self::CONTENT,
        ];
    }

    /**
    * @return $this
    */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->label = $config->get(self::LABEL);
        $this->content = $config->get(self::CONTENT);

        return $this;
    }
}
