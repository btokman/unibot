<?php

namespace console\controllers;

use backend\modules\importExport\config\ImportExportConfig;
use backend\modules\importExport\helpers\ImportExportHelper;
use backend\modules\importExport\models\ImportExportMessage;
use moonland\phpexcel\Excel;
use yii\console\Controller;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\helpers\Json;

/**
 * import/export main functionality
 *
 * Class ImportExportController
 * @package console\controllers
 */
class ImportExportController extends Controller
{
    /**
     * Import from excel to db
     *
     * @param $selectedConfig
     * @param $matchColumns
     * @param $importFilePath
     * @param null $importFileFormat
     */
    public function actionImport(
        $selectedConfig,
        $matchColumns,
        $importFilePath,
        $importFileFormat = null
    ) {
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            //get init data
            $importFilePath = base64_decode($importFilePath);
            $importFileFormat = base64_decode($importFileFormat);
            $selectedConfig = Json::decode(base64_decode($selectedConfig));
            $matchColumns = Json::decode(base64_decode($matchColumns));

            $columnTypeIndex = ImportExportConfig::IMPORT_COLUMN_INDEX['column_type'];
            $classNameIndex = ImportExportConfig::IMPORT_COLUMN_INDEX['class_name'];
            $fieldOrFunctionIndex = ImportExportConfig::IMPORT_COLUMN_INDEX['field_or_function_in_model'];

            if ($importFileFormat == '') {
                $importFileFormat = null;
            }

            /** @var array[] $excelData */
            $excelData = Excel::import($importFilePath, [
                'format' => $importFileFormat
            ]);

            //find key column indexes for each models in config
            $keyIndexArray = [];

            foreach ($selectedConfig['columns'] as $index => $column) {
                if ($column[$columnTypeIndex] == ImportExportConfig::IMPORT_COLUMN_MODEL_IDENTIFIER) {
                    $modelName = $column[$classNameIndex];
                    $keyIndexArray[$modelName]['configIndex'] = $index;
                    $keyIndexArray[$modelName]['excelIndex'] = array_search($index, $matchColumns);
                }
            }

            foreach ($keyIndexArray as $className => $indexes) {
                $configIndex = $indexes['configIndex'];
                $excelIndex = $indexes['excelIndex'];

                foreach ($excelData as $row) {
                    $rowData = array_values($row);

                    //get model by key
                    $keyField = $selectedConfig['columns'][$configIndex][$fieldOrFunctionIndex];
                    $keyValue = $rowData[$excelIndex];
                    $reflectionClass = new \ReflectionClass($className);
                    $instance = $reflectionClass->newInstanceWithoutConstructor();
                    $model = $instance::find()->where([$keyField => $keyValue])->one();

                    $resultCreate = true;
                    if (!isset($model)) {
                        $resultCreate = self::importCreateUpdateModel(
                            $model,
                            $selectedConfig,
                            $className,
                            $matchColumns,
                            $row
                        );
                    } elseif ($selectedConfig['options']['onFound'] == ImportExportConfig::ON_FOUND_SKIP) {
                        continue;
                    } elseif ($selectedConfig['options']['onFound'] == ImportExportConfig::ON_FOUND_UPDATE) {
                        $resultCreate = self::importCreateUpdateModel(
                            $model,
                            $selectedConfig,
                            $className,
                            $matchColumns,
                            $row
                        );
                    }

                    if ($resultCreate !== true) {
                        if ($selectedConfig['options']['onSaveError'] == ImportExportConfig::ON_SAVE_ERROR_TERMINATE) {
                            $transaction->rollBack();
                        }

                        $message = \Yii::t('back/import-export', 'Can\'t save model with ')
                            . $keyField
                            . ' = '
                            . $keyValue
                            . '. '
                            . \Yii::t('back/import-export', 'Error message: ')
                            . Json::encode($resultCreate);

                        ImportExportHelper::addImportExportMessage(
                            ImportExportMessage::MESSAGE_TYPE_ERROR,
                            ImportExportMessage::MESSAGE_HANDLER_IMPORT,
                            $message,
                            false
                        );

                        if ($selectedConfig['options']['onSaveError'] == ImportExportConfig::ON_SAVE_ERROR_TERMINATE) {
                            $message = \Yii::t('back/import-export', 'Import error. View log for details');
                            ImportExportHelper::addImportExportMessage(
                                ImportExportMessage::MESSAGE_TYPE_ERROR,
                                ImportExportMessage::MESSAGE_HANDLER_IMPORT,
                                $message
                            );

                            break;
                        }
                    }
                }
            }

            //add message to show
            $transaction->commit();
            ImportExportHelper::addImportExportMessage(
                ImportExportMessage::MESSAGE_TYPE_SUCCESS,
                ImportExportMessage::MESSAGE_HANDLER_IMPORT,
                \Yii::t('back/import-export', 'Import completed successfully')
            );
        } catch (\Exception $e) {
            $transaction->rollBack();
            $message = \Yii::t('back/import-export', 'Import error: ')
                . $e->getMessage()
                . '('
                . $e->getFile()
                . ' - '
                . $e->getLine()
                . ')';

            ImportExportHelper::addImportExportMessage(
                ImportExportMessage::MESSAGE_TYPE_ERROR,
                ImportExportMessage::MESSAGE_HANDLER_IMPORT,
                $message,
                false
            );
            $message = \Yii::t('back/import-export', 'Import error. View log for details');
            ImportExportHelper::addImportExportMessage(
                ImportExportMessage::MESSAGE_TYPE_ERROR,
                ImportExportMessage::MESSAGE_HANDLER_IMPORT,
                $message
            );
        } catch (\Error $e) {
            $transaction->rollBack();
            $message = \Yii::t('back/import-export', 'Import error: ')
                . $e->getMessage()
                . '('
                . $e->getFile()
                . ' - '
                . $e->getLine()
                . ')';

            ImportExportHelper::addImportExportMessage(
                ImportExportMessage::MESSAGE_TYPE_ERROR,
                ImportExportMessage::MESSAGE_HANDLER_IMPORT,
                $message,
                false
            );

            $message = \Yii::t('back/import-export', 'Import error. View log for details');
            ImportExportHelper::addImportExportMessage(
                ImportExportMessage::MESSAGE_TYPE_ERROR,
                ImportExportMessage::MESSAGE_HANDLER_IMPORT,
                $message
            );
        }
    }

    /**
     * @param $model
     * @param $selectedConfig
     * @param $className
     * @param $matchColumns
     * @param $row
     * @return array|bool
     */
    protected function importCreateUpdateModel($model, $selectedConfig, $className, $matchColumns, $row)
    {
        $columnTypeIndex = ImportExportConfig::IMPORT_COLUMN_INDEX['column_type'];
        $classNameIndex = ImportExportConfig::IMPORT_COLUMN_INDEX['class_name'];
        $fieldOrFunctionIndex = ImportExportConfig::IMPORT_COLUMN_INDEX['field_or_function_in_model'];

        $rowData = array_values($row);
        $rowLabel = array_keys($row);
        if (!isset($model)) {
            /** @var ActiveRecord $model */
            $model = new $className();
        }
        foreach ($selectedConfig['columns'] as $index => $column) {
            $excelIndex = null;
            if (((
                        $column[$columnTypeIndex] == ImportExportConfig::IMPORT_COLUMN_FIELD
                    ) ||
                    (
                        $column[$columnTypeIndex] == ImportExportConfig::IMPORT_COLUMN_MODEL_IDENTIFIER
                    )) &&
                (
                    $column[$classNameIndex] == $className
                )
            ) {
                $excelIndex = array_search($index, $matchColumns);
                if ($excelIndex !== false) {
                    $excelValue = $rowData[$excelIndex];
                    $field = $column[$fieldOrFunctionIndex];

                    $fieldType = $model->getTableSchema()->columns[$field]->type;
                    settype($excelValue, $fieldType);
                    $model->{$field} = $excelValue;
                }
            }
        }

        //run before save function fields
        foreach ($selectedConfig['columns'] as $index => $column) {
            $excelIndex = null;

            if ((
                    $column[$columnTypeIndex] == ImportExportConfig::IMPORT_COLUMN_FUNCTION_BEFORE_SAVE
                ) &&
                (
                    $column[$classNameIndex] == $className
                )
            ) {
                $excelIndex = array_search($index, $matchColumns);
                if ($excelIndex !== false) {
                    $excelValue = $rowData[$excelIndex];
                    $excelLabel = $rowLabel[$excelIndex];
                    $model->{$column[$fieldOrFunctionIndex]}($excelValue, $excelLabel);
                }
            }
        }

        $result = $model->save();

        if (!$result) {
            return $model->errors;
        }

        //run after save function fields
        foreach ($selectedConfig['columns'] as $index => $column) {
            $excelIndex = null;

            if ((
                    $column[$columnTypeIndex] == ImportExportConfig::IMPORT_COLUMN_FUNCTION_AFTER_SAVE
                ) &&
                (
                    $column[$classNameIndex] == $className
                )
            ) {
                $excelIndex = array_search($index, $matchColumns);
                if ($excelIndex !== false) {
                    $excelValue = $rowData[$excelIndex];
                    $excelLabel = $rowLabel[$excelIndex];
                    $model->{$column[$fieldOrFunctionIndex]}($excelValue, $excelLabel);
                }
            }
        }

        return true;
    }


    /**
     * Export from db to excel
     *
     * @param $activeQuery
     * @param $configLabel
     * @param $additionalParams
     * @return bool
     */
    public function actionExport(
        $activeQuery,
        $configLabel,
        $additionalParams
    ) {
        try {
            $configLabel = base64_decode($configLabel);
            $stdActiveQuery = Json::decode(base64_decode($activeQuery), true);
            $additionalParams = Json::decode(base64_decode($additionalParams));

            $activeQuery = $this->recastArray(ActiveQuery::className(), $stdActiveQuery);

            $columnTypeIndex = ImportExportConfig::EXPORT_COLUMN_INDEX['column_type'];
            $fieldOrFunctionIndex = ImportExportConfig::EXPORT_COLUMN_INDEX['field_or_function_in_model'];

            $data = $activeQuery->all();

            $checkConfig = ImportExportConfig::checkExportConfig();
            if ($checkConfig === false) {
                return false;
            }
            $selectedConfig = ImportExportConfig::getSelectedExportConfig($configLabel, $data);
            if ($selectedConfig === false) {
                return false;
            }
            $exportObjArray = [];
            $columnsArray = [];
            $headersArray = [];
            foreach ($data as $exportObj) {
                $newExportObject = new \stdClass();
                foreach ($selectedConfig['columns'] as $column) {
                    switch ($column[$columnTypeIndex]) {
                        case ImportExportConfig::EXPORT_COLUMN_FILED:
                            $field = $column[$fieldOrFunctionIndex];
                            //$value =  $exportObj->{$field};
                            $value = self::getFieldValue(
                                ImportExportMessage::MESSAGE_HANDLER_EXPORT,
                                $exportObj,
                                $field
                            );
                            $newExportObject->{$field} = $value;

                            if (!in_array($field, $columnsArray)) {
                                $columnsArray[] = $field;
                                $headersArray[$field] = $column[ImportExportConfig::EXPORT_COLUMN_INDEX['column_options']]['excelColumnLabel'] ?? $field;
                            }
                            break;

                        case ImportExportConfig::EXPORT_COLUMN_FUNCTION:
                            $function = $column[$fieldOrFunctionIndex];
                            //$value = $exportObj->{$function}();
                            $value = self::getFieldValue(
                                ImportExportMessage::MESSAGE_HANDLER_EXPORT,
                                $exportObj,
                                $function,
                                true,
                                $additionalParams
                            );
                            $newExportObject->{$function} = $value;

                            if (!in_array($function, $columnsArray)) {
                                $columnsArray[] = $function;
                                $headersArray[$function] = $column[ImportExportConfig::EXPORT_COLUMN_INDEX['column_options']]['excelColumnLabel'] ?? $function;
                            }
                            break;

                        case ImportExportConfig::EXPORT_COLUMN_DYNAMIC:
                            $columnsLabelFunction = $column[$fieldOrFunctionIndex]['columnsLabelFunction'];
                            $valueByColumnNameFunction = $column[$fieldOrFunctionIndex]['valueByColumnNameFunction'];

                            // $columnsLabels = $exportObj->{$columnsLabelFunction}();
                            $columnsLabels = self::getFieldValue(
                                ImportExportMessage::MESSAGE_HANDLER_EXPORT,
                                $exportObj,
                                $columnsLabelFunction,
                                true,
                                $additionalParams
                            );

                            foreach ($columnsLabels as $columnLabel) {
                                //$value = $exportObj->{$valueByColumnNameFunction}($columnLabel);
                                $functionParams = [];
                                $functionParams[] = $columnLabel;
                                $functionParams = array_merge($functionParams, $additionalParams);
                                $value = self::getFieldValue(
                                    ImportExportMessage::MESSAGE_HANDLER_EXPORT,
                                    $exportObj,
                                    $valueByColumnNameFunction,
                                    true,
                                    $functionParams
                                );

                                $field = ImportExportConfig::EXPORT_DYNAMIC_FIELD_PREFIX . Inflector::transliterate($columnLabel);
                                $field = str_replace('-', '_', $field);
                                $newExportObject->{$field} = $value;

                                if (!in_array($field, $columnsArray)) {
                                    $columnsArray[] = $field;
                                    $headersArray[$field] = $columnLabel;
                                }
                            }
                            break;
                    }
                }

                $exportObjArray[] = $newExportObject;
            }


            $exportFolder = ImportExportConfig::EXPORT_FOLDER;
            $exportPath = \Yii::getAlias('@backend') . '/web' . $exportFolder;
            FileHelper::createDirectory($exportPath);
            $exportName = $configLabel . '_' . uniqid();
            $exportExt = '.xls';

            $downloadPath = $exportFolder . '/' . $exportName . $exportExt;

            $messageLog = \Yii::t('back/import-export', $downloadPath);
            $messageGlobal = \Yii::t('back/import-export', 'Export completed successfully');
            ImportExportHelper::addDoubleImportExportMessage(
                ImportExportMessage::MESSAGE_TYPE_SUCCESS,
                ImportExportMessage::MESSAGE_HANDLER_EXPORT,
                $messageGlobal,
                $messageLog
            );

            Excel::export([
                'models' => $exportObjArray,
                'columns' => $columnsArray,
                'headers' => $headersArray,
                'savePath' => $exportPath,
                'fileName' => $exportName,
            ]);

            return true;
        } catch (\Exception $e) {
            $message = \Yii::t('back/import-export', 'Export error: ')
                . $e->getMessage()
                . '('
                . $e->getFile()
                . ' - '
                . $e->getLine()
                . ')';

            ImportExportHelper::addImportExportMessage(
                ImportExportMessage::MESSAGE_TYPE_ERROR,
                ImportExportMessage::MESSAGE_HANDLER_EXPORT,
                $message,
                false
            );
            $message = \Yii::t('back/import-export', 'Export error. View log for details');
            ImportExportHelper::addImportExportMessage(
                ImportExportMessage::MESSAGE_TYPE_ERROR,
                ImportExportMessage::MESSAGE_HANDLER_EXPORT,
                $message
            );
        }
        return false;
    }


    /**
     * Recast function for work with ActiveQuery
     *
     * @param $className
     * @param \stdClass $object
     * @return mixed
     */
    protected function recast($className, \stdClass &$object)
    {
        if (!class_exists($className)) {
            throw new \InvalidArgumentException(sprintf('Inexistant class %s.', $className));
        }

        $new = new $className($object->modelClass);

        foreach ($object as $property => &$value) {
            $new->$property = &$value;
            unset($object->$property);
        }
        unset($value);
        $object = (unset)$object;
        return $new;
    }

    /**
     * Recast function for work with ActiveQuery
     *
     * @param $className
     * @param  $object
     * @return mixed
     */
    protected function recastArray($className,  &$object)
    {
        if (!class_exists($className)) {
            throw new \InvalidArgumentException(sprintf('Inexistant class %s.', $className));
        }

        $new = new $className($object['modelClass']);

        foreach ($object as $property => &$value) {
            $new->$property = &$value;
            unset($object->$property);
        }
        unset($value);
        $object = (unset)$object;
        return $new;
    }


    /**
     * Universal function to get value of field or run function from string name
     *
     * @param $handler
     * @param $obj
     * @param $pathStr
     * @param bool $isFunction
     * @param array $functionParams
     * @return mixed|null
     */
    protected function getFieldValue($handler, $obj, $pathStr, $isFunction = false, $functionParams = [])
    {
        $val = null;

        $path = preg_split('/->/', $pathStr);


        $node = $obj;
        while (($prop = array_shift($path)) !== null) {
            if (!is_object($obj)) {
                $val = null;
                break;
            }
            if (($isFunction) && (count($path) == 0)) {
                //run function
                if (isset($node)) {
                    $result = call_user_func_array([$node, $prop], $functionParams);
                    return $result;
                } else {
                    return $node;
                }
            } else {
                preg_match('/\[([^\)]*)\]/', $prop, $matches);
                if (count($matches) > 0) {
                    $index = $matches[1];
                    $prop = str_replace($index, '', $prop);
                    $prop = str_replace(['[', ']'], '', $prop);

                    $result = $node->$prop[$index] ?? null;

                    if ((!isset($result)) && (isset($node))) {
                        $message = \Yii::t('back/import-export', 'Model ')
                            . mb_substr(Json::encode($node), 0, 50) . '...'
                            . \Yii::t('back/import-export', ' has no property: ')
                            . ' "' . $prop . '" '
                            . \Yii::t('back/import-export', 'with index: ')
                            . $index;

                        ImportExportHelper::addImportExportMessage(
                            ImportExportMessage::MESSAGE_TYPE_ERROR,
                            $handler,
                            $message,
                            false
                        );
                    }

                    $val = $result;
                    $node = $result;
                } else {
                    $result = $node->$prop  ?? null;

                    if ((!isset($result)) && (isset($node))) {
                        $message = \Yii::t('back/import-export', 'Model has no property: ')
                            . $prop;

                        ImportExportHelper::addImportExportMessage(
                            ImportExportMessage::MESSAGE_TYPE_ERROR,
                            $handler,
                            $message,
                            false
                        );
                    }

                    $val = $result;
                    $node = $result;
                }
            }
        }
        return $val;
    }
}
