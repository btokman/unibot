<?php

return [
    'label' => Yii::t('back/import-export', 'Import / Export'),
    'items' => [
        [
            'label' => Yii::t('back/import-export', 'Import to DB'),
            'url' => ['/importExport/import-export/index'],
        ],
        [
            'label' => Yii::t('back/import-export', 'Import log'),
            'url' => ['/importExport/import-export/import-log'],
        ],
        [
            'label' => Yii::t('back/import-export', 'Export to Excel log'),
            'url' => ['/importExport/import-export/export-log'],
        ],
    ]
];
