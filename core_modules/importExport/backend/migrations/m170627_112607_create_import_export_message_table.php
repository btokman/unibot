<?php

use console\components\Migration;

/**
 * Class m170627_112607_create_import_export_message_table migration
 */
class m170627_112607_create_import_export_message_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%import_export_message}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'type' => $this->string()->notNull(),
                'handler' => $this->string()->notNull(),
                'message' => $this->text()->notNull(),
                'is_show' => $this->boolean()->notNull()->defaultValue(1),
                'is_shown' => $this->boolean()->notNull()->defaultValue(0),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-import_export_message-id',
            'import_export_message',
            'id',
            true
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
