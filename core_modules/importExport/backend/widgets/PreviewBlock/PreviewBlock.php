<?php
namespace backend\modules\importExport\widgets\PreviewBlock;

use yii\base\Widget;

/**
 * Class PreviewBlock
 * @package backend\modules\importExport\widgets\PreviewBlock
 */
class PreviewBlock extends Widget
{
    public $dataForPreview;
    public $selectedConfig;

    public function run()
    {
        return $this->render('previewBlockView', [
            'dataForPreview' => $this->dataForPreview,
            'selectedConfig' => $this->selectedConfig,
        ]);
    }
}
