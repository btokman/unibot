<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 13.02.2017
 * Time: 15:52
 */

use backend\modules\importExport\helpers\ImportExportHelper;
use backend\modules\importExport\widgets\PreviewDropdown\PreviewDropdown;
use yii\helpers\Html;

echo Html::beginTag('table', ['class' => 'table table-striped table-bordered']);
echo Html::beginTag('thead', ['class' => 'form-group content-append excel-preview-header']);
echo Html::beginForm('', 'post', ['id' => 'import-preview-form', 'name' => 'import-preview-form']);
echo Html::submitButton(\Yii::t('back/back/import-export', 'Import'),['class' => 'btn btn-success preview-import-btn']);
echo Html::a(\Yii::t('back/import-export', 'Cancel'), \backend\modules\importExport\models\ImportForm::getCancelUrl(), ['class' => 'btn btn-info cancel-import-btnn']);
//echo Html::input('hidden', 'import-preview-flag', '1');
echo Html::input('hidden', 'import-selected-config', \yii\helpers\Json::encode($selectedConfig));
echo Html::beginTag('tr');

/** @var mixed $dataForPreview */
/** @var mixed[] $selectedConfig */


foreach ($dataForPreview[0] as $key => $row) {
    echo Html::beginTag('th');

    echo PreviewDropdown::widget([
        'key' => $key,
        'selectedConfig' => $selectedConfig
    ]);
    echo Html::endTag('th');
}

echo Html::endTag('tr');

echo Html::beginTag('tr');
foreach ($dataForPreview[0] as $key => $row) {
    echo Html::tag('th', $key);
}
echo Html::endTag('tr');
echo Html::endForm();
echo Html::endTag('thead');

//content
foreach ($dataForPreview as $row) {
    echo Html::beginTag('tr');
    foreach ($row as $column) {
        echo Html::tag('td', ImportExportHelper::formatForPreview($column), ['title' => strip_tags($column)]);
    }
    echo Html::endTag('tr');
}

echo Html::endTag('table');
