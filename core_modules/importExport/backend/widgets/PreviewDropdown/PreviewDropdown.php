<?php

namespace backend\modules\importExport\widgets\PreviewDropdown;

use backend\modules\importExport\config\ImportExportConfig;
use yii\base\Widget;

/**
 * Class PreviewDropdown
 * @package backend\modules\importExport\widgets\PreviewDropdown
 */
class PreviewDropdown extends Widget
{
    public $selectedConfig;
    public $key;

    protected $items = [];
    protected $selected = [];

    public function init()
    {
        $items[ImportExportConfig::COMMON_PREVIEW_OPTIONS['not_parse']] = \Yii::t('back/import-export', 'Not parse');

        $selectedSet = false;
        foreach ($this->selectedConfig['columns'] as $columnIndex => $columnData) {

            $configColumnLabel = $columnData[ImportExportConfig::IMPORT_COLUMN_INDEX['column_options']]['excelColumnLabel'];
            $configColumnPreviewLabel = $columnData[ImportExportConfig::IMPORT_COLUMN_INDEX['column_options']]['previewDropdownLabel'];
            $currentColumnLabel = $this->key;

            if ((mb_strtolower($configColumnLabel) == mb_strtolower($currentColumnLabel)) && (!$selectedSet)) {
                $this->selected = [$columnIndex];
                $selectedSet = true;
                $items[$columnIndex] = $configColumnPreviewLabel;
            } else {
                $items[$columnIndex] = $configColumnPreviewLabel;
            }
        }
        $this->items = $items;
    }

    public function run()
    {
        return $this->render('previewDropdownView', [
            'items' => $this->items,
            'selected' => $this->selected,
        ]);
    }
}
