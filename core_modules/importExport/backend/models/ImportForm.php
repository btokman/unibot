<?php

namespace backend\modules\importExport\models;

use backend\modules\importExport\config\ImportExportConfig;
use common\components\model\Helper;
use metalguardian\fileProcessor\behaviors\UploadBehavior;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Login form
 */
class ImportForm extends Model
{
    public $importFile;
    public $importType;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['importFile'], 'required'],
            [['importFile'], 'file', 'extensions' => ['xls', 'xlsx']],
            [['importType'], 'integer']
        ];
    }

    public function behaviors()
    {
        return [
            'file' => [
                'class' => UploadBehavior::className(),
                'attribute' => ['importFile'],
            ],
        ];
    }


    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/import-export', 'Import to db');
    }

    public static function getCancelUrl($params = [])
    {
        $route = '/importExport/import-export/index';
        return Helper::createUrl($route, $params);
    }


    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'form-set' => [
                \Yii::t('back/import-export', 'Main') => [

                    'importType' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => ImportExportConfig::getImportTypeOptions()
                    ],
                    'importFile' => [
                        'type' => ActiveFormBuilder::INPUT_FILE,
                    ],
                ],

            ],
        ];
    }
}
