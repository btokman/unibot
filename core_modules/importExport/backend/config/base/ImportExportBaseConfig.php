<?php

namespace backend\modules\importExport\config\base;

use backend\modules\importExport\helpers\ImportExportHelper;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * Created by PhpStorm.
 * User: art
 * Date: 28.04.2017
 * Time: 11:47
 */
class ImportExportBaseConfig
{
    //columns type import
    const IMPORT_COLUMN_MODEL_IDENTIFIER = 1;
    const IMPORT_COLUMN_FIELD = 2;
    const IMPORT_COLUMN_FUNCTION_AFTER_SAVE = 3;
    const IMPORT_COLUMN_FUNCTION_BEFORE_SAVE = 4;

    //array for validate
    protected static $importColumnTypeArray = [
        self::IMPORT_COLUMN_FIELD,
        self::IMPORT_COLUMN_MODEL_IDENTIFIER,
        self::IMPORT_COLUMN_FUNCTION_AFTER_SAVE,
        self::IMPORT_COLUMN_FUNCTION_BEFORE_SAVE
    ];

    //import column settings
    const ON_FOUND_SKIP = 1;
    const ON_FOUND_UPDATE = 2;

    const ON_SAVE_ERROR_TERMINATE = 1;
    const ON_SAVE_ERROR_CONTINUE = 2;


    const IMPORT_COLUMN_INDEX = [
        'class_name' => 0,
        'column_type' => 1,
        'field_or_function_in_model' => 2,
        'column_options' => 3,

    ];
    const COMMON_PREVIEW_OPTIONS = [
        'not_parse' => 'not_parse'
    ];

    //columns type export
    const EXPORT_COLUMN_FILED = 1;
    const EXPORT_COLUMN_FUNCTION = 2;
    const EXPORT_COLUMN_DYNAMIC = 3;

    //array for validate
    protected static $exportColumnTypeArray = [
        self::EXPORT_COLUMN_FILED,
        self::EXPORT_COLUMN_FUNCTION,
        self::EXPORT_COLUMN_DYNAMIC
    ];

    const EXPORT_COLUMN_INDEX = [
        'column_type' => 0,
        'field_or_function_in_model' => 1,
        'column_options' => 2,

    ];

    const EXPORT_DYNAMIC_FIELD_PREFIX = 'c_';

    //general settings
    const PREVIEW_LINE_NUMBER = 10;
    const MESSAGE_LOG_SUCCESS_COLOR = '#90C695';
    const MESSAGE_LOG_ERROR_COLOR = '#F1A9A0';
    const EXPORT_FOLDER = '/uploads/export';


    public static function getImportTypeOptions()
    {
        $importTypeArray = [];
        foreach (static::getImportConfig() as $importType) {
            $importTypeArray[] = $importType['label'];
        }
        return $importTypeArray;
    }


    protected static function importConfig()
    {
        return [];
    }

    protected static function exportConfig()
    {
        return [];
    }

    public static function getImportConfig()
    {

        $config = static::importConfig();


        for ($i = 0; $i < count($config); $i++) {
            //set default data if it not set
            if (!isset($config[$i]['options']['onFound'])) {
                $config[$i]['options']['onFound'] = self::ON_FOUND_UPDATE;
            }
            if (!isset($config[$i]['options']['onSaveError'])) {
                $config[$i]['options']['onSaveError'] = self::ON_SAVE_ERROR_TERMINATE;
            }

            for ($j=0; $j< count($config[$i]['columns']); $j++){

                if (!isset($config[$i]['columns'][$j][self::IMPORT_COLUMN_INDEX['column_options']])){
                    $config[$i]['columns'][$j][] = [];
                }

                if (!isset($config[$i]['columns'][$j][self::IMPORT_COLUMN_INDEX['column_options']]['excelColumnLabel'])){
                    $config[$i]['columns'][$j][self::IMPORT_COLUMN_INDEX['column_options']]['excelColumnLabel'] =
                        $config[$i]['columns'][$j][self::IMPORT_COLUMN_INDEX['field_or_function_in_model']];
                }

                if (!isset($config[$i]['columns'][$j][self::IMPORT_COLUMN_INDEX['column_options']]['previewDropdownLabel'])){
                    $config[$i]['columns'][$j][self::IMPORT_COLUMN_INDEX['column_options']]['previewDropdownLabel'] =
                        $config[$i]['columns'][$j][self::IMPORT_COLUMN_INDEX['field_or_function_in_model']];
                }
            }
        }

        return $config;
    }

    public static function getExportConfig()
    {
        $config = static::exportConfig();
        return $config;
    }

    public static function checkImportConfig($config = null)
    {

        if (!isset($config)) {
            $config = static::getImportConfig();
        }


        if (count($config) == 0) {
            $message = \Yii::t('back/import-export', 'Import configuration error: config not set');
            throw new HttpException(
                501,
                $message
            );
        }

        //check conformity between key count and model count
        $keyColumnPerModel = 1;
        foreach ($config as $configSingle) {
            if (!isset($configSingle['label'])) {
                $message = \Yii::t('back/import-export', '"label" is required for config');
                throw new HttpException(
                    501,
                    $message
                );
            }

            if (!isset($configSingle['columns'])) {
                $message = \Yii::t('back/import-export', '"columns" is required for config');
                throw new HttpException(
                    501,
                    $message
                );
            }

            $checkArray = [];
            foreach ($configSingle['columns'] as $index => $column) {
                if (!isset($column[self::IMPORT_COLUMN_INDEX['class_name']])) {
                    $message = \Yii::t('back/import-export', '"class_name" is required for config columns');
                    throw new HttpException(
                        501,
                        $message
                    );
                }
                if (
                    (!isset($column[self::IMPORT_COLUMN_INDEX['column_type']])) ||
                    (!in_array(
                        $column[self::IMPORT_COLUMN_INDEX['column_type']],
                        self::$importColumnTypeArray)
                    )
                ) {
                    $message = \Yii::t(
                        'back/import-export',
                        '"column_type" is required for config columns or bad value'
                    );
                    throw new HttpException(
                        501,
                        $message
                    );
                }
                if (!isset($column[self::IMPORT_COLUMN_INDEX['field_or_function_in_model']])) {
                    $message = \Yii::t(
                        'back/import-export',
                        '"field_or_function_in_model" is required for config columns'
                    );
                    throw new HttpException(
                        501,
                        $message
                    );
                }

                if (!isset($checkArray[$column[self::IMPORT_COLUMN_INDEX['class_name']]])) {
                    $checkArray[$column[self::IMPORT_COLUMN_INDEX['class_name']]] = [];
                }

                if ($column[self::IMPORT_COLUMN_INDEX['column_type']] == self::IMPORT_COLUMN_MODEL_IDENTIFIER) {
                    $checkArray[$column[self::IMPORT_COLUMN_INDEX['class_name']]][] = $index;
                }
            }

            foreach ($checkArray as $className => $keyColumn) {
                //if more or less
                if (count($keyColumn) != $keyColumnPerModel) {
                    $message = \Yii::t(
                        'back/import-export',
                        'Must be one model identifier column per model class. Error with class: '
                    );
                    $message .= $className;
                    $message .= '. ';
                    $message .= \Yii::t('back/import-export', 'Config: ');
                    $message .= $configSingle['label'];
                    throw new HttpException(
                        501,
                        $message
                    );
                }
            }
        }
    }

    public static function checkExportConfig($config = null)
    {
        if (!isset($config)) {
            $config = static::getExportConfig();
        }


        if (count($config) == 0) {
            $messageLog = \Yii::t('back/import-export', 'Export configuration error: config not set');
            ImportExportHelper::addExportErrorMessage($messageLog);
            return false;
        }

        foreach ($config as $configSingle) {
            if (!isset($configSingle['label'])) {
                $messageLog = \Yii::t('back/import-export', '"label" is required for config');
                ImportExportHelper::addExportErrorMessage($messageLog);
                return false;
            }

            if (!isset($configSingle['columns'])) {
                $messageLog = \Yii::t('back/import-export', '"columns" is required for config');
                ImportExportHelper::addExportErrorMessage($messageLog);
                return false;
            }

            foreach ($configSingle['columns'] as $index => $column) {
                if ((!isset($column[self::EXPORT_COLUMN_INDEX['column_type']])) || (!in_array($column[self::EXPORT_COLUMN_INDEX['column_type']],
                        self::$exportColumnTypeArray))
                ) {
                    $message = \Yii::t('back/import-export', '"column_type" is required for config columns');
                    ImportExportHelper::addExportErrorMessage($message);
                    return false;
                }

                if (!isset($column[self::EXPORT_COLUMN_INDEX['field_or_function_in_model']])) {
                    $message = \Yii::t(
                        'back/import-export',
                        '"field_or_function_in_model" is required for config columns'
                    );
                    ImportExportHelper::addExportErrorMessage($message);
                    return false;
                }
            }
        }
        return true;
    }

    public static function getSelectedExportConfig($configLabel, $data)
    {
        //if empty data
        if (count($data) == 0) {
            $messageLog = \Yii::t('back/import-export', 'empty data for export');
            ImportExportHelper::addExportErrorMessage($messageLog);
            return false;
        }

        if (!($data[0] instanceof ActiveRecord)) {
            $messageLog = \Yii::t('back/import-export', 'export data not instance of ActiveQuery');
            ImportExportHelper::addExportErrorMessage($messageLog);
            return false;
        }

        $exportConfig = static::getExportConfig();
        $selectedConfig = null;

        foreach ($exportConfig as $singleConfig) {
            if ($singleConfig['label'] == $configLabel) {
                $selectedConfig = $singleConfig;
            }
        }

        if (!isset($selectedConfig)) {
            $messageLog = \Yii::t('back/import-export', 'export config with label')
                . " '"
                . $configLabel
                . "' "
                . \Yii::t('back/import-export', 'does not exist');
            ImportExportHelper::addExportErrorMessage($messageLog);
            return false;
        }

        return $selectedConfig;
    }
}
