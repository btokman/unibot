<?php
use yii\helpers\Html;
use \backend\components\FormBuilder;

/* @var $this yii\web\View */


$this->title = 'import products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="panel-body">

        <?php
        $translationModels = [];

        if ($model instanceof \common\components\model\Translateable) {
            $translationModels = $model->getTranslationModels();
        }
        $action = isset($action) ? $action : '';
        ?>

        <div class="menu-form">
            <?= Html::errorSummary(
                \yii\helpers\ArrayHelper::merge([$model], $translationModels),
                [
                    'class' => 'alert alert-danger'
                ]
            );
            ?>
            <?php /** @var FormBuilder $form */
            $form = FormBuilder::begin([
                'action' => $action,
                'enableClientValidation' => true,
                'options' => [
                    'id' => 'main-form',
                    'enctype' => 'multipart/form-data',
                ]
            ]); ?>

            <?php
            $items = [];

            $formConfig = $model->getFormConfig();

            if (isset($formConfig['form-set'])) {
                $i = 0;
                foreach ($formConfig['form-set'] as $tabName => $tabConfig) {
                    $class = 'tab_content_' . ++$i;
                    $items[] = [
                        'label' => $tabName,
                        'content' => $form->prepareRows($model, $tabConfig, $translationModels, true),
                        'options' => [
                            'class' => $class,
                        ],
                        'linkOptions' => [
                            'class' => $class,
                        ],
                    ];
                }
            } else {
                $items[] = [
                    'label' => 'Content',
                    'content' => $form->prepareRows($model, $formConfig, $translationModels),
                    'active' => true,
                    'options' => [
                        'class' => 'tab_content_content',
                    ],
                    'linkOptions' => [
                        'class' => 'tab_content',
                    ],
                ];
            }


            ?>

            <?= \yii\bootstrap\Tabs::widget(['items' => $items]) ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="btn-group">
                        <?= Html::submitButton(Yii::t('app', 'Upload'), ['class' => 'btn btn-success']); ?>
                    </div>
                </div>
            </div>

            <?php FormBuilder::end(); ?>

        </div>


    </div>
</div>