<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 03.05.2017
 * Time: 10:34
 */

use backend\modules\importExport\models\ImportForm;
use backend\modules\importExport\widgets\PreviewBlock\PreviewBlock;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/** @var mixed[] $selectedConfig */
/** @var mixed $dataForPreview */

$this->title =  \Yii::t('back/import-export', 'import products preview');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="panel panel-default ">
    <div class="panel-heading">
        <h1 class="panel-title inline-block"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="panel-body">
        <div id="db-import-preview-block">
            <?= PreviewBlock::widget([
                'dataForPreview' => $dataForPreview,
                'selectedConfig' => $selectedConfig,
            ]) ?>
        </div>

    </div>
</div>