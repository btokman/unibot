<?php
namespace backend\modules\importExport\components;

/**
 * Created by PhpStorm.
 * User: art
 * Date: 29.06.2017
 * Time: 16:13
 */
trait ExportTrait
{
    public static function find()
    {
        return new ExportActiveQuery(get_called_class());
    }
}
