<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 29.06.2017
 * Time: 15:56
 */

namespace backend\modules\importExport\components;

use vova07\console\ConsoleRunner;
use yii\db\ActiveQuery;
use yii\helpers\Json;

class ExportActiveQuery extends ActiveQuery
{

    public function export($configLabel, ...$additionalParams)
    {
        $consoleRunner = new ConsoleRunner(['file' => \Yii::getAlias('@app') . '/../yii']);
        //base64 for safe bash command params
        $consoleRunner->run(
            'import-export/export '
            . base64_encode(Json::encode($this))
            . ' '
            . base64_encode($configLabel)
            . ' '
            . base64_encode(Json::encode($additionalParams))
        );

        \Yii::$app->session->setFlash('success', \Yii::t(
            'back/import-export',
            'Data sent to the export. We will inform you when it is finished (message will be displayed when you refresh the page)'
        ));
    }
}
