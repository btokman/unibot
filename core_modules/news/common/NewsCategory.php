<?php

namespace common\models;

use common\components\model\Helper;
use Yii;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;

/**
 * This is the model class for table "{{%news_category}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property integer $published
 * @property integer $position
 *
 * @property NewsCategoryTranslation[] $translations
 * @property NewsToNewsCategory[] $newsToNewsCategories
 */
class NewsCategory extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_category}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(NewsCategoryTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsToNewsCategories()
    {
        return $this->hasMany(NewsToNewsCategory::className(), ['category_id' => 'id']);
    }

    public function getPageUrl($params = [])
    {
        if(!isset($params['category'])) {
            $params['category'] = $this->alias;
        }
        return static::createUrl('/news/news/index', $params);
    }

    public static function findAllCategories()
    {
        $models = static::find()
            ->isPublished()
            ->orderBy(['position' => SORT_ASC])
            ->all();
        return $models ? $models : [];
    }
}
