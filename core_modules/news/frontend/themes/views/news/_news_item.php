<?php
use metalguardian\fileProcessor\helpers\FPM;

/**
 * @author walter
 *
 * @var $model \common\models\News
 */
?>
<div>
    <a href="<?= $model->getPageUrl(); ?>" data-pjax="0">
        <img src="<?= FPM::originalSrc($model->image->file_id ?? null, 'admin', 'file'); ?>" alt="" />
        <div><?= $model->label; ?></div>
        <div><?= $model->getPublishDate(); ?></div>
    </a>
</div>
