<?php
use yii\widgets\Pjax;
use yii\widgets\ListView;
/**
 * @author walter
 *
 * @var $staticPage \common\models\NewsStaticPage
 */
?>
<h1>
    <?= $staticPage->label; ?>
</h1>
<p><?= $staticPage->description; ?></p>
<?php Pjax::begin(); ?>
<div>
    <?= \frontend\modules\news\widgets\categoriesList\Widget::widget(['activeCategoryAlias' => $activeCategoryAlias]); ?>
</div>
<div>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}{pager}',
        'itemOptions' => [
            'tag' => false
        ],
        'itemView' => '_news_item',
        'options' => [
            'tag' => 'div',
            'class' => 'links-wrapper'
        ],
    ]) ?>
</div>
<?php Pjax::end(); ?>