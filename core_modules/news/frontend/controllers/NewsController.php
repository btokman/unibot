<?php
/**
 * @author walter
 */

namespace frontend\modules\news\controllers;


use common\models\News;
use common\models\NewsCategory;
use common\models\NewsStaticPage;
use frontend\components\ConfigurationMetaTagRegister;
use frontend\components\FrontendController;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class NewsController extends FrontendController
{
    public function actionIndex()
    {
        $staticPage = (new NewsStaticPage())->get();
        $pageSize = $staticPage->paginationСount;
        $activeCategoryAlias = \Yii::$app->request->get('category');
        $dataProvider = new ActiveDataProvider([
            'query' => News::getNewsListQuery($activeCategoryAlias),
            'pagination' => [
                'pageSize' => $pageSize,
                'defaultPageSize' => $pageSize
            ],
        ]);

        ConfigurationMetaTagRegister::register($staticPage);
        return $this->render('index', [
            'staticPage' => $staticPage,
            'dataProvider' => $dataProvider,
            'activeCategoryAlias' => $activeCategoryAlias
        ]);
    }

    public function actionView($alias)
    {
        $model = News::findByAlias($alias);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        MetaTagRegister::register($model);
        return $this->render('view', ['model' => $model]);
    }
}