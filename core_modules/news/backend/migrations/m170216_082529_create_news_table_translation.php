<?php

use console\components\Migration;

/**
 * Class m170216_082529_create_news_table_translation migration
 */
class m170216_082529_create_news_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%news_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%news}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
                'content' => $this->text()->comment('Content'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-news_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-news_translation-model_id-news-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

