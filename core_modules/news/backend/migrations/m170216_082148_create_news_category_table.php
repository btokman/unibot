<?php

use console\components\Migration;

/**
 * Class m170216_082148_create_news_category_table migration
 */
class m170216_082148_create_news_category_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%news_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'alias' => $this->string()->notNull()->comment('Alias'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
