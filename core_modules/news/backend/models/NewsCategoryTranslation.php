<?php

namespace backend\modules\news\models;

use Yii;

/**
* This is the model class for table "{{%news_category_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class NewsCategoryTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%news_category_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/news-category', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/news-category', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/news-category', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
         ];
    }
}
