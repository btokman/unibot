<?php

namespace backend\modules\news\models;

use common\models\NewsToNewsCategory;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property string $content
 * @property integer $publish_date
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $published
 * @property integer $position
 *
 * @property NewsToNewsCategory[] $newsToNewsCategories
 * @property NewsCategory[] $newsCategories
 * @property NewsTranslation[] $translations
 */
class News extends ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    /**
     * Attribute for imageUploader
     */
    public $image;

    public $categories = [];

    //for filters
    public $category_id;

    /**
     * Temporary sign which used for saving images before model save
     * @var string
     */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'publish_date', 'alias'], 'required'],
            [['content'], 'string'],
            [['publish_date'], 'datetime', 'format' => 'dd.MM.yyyy HH:mm'],
            [['published', 'position'], 'integer'],
            [['label', 'alias'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
            [['categories'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/news', 'ID'),
            'label' => Yii::t('back/news', 'Label'),
            'alias' => Yii::t('back/news', 'Alias'),
            'content' => Yii::t('back/news', 'Content'),
            'publish_date' => Yii::t('back/news', 'Publish Date'),
            'created_at' => Yii::t('back/news', 'Created At'),
            'updated_at' => Yii::t('back/news', 'Updated At'),
            'published' => Yii::t('back/news', 'Published'),
            'position' => Yii::t('back/news', 'Position'),
            'image' => Yii::t('back/news', 'Image'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'content',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsToNewsCategories()
    {
        return $this->hasMany(NewsToNewsCategory::className(), ['news_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsCategories()
    {
        return $this->hasMany(NewsCategorySearch::className(), ['id' => 'category_id'])
            ->viaTable(NewsToNewsCategory::tableName(), ['news_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(NewsTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/news', 'News');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'publish_date',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return \Yii::$app->formatter->asDatetime($model->publish_date, 'medium');
                        },
                        'filter' => DatePicker::widget(
                            [
                                'model' => $this,
                                'attribute' => 'publish_date',
                                'options' => [
                                    'class' => 'form-control form-control-filters'
                                ],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd.mm.yyyy'
                                ]
                            ]
                        ),
                    ],
                    'label',
                    'alias',
                    [
                        'attribute' => 'category_id',
                        'value' => function ($model) {
                            return $model->getCategoriesString();
                        },
                        'filter' => NewsCategory::getItems(),
                    ],
                    // 'content:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'created_at',
                        'value' => Yii::$app->formatter->asDatetime($this->created_at, 'medium'),
                    ],
                    [
                        'attribute' => 'updated_at',
                        'value' => Yii::$app->formatter->asDatetime($this->updated_at, 'medium'),
                    ],
                    [
                        'attribute' => 'publish_date',
                        'value' => Yii::$app->formatter->asDatetime($this->publish_date, 'medium'),
                    ],
                    [
                        'attribute' => 'categories',
                        'value' => $this->getCategoriesString(),
                    ],
                    'label',
                    'alias',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'publish_date',
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new NewsSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'image' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'image',
                    'saveAttribute' => \common\models\News::SAVE_ATTRIBUTE_IMAGE,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => $this->isNewRecord ? 's_name form-control' : 'form-control'
                ],
            ],
            'alias' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => $this->isNewRecord ? 's_alias form-control' : 'form-control'
                ],
            ],
            'categories' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => NewsCategory::getItems(),
                'options' => [
                    'prompt' => '',
                    'multiple' => true
                ]
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'content',
                ]
            ],
            'publish_date' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DateTimePicker::className(),
                'options' => [
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy hh:ii',
                        'todayHighlight' => true,
                        'default' => time(),
                    ],
                ],
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }

    public function afterFind()
    {
        parent::afterFind();
        if ($this->publish_date) {
            $this->publish_date = Yii::$app->formatter->asDatetime($this->publish_date, 'dd.MM.yyyy HH:mm');
        }
        $this->categories = ArrayHelper::map($this->newsCategories, 'id', 'id');
    }


    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
        $this->saveCategories();
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
        $this->deleteCategories();
    }

    protected function saveCategories()
    {
        $this->deleteCategories();
        $data = [];
        if ($this->categories) {
            foreach ($this->categories as $category) {
                $data[] = [
                    'news_id' => $this->id,
                    'category_id' => $category,
                ];
            }
            if ($data) {
                \Yii::$app->db->createCommand()->batchInsert(NewsToNewsCategory::tableName(),
                    ['news_id', 'category_id'],
                    $data)->execute();
            }
        }
    }

    protected function deleteCategories()
    {
        NewsToNewsCategory::deleteAll('news_id=:id', [':id' => $this->id]);
    }

    public function beforeSave($insert)
    {
        $parent = parent::beforeSave($insert);
        if ($parent) {
            $this->publish_date = \Yii::$app->formatter->asTimestamp($this->publish_date);
        }
        return $parent;
    }

    protected function getCategoriesString()
    {
        $str = '';
        if ($this->newsCategories) {
            $array = ArrayHelper::map($this->newsCategories, 'id', 'label');
            $str = implode(', ', $array);
        }
        return $str;
    }
}
