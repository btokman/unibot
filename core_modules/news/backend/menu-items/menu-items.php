<?php

return [
    'label' => Yii::t('back/news', 'News'),
    'items' => [
        [
            'label' => Yii::t('back/news-static-page', 'News Static Page'),
            'url' => ['/news/news-static-page/update'],
        ],
        [
            'label' => Yii::t('back/news-category', 'News Category'),
            'url' => ['/news/news-category/index'],
        ],
        [
            'label' => Yii::t('back/news', 'News'),
            'url' => ['/news/news/index'],
        ],
    ]
];
