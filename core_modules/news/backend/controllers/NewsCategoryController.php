<?php

namespace backend\modules\news\controllers;

use backend\components\BackendController;
use backend\modules\news\models\NewsCategory;

/**
 * NewsCategoryController implements the CRUD actions for NewsCategory model.
 */
class NewsCategoryController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return NewsCategory::className();
    }
}
