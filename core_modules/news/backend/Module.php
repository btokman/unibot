<?php

namespace backend\modules\news;

use common\components\interfaces\CoreModuleBackendInterface;

class Module extends \yii\base\Module implements CoreModuleBackendInterface
{
    public $controllerNamespace = 'backend\modules\news\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
