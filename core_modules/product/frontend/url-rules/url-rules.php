<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 12.03.2017
 * Time: 21:14
 */
return [
    'product/categories' => 'product/product-category/index',
    'product/category/<alias>' => 'product/product-category/view',
    'product/<id>' => 'product/product/view',
];
