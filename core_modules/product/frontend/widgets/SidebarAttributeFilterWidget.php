<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 13.03.2017
 * Time: 01:02
 */

namespace frontend\modules\product\widgets;

use yii\base\Widget;

class SidebarAttributeFilterWidget extends Widget
{
     public $activeAttributeFilters;
     public $attributeFilters;
    /**
     * @return string
     */
    public function run()
    {
        $getData = \Yii::$app->request->get();
        return $this->render('_sidebarAttributeFilterWidgetView', [
            'activeAttributeFilters' => $this->activeAttributeFilters,
            'attributeFilters' => $this->attributeFilters,
            'getData' => $getData,
        ]);
    }
}
