<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 13.03.2017
 * Time: 01:02
 */

namespace frontend\modules\product\widgets;

use yii\base\Widget;

class SidebarFilterWidget extends Widget
{

     public $model;
     public $categoryBoundaryMinPrice;
     public $categoryBoundaryMaxPrice;
     public $baseFiltersForm;
     public $categoryMinPrice;
     public $categoryMaxPrice;
     public $categoryBrands;
     public $activeCategoryBrands;
     public $activeAttributeFilters;
     public $attributeFilters;
    /**
     * @return string
     */
    public function run()
    {
        $session = \Yii::$app->session;
        $getData = \Yii::$app->request->get();
        return $this->render('sidebarFilterWidgetView', [
            'model' => $this->model,
            'categoryBoundaryMinPrice' => $this->categoryBoundaryMinPrice,
            'categoryBoundaryMaxPrice' => $this->categoryBoundaryMaxPrice,
            'baseFiltersForm' => $this->baseFiltersForm,
            'categoryMinPrice' => $this->categoryMinPrice,
            'categoryMaxPrice' => $this->categoryMaxPrice,
            'categoryBrands' => $this->categoryBrands,
            'activeCategoryBrands' => $this->activeCategoryBrands,
            'activeAttributeFilters' => $this->activeAttributeFilters,
            'attributeFilters' => $this->attributeFilters,
            'session' => $session,
            'getData' => $getData,
        ]);
    }
}
