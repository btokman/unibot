<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 13.03.2017
 * Time: 01:02
 */

namespace frontend\modules\product\widgets;

use yii\base\Widget;

class SidebarBrandFilterWidget extends Widget
{
     public $categoryBrands;
     public $activeCategoryBrands;
    /**
     * @return string
     */
    public function run()
    {
        $getData = \Yii::$app->request->get();
        return $this->render('_sidebarBrandFilterWidgetView', [
            'categoryBrands' => $this->categoryBrands,
            'activeCategoryBrands' => $this->activeCategoryBrands,
            'getData' => $getData,
        ]);
    }
}
