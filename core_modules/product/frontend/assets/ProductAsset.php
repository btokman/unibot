<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace frontend\modules\product\assets;

use yii\web\AssetBundle;

class ProductAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/product/assets';
    public $css = [
        'css/product.css',
    ];
    public $js = [
        'js/product.js',
    ];

    public $depends = [
        '\frontend\assets\AppAsset',
    ];
}
