<?php

namespace frontend\modules\product\components;

use common\models\ProductAttribute;
use yii\helpers\Json;

/**
 * Created by PhpStorm.
 * User: art
 * Date: 24.07.2017
 * Time: 13:44
 */
class ProductCategoryHelper
{
    /**
     * @param $postData
     * @return array
     */
    public static function generateGetFromPost($postData)
    {
        $session = \Yii::$app->session;

        if ($postData['BaseFiltersForm']['userSetFilterFrom']) {
            $session->set('userMinPrice', $postData['BaseFiltersForm']['priceFrom']);
        }

        if ($postData['BaseFiltersForm']['userSetFilterTo']) {
            $session->set('userMaxPrice', $postData['BaseFiltersForm']['priceTo']);
        }

        $userMinPrice = $session->get('userMinPrice');
        $userMaxPrice = $session->get('userMaxPrice');

        $getData = [];
        if (isset($postData['BaseFiltersForm']['options'])) {
            foreach ($postData['BaseFiltersForm']['options'] as $attribute) {
                $attributeObj = Json::decode($attribute);

                if (isset($getData[$attributeObj['alias']])) {
                    $getData[$attributeObj['alias']] .= ',';
                } else {
                    $getData[$attributeObj['alias']] = '';
                }

                $getData[$attributeObj['alias']] .= $attributeObj['value'];
            }
        }
        if (isset($postData['BaseFiltersForm']['brands'])) {
            $getData['brands'] = '';
            foreach ($postData['BaseFiltersForm']['brands'] as $attribute) {
                $getData['brands'] .= ',';
                $getData['brands'] .= $attribute;
            }
        }

        if (isset($userMinPrice)) {
            $getData['pricefrom'] = $userMinPrice;
        }

        if (isset($userMaxPrice)) {
            $getData['priceto'] = $userMaxPrice;
        }

        return $getData;
    }

    /**
     * @param $getData
     * @return mixed
     */
    public static function generatePostFromGet($getData)
    {
        $session = \Yii::$app->session;
        $session->remove('userMinPrice');
        $session->remove('userMaxPrice');

        $ignoredParams = [
            'alias', //because category alias has same name
        ];

        $serializeData['BaseFiltersForm']['options'] = [];
        $serializeData['BaseFiltersForm']['brands'] = [];
        $serializeData['BaseFiltersForm']['priceFrom'] = [];
        $serializeData['BaseFiltersForm']['priceTo'] = [];

        foreach ($getData as $getParamAlias => $getParamValueStr) {
            $paramValue = [];

            if (in_array($getParamAlias, $ignoredParams)) {
                continue;
            }

            switch ($getParamAlias) {
                case 'brands':
                    $getParamValueArray = explode(',', $getParamValueStr);
                    foreach ($getParamValueArray as $getParamValue) {
                        array_push($serializeData['BaseFiltersForm']['brands'], $getParamValue);
                    }
                    break;

                case 'pricefrom':
                    $serializeData['BaseFiltersForm']['priceFrom'] = $getParamValueStr;
                    $session->set('userMinPrice', $getParamValueStr);
                    break;

                case 'priceto':
                    $serializeData['BaseFiltersForm']['priceTo'] = $getParamValueStr;
                    $session->set('userMaxPrice', $getParamValueStr);
                    break;

                default:
                    //attributes
                    /** @var ProductAttribute $prodAttribute */
                    $prodAttribute = ProductAttribute::find()
                        ->select(['id'])
                        ->where([
                            'alias' => $getParamAlias,
                            'published' => 1
                        ])
                        ->one();

                    $getParamValueArray = explode(',', $getParamValueStr);

                    foreach ($getParamValueArray as $getParamValue) {
                        if (isset($prodAttribute)) {
                            $paramValue['id'] = $prodAttribute->id;
                            $paramValue['value'] = $getParamValue;
                            $paramValue['alias'] = $getParamAlias;
                        }

                        array_push($serializeData['BaseFiltersForm']['options'], Json::encode($paramValue));
                    }
                    break;
            }
        }

        return $serializeData;
    }
}
