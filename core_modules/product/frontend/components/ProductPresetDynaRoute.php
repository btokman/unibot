<?php

namespace frontend\modules\product\components;

use yii\base\BootstrapInterface;

class ProductPresetDynaRoute implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $pages = ProductPresetHelper::getProductPresetPages();
        $routeArray = [];
        foreach ($pages as $page){
            $routeArray[$page->alias] = 'product/product-category/view'; // Adding rules to array on by one
        }

        $app->urlManager->addRules($routeArray);// Append new rules to original rules
    }
}
