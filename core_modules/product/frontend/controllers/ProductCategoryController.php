<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 12.03.2017
 * Time: 21:17
 */

namespace frontend\modules\product\controllers;

use common\models\ProductCategory;
use frontend\components\FrontendController;
use frontend\modules\product\assets\ProductAsset;
use frontend\modules\product\components\ProductCategoryHelper;
use frontend\modules\product\components\ProductPresetHelper;
use frontend\modules\product\helpers\NestedSetHelper;
use frontend\modules\product\models\BaseFiltersForm;
use frontend\modules\product\Module;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ProductCategoryController extends FrontendController
{

    public function actionIndex()
    {
        $roots = ProductCategory::find()->andWhere(['published' => 1])->orderBy('root, left')->roots()->all();
        $allCategories = ProductCategory::find()->where(['published' => 1])->orderBy('root, left')->all();

        $navXConfig = NestedSetHelper::buildNavXConfig($roots);
        $categoriesArray = NestedSetHelper::buildCategoryArray($roots);


        return $this->render(
            'index',
            [
                'allCategories' => $allCategories,
                'navXConfig' => $navXConfig,
                'categoriesArray' => $categoriesArray,
            ]
        );
    }

    /**
     * @param null $alias
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($alias = null)
    {
        ProductAsset::register(\Yii::$app->view);
        $module = \Yii::$app->getModule('product');
        $postData = \Yii::$app->request->post();

        //generate GET when ajax
        if (\Yii::$app->request->isAjax) {
            $_GET = ProductCategoryHelper::generateGetFromPost($postData);
        }

        //Preset pages
        $isPresetPage = false;
        $presetPageData = ProductPresetHelper::checkPresetPage();
        if ($presetPageData !== false) {
            if ($presetPageData instanceof Response) {
                return $presetPageData;
            }
            $isPresetPage = true;
            $_GET = $presetPageData['get_data'];
            $alias = $presetPageData['alias'];
            $pageLabel = $presetPageData['page_label'];
        }

        $getData = \Yii::$app->request->get();

        /** @var ProductCategory $model */
        $model = ProductCategory::findSingleCategory($alias);
        if (!$model) {
            throw new NotFoundHttpException();
        }

        if (!$isPresetPage) {
            MetaTagRegister::register($model);
            $pageLabel = $model->label;
        }

        $baseFiltersForm = new BaseFiltersForm();

        //settings
        $perPage = ProductCategory::DEFAULT_PRODUCT_PER_PAGE;

        //get data
        $query = $model->getBaseProductQuery();

        //show/hide packing in catalog
        $itemView = ProductCategory::VIEW_TYPE_PRODUCT;
        if ($module->params['packingType'] == Module::PACKING_AS_OPTION) {
            $query->andWhere([
                'show_as_separate' => 1
            ]);

            $itemView = ProductCategory::VIEW_TYPE_PRODUCT_AND_PACKING;
        }

        //apply base filter
        //if first load page and get data from GET params
        if (count($postData) == 0) {
            $postData = ProductCategoryHelper::generatePostFromGet($getData);
        }
        //if base filter data send as POST
        if ($baseFiltersForm->load($postData) && $baseFiltersForm->validate()) {
            $query = $baseFiltersForm->setFilter($query);
        }

        //get brands
        $categoryBrands = $model->getCategoryBrands();
        $activeCategoryBrands = $model->getCategoryBrands($query);

        //get sidebar filters
        $attributeFilters = $model->getAttributesValue();
        $activeAttributeFilters = $model->getAttributesValue($query);

        //get min/max price
        $categoryMinPrice = $model->getMinPrice($query);
        $categoryMaxPrice = $model->getMaxPrice($query);
        $categoryBoundaryMinPrice = $model->getMinPrice($query, true);
        $categoryBoundaryMaxPrice = $model->getMaxPrice($query, true);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $perPage,

            ],
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'attributeFilters' => $attributeFilters,
            'activeAttributeFilters' => $activeAttributeFilters,
            'baseFiltersForm' => $baseFiltersForm,
            'itemView' => $itemView,
            'getData' => $getData,
            'pageLabel' => $pageLabel ?? '',
            'categoryBrands' => $categoryBrands,
            'activeCategoryBrands' => $activeCategoryBrands,
            'categoryMinPrice' => $categoryMinPrice,
            'categoryMaxPrice' => $categoryMaxPrice,
            'categoryBoundaryMinPrice' => $categoryBoundaryMinPrice,
            'categoryBoundaryMaxPrice' => $categoryBoundaryMaxPrice,
        ]);
    }
}
