<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 12.03.2017
 * Time: 21:17
 */
namespace frontend\modules\product\controllers;

use common\models\Product;
use frontend\components\FrontendController;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use yii\web\NotFoundHttpException;

class ProductController extends FrontendController
{
    public function actionView($id = null)
    {
        $model = Product::findSingleProduct($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        MetaTagRegister::register($model);

        return $this->render('view', [
            'model' => $model,
        ]);
    }
}
