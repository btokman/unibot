<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 13.03.2017
 * Time: 01:04
 */

use common\models\ProductCategory;
use yii\helpers\Html;

$level = 0;
/** @var ProductCategory[] $models */
foreach ($models as $n => $category) {
    if ($category->depth == $level) {
        echo Html::endTag('li');
    } else {
        if ($category->depth > $level) {
            echo Html::beginTag('ul');
        } else {
            echo Html::endTag('li');
            for ($i = $level - $category->depth; $i; $i--) {
                echo Html::endTag('ul');
                echo Html::endTag('li');
            }
        }
    }

    echo Html::beginTag('li');
    echo Html::a($category->label, ProductCategory::getSingleUrl(['alias' => $category->alias]));
    $level = $category->depth;
}

for ($i = $level; $i; $i--) {
    echo Html::endTag('li');
    echo Html::endTag('ul');
}
