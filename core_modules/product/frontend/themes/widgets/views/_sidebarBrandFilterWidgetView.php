<?php
use yii\helpers\Html;
?>
<b>Brands</b>
<ul>
    <?php
    foreach ($categoryBrands as $brand):

        $checked = false;
        $disabled = true;
        $selectedFilter = $getData['brands'] ?? null;
        if (isset($selectedFilter)) {
            $disabled = false;
            $selectedFilter = explode(',', urldecode($selectedFilter));
            if (in_array($brand['id'], $selectedFilter)) {
                $checked = true;
            }
        }

        //if checkbox in one block with active or it in activeAttributeList
        foreach ($activeCategoryBrands as $activeBrand) {
            if ($activeBrand['id'] == $brand['id']) {
                $disabled = false;
            }
        }
        ?>
        <li>
            <?=
            Html::beginTag('div', ['class' => 'checkbox']) .
            Html::checkbox('BaseFiltersForm[brands][]', $checked, [
                'id' => 'brand' . $brand['id'],
                'value' => $brand['id'],
                'label' => $brand['label'],
                'data-class' => 'base-filter',
                'uncheck' => null,
                'disabled' => $disabled,
            ]) .
            Html::endTag('div');
            ?>
        </li>
    <?php endforeach; ?>
</ul>
