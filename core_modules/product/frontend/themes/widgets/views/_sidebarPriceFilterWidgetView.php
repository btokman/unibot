<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 24.07.2017
 * Time: 17:20
 */
?>
<b>Price</b>
<div class="row">
    <div class="col-md-6">
        <?php
        $inputMinValue = $getData['pricefrom'] ?? $categoryBoundaryMinPrice;

        if ($inputMinValue < $categoryBoundaryMinPrice) {
            $inputMinValue = $categoryBoundaryMinPrice;
        }
        echo $form->field($baseFiltersForm, 'priceFrom',
            [
                'template' => '{input}',
                'inputOptions' => [
                    'value' => $inputMinValue,
                    'data-class' => 'base-filter-price-from',
                    'init-price-from' => $categoryMinPrice,
                    'boundary-price' => $categoryBoundaryMinPrice,
                ]
            ]) ?>
        <span>min: <?= $categoryBoundaryMinPrice ?></span>

    </div>
    <div class="col-md-6">

        <?php
        $inputMaxValue = $getData['priceto'] ?? $categoryBoundaryMaxPrice;
        if ($inputMaxValue > $categoryBoundaryMaxPrice) {
            $inputMaxValue = $categoryBoundaryMaxPrice;
        }
        echo $form->field($baseFiltersForm, 'priceTo',
            [
                'template' => '{input}',
                'inputOptions' => [
                    'value' => $inputMaxValue,
                    'data-class' => 'base-filter-price-to',
                    'init-price-to' => $categoryMaxPrice,
                    'boundary-price' => $categoryBoundaryMaxPrice,
                ]
            ])

        ?>
        <span>max: <?= $categoryBoundaryMaxPrice ?></span>
    </div>
    <?=
    //auxiliary field for url generation (is user set price in current filter change action)
    $form->field($baseFiltersForm, 'userSetFilterFrom', [
        'inputOptions' => [
            'id' => 'user-set-filter-from',
            'value' => 0,
        ],

    ])->label(false)->hiddenInput();
    ?>
    <?=
    //auxiliary field for url generation (is user set price in current filter change action)
    $form->field($baseFiltersForm, 'userSetFilterTo', [
        'inputOptions' => [
            'id' => 'user-set-filter-to',
            'value' => 0,
        ],

    ])->label(false)->hiddenInput();
    ?>

    <?=
    //auxiliary field for url generation (price that user set)
    $form->field($baseFiltersForm, 'userMinPrice', [
        'inputOptions' => [
            'id' => 'user-min-price',
            'value' => $session->get('userMinPrice') ?? '',
        ],

    ])->label(false)->hiddenInput();
    ?>
    <?=
    //auxiliary field for url generation (price that user set)
    $form->field($baseFiltersForm, 'userMaxPrice', [
        'inputOptions' => [
            'id' => 'user-max-price',
            'value' => $session->get('userMaxPrice') ?? '',
        ],

    ])->label(false)->hiddenInput();
    ?>
</div>
