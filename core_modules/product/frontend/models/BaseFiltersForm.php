<?php

namespace frontend\modules\product\models;

use common\components\model\ActiveQuery;
use common\models\ProductAttribute;
use common\models\ProductEav;
use yii\base\Model;
use yii\helpers\Json;

/**
 * Created by PhpStorm.
 * User: art
 * Date: 25.01.2017
 * Time: 13:04
 */
class BaseFiltersForm extends Model
{
    /**
     * @var mixed[] list of selected filters
     */
    public $options;
    /**
     * @var mixed[] list of selected brands
     */
    public $brands;

    /**
     * @var integer price from
     */
    public $priceFrom;
    /**
     * @var integer price to
     */
    public $priceTo;

    /**
     * @var bool is user set filter from
     */
    public $userSetFilterFrom;
    /**
     * @var bool is user set filter to
     */
    public $userSetFilterTo;


    public $userMinPrice;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['priceFrom', 'priceTo','userSetFilterFrom','userSetFilterTo'], 'integer'],
            [['options','brands'], 'safe'],
        ];
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @return mixed
     */
    public function setFilter($query)
    {
        $session = \Yii::$app->session;
        $postData = \Yii::$app->request->post();

        //get data from previous user action
        $userMinPrice = $session->get('userMinPrice');
        $userMaxPrice = $session->get('userMaxPrice');

        //brands filter
        $query->andFilterWhere(['product.brand_id' => $this->brands]);

        //price filter if user set or it's first page loading
        if (count($postData) == 0 ){
            $query->andFilterWhere(['>=', 'product.price', $this->priceFrom]);
            $query->andFilterWhere(['<=', 'product.price', $this->priceTo]);
        }
        if (isset($userMinPrice)){
            $query->andFilterWhere(['>=', 'product.price', $userMinPrice]);
        }
        if (isset($userMaxPrice)){
            $query->andFilterWhere(['<=', 'product.price', $userMaxPrice]);
        }

        //options filter
        if ((isset($this->options)) && (count($this->options) > 0)) {
            //select packing by filter
            $packingArrayQuery = ProductEav::find()
                ->select(['product_eav.product_id', 'COUNT(product_eav.product_id ) as filter_count']);

            $queryOrArray = [];
            $queryOrArray[] = 'or';

            $uniqueOptions = [];

            for ($s = 0; $s < count($this->options); $s++) {
                $optionArray = json_decode($this->options[$s]);

                if (!in_array($optionArray->id, $uniqueOptions)) {
                    $uniqueOptions[] = $optionArray->id;
                }

                $queryOrArray[] = [
                    'product_eav.attribute_id' => $optionArray->id,
                    'LOWER(product_eav.value)' => strtolower($optionArray->value)
                ];

                if ($s != (count($this->options) - 1)) {
                    $nextOptionArray = json_decode($this->options[$s + 1]);
                    if ($nextOptionArray->id != $optionArray->id) {
                        $packingArrayQuery->andWhere([
                            'product_id' => ProductEav::find()->select(['product_id'])->andFilterWhere($queryOrArray)
                        ]);
                        $queryOrArray = [];
                        $queryOrArray[] = 'or';
                    }
                }
            }

            if (count($queryOrArray) > 1) {
                $packingArrayQuery->andWhere([
                    'product_id' => ProductEav::find()->select(['product_id'])->andFilterWhere($queryOrArray)
                ]);
            }

            $packingArrayQuery->addGroupBy('product_eav.product_id');
            $packingArrayData = $packingArrayQuery->asArray()->all();

            $packingArray = [];
            foreach ($packingArrayData as $item) {
                $packingArray[] = $item['product_id'];
            }
            $query->andWhere(['product.id' => $packingArray]);
        }

        return $query;
    }

}
