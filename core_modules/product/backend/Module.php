<?php

namespace backend\modules\product;

use backend\modules\product\assets\ProductAsset;
use common\components\interfaces\CoreModuleBackendInterface;

class Module extends \yii\base\Module implements CoreModuleBackendInterface
{
    public $controllerNamespace = 'backend\modules\product\controllers';

    // public $usePacking = false;


    public function init()
    {
        parent::init();

        ProductAsset::register(\Yii::$app->view);
        // custom initialization code goes here
        \Yii::configure($this, require(\Yii::getAlias('@common') . '/models/ProductConfig.php'));
    }
}
