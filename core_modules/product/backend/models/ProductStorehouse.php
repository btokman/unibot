<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use backend\modules\product\assets\ProductAsset;
use backend\modules\product\widgets\oneToManyWidget\OneToManyWidget;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%product_storehouse}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $address
 * @property string $work_time
 * @property double $lat
 * @property double $lng
 * @property string $short_desc
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductStorehouseTranslation[] $translations
 */
class ProductStorehouse extends ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    public $phones;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_storehouse}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['lat', 'lng'], 'number'],
            [['content'], 'string'],
            [['published', 'position'], 'integer'],
            [['label', 'address', 'work_time', 'short_desc'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['phones'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-storehouse', 'ID'),
            'label' => Yii::t('back/product-storehouse', 'Label'),
            'address' => Yii::t('back/product-storehouse', 'Address'),
            'work_time' => Yii::t('back/product-storehouse', 'Work time'),
            'lat' => Yii::t('back/product-storehouse', 'Latitude'),
            'lng' => Yii::t('back/product-storehouse', 'Longitude'),
            'short_desc' => Yii::t('back/product-storehouse', 'Short Description'),
            'content' => Yii::t('back/product-storehouse', 'Content'),
            'published' => Yii::t('back/product-storehouse', 'Published'),
            'position' => Yii::t('back/product-storehouse', 'Position'),
            'created_at' => Yii::t('back/product-storehouse', 'Created At'),
            'updated_at' => Yii::t('back/product-storehouse', 'Updated At'),
            'phones' => Yii::t('back/product-storehouse', 'Phones'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'address',
            'work_time',
            'short_desc',
            'content',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductStorehouseTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-storehouse', 'Product Storehouse');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'address',
                    'work_time',
                    'short_desc',
                    // 'content:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'address',
                    'work_time',
                    'lat',
                    'lng',
                    'short_desc',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductStorehouseSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $mapId = uniqid();

        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'address' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'work_time' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            [
                'label' => false,
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => '<div id="product_map_' . $mapId . '" class="product_map"></div>'
            ],
            'lat' => [
                'label' => false,
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'readonly' => true,
                    'id' => 'product_map_lat_' . $mapId,
                    'class' => 'form-control',
                    'value' => $this->isNewRecord ? '50.450509053586615' : $this->lat
                ],
            ],
            'lng' => [
                'label' => false,
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'hint' => Yii::t(
                    'back/product-storehouse',
                    'To fill parameters of latitude and longitude, just click on the map'
                ),
                'options' => [
                    'readonly' => true,
                    'maxlength' => true,
                    'id' => 'product_map_lng_' . $mapId,
                    'class' => 'form-control',
                    'value' => $this->isNewRecord ? '30.5255126953125' : $this->lng
                ],
            ],
            'phones' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => OneToManyWidget::className(),
                'options' => [
                    'data' => $this->getPhones()
                ]
            ],
            'short_desc' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'content',
                ]
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }

    public function getPhones()
    {
        return ArrayHelper::map(
            ProductStorehousePhone::find()
                ->where(['storehouse_id' => $this->id])
                ->orderBy(['position' => SORT_ASC])
                ->all(),
            'id',
            'phone'
        );
    }


    public function afterSave($insert, $changedAttributes)
    {
        ProductStorehousePhone::deleteAll([
            'storehouse_id' => $this->id
        ]);
        //save phones
        $position = 0;
        foreach ($this->phones as $phone) {
            if ($phone != '') {
                $newPhone = new ProductStorehousePhone();
                $newPhone->phone = $phone;
                $newPhone->storehouse_id = $this->id;
                $newPhone->position = $position;
                $newPhone->save();

                $position++;
            }
        }
    }
}
