<?php

namespace backend\modules\product\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%product_to_similar}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $similar_id
 *
 * @property Product $product
 * @property Product $similar
 */
class ProductToSimilar extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_to_similar}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'similar_id'], 'required'],
            [['product_id', 'similar_id'], 'integer'],
            [['product_id'], 'exist', 'targetClass' => \common\models\Product::className(), 'targetAttribute' => 'id'],
            [['similar_id'], 'exist', 'targetClass' => \common\models\Product::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-to-similar', 'ID'),
            'product_id' => Yii::t('back/product-to-similar', 'Product'),
            'similar_id' => Yii::t('back/product-to-similar', 'Similar product'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSimilar()
    {
        return $this->hasOne(Product::className(), ['id' => 'similar_id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-to-similar', 'Product To Similar');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'product_id',
                    'similar_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'product_id',
                    'similar_id',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductToSimilarSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'product_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Product::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'similar_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Product::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
        ];

        return $config;
    }
}
