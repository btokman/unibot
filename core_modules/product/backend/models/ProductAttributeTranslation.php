<?php

namespace backend\modules\product\models;

use Yii;

/**
* This is the model class for table "{{%product_attribute_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class ProductAttributeTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%product_attribute_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/product-attribute', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/product-attribute', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/product-attribute', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
         ];
    }
}
