<?php

namespace backend\modules\product\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product_category_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 * @property string $short_desc
 * @property string $content
 */
class ProductCategoryTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_category_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/product-category', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/product-category', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/product-category', 'Label') . ' [' . $this->language . ']',
            'short_desc' => Yii::t('back/product-category', 'Short description') . ' [' . $this->language . ']',
            'content' => Yii::t('back/product-category', 'Content') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['label', 'short_desc'], 'string', 'max' => 255],
        ];
    }
}
