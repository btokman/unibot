<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use common\components\model\ActiveRecord;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;

/**
 * This is the model class for table "{{%product_category_to_attribute_group}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $attribute_group_id
 *
 * @property ProductCategory $category
 * @property ProductAttributeGroup $attributeGroup
 */
class ProductCategoryToAttributeGroup extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_category_to_attribute_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'attribute_group_id'], 'required'],
            [['category_id', 'attribute_group_id'], 'integer'],
            [
                ['category_id'],
                'exist',
                'targetClass' => \common\models\ProductCategory::className(),
                'targetAttribute' => 'id'
            ],
            [
                ['attribute_group_id'],
                'exist',
                'targetClass' => \common\models\ProductAttributeGroup::className(),
                'targetAttribute' => 'id'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-category-to-attribute-group', 'ID'),
            'category_id' => Yii::t('back/product-category-to-attribute-group', 'Category'),
            'attribute_group_id' => Yii::t('back/product-category-to-attribute-group', 'Attribute group'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeGroup()
    {
        return $this->hasOne(ProductAttributeGroup::className(), ['id' => 'attribute_group_id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-category-to-attribute-group', 'Product Category To Attribute Group');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'category_id',
                    'attribute_group_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'category_id',
                    'attribute_group_id',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductCategoryToAttributeGroupSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'category_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ProductCategory::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'attribute_group_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ProductAttributeGroup::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
        ];

        return $config;
    }
}
