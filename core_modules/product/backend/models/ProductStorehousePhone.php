<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use common\components\model\ActiveRecord;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;

/**
 * This is the model class for table "{{%product_storehouse_phone}}".
 *
 * @property integer $id
 * @property string $phone
 * @property integer $storehouse_id
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductStorehouse $storehouse
 */
class ProductStorehousePhone extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_storehouse_phone}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'storehouse_id'], 'required'],
            [['storehouse_id', 'position'], 'integer'],
            [['phone'], 'string', 'max' => 255],
            [
                ['storehouse_id'],
                'exist',
                'targetClass' => \common\models\ProductStorehouse::className(),
                'targetAttribute' => 'id'
            ],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-strorehouse-phone', 'ID'),
            'phone' => Yii::t('back/product-strorehouse-phone', 'Phone'),
            'storehouse_id' => Yii::t('back/product-strorehouse-phone', 'Storehouse'),
            'position' => Yii::t('back/product-strorehouse-phone', 'Position'),
            'created_at' => Yii::t('back/product-strorehouse-phone', 'Created At'),
            'updated_at' => Yii::t('back/product-strorehouse-phone', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorehouse()
    {
        return $this->hasOne(ProductStorehouse::className(), ['id' => 'storehouse_id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-strorehouse-phone', 'Product Storehouse Phone');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'phone',
                    'storehouse_id',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'phone',
                    'storehouse_id',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductStorehousePhoneSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'phone' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'storehouse_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ProductStorehouse::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }
}
