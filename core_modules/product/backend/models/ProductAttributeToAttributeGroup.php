<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use common\components\model\ActiveRecord;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;

/**
 * This is the model class for table "{{%product_attribute_to_attribute_group}}".
 *
 * @property integer $id
 * @property integer $attribute_id
 * @property integer $attribute_group_id
 */
class ProductAttributeToAttributeGroup extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_attribute_to_attribute_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id', 'attribute_group_id'], 'required'],
            [['attribute_id', 'attribute_group_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-attribute-to-attribute-group', 'ID'),
            'attribute_id' => Yii::t('back/product-attribute-to-attribute-group', 'Attribute'),
            'attribute_group_id' => Yii::t('back/product-attribute-to-attribute-group', 'Attribute Group'),
        ];
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-attribute-to-attribute-group', 'Product Attribute To Attribute Group');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'attribute_id',
                    'attribute_group_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'attribute_id',
                    'attribute_group_id',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductAttributeToAttributeGroupSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'attribute_id' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'attribute_group_id' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }
}
