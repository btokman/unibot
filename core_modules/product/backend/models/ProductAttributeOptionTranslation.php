<?php

namespace backend\modules\product\models;

use Yii;

/**
* This is the model class for table "{{%product_attribute_option_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class ProductAttributeOptionTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%product_attribute_option_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/product-attribute-option', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/product-attribute-option', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/product-attribute-option', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
         ];
    }
}
