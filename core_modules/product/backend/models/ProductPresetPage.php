<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use common\components\model\ActiveRecord;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%product_preset_page}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $alias
 * @property string $origin_url
 *
 */
class ProductPresetPage extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_preset_page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'alias', 'origin_url'], 'required'],
            [['published'], 'integer'],
            [['origin_url'], 'url'],
            [['label', 'alias', 'origin_url'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
                ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => \Yii::t('back/product-preset-page', 'Label'),
            'published' => \Yii::t('back/product-preset-page', 'Published'),
            'created_at' => \Yii::t('back/product-preset-page', 'Created At'),
            'updated_at' => \Yii::t('back/product-preset-page', 'Updated At'),
            'alias' => \Yii::t('back/product-preset-page', 'Alias'),
            'origin_url' => \Yii::t('back/product-preset-page', 'Origin URL'),
        ];
    }


    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }


    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/product-preset-page', 'Preset Page');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'published:boolean',
                    'alias',
                    'origin_url:url',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    'published:boolean',
                    'alias',
                    'origin_url:url',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ProductPresetPageSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_name form-control'
                ],
            ],
            'alias' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_alias form-control'
                ],
                'hint' => ''
            ],
            'origin_url' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


}
