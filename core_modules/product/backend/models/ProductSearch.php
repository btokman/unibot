<?php

namespace backend\modules\product\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProductSearch represents the model behind the search form about `Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_packing', 'parent_product_id', 'brand_id', 'manufacturer_id', 'availability_id', 'price_type', 'rating_voice_count', 'show_as_separate', 'published', 'position'], 'integer'],
            [['packing_link_text', 'sku', 'label', 'alias', 'short_desc', 'content'], 'safe'],
            [['price', 'old_price', 'rating_total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_packing' => $this->is_packing,
            'parent_product_id' => $this->parent_product_id,
            'brand_id' => $this->brand_id,
            'manufacturer_id' => $this->manufacturer_id,
            'availability_id' => $this->availability_id,
            'price' => $this->price,
            'old_price' => $this->old_price,
            'price_type' => $this->price_type,
            'rating_total' => $this->rating_total,
            'rating_voice_count' => $this->rating_voice_count,
            'show_as_separate' => $this->show_as_separate,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'packing_link_text', $this->packing_link_text])
            ->andFilterWhere(['like', 'sku', $this->sku])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'short_desc', $this->short_desc])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
