<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductAttributeOption;

/**
 * ProductAttributeOptionController implements the CRUD actions for ProductAttributeOption model.
 */
class ProductAttributeOptionController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductAttributeOption::className();
    }
}
