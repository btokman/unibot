<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductEav;

/**
 * ProductEavController implements the CRUD actions for ProductEav model.
 */
class ProductEavController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductEav::className();
    }
}
