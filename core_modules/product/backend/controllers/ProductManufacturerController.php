<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductManufacturer;

/**
 * ProductManufacturerController implements the CRUD actions for ProductManufacturer model.
 */
class ProductManufacturerController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductManufacturer::className();
    }
}
