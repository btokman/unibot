<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductToStorehouse;

/**
 * ProductToStorehouseController implements the CRUD actions for ProductToStorehouse model.
 */
class ProductToStorehouseController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductToStorehouse::className();
    }
}
