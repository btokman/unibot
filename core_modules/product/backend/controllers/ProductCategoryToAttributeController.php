<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductCategoryToAttribute;

/**
 * ProductCategoryToAttributeController implements the CRUD actions for ProductCategoryToAttribute model.
 */
class ProductCategoryToAttributeController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductCategoryToAttribute::className();
    }
}
