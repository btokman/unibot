<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductPresetPage;

/**
 * ProductPresetPageController implements the CRUD actions for ProductPresetPage model.
 */
class ProductPresetPageController extends BackendController
{

    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductPresetPage::className();
    }
}
