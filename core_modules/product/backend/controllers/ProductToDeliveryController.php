<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductToDelivery;

/**
 * ProductToDeliveryController implements the CRUD actions for ProductToDelivery model.
 */
class ProductToDeliveryController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductToDelivery::className();
    }
}
