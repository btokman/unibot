<?php
/**
 * Author: art
 */
namespace backend\modules\product\widgets\oneToManyWidget;

use yii\base\Widget;

/**
 * Class Widget
 */
class OneToManyWidget extends Widget
{
    /**
     * @var array
     */
    public $model;
    public $attribute;
    public $data;

    /**
     * @return string
     */
    public function run()
    {
        if (count($this->data) == 0) {
            $this->data[0] = '';
        }
        return $this->render(
            'oneToManyWidgetView',
            [
                'data' => $this->data,
                'model' => $this->model,
                'attribute' => $this->attribute,
            ]
        );
    }
}
