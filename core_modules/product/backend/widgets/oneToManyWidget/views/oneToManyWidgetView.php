<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 26.02.2017
 * Time: 16:58
 */

/** @var mixed $attribute */
use yii\bootstrap\Html;
use yii\helpers\StringHelper;

echo Html::beginTag('table', ['id' => 'package-product-table', 'class' => 'table']);

$i = 0;
foreach ($data as $key => $value) :
    $removeBtnClassString = 'glyphicon glyphicon-remove text-danger remove-entity-btn pointer';
    if ($i == 0) {
        $removeBtnClassString = 'glyphicon glyphicon-remove text-danger remove-entity-btn pointer one-to-many-hidden';
    }
    echo Html::beginTag('tr', ['class' => 'tr_clone']);

    echo Html::beginTag('td');
    echo Html::input('text', StringHelper::basename($model->className()) . '[' . $attribute . '][]', $value,
        ['class' => 'full_width']);
    echo Html::endTag('td');


    echo Html::beginTag('td');
    echo Html::tag('i', '', ['class' => 'glyphicon glyphicon-plus add-entity-btn pointer']);
    echo '&nbsp;';
    echo Html::tag('i', '',
        ['class' => $removeBtnClassString]);

    echo Html::endTag('td');

    echo Html::endTag('tr');
    $i++;
endforeach;

echo Html::endTag('table');
