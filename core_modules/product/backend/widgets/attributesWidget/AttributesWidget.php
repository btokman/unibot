<?php
/**
 * Author: art
 */
namespace backend\modules\product\widgets\attributesWidget;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * Class Widget
 */
class AttributesWidget extends Widget
{
    /**
     * @var array
     */
    public $model;
    public $attribute;
    public $showTable = true;
    public $className = '';


    /**
     * @return string
     */
    public function run()
    {
        $this->className = StringHelper::basename(get_class($this->model));

        $groupName = $this->className.'[attributeGroupsSelected]';
        $attributeName = $this->className.'[attributesSelected]';
        $attributeId = \Yii::$app->security->generateRandomString();
        $groupId = \Yii::$app->security->generateRandomString();
        $attributeTableId = \Yii::$app->security->generateRandomString(); //use sign for universal variant

        //load groups
        $selectedGroups = $this->model->attributeGroups;
        $selectedAttributeGroups = ArrayHelper::map($selectedGroups, 'id', 'label');
        $selectedAttributeGroupsId = array_keys($selectedAttributeGroups);

        //load attributes list
        $selectedAttributes = $this->model->attributesObj;
        $selectedAttributesValue = ArrayHelper::map($selectedAttributes, 'id', 'label');
        $selectedAttributesId = array_keys($selectedAttributesValue);

        return $this->render(
            'attributesWidgetView',
            [
                'model' => $this->model,
                'attribute' => $this->attribute,
                'groupName' => $groupName,
                'attributeName' => $attributeName,
                'attributeId' => $attributeId,
                'attributeTableId' => $attributeTableId,
                'groupId' => $groupId,
                'attributeGroupUrl' => self::getAttributeGroupUrl(),
                'attributeUrl' => self::getAttributeUrl(),
                'attributesByGroupUrl' => self::getAttributesByGroupUrl(),
                'attributesByIdUrl' => self::getAttributesByIdUrl(),
                'getValueUrl' => self::getAttributeValueUrl(),
                'selectedAttributeGroups' => $selectedAttributeGroups,
                'selectedAttributeGroupsId' => $selectedAttributeGroupsId,
                'selectedAttributes' => $selectedAttributesValue,
                'selectedAttributesId' => $selectedAttributesId,
                'showTable' => $this->showTable,
                'className' => $this->className
            ]
        );
    }

    private function getAttributeGroupUrl()
    {
        return Url::toRoute('/product/product-attribute-group/get-select-items');
    }

    private function getAttributeUrl()
    {
        return Url::toRoute('/product/product-attribute/get-select-items');
    }

    private function getAttributesByGroupUrl()
    {
        return Url::toRoute('/product/product-attribute/get-by-group');
    }

    private function getAttributesByIdUrl()
    {
        return Url::toRoute('/product/product-attribute/get-by-id');
    }

    private function getAttributeValueUrl()
    {
        return Url::toRoute('/product/product-attribute/get-attribute-value');
    }
}
