<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 01.03.2017
 * Time: 14:12
 */
use backend\modules\product\models\ProductCategory;
use kartik\tree\Module;
use kartik\tree\TreeView;

echo TreeView::widget([
    // single query fetch to render the tree
    // use the Product model you have in the previous step
    'query' => ProductCategory::find()->addOrderBy('root, left'),
    'headingOptions' => ['label' => 'Categories'],
    'fontAwesome' => false,     // optional
    'isAdmin' => false,         // optional (toggle to enable admin mode)
    'displayValue' => 1,        // initial display value
    'softDelete' => true,       // defaults to true
    'cacheSettings' => [
        'enableCache' => true   // defaults to true
    ],
    'iconEditSettings' => [
        'show' => 'none',
    ],
    'nodeAddlViews' => [
        Module::VIEW_PART_2 => '@app/modules/product/views/product-category/categoryFields'
    ]

]);
