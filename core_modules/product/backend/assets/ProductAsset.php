<?php
/**
 * Created by PhpStorm.
 * User: art
 */

namespace backend\modules\product\assets;

use yii\web\AssetBundle;

class ProductAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/product/assets';
    public $css = [
        'css/product.css',
    ];
    public $js = [
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyBz8af6CfuTJsndZYBf_YGFOzbgAI3g_4A',
        'js/product.js',
    ];

    public $depends = [
        '\backend\assets\AppAsset',
        '\kartik\select2\Select2Asset',
    ];
}
