<?php

use console\components\Migration;

/**
 * Class m170310_140002_create_product_table_translation migration
 */
class m170310_140002_create_product_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%product_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%product}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'packing_link_text' => $this->string()->comment('Packing link text'),
                'label' => $this->string()->comment('Label'),
                'short_desc' => $this->string()->comment('Short description'),
                'content' => $this->text()->comment('Content'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-product_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-product_translation-model_id-product-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
