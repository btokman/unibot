<?php

use console\components\Migration;

/**
 * Class m170327_170620_create_product_category_to_attribute_group_table migration
 */
class m170327_170620_create_product_category_to_attribute_group_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_category_to_attribute_group}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'category_id' => $this->integer()->notNull()->comment('Category'),
                'attribute_group_id' => $this->integer()->notNull()->comment('Attribute group'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk-category_to_group-category_id-category-id',
            $this->tableName,
            'category_id',
            '{{%product_category}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-category_to_group_group_id-product_attribute_group-id',
            $this->tableName,
            'attribute_group_id',
            '{{%product_attribute_group}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-category_to_group-category_id-category-id', $this->tableName);
        $this->dropForeignKey('fk-category_to_group_group_id-product_attribute_group-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
