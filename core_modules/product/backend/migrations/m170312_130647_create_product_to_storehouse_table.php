<?php

use console\components\Migration;

/**
 * Class m170312_130647_create_product_to_storehouse_table migration
 */
class m170312_130647_create_product_to_storehouse_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_to_storehouse}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'product_id' => $this->integer()->notNull()->comment('Product'),
                'storehouse_id' => $this->integer()->notNull()->comment('Storehouse'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-product_to_storehouse-product_id-product-id',
            $this->tableName,
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-product_to_storehouse-storehouse_id-product_storehouse-id',
            $this->tableName,
            'storehouse_id',
            '{{%product_storehouse}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_to_storehouse-product_id-product-id', $this->tableName);
        $this->dropForeignKey('fk-product_to_storehouse-storehouse_id-product_storehouse-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
