<?php

use console\components\Migration;

/**
 * Class m170312_155451_create_product_to_similar_table migration
 */
class m170312_155451_create_product_to_similar_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_to_similar}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'product_id' => $this->integer()->notNull()->comment('Product'),
                'similar_id' => $this->integer()->notNull()->comment('Similar product'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-product_to_similar-product_id-product-id',
            $this->tableName,
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-product_to_similar-similar_id-product-id',
            $this->tableName,
            'similar_id',
            '{{%product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_to_similar-product_id-product-id', $this->tableName);
        $this->dropForeignKey('fk-product_to_similar-similar_id-product-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
