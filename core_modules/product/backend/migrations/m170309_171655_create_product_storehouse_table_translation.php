<?php

use console\components\Migration;

/**
 * Class m170309_171655_create_product_storehouse_table_translation migration
 */
class m170309_171655_create_product_storehouse_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%product_storehouse_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%product_storehouse}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
                'address' => $this->string()->comment('Address'),
                'work_time' => $this->string()->comment('Work time'),
                'short_desc' => $this->string()->comment('Short Description'),
                'content' => $this->text()->comment('Content'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-p_s_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-p_s_translation-model_id-p_s-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
