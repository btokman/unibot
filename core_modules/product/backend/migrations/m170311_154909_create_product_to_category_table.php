<?php

use console\components\Migration;

/**
 * Class m170311_154909_create_product_to_category_table migration
 */
class m170311_154909_create_product_to_category_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_to_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'product_id' => $this->integer()->notNull()->comment('Product id'),
                'category_id' => $this->integer()->notNull()->comment('Category id'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-product_to_category-product_id-product-id',
            $this->tableName,
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-product_to_category-category_id-product_category-id',
            $this->tableName,
            'category_id',
            '{{%product_category}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_to_category-product_id-product-id', $this->tableName);
        $this->dropForeignKey('fk-product_to_category-category_id-product_category-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
