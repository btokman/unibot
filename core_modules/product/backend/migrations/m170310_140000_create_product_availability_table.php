<?php

use console\components\Migration;

/**
 * Class m170310_140000_create_product_availability_table migration
 */
class m170310_140000_create_product_availability_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_availability}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
