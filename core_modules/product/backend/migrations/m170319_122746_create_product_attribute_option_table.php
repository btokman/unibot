<?php

use console\components\Migration;

/**
 * Class m170319_122746_create_product_attribute_option_table migration
 */
class m170319_122746_create_product_attribute_option_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_attribute_option}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'attribute_id' => $this->integer()->null()->comment('Attribute'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-product_attribute_option-attribute_id-product_attribute-id',
            $this->tableName,
            'attribute_id',
            '{{%product_attribute}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_attribute_option-attribute_id-product_attribute-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
