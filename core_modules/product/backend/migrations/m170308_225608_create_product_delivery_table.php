<?php

use console\components\Migration;

/**
 * Class m170308_225608_create_product_delivery_table migration
 */
class m170308_225608_create_product_delivery_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_delivery}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'short_desc' => $this->string()->null()->comment('Short description'),
                'content' => $this->text()->null()->comment('Content'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
