<?php

use console\components\Migration;

/**
 * Class m170311_175008_create_product_to_delivery_table migration
 */
class m170311_175008_create_product_to_delivery_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_to_delivery}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'product_id' => $this->integer()->notNull()->comment('Product'),
                'delivery_id' => $this->integer()->notNull()->comment('Delivery'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-product_to_delivery-product_id-product-id',
            $this->tableName,
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-product_to_delivery-delivery_id-product_delivery-id',
            $this->tableName,
            'delivery_id',
            '{{%product_delivery}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_to_delivery-product_id-product-id', $this->tableName);
        $this->dropForeignKey('fk-product_to_delivery-delivery_id-product_delivery-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
