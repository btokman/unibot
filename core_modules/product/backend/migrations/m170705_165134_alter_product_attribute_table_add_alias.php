<?php

use console\components\Migration;

/**
 * Class m170705_165134_alter_product_attribute_table_add_alias migration
 */
class m170705_165134_alter_product_attribute_table_add_alias extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_attribute}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'alias', $this->string()->notNull()->after('label')->comment('Alias'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'alias');
    }
}
