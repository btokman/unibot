<?php

use console\components\Migration;

/**
 * Class m170311_164215_create_product_video_table migration
 */
class m170311_164215_create_product_video_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_video}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'product_id' => $this->integer()->notNull()->comment('Product'),
                'video_link' => $this->string()->notNull()->comment('Video link'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-product_video-product_id-product-id',
            $this->tableName,
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_video-product_id-product-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
