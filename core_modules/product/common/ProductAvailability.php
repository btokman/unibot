<?php

namespace common\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_availability}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $created_at
 * @property integer $updated_at
 */
class ProductAvailability extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_availability}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductAvailabilityTranslation::className(), ['model_id' => 'id']);
    }
}
