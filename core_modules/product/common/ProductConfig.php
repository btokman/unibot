<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 27.02.2017
 * Time: 19:31
 */
use frontend\modules\product\Module;

return [
    'components' => [
        // list of component configurations
    ],
    'params' => [
        'usePacking' => $this->params['usePacking'] ?? true,
        'useEav' => $this->params['useEav'] ?? true,
        'packingType' => $this->params['packingType'] ?? Module::PACKING_AS_PRODUCT,
        'allowChoosePackingType' => $this->params['allowChoosePackingType'] ?? true,
        'deleteExistingPackingWhenPackingIsDisabled' => $this->params['deleteExistingPackingWhenPackingIsDisabled'] ?? false,
    ],
];
