<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_to_similar}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $similar_id
 *
 * @property Product $product
 * @property Product $similar
 */
class ProductToSimilar extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_to_similar}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSimilar()
    {
        return $this->hasOne(Product::className(), ['id' => 'similar_id']);
    }
}
