<?php

namespace common\models;

use backend\modules\product\components\NestedSetsBehavior;
use kartik\tree\models\TreeTrait;
use Yii;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%product_category}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property string $short_desc
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $left
 * @property integer $depth
 * @property integer $right
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductCategoryTranslation[] $translations
 */
class ProductCategory extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    use TreeTrait;


    /**
     *
     */
    const SAVE_ATTRIBUTE_IMAGES = 'CategoryImage';
    /**
     *
     */
    const DEFAULT_PRODUCT_PER_PAGE = 20;
    /**
     *
     */
    const VIEW_TYPE_PRODUCT = '_baseProductListView';
    /**
     *
     */
    const VIEW_TYPE_PRODUCT_AND_PACKING = '_baseProductWithPackingListView';

    /**
     * @var string the classname for the TreeQuery that implements the NestedSetQueryBehavior.
     * If not set this will default to kartik    ree\models\TreeQuery.
     */
    public static $treeQueryClass; // change if you need to set your own TreeQuery

    /**
     * @var bool whether to HTML encode the tree node names. Defaults to true.
     */
    public $encodeNodeNames = true;

    /**
     * @var bool whether to HTML purify the tree node icon content before saving.
     * Defaults to true.
     */
    public $purifyNodeIcons = true;

    /**
     * @var array activation errors for the node
     */
    public $nodeActivationErrors = [];

    /**
     * @var array node removal errors
     */
    public $nodeRemovalErrors = [];

    /**
     * @var bool attribute to cache the active state before a model update. Defaults to true.
     */
    public $activeOrig = true;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_category}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'short_desc',
            'content',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'root',
                'leftAttribute' => 'left',
                'rightAttribute' => 'right',
                'depthAttribute' => 'depth',
            ],
        ];
    }

    /**
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return ProductCategoryQuery
     */
    public static function find()
    {
        return new ProductCategoryQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductCategoryTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBaseProductQuery()
    {
        return Product::find()
            ->select(['product.*'])
            ->where([
                'published' => 1,
            ])
            ->leftJoin('product_to_category', 'product_to_category.product_id = product.id')
            ->andWhere(['product_to_category.category_id' => $this->id]);
    }

    /**
     * @param $query
     */
    public function getMinPrice($query, $getBaundaryValues = false)
    {
        $queryMin = clone $query;

        if ($getBaundaryValues){
            $newWhereConditions = [];
            foreach ($queryMin->where as $whereCondition){
                if ((isset($whereCondition[1])) && ($whereCondition[1] == 'product.price')){
                    continue;
                }
                $newWhereConditions[] = $whereCondition;

            }
            $queryMin->where = $newWhereConditions;
        }

        $queryMin->orderBy([
            'product.price' => SORT_ASC,
        ])->groupBy('product.id')->limit(1);

        $minPriceProduct = $queryMin->asArray()->one();
        return floor($minPriceProduct['price']);
    }


    /**
     * @param $query
     */
    public function getMaxPrice($query, $getBaundaryValues = false)
    {
        $queryMax = clone $query;

        if ($getBaundaryValues){
            $newWhereConditions = [];
            foreach ($queryMax->where as $whereCondition){
                if ((isset($whereCondition[1])) && ($whereCondition[1] == 'product.price')){
                    continue;
                }
                $newWhereConditions[] = $whereCondition;

            }
            $queryMax->where = $newWhereConditions;
        }

        $queryMax->orderBy([
            'product.price' => SORT_DESC,
        ])->groupBy('product.id')->limit(1);

        $maxPriceProduct = $queryMax->asArray()->one();
        return ceil($maxPriceProduct['price']);
    }

    /**
     * @return mixed[]
     */
    public function getCategoryBrands($query = null)
    {
        if (isset($query)) {
            $productQuery = clone $query;
            $productsIds = ArrayHelper::getColumn($productQuery->asArray()->all(), 'id');
        }

        $brandData = ProductBrand::find()
            ->select(['product_brand.id', 'product_brand.label'])
            ->leftJoin('product', 'product.brand_id = product_brand.id')
            ->leftJoin('product_to_category', 'product_to_category.product_id = product.id')
            ->where([
                'product_to_category.category_id' => $this->id,
                'product_brand.published' => 1
            ]);

        if (isset($query)) {
            $attributeFiltersData = $brandData->andWhere([
                'product.id' => $productsIds,
            ]);
        }

        $brandData = $brandData->groupBy('product_brand.id')
            ->orderBy([
                'product_brand.position' => SORT_DESC,
            ])
            ->asArray()
            ->all();

        return $brandData;
    }

    /**
     * @return array
     */
    public function getAttributesValue($query = null)
    {
        if (isset($query)) {
            $productQuery = clone $query;
            $productsIds = ArrayHelper::getColumn($productQuery->asArray()->all(), 'id');
        }


        $attributeFiltersData = ProductEav::find()
            ->select([
                'product_attribute.label',
                'product_attribute.alias',
                'product_eav.value',
                'product_attribute_option.label as multiple_attribute_value',
                'MAX(product_attribute.id) as attribute_id'
            ])
            ->leftJoin('product_attribute', 'product_attribute.id = product_eav.attribute_id')
            ->leftJoin('product_attribute_option', 'product_attribute_option.id = product_eav.value')
            ->leftJoin('product_category_to_attribute',
                'product_category_to_attribute.attribute_id = product_attribute.id')
            ->leftJoin('product_category', 'product_category.id = product_category_to_attribute.category_id')
            ->leftJoin('product', 'product.id = product_eav.product_id')
            ->leftJoin('product_to_category', 'product_to_category.product_id = product.id')
            ->where(
                [
                    'product_category.id' => $this->id,
                    'product_attribute.published' => 1,
                    'product_to_category.category_id' => $this->id
                ]
            );

        if (isset($query)) {
            $attributeFiltersData = $attributeFiltersData->andWhere([
                'product.id' => $productsIds,
            ]);
        }

        $attributeFiltersData = $attributeFiltersData->orderBy([
            'product_attribute.position' => SORT_DESC,
        ])
            ->groupBy([
                'product_attribute.label',
                'product_attribute.alias',
                'product_eav.value',
                'product_attribute_option.label',
                'product_attribute.id'
            ])
            ->orderBy(['product_eav.value' => SORT_DESC])
            ->asArray()
            ->all();


        $attributeFilters = [];
        foreach ($attributeFiltersData as $attributeFilter) {


            $value = $attributeFilter['value'];
            $displayValue = $attributeFilter['multiple_attribute_value'] ?? $attributeFilter['value'];

            $attributeFilters[$attributeFilter['label']]['values'][$value] = $displayValue;
            $attributeFilters[$attributeFilter['label']]['attribute_id'] = $attributeFilter['attribute_id'];
            $attributeFilters[$attributeFilter['label']]['alias'] = $attributeFilter['alias'];
        }

        return $attributeFilters;
    }


    /**
     * @param $alias string
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findSingleCategory($alias)
    {
        $model = static::find()
            ->where([
                'published' => 1,
                'alias' => $alias,
            ])
            ->one();

        return $model;
    }

    /**
     * Link for single category
     *
     * @param array $params
     * @return string
     */
    public static function getSingleUrl($params = [])
    {
        return self::createUrl('/product/product-category/view', $params);
    }


    /**
     * Url for category view
     *
     * @param array $params
     * @return string
     */
    public static function getCategoryViewUrl($params = [])
    {
        return self::createUrl('/product/product-category/view', $params);
    }
}
