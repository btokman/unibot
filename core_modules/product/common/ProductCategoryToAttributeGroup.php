<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_category_to_attribute_group}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $attribute_group_id
 *
 * @property ProductCategory $category
 * @property ProductAttributeGroup $attributeGroup
 */
class ProductCategoryToAttributeGroup extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_category_to_attribute_group}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeGroup()
    {
        return $this->hasOne(ProductAttributeGroup::className(), ['id' => 'attribute_group_id']);
    }
}
