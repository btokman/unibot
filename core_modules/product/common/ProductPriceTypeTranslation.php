<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_price_type_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 */
class ProductPriceTypeTranslation extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_price_type_translation}}';
    }
}
