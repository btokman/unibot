<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_category_to_attribute}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $attribute_id
 *
 * @property ProductAttribute $attribute0
 * @property ProductCategory $category
 */
class ProductCategoryToAttribute extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_category_to_attribute}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute0()
    {
        return $this->hasOne(ProductAttribute::className(), ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id']);
    }
}
