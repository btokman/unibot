<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_to_attribute_group}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $attribute_group_id
 *
 * @property Product $product
 */
class ProductToAttributeGroup extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_to_attribute_group}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
