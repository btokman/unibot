<?php

namespace common\models;

use backend\modules\product\models\ProductPacking;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property integer $id
 * @property integer $is_packing
 * @property integer $parent_product_id
 * @property string $packing_link_text
 * @property string $sku
 * @property string $label
 * @property string $alias
 * @property integer $brand_id
 * @property integer $manufacturer_id
 * @property integer $availability_id
 * @property string $short_desc
 * @property string $content
 * @property string $price
 * @property string $old_price
 * @property integer $price_type
 * @property double $rating_total
 * @property integer $rating_voice_count
 * @property integer $show_as_separate
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductAvailability $availability
 * @property ProductBrand $brand
 * @property ProductManufacturer $manufacturer
 * @property Product $parentProduct
 * @property Product[] $products
 * @property ProductTranslation[] $translations
 */
class Product extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    const SAVE_ATTRIBUTE_IMAGES = 'ProductImage';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'packing_link_text',
            'label',
            'short_desc',
            'content',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @param $id integer
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findSingleProduct($id)
    {
        $model = static::find()
            ->where([
                'published' => 1,
                'id' => $id,
            ])
            ->one();

        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvailability()
    {
        return $this->hasOne(ProductAvailability::className(), ['id' => 'availability_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(ProductBrand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(ProductManufacturer::className(), ['id' => 'manufacturer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'parent_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributesObj()
    {
        return $this->hasMany(
            ProductAttribute::className(),
            ['id' => 'attribute_id']
        )->viaTable(
            ProductEav::tableName(),
            ['product_id' => 'id']
        );
    }

    public function getAttributesValue()
    {
        //get eav filters
        $attributeFiltersData = ProductEav::find()
            ->select([
                '`product_attribute`.`label`',
                '`product_eav`.`value`',
                '`product_attribute_option`.`label` as `multiple_attribute_value`',
                'MAX(`product_attribute`.`id`) as `attribute_id`'
            ])
            ->leftJoin('product_attribute', '`product_attribute`.`id` = `product_eav`.`attribute_id`')
            ->leftJoin('product_attribute_option', '`product_attribute_option`.`id` = `product_eav`.`value`')
            ->where(
                [
                    '`product_eav`.`product_id`' => $this->id,
                    '`product_attribute`.`published`' => 1
                ]
            )
            ->groupBy(['`product_attribute`.`label`', '`product_eav`.`value`', '`product_attribute_option`.`label`'])
            ->asArray()
            ->all();

        $attributeFilters = [];
        foreach ($attributeFiltersData as $attributeFilter){

            $value = $attributeFilter['value'];
            $displayValue = $attributeFilter['multiple_attribute_value'] ?? $attributeFilter['value'];

            $attributeFilters[$attributeFilter['label']]['values'][$value] = $displayValue ;
            $attributeFilters[$attributeFilter['label']]['attribute_id'] = $attributeFilter['attribute_id'];
        }

        return $attributeFilters;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackings()
    {
        return $this->hasMany(ProductPacking::className(), ['parent_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Link for single category
     *
     * @param array $params
     * @return string
     */
    public static function getSingleUrl($params = [])
    {
        return self::createUrl('/product/product/view', $params);
    }
}
