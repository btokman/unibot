<?php

namespace backend\modules\currency\models;

use creocoder\translateable\TranslateableBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\model\Translateable;
use common\components\model\ActiveRecord;
use backend\components\TranslateableTrait;
use backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%currency}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $code
 * @property double $rate
 * @property integer $default
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CurrencyTranslation[] $translations
 */
class Currency extends ActiveRecord implements BackendModel, Translateable
{
    use TranslateableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'code', 'rate'], 'required'],
            [['rate'], 'number'],
            [['default', 'published', 'position'], 'integer'],
            [['label', 'code'], 'string', 'max' => 255],
            [['rate'], 'default', 'value' => 1.0],
            [['rate'], 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number'],
            [['default'], 'default', 'value' => 0],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/currency', 'ID'),
            'label' => Yii::t('back/currency', 'Label'),
            'code' => Yii::t('back/currency', 'Code'),
            'rate' => Yii::t('back/currency', 'Rate'),
            'default' => Yii::t('back/currency', 'Default'),
            'published' => Yii::t('back/currency', 'Published'),
            'position' => Yii::t('back/currency', 'Position'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(CurrencyTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return Yii::t('back/currency', 'Currency');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
//                    'code',
                    'rate',
                    'default:boolean',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    'code',
                    'rate',
                    'default:boolean',
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new CurrencySearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'code' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'rate' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'hint' => Yii::t('back/currency', 'Compare to default currency.'),
            ],
            'default' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];
        return $config;
    }
}
