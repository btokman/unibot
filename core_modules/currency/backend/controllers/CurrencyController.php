<?php

namespace backend\modules\currency\controllers;

use backend\components\BackendController;
use backend\modules\currency\models\Currency;

/**
 * CurrencyController implements the CRUD actions for Currency model.
 */
class CurrencyController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Currency::className();
    }
}
