<?php

use console\components\Migration;

/**
 * Class m170212_173012_create_table_currency_translation migration
 */
class m170212_173012_create_table_currency_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%currency_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%currency}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),
                'label' => $this->string()->null()->comment('Label'),
            ],
            $this->tableOptions
        );

        $this->addPrimaryKey('pk-currency_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-currency_translation-model_id-currency-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

