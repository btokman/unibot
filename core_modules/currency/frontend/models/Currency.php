<?php
namespace frontend\modules\currency\models;

use common\components\Translate;

/**
 * Class Currency
 *
 * @package frontend\modules\currency\models
 */
class Currency extends \common\models\Currency
{
    use Translate;
}
