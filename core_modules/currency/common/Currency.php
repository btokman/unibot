<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\TranslateableTrait;
use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "{{%currency}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $code
 * @property double $rate
 * @property integer $default
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CurrencyTranslation[] $translations
 */
class Currency extends ActiveRecord implements Translateable
{
    use TranslateableTrait;

    const SESSION_KEY = 'currency';

    /**
     * Stores default currency object
     *
     * @var self
     */
    public static $defaultCurrency;

    /**
     * Stores current user currency
     *
     * @var self
     */
    public static $current;

    /**
     * Stores all published currencies
     *
     * @var Currency[]
     */
    public static $models = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'label' => Yii::t('app', 'Label'),
            'code' => Yii::t('app', 'Code'),
            'rate' => Yii::t('app', 'Rate'),
            'default' => Yii::t('app', 'Default'),
            'published' => Yii::t('app', 'Published'),
            'position' => Yii::t('app', 'Position'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(CurrencyTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return self
     */
    public static function getDefault()
    {
        if (static::$defaultCurrency === null) {
            static::$defaultCurrency = static::findOne(['default' => 1]);
        }
        return static::$defaultCurrency;
    }

    /**
     * @return array
     */
    public static function getCurrencies()
    {
        if (empty(static::$models)) {
            static::$models = static::find()->isPublished()->orderBy(['position' => SORT_ASC])->all();
        }
        return static::$models;
    }

    /**
     * @return Currency
     */
    public static function getCurrent()
    {
        if (!static::$current) {
            $session = Yii::$app->getSession();
            $key = static::SESSION_KEY;
            if ($session->has($key)) {
                $currentId = $session->get($key);
                static::$current = static::findOne($currentId);
            } else {
                $session->set($key, static::getDefault()->id);
                $defaultCurrencyId = $session->get($key);
                static::$current = static::findOne($defaultCurrencyId);
            }
        }
        return static::$current;
    }

    /**
     * Convert amount of current model object (currency) to default currency
     * By default returns in `12 345.67` format
     *
     * @param int|float $amount
     * @param int $decimals
     * @param string $decPoint
     * @param string $thousandsSep
     *
     * @return string
     */
    public function convertToDefault($amount, $decimals = 2, $decPoint = '.', $thousandsSep = ' ')
    {
        $result = $amount * $this->rate;
        return number_format($result, $decimals, $decPoint, $thousandsSep);
    }

    /**
     * Convert amount of current model object (currency) to particular currency
     * By default returns in `12 345.67` format
     *
     * @param int|float $amount
     * @param self $currency
     * @param int $decimals
     * @param string $decPoint
     * @param string $thousandsSep
     *
     * @return string
     */
    public function convertTo($currency, $amount, $decimals = 2, $decPoint = '.', $thousandsSep = ' ')
    {
        $result = $amount * ($currency->rate / $this->rate);
        return number_format($result, $decimals, $decPoint, $thousandsSep);
    }

    /**
     * Convert amount from one currency to another
     * By default returns in `12 345.67` format
     *
     * @param int|float $amount
     * @param self $currencyFrom
     * @param self $currencyTo
     * @param int $decimals
     * @param string $decPoint
     * @param string $thousandsSep
     *
     * @return string
     */
    public static function convert($amount, $currencyFrom, $currencyTo, $decimals = 2, $decPoint = '.', $thousandsSep = ' ')
    {
        $result = $amount * ($currencyTo->rate / $currencyFrom->rate);
        return number_format($result, $decimals, $decPoint, $thousandsSep);
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public static function getChangeUrl($params = [])
    {
        return static::createUrl('/currency/currency/set-currency', $params);
    }
}
