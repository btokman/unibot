<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%video_translation}}".
 *
 * @property integer $model_id
 * @property string  $language
 * @property string  $label
 * @property string  $description
 * @property string  $content
 * @property string  $video_link
 */
class VideoGalleryVideoTranslation extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%video_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label'       => Yii::t('back/video', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('back/video', 'Description') . ' [' . $this->language . ']',
            'content'     => Yii::t('back/video', 'Content') . ' [' . $this->language . ']',
            'video_link'  => Yii::t('back/video', 'Video link') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'content', 'video_link'], 'string'],
            [['label'], 'string', 'max' => 255],
        ];
    }
}
