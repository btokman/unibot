<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%video_album_translation}}".
 *
 * @property integer $model_id
 * @property string  $language
 * @property string  $label
 * @property string  $description
 * @property string  $content
 * @property string  $author
 */
class VideoGalleryAlbumTranslation extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%video_album_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label'       => Yii::t('back/videoAlbum', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('back/videoAlbum', 'Description') . ' [' . $this->language . ']',
            'content'     => Yii::t('back/videoAlbum', 'Content') . ' [' . $this->language . ']',
            'author'      => Yii::t('back/videoAlbum', 'Author') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'content'], 'string'],
            [['label', 'author'], 'string', 'max' => 255],
        ];
    }
}
