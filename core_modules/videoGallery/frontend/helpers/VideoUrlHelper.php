<?php

namespace frontend\modules\videoGallery\helpers;

use common\components\model\Helper;

class VideoUrlHelper
{
    use Helper;

    /**
     * Method returns videos list page route
     *
     * @return string
     */
    public static function getVideoListRoute(): string
    {
        return '/videoGallery/video/list';
    }

    /**
     * Method returns video view page route
     *
     * @return string
     */
    public static function getVideoViewRoute(): string
    {
        return '/videoGallery/video/view';
    }

    /**
     * Method returns videos list page url
     *
     * @return string
     */
    public static function getVideoListUrl(): string
    {
        return static::createUrl(static::getVideoListRoute(), []);
    }

    /**
     * Method returns video view page url
     *
     * @param array $params
     *
     * @return string
     */
    public static function getVideoViewUrl(array $params): string
    {
        return static::createUrl(static::getVideoViewRoute(), $params);
    }
}