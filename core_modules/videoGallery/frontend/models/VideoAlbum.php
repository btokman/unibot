<?php

namespace frontend\modules\videoGallery\models;

use common\components\model\DefaultQuery;
use common\components\Translate;
use frontend\modules\videoGallery\helpers\VideoAlbumUrlHelper;
use Yii;
use yii\data\ActiveDataProvider;

class VideoAlbum extends \common\models\VideoGalleryAlbum
{
    use Translate;

    /**
     * Method returns video model for given alias or null
     *
     * @param string $alias
     *
     * @return VideoAlbum | null
     */
    public static function getAlbumByAlias(string $alias)
    {
        $query = static::_getQuery();

        $query->andWhere(['t.alias' => $alias]);

        return $query->one();
    }

    /**
     * Method returns active data provider for video albums
     *
     * @param null | int $parentId
     * @param int        $pageSize
     *
     * @return \yii\data\ActiveDataProvider
     */
    public static function getAlbumDataProvider($parentId = null, int $pageSize = 10): ActiveDataProvider
    {
        $query = static::_getQuery();

        if ($parentId) {
            $query->andWhere(['t.parent_id' => $parentId]);
        }

        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => ['pageSize' => $pageSize],
        ]);
    }

    /**
     * Method returns default search query for video albums
     *
     * @return DefaultQuery
     */
    private static function _getQuery(): DefaultQuery
    {
        return VideoAlbum::find()
            ->alias('t')
            ->andWhere([
                't.published' => true,
            ])
            ->andWhere('t.parent_id IS NULL OR parent.published IS NOT FALSE')
            ->joinWith(['parent'])
            ->distinct(true)
            ->orderBy(['t.created_at' => SORT_ASC]);
    }

    /**
     * Relations for parent video album
     *
     * @return \yii\db\Query
     */
    public function getParent()
    {
        return $this->hasOne(VideoAlbum::className(), ['id' => 'parent_id'])
            ->alias('parent');
    }

    /**
     * Method returns view page url for current video album
     *
     * @return string
     */
    public function getAlbumUrl(): string
    {
        return VideoAlbumUrlHelper::getVideoAlbumViewUrl(['albumAlias' => $this->alias]);
    }

    /**
     * Method return creation date for video album in given format
     *
     * @param string $format
     *
     * @return string
     */
    public function getCreatedDate(string $format = 'dd.MM.yyyy HH:mm'): string
    {
        return Yii::$app->getFormatter()->asDatetime($this->created_at, $format);
    }
}