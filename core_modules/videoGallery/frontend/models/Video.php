<?php

namespace frontend\modules\videoGallery\models;

use common\components\model\DefaultQuery;
use frontend\modules\videoGallery\helpers\VideoUrlHelper;
use Yii;
use yii\data\ActiveDataProvider;

class Video extends \common\models\VideoGalleryVideo
{
    /**
     * Method returns active data provider for videos
     *
     * @param null | int $albumId
     * @param int        $pageSize
     *
     * @return \yii\data\ActiveDataProvider
     */
    public static function getActiveDateProvider($albumId = null, int $pageSize = 10): ActiveDataProvider
    {
        $query = static::_getQuery();

        if ($albumId) {
            $query->andWhere(['album.id' => $albumId]);
        }

        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => ['pageSize' => $pageSize],
        ]);
    }

    /**
     * Method returns video model for given alias or null
     *
     * @param string $alias
     *
     * @return Video | null
     */
    public static function getVideoByAlias(string $alias)
    {
        return static::_getQuery()
            ->andWhere(['t.alias' => $alias])
            ->one();
    }

    /**
     * Default video search query
     *
     * @return \common\components\model\DefaultQuery
     */
    private static function _getQuery(): DefaultQuery
    {
        return Video::find()
            ->alias('t')
            ->andWhere([
                't.published' => true,
            ])
            ->andWhere('videoAlbum.id IS NULL OR videoAlbum.published IS NOT FALSE')
            ->andWhere('videoAlbum.parent_id IS NULL OR parent.published IS NOT FALSE')
            ->joinWith(['videoAlbum'])
            ->distinct(true)
            ->orderBy(['t.created_at' => SORT_ASC]);
    }

    /**
     * Relation for video album
     *
     * @return \yii\db\Query
     */
    public function getVideoAlbum()
    {
        return $this->hasOne(VideoAlbum::className(), ['id' => 'album_id'])
            ->alias('videoAlbum')
            ->joinWith(['parent']);
    }

    /**
     * Method return creation date for video in given format
     *
     * @param string $format
     *
     * @return string
     */
    public function getCreatedDate(string $format = 'dd.MM.yyyy HH:mm'): string
    {
        return Yii::$app->getFormatter()->asDatetime($this->created_at, $format);
    }

    /**
     * Method returns view page url for current video
     *
     * @return string
     */
    public function getVideoUrl(): string
    {
        return VideoUrlHelper::getVideoViewUrl(['videoAlias' => $this->alias]);
    }
}