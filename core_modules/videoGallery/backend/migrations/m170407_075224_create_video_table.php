<?php

use console\components\Migration;

/**
 * Class m170407_075224_create_video_table migration
 */
class m170407_075224_create_video_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%video}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id'       => $this->primaryKey(),
                'album_id' => $this->integer()->unsigned()->defaultValue(null)->comment('Album'),

                'label'       => $this->string()->notNull()->comment('Label'),
                'alias'       => $this->string()->defaultValue(null)->comment('Alias'),
                'description' => $this->text()->defaultValue(null)->comment('Description'),
                'content'     => $this->text()->defaultValue(null)->comment('Content'),
                'video_link'  => $this->text()->defaultValue(null)->comment('Video link'),

                'published' => $this->boolean()->unsigned()->notNull()->defaultValue(true)->comment('Published'),
                'position'  => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->createIndex('key-album_id', $this->tableName, 'album_id');

        $this->addForeignKey(
            'fk-video-album_id-to-video_album-id',
            $this->tableName,
            'album_id',
            '{{%video_album}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
