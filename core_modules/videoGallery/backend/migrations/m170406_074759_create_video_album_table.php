<?php

use console\components\Migration;

/**
 * Class m170406_074759_create_video_album_table migration
 */
class m170406_074759_create_video_album_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%video_album}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'parent_id' => $this->integer()->unsigned()->defaultValue(null)->comment('Parent album'),

                'label'       => $this->string()->notNull()->comment('Label'),
                'alias'       => $this->string()->notNull()->comment('Alias'),
                'description' => $this->text()->defaultValue(null)->comment('Description'),
                'content'     => $this->text()->defaultValue(null)->comment('Content'),
                'author'      => $this->string()->defaultValue(null)->comment('Author'),

                'published' => $this->boolean()->unsigned()->notNull()->defaultValue(true)->comment('Published'),
                'position'  => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->createIndex('key-parent_id', $this->tableName, 'parent_id');

        $this->addForeignKey(
            'fk-video_album-parent_id-to-video_album-id',
            $this->tableName,
            'parent_id',
            $this->tableName,
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
