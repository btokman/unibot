<?php

return [
    'label' => Yii::t('back/videoGallery', 'Video gallery'),
    'items' => [
        [
            'label' => Yii::t('back/videoAlbum', 'Video albums'),
            'url' => ['/videoGallery/video-album/index'],
        ],
        [
            'label' => Yii::t('back/video', 'Videos'),
            'url' => ['/videoGallery/video-album/index'],
        ],
    ]
];
