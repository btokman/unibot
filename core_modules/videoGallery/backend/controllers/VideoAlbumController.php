<?php

namespace backend\modules\videoGallery\controllers;

use backend\components\BackendController;
use backend\modules\videoGallery\models\VideoAlbum;

/**
 * VideoAlbumController implements the CRUD actions for VideoAlbum model.
 */
class VideoAlbumController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return VideoAlbum::className();
    }
}
