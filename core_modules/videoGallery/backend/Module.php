<?php

namespace backend\modules\videoGallery;

use common\components\interfaces\CoreModuleBackendInterface;

class Module extends \yii\base\Module implements CoreModuleBackendInterface
{
    public $controllerNamespace = 'backend\modules\videoGallery\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
