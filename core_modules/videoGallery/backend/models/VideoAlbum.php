<?php

namespace backend\modules\videoGallery\models;

use backend\components\BackendModel;
use backend\components\ImperaviContent;
use backend\components\TranslateableTrait;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\components\model\Translateable;
use kartik\widgets\DatePicker;
use kartik\widgets\DateTimePicker;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\helpers\ArrayHelper;

class VideoAlbum extends \common\models\VideoGalleryAlbum implements BackendModel, Translateable
{
    use TranslateableTrait;

    // Date formats used for this model
    const DATETIME_FORMAT_FOR_FORM_PAGE = 'dd.MM.yyyy HH:mm';
    const DATE_FORMAT_FOR_INDEX_PAGE = 'dd.MM.yyyy';

    // String representation of timestamp date values
    public $createdAtString;

    // Attribute for image upload
    public $imageUpload;

    public $sign;

    private static function getAlbumsForParentship($exept = null): array
    {
        $query = VideoAlbum::find()
            ->select(['label'])
            ->andWhere(['parent_id' => null])
            ->indexBy('id');

        if ($exept) {
            $query->andWhere(['!=', 'id', $exept]);
        }

        return $query
            ->column();
    }

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = Yii::$app->getSecurity()->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'published', 'position'], 'integer'],
            [['label', 'alias'], 'required'],
            [['description', 'content'], 'string'],
            [['label', 'alias', 'author'], 'string', 'max' => 255],
            [
                ['parent_id'],
                'exist',
                'targetClass'     => VideoAlbum::className(),
                'targetAttribute' => 'id',
            ],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'safe'],
            [
                ['createdAtString'],
                'date',
                'format' => static::DATETIME_FORMAT_FOR_FORM_PAGE,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('back/videoAlbum', 'ID'),
            'parent_id'       => Yii::t('back/videoAlbum', 'Parent album'),
            'label'           => Yii::t('back/videoAlbum', 'Label'),
            'alias'           => Yii::t('back/videoAlbum', 'Alias'),
            'description'     => Yii::t('back/videoAlbum', 'Description'),
            'content'         => Yii::t('back/videoAlbum', 'Content'),
            'author'          => Yii::t('back/videoAlbum', 'Author'),
            'published'       => Yii::t('back/videoAlbum', 'Published'),
            'position'        => Yii::t('back/videoAlbum', 'Position'),
            'createdAtString' => Yii::t('back/videoAlbum', 'Created date'),
            'imageUpload'     => Yii::t('back/videoAlbum', 'Image upload'),
        ];
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->createdAtString = $this->_convertTimestampToString($this->created_at);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            ['timestamp' => ['class' => \yii\behaviors\TimestampBehavior::className()]]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(VideoAlbum::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideoAlbums()
    {
        return $this->hasMany(VideoAlbum::className(), ['parent_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/videoAlbum', 'Video Albums');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    'label',
                    'alias',
                    [
                        'attribute' => 'parent_id',
                        'value'     => function (VideoAlbum $data) {
                            return $data->parent->label ?? false;
                        },
                        'filter'    => VideoAlbum::getItems(),
                    ],
                    [
                        'attribute' => 'createdAtString',
                        'format'    => 'html',
                        'value'     => function (VideoAlbum $model) {
                            return Yii::$app->formatter->asDatetime(
                                $model->created_at,
                                static::DATE_FORMAT_FOR_INDEX_PAGE
                            );
                        },
                        'filter'    => DatePicker::widget([
                            'model'         => $this,
                            'attribute'     => 'createdAtString',
                            'options'       => [
                                'class' => 'form-control form-control-filters',
                            ],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format'    => 'dd.mm.yyyy',
                            ],
                        ]),
                    ],
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'alias',
                    [
                        'attribute' => 'parent_id',
                        'value'     => $this->parent->label ?? false,
                    ],
                    [
                        'attribute' => 'description',
                        'format'    => 'html',
                    ],
                    [
                        'attribute' => 'content',
                        'format'    => 'html',
                    ],
                    'author',
                    'createdAtString',
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new VideoAlbumSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'imageUpload'     => [
                'type'  => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model'                 => $this,
                    'attribute'             => 'imageUpload',
                    'saveAttribute'         => static::VIDEO_ALBUM_IMAGE,
                    'multiple'              => false,
                    'allowedFileExtensions' => ['jpeg', 'png', 'jpg'],
                ]),
            ],
            'label'           => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class'     => $this->isNewRecord ? 's_name form-control' : 'form-control',
                ],
            ],
            'alias'           => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class'     => $this->isNewRecord ? 's_alias form-control' : 'form-control',
                ],
            ],
            'parent_id'       => [
                'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items'   => VideoAlbum::getItems(),
                'options' => [
                    'prompt' => Yii::t('back/videoAlbum', 'Select parent album'),
                ],
            ],
            'description'     => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
            ],
            'content'         => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
            ],
            'author'          => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => ['maxlength' => true],
            ],
            'createdAtString' => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DateTimePicker::className(),
                'options'     => [
                    'pluginOptions' => [
                        'format'         => 'dd.mm.yyyy hh:ii',
                        'todayHighlight' => true,
                        'default'        => time(),
                    ],
                ],
            ],
            'published'       => ['type' => ActiveFormBuilder::INPUT_CHECKBOX],
            'position'        => ['type' => ActiveFormBuilder::INPUT_TEXT],
        ];

        return $config;
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            // Convert string date values into timestamp values
            $this->created_at = $this->_convertStringToTimestamp($this->createdAtString);

            return true;
        }

        return false;
    }

    /**
     * Method converts timestamp date value from DB to string representation
     *
     * @param int $timestamp
     *
     * @return string
     */
    private function _convertTimestampToString($timestamp): string
    {
        return Yii::$app->getFormatter()->asDatetime(
            $timestamp ?? time(),
            static::DATETIME_FORMAT_FOR_FORM_PAGE
        );
    }

    /**
     * Method accepts string date representation and converts it to timestamp value
     * If no date value given then method return current time in timestamp
     *
     * @param string | null $date
     *
     * @return int
     */
    private function _convertStringToTimestamp($date): int
    {
        return $date
            ? Yii::$app->getFormatter()->asTimestamp($date)
            : time();
    }
}
