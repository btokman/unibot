<?php

use yii\widgets\ListView;
use yii\widgets\LinkPager;

/**
 * @var $this yii\web\View
 * @var $fixed \frontend\modules\blog\models\Blog
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $page \common\models\BlogPageSettings
 */

$this->title = $page->blogIndexPageTitle;
?>

<h1><?= $this->title ?></h1>

<?php if (isset($fixed)) : ?>
<div class="fixed-item" style="background: green">
    <?= $this->render('_item', ['model' => $fixed]) ?>
</div>
<?php endif; ?>

<?= ListView::widget([
    'layout' => '{items}',
    'emptyText' => isset($fixed) ? '' : Yii::t('front/blog', 'No items found'),
    'dataProvider' => $dataProvider,
    'options' => ['tag' => 'div', 'class' => 'blog-items'],
    'itemOptions' => ['tag' => 'div', 'class' => 'blog-item'],
    'itemView' => '_item',
]); ?>

<?= LinkPager::widget(['pagination' => $dataProvider->getPagination()]) ?>
