<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model \frontend\modules\blog\models\Blog the data model
 * @var $key mixed the key value associated with the data item
 * @var $index integer, the zero-based index of the data item in the items array returned by [[dataProvider]].
 * @var $widget yii\widgets\ListView, this widget instance
 */
?>

<div class="blog-item-inner">
    <h3><?= Html::a($model->label, $model->getViewUrl()) ?></h3>
    <?= $model->getIndexImage('admin', 'file', 'image', ['alt' => 'Do not forget about ALT attr']) ?>
    <div><?= $model->description ?></div>

    <?php if ($model->blogAuthor) : ?>
        <strong><?= $model->blogAuthor->fullname ?></strong>
    <?php endif; ?>

    <?php if ($model->blogCategories) : ?>
        <?php foreach ($model->blogCategories as $category) : ?>
            <?php /* @var \common\models\BlogCategory $category */?>
            <?= Html::a($category->label, $category->getBlogCategoryUrl()) ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
