<?php

use yii\helpers\Html;
use frontend\widgets\openGraphMetaTags\Widget as OpenGraphWidget;

/**
 * @var $this yii\web\View
 * @var $model frontend\modules\blog\models\Blog
 */

OpenGraphWidget::widget([
    'title' => $model->label,
    'description' => $model->getOpenGraphDescription(),
    'url' => $model->getViewUrl(true),
    'image' => $model->getOpenGraphImage('admin', 'file'),
]);

$this->title = $model->label;
?>

<p><?= Html::a('Back to list', $model->getIndexUrl()) ?></p>

<article>

    <h1><?= $model->label ?></h1>

    <time><?= $model->publish_at ?></time>

    <?= $model->getIndexImage('admin', 'file', 'image') ?>

    <section><?= $model->content ?></section>

    <?php if ($model->blogAuthor) : ?>
        <strong><?= $model->blogAuthor->fullname ?></strong>
    <?php endif; ?>

    <?php if ($model->blogCategories) : ?>
        <?php foreach ($model->blogCategories as $category) : ?>
            <?php /* @var \common\models\BlogCategory $category */?>
            <?= Html::a($category->label, $category->getBlogCategoryUrl()) ?>
        <?php endforeach; ?>
    <?php endif; ?>

</article>
