<?php

namespace frontend\modules\blog;

use yii\base\Module as BaseModule;
use common\components\interfaces\CoreModuleFrontendInterface;

/**
 * Class Module
 *
 * @package frontend\modules\blog
 * @author Bogdan Fedun <delagics@gmail.com>
 */
class Module extends BaseModule implements CoreModuleFrontendInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\blog\controllers';
}
