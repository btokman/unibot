<?php

namespace backend\modules\blog;

use yii\base\Module as BaseModule;
use common\components\interfaces\CoreModuleBackendInterface;

/**
 * Class Module
 *
 * @package backend\modules\blog
 * @author Bogdan Fedun <delagics@gmail.com>
 */
class Module extends BaseModule implements CoreModuleBackendInterface
{
    public $controllerNamespace = 'backend\modules\blog\controllers';
}
