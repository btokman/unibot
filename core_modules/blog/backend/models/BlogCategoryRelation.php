<?php

namespace backend\modules\blog\models;

use Yii;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\models\Blog as CommonBlog;
use common\models\BlogCategory as CommonBlogCategory;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;

/**
 * This is the model class for table "{{%blog_category_relation}}".
 *
 * @property integer $id
 * @property integer $blog_id
 * @property integer $category_id
 * @property integer $position
 *
 * @property Blog $blog
 * @property BlogCategory $category
 */
class BlogCategoryRelation extends ActiveRecord implements BackendModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_category_relation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blog_id', 'category_id'], 'required'],
            [['blog_id', 'category_id', 'position'], 'integer'],
            [['blog_id'], 'exist', 'targetClass' => CommonBlog::className(), 'targetAttribute' => 'id'],
            [['category_id'], 'exist', 'targetClass' => CommonBlogCategory::className(), 'targetAttribute' => 'id'],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/blog/category', 'ID'),
            'blog_id' => Yii::t('back/blog/category', 'Blog ID'),
            'category_id' => Yii::t('back/blog/category', 'Category ID'),
            'position' => Yii::t('back/blog/category', 'Position'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlog()
    {
        return $this->hasOne(Blog::className(), ['id' => 'blog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(BlogCategory::className(), ['id' => 'category_id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/blog/category', 'Blog Category Relation');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'blog_id',
                    'category_id',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'blog_id',
                    'category_id',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new BlogCategoryRelationSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'blog_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CommonBlog::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'category_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CommonBlogCategory::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }
}
