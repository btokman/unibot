<?php

namespace backend\modules\blog\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%blog_author_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $firstname
 * @property string $lastname
 * @property string $bio
 */
class BlogAuthorTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_author_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/blog/author', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/blog/author', 'Language') . ' [' . $this->language . ']',
            'firstname' => Yii::t('back/blog/author', 'First name') . ' [' . $this->language . ']',
            'lastname' => Yii::t('back/blog/author', 'Last name') . ' [' . $this->language . ']',
            'bio' => Yii::t('back/blog/author', 'Bio') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bio'], 'string'],
            [['firstname', 'lastname'], 'string', 'max' => 255],
        ];
    }
}
