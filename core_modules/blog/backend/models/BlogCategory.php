<?php

namespace backend\modules\blog\models;

use Yii;
use creocoder\translateable\TranslateableBehavior;
use metalguardian\formBuilder\ActiveFormBuilder;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\components\model\ActiveRecord;
use backend\components\BackendModel;
use common\components\model\Translateable;
use backend\components\TranslateableTrait;

/**
 * This is the model class for table "{{%blog_category}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property integer $published
 *
 * @property BlogCategoryTranslation[] $translations
 */
class BlogCategory extends ActiveRecord implements BackendModel, Translateable
{
    use TranslateableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'alias'], 'required'],
            [['published'], 'integer'],
            [['label', 'alias'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/blog/category', 'ID'),
            'label' => Yii::t('back/blog/category', 'Label'),
            'alias' => Yii::t('back/blog/category', 'Alias'),
            'published' => Yii::t('back/blog/category', 'Published'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo' => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BlogCategoryTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/blog/category', 'Blog Category');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'alias',
                    'published:boolean',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'alias',
                    'published:boolean',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new BlogCategorySearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_name form-control'
                ],
            ],
            'alias' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_alias form-control'
                ],
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }
}
