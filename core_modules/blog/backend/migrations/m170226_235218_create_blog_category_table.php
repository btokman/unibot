<?php

use console\components\Migration;

/**
 * Class m170226_235218_create_blog_category_table migration
 */
class m170226_235218_create_blog_category_table extends Migration
{
    /**
     * @var string migration table name
     */
    public $tableName = '{{%blog_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'alias' => $this->string()->notNull()->comment('Alias'),
                'published' => $this->smallInteger(1)->notNull()->defaultValue(1)->comment('Published'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
