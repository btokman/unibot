<?php

use console\components\Migration;

/**
 * Class m170226_235218_create_blog_category_table_translation migration
 */
class m170226_235218_create_blog_category_table_translation extends Migration
{
    /**
     * @var string migration related table name
     */
    public $tableName = '{{%blog_category_translation}}';

    /**
     * @var string main table name, to make constraints
     */
    public $tableNameRelated = '{{%blog_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
            ],
            $this->tableOptions
        );

        $this->addPrimaryKey('pk-blog_category_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-blog_category_translation-model_id-blog_category-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

