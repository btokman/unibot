<?php

use console\components\Migration;

/**
 * Class m170226_234803_create_blog_author_table_translation migration
 */
class m170226_234803_create_blog_author_table_translation extends Migration
{
    /**
     * @var string migration related table name
     */
    public $tableName = '{{%blog_author_translation}}';

    /**
     * @var string main table name, to make constraints
     */
    public $tableNameRelated = '{{%blog_author}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'firstname' => $this->string()->comment('First name'),
                'lastname' => $this->string()->comment('Last name'),
                'bio' => $this->text()->comment('Bio'),
            ],
            $this->tableOptions
        );

        $this->addPrimaryKey('pk-blog_author_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-blog_author_translation-model_id-blog_author-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

