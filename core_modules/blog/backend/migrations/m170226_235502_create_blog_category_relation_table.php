<?php

use console\components\Migration;

/**
 * Class m170226_235502_create_blog_category_relation_table migration
 */
class m170226_235502_create_blog_category_relation_table extends Migration
{
    /**
     * @var string migration table name
     */
    public $tableName = '{{%blog_category_relation}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'blog_id' => $this->integer()->notNull()->comment('Blog ID'),
                'category_id' => $this->integer()->notNull()->comment('Category ID'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-blog_category_relation-blog_id-blog-id',
            $this->tableName,
            'blog_id',
            '{{%blog}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-blog_category_relation-category_id-blog_category-id',
            $this->tableName,
            'category_id',
            '{{%blog_category}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-blog_category_relation-blog_id-blog-id', $this->tableName);
        $this->dropForeignKey('fk-blog_category_relation-category_id-blog_category-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
