<?php

return [
    'label' => Yii::t('back/blog', 'Blog'),
    'items' => [
        // Blog
        [
            'label' => Yii::t('back/blog', 'List'),
            'url' => ['/blog/blog/index'],
        ],
        [
            'label' => Yii::t('back/blog', 'Create'),
            'url' => ['/blog/blog/create'],
        ],
        // Blog authors
        [
            'label' => Yii::t('back/blog', 'Authors'),
            'url' => ['/blog/blog-author/index'],
        ],
        [
            'label' => Yii::t('back/blog', 'Add author'),
            'url' => ['/blog/blog-author/create'],
        ],
        // Blog categories
        [
            'label' => Yii::t('back/blog', 'Categories'),
            'url' => ['/blog/blog-category/index'],
        ],
        [
            'label' => Yii::t('back/blog', 'Add category'),
            'url' => ['/blog/blog-category/create'],
        ],
        [
            'label' => Yii::t('back/blog', 'Settings'),
            'url' => ['/blog/blog-page/update'],
        ],
    ]
];
