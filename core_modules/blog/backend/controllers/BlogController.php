<?php

namespace backend\modules\blog\controllers;

use backend\components\BackendController;
use backend\modules\blog\models\Blog;

/**
 * BlogController implements the CRUD actions for Blog model.
 */
class BlogController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Blog::className();
    }
}
