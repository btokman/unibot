<?php

namespace backend\modules\blog\controllers;

use backend\components\BackendController;
use backend\modules\blog\models\BlogCategory;

/**
 * BlogCategoryController implements the CRUD actions for BlogCategory model.
 */
class BlogCategoryController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BlogCategory::className();
    }
}
