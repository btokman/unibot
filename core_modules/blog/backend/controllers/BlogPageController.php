<?php

namespace backend\modules\blog\controllers;

use backend\modules\blog\models\BlogPageSettings;
use backend\modules\configuration\components\ConfigurationController;

/**
 * BlogPageController implements the CRUD actions for BlogPageSettings model.
 */
class BlogPageController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BlogPageSettings::className();
    }
}
