<?php

namespace backend\modules\blog\controllers;

use backend\components\BackendController;
use backend\modules\blog\models\BlogAuthor;

/**
 * BlogAuthorController implements the CRUD actions for BlogAuthor model.
 */
class BlogAuthorController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BlogAuthor::className();
    }
}
