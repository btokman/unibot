<?php

namespace common\models;

use Yii;
use common\models\base\StaticPage;

class BlogPageSettings extends StaticPage
{
    /**
     * @var string blog page settings constants.
     */
    const BLOG_INDEX_PAGE_TITLE = 'blogIndexPageTitle';
    const BLOG_INDEX_PAGE_SIZE = 'blogIndexPageSize';
    const BLOG_INDEX_PAGE_FIXED_POST = 'blogIndexPageFixedPost';

    /**
     * @var string blog index page title.
     */
    public $blogIndexPageTitle;

    /**
     * @var int blog index page size.
     */
    public $blogIndexPageSize;

    /**
     * @var int the ID of fixed post on blog index page.
     */
    public $blogIndexPageFixedPost;

    /**
     * @return array
     */
    public function getKeys()
    {
        return [
            self::BLOG_INDEX_PAGE_TITLE,
            self::BLOG_INDEX_PAGE_SIZE,
            self::BLOG_INDEX_PAGE_FIXED_POST,
        ];
    }

    /**
     * @return $this
     */
    public function get()
    {
        /** @var \common\components\ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->blogIndexPageTitle = $config->get(self::BLOG_INDEX_PAGE_TITLE);
        $this->blogIndexPageSize = $config->get(self::BLOG_INDEX_PAGE_SIZE);
        $this->blogIndexPageFixedPost = $config->get(self::BLOG_INDEX_PAGE_FIXED_POST);

        return $this;
    }
}
