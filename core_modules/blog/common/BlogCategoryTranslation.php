<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%blog_category_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 */
class BlogCategoryTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_category_translation}}';
    }
}
