<?php

namespace common\models;

use creocoder\translateable\TranslateableBehavior;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;

/**
 * This is the model class for table "{{%blog_author}}".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property integer $gender
 * @property string $bio
 *
 * @property BlogAuthorTranslation[] $translations
 * @property string $fullname
 */
class BlogAuthor extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    /**
     * @var string author full name, resolved by getter.
     */
    private $_fullname;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_author}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'firstname',
            'lastname',
            'bio',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * Getter for `$fullname` model property.
     *
     * @return string
     */
    public function getFullname()
    {
        if ($this->_fullname === null) {
            $this->_fullname = $this->firstname . ' ' . $this->lastname;
        }

        return $this->_fullname;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BlogAuthorTranslation::className(), ['model_id' => 'id']);
    }
}
