<?php

namespace backend\modules\socialShareContent\models;

use backend\components\BackendModel;
use backend\components\Translateable;
use backend\components\TranslateableTrait;
use common\components\model\ActiveRecord;
use creocoder\translateable\TranslateableBehavior;
use metalguardian\fileProcessor\behaviors\UploadBehavior;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class SocialShareContent
 */
class SocialShareContent extends ActiveRecord implements BackendModel, Translateable
{
    use TranslateableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%social_share_content}}';
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/social_share_content', 'Social Share Content');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_name', 'model_id'], 'required'],
            [['model_id'], 'integer'],
            [['description'], 'string'],
            [['model_name'], 'string', 'max' => 50],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_name' => 'Model Name',
            'model_id' => 'Model ID',
            'title' => Yii::t('back/social_share_content', 'Title'),
            'description' => Yii::t('back/social_share_content', 'Description'),
            'image_id' => Yii::t('back/social_share_content', 'Image'),
        ];
    }

    /**
     * Has search form on index template page
     *
     * @return bool
     */
    public function hasSearch()
    {
        return false;
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'model_name',
                    'model_id',
                    'title',
                    'description:ntext',
                    // 'image_id:ntext',

                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'model_name',
                    'model_id',
                    'title',
                    'description:ntext',
                    'image_id:ntext',
                ];
                break;
        }
        return [];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'model_id',
            'title',
            'description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(SocialShareContentTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new SocialShareContentSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'title' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,
            ],
            'image_id' => [
                'type' => ActiveFormBuilder::INPUT_FILE,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'socialShareImage' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'image_id',
                'validator' => ['extensions' => ['jpg', 'jpeg', 'gif', 'png']],
                'required' => false,
            ],
        ]);
    }
}
