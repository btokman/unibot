<?php
/**
 * @var \yii\db\ActiveRecord $model
 * @var \yii\widgets\ActiveForm $form
 */
use backend\components\Translateable;
use yii\helpers\Html;

echo Html::beginTag('div', ['class' => 'form-group']);

$content = null;
foreach ($model->formConfig as $attribute => $element) {
    $content .= $form->renderField($model, $attribute, $element);
    $content .= $form->renderUploadedFile($model, $attribute, $element);
    if ($model instanceof Translateable && $model->isTranslateAttribute($attribute)) {
        foreach ($model->getTranslationModels() as $languageModel) {
            $content .= $form->renderField($languageModel, '[' . $languageModel->language . ']' . $attribute, $element);
            $content .= $form->renderUploadedFile($languageModel, $attribute, $element, $languageModel->language);
        }
    }
}
echo $content;
echo Html::endTag('div');
