<?php

namespace backend\modules\photoGallery;

use common\components\interfaces\CoreModuleBackendInterface;

class Module extends \yii\base\Module implements CoreModuleBackendInterface
{
    public $controllerNamespace = 'backend\modules\photoGallery\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
