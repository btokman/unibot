<?php

use console\components\Migration;

/**
 * Class m170414_114556_create_photo_gallery_entity_table_translation migration
 */
class m170414_114556_create_photo_gallery_entity_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%photo_gallery_entity_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%photo_gallery_entity}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label'       => $this->string()->defaultValue(null)->comment('Label'),
                'alt'         => $this->string()->defaultValue(null)->comment('Alt'),
                'description' => $this->text()->defaultValue(null)->comment('Description'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-p_g_e_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-p_g_e_translation-model_id-p_g_e-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

