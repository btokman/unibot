<?php

use console\components\Migration;

/**
 * Class m170413_152008_creaet_photo_gallery_album_table migration
 */
class m170413_152008_creaet_photo_gallery_album_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%photo_gallery_album}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id'        => $this->primaryKey(),
                'parent_id' => $this->integer()->unsigned()->defaultValue(null)->comment('Parent album'),

                'label'       => $this->string()->defaultValue(null)->comment('Label'),
                'alias'       => $this->string()->defaultValue(null)->comment('Alias'),
                'description' => $this->text()->defaultValue(null)->comment('Description'),
                'content'     => $this->text()->defaultValue(null)->comment('Content'),
                'author'      => $this->string()->defaultValue(null)->comment('Author'),

                'published' => $this->boolean()->unsigned()->notNull()->defaultValue(true)->comment('Published'),
                'position'  => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->createIndex('key-parent_id', $this->tableName, 'parent_id');

        $this->addForeignKey(
            'fk-photo_gallery_album-parent_id-to-photo_gallery_album-id',
            $this->tableName,
            'parent_id',
            $this->tableName,
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
