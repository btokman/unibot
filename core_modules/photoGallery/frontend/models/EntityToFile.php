<?php

namespace frontend\modules\photoGallery\models;

use yii\db\ActiveQuery;

class EntityToFile extends \common\models\EntityToFile
{
    /**
     * Relation for images metadata models
     *
     * @return ActiveQuery
     */
    public function getMetaData(): ActiveQuery
    {
        return $this->hasOne(PhotoGalleryEntity::className(), ['file_id' => 'id'])
            ->alias('metaData');
    }
}