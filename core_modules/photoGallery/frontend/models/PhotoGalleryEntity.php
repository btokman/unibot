<?php

namespace frontend\modules\photoGallery\models;

use common\components\Translate;

class PhotoGalleryEntity extends \common\models\PhotoGalleryEntity
{
    use Translate;
}