<?php

namespace frontend\modules\photoGallery;

/**
 * photoGallery module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\photoGallery\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        $this->viewPath = '@app/themes/basic/modules/photoGallery';

    }
}
