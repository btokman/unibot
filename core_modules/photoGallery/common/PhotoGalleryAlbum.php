<?php

namespace common\models;

use creocoder\translateable\TranslateableBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;


/**
 * This is the model class for table "{{%photo_gallery_album}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string  $label
 * @property string  $alias
 * @property string  $description
 * @property string  $content
 * @property string  $author
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 **/
class PhotoGalleryAlbum extends \common\components\model\ActiveRecord
{
    const PHOTO_ALBUM_IMAGE = 'photo-album-image';
    const PHOTO_ALBUM_IMAGES = 'photo-album-images';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%photo_gallery_album}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PhotoGalleryAlbumTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
            'content',
            'author',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class'                 => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo'           => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }
}
