<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%photo_gallery_entity_translation}}".
 *
 * @property integer $model_id
 * @property string  $language
 * @property string  $label
 * @property string  $alt
 * @property string  $description
 */
class PhotoGalleryEntityTranslation extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%photo_gallery_entity_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label'       => Yii::t('back/photo', 'Label') . ' [' . $this->language . ']',
            'alt'         => Yii::t('back/photo', 'Alt') . ' [' . $this->language . ']',
            'description' => Yii::t('back/photo', 'Description') . ' [' . $this->language . ']',
        ];
    }

    public function rules()
    {
        return [
            [['description'], 'string'],
            [['label', 'alt'], 'string', 'max' => 255],
        ];
    }

}
