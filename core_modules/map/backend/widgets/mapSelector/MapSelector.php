<?php
namespace backend\modules\map\widgets\mapSelector;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\InputWidget;
use common\components\model\ActiveRecord;
use backend\modules\map\behaviors\MapBehavior;
use backend\modules\map\models\MapEntity;

/**
 * Class MapSelector
 *
 * @package backend\modules\map\widgets
 */
class MapSelector extends InputWidget
{

    /**
     * @var ActiveRecord
     */
    public $model;

    /**
     * @inheritdoc
     */
    public $attribute;

    /**
     * Hide or show inputs with coordinates and zoom
     *
     * @var bool
     */
    public $hiddenFields = true;

    /**
     * Options for map entity inputs
     *
     * @var array
     */
    public $fieldOptions = [];

    /**
     * Min height of map container. `400px` for default
     *
     * @var int
     */
    public $height = 400;

    /**
     * Set scrollable or static map. `true` for default
     *
     * @var bool
     */
    public $scrollable = true;

    /**
     * Relative path to the custom marker. Marker should be available in the web root directory.
     * If set to `false` - the default google maps marker will be displayed
     *
     * @var bool|string
     */
    public $marker = false;

    /**
     * Display search input for google places. `false` for default
     *
     * @var bool
     */
    public $autoComplete = false;

    /**
     * MapEntity model
     *
     * @var MapEntity
     */
    private $_entityModel;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $behavior = $this->model->getBehavior('map');
        if (!$behavior || !($behavior instanceof MapBehavior)) {
            throw new InvalidConfigException('Map behavior should be attached to model');
        }

        $this->_entityModel = $this->getEntityModel();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        MapAsset::register($this->view);

        if ($this->autoComplete) {
            echo Html::textInput('auto-complete', '', ['id' => 'auto-complete', 'class' => 'form-control']);
        }

        echo Html::tag('div', '', [
                'id' => $this->attribute,
                'class' => 'form-control map-class',
                'style' => ['min-height' => "{$this->height}px"],
                'data' => [
                    'scrollable' => $this->scrollable,
                    'marker' => $this->marker,
                ]
            ]
        );
        $this->renderMapDataFields();
    }

    /**
     * Render fields with coordinated and zoom
     */
    public function renderMapDataFields()
    {
        Html::addCssClass($this->fieldOptions, ArrayHelper::getValue($this->fieldOptions, 'class', 'form-control'));
        foreach ($this->getAttributes() as $attribute) {
            if (!$this->hiddenFields) {
                echo Html::activeLabel($this->_entityModel, $attribute, ['class' => 'control-label']);
                echo Html::activeTextInput($this->_entityModel, $attribute, $this->fieldOptions);
            } else {
                echo Html::activeHiddenInput($this->_entityModel, $attribute);
            }
        }
    }

    /**
     * @return array
     */
    private function getAttributes()
    {
        return [
            'latitude',
            'longitude',
            'zoom',
        ];
    }

    /**
     * @return MapEntity
     */
    private function getEntityModel()
    {
        /** @var ActiveRecord $model */
        $model = $this->model;
        $entityModel =  MapEntity::findOne([
            'entity_model_name' => $model->formName(),
            'entity_model_id' => $model->id,
        ]);
        if ($entityModel ===  null) {
            $entityModel = new MapEntity();
            $entityModel->loadDefaultValues();
        }
        return $entityModel;
    }
}
