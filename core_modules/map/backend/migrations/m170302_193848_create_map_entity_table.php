<?php

use console\components\Migration;

/**
 * Class m170302_193848_create_map_entity_table migration
 */
class m170302_193848_create_map_entity_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%map_entity}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'entity_model_name' => $this->string()->notNull()->comment('Entity model name'),
                'entity_model_id' => $this->integer()->notNull()->comment('Entity model ID'),
                'latitude' => $this->double()->notNull()->defaultValue(50.410719183041)->comment('Latitude'),
                'longitude' => $this->double()->notNull()->defaultValue(30.534291565418)->comment('Longitude'),
                'zoom' => $this->float()->notNull()->defaultValue(11)->comment('Zoom'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
