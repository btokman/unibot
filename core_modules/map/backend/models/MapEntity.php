<?php

namespace backend\modules\map\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%map_entity}}".
 *
 * @property integer $id
 * @property string $entity_model_name
 * @property integer $entity_model_id
 * @property double $latitude
 * @property double $longitude
 * @property double $zoom
 */
class MapEntity extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%map_entity}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_model_name', 'entity_model_id', 'latitude', 'longitude'], 'required'],
            [['entity_model_id'], 'integer'],
            [['latitude', 'longitude', 'zoom'], 'number'],
            [['entity_model_name'], 'string', 'max' => 255],
            [['zoom'], 'default', 'value' => 11.0],
            [['latitude'], 'default', 'value' => 50.410719183041],
            [['longitude'], 'default', 'value' => 30.534291565418],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/map', 'ID'),
            'entity_model_name' => Yii::t('back/map', 'Entity model name'),
            'entity_model_id' => Yii::t('back/map', 'Entity model ID'),
            'latitude' => Yii::t('back/map', 'Latitude'),
            'longitude' => Yii::t('back/map', 'Longitude'),
            'zoom' => Yii::t('back/map', 'Zoom'),
        ];
    }
}
