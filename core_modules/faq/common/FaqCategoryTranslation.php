<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%faq_category_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 */
class FaqCategoryTranslation extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%faq_category_translation}}';
    }
}
