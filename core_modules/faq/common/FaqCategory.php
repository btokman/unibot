<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%faq_category}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property FaqCategoryTranslation[] $translations
 * @property FaqQuestion[] $faqQuestions
 * @property FaqRequest[] $faqRequests
 */
class FaqCategory extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%faq_category}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(FaqCategoryTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaqQuestions()
    {
        return $this->hasMany(FaqQuestion::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaqRequests()
    {
        return $this->hasMany(FaqRequest::className(), ['category_id' => 'id']);
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public static function getPagesUrl($params = [])
    {
        return static::createUrl('/faq/default/index', $params);
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function getPageUrl($params = [])
    {
        $params['alias'] = $this->alias;

        return static::createUrl('/faq/default/category', $params);
    }
}
