<?php

namespace backend\modules\faq\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%faq_question}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $label
 * @property string $question
 * @property string $answer
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property FaqCategory $category
 * @property FaqQuestionTranslation[] $translations
 */
class FaqQuestion extends ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%faq_question}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'published', 'position'], 'integer'],
            [['label'], 'required'],
            [['question', 'answer'], 'string'],
            [['label'], 'string', 'max' => 255],
            [
                ['category_id'],
                'exist',
                'targetClass' => \common\models\FaqCategory::className(),
                'targetAttribute' => 'id'
            ],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/faq', 'ID'),
            'category_id' => Yii::t('back/faq', 'Category'),
            'label' => Yii::t('back/faq', 'Label'),
            'question' => Yii::t('back/faq', 'Question'),
            'answer' => Yii::t('back/faq', 'Answer'),
            'published' => Yii::t('back/faq', 'Published'),
            'position' => Yii::t('back/faq', 'Position'),
            'created_at' => Yii::t('back/faq', 'Created At'),
            'updated_at' => Yii::t('back/faq', 'Updated At'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'question',
            'answer',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(FaqCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(FaqQuestionTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/faq', 'Faq Question');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'category_id',
                        'value' => function ($data) {
                            /** @var FaqQuestion $data */
                            return ArrayHelper::getValue($data, 'category.label');
                        },
                        'filter' => \common\models\FaqCategory::getItems(),
                    ],
                    'label',
                    // 'question:ntext',
                    // 'answer:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'category_id',
                        'value' => ArrayHelper::getValue($this, 'category.label'),
                    ],
                    'label',
                    [
                        'attribute' => 'question',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'answer',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new FaqQuestionSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'category_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\FaqCategory::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'question' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'question',
                ]
            ],
            'answer' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'answer',
                ]
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }


}
