<?php

return [
    'label' => Yii::t('back/faq', 'faq'),
    'items' => [
        [
            'label' => Yii::t('back/faq', 'faq category'),
            'url' => ['/faq/faq-category/index'],
        ],
        [
            'label' => Yii::t('back/faq', 'faq question'),
            'url' => ['/faq/faq-question/index'],
        ],
        [
            'label' => Yii::t('back/faq', 'faq request'),
            'url' => ['/faq/faq-request/index'],
        ],
    ]
];
