<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \frontend\modules\faq\models\FaqCategory $category
 */

use frontend\modules\faq\widgets\categoryMenu\CategoryMenuWidget;
use frontend\modules\faq\widgets\requestForm\RequestFormWidget;
use yii\widgets\ListView;

?>
<div class="col-lg-3">
    <?= CategoryMenuWidget::widget() ?>
</div>
<div class="col-lg-9">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'layout' => "{items}\n{pager}"
    ]) ?>

    <?= RequestFormWidget::widget(['category' => $category]) ?>
</div>
