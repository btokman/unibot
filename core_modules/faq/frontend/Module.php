<?php

namespace frontend\modules\faq;

use common\components\interfaces\CoreModuleFrontendInterface;

/**
 * faq module definition class
 */
class Module extends \yii\base\Module implements CoreModuleFrontendInterface
{
    /**
     * @inheritdoc
     */
    //public $controllerNamespace = 'frontend\modules\faq\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
