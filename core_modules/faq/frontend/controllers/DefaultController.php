<?php

namespace frontend\modules\faq\controllers;

use frontend\modules\faq\models\FaqCategory;
use frontend\modules\faq\models\FaqQuery;
use frontend\modules\faq\models\FaqQuestion;
use frontend\modules\faq\models\FaqRequestForm;
use frontend\modules\faq\widgets\requestForm\RequestFormWidget;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `faq` module
 */
class DefaultController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        /** @var FaqQuery $query */
        $query = FaqQuestion::find()
            ->isPublished()
            ->isOrdered();

        /** @var ActiveDataProvider $dataProvider */
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1,
                'defaultPageSize' => 1,
            ],
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @param $alias
     *
     * @return string
     */
    public function actionCategory($alias)
    {
        $category = FaqCategory::find()
            ->byAlias($alias)
            ->one();

        /** @var FaqQuery $query */
        $query = FaqQuestion::find()
            ->byCategory($alias)
            ->isPublished()
            ->isOrdered();

        /** @var ActiveDataProvider $dataProvider */
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1,
                'defaultPageSize' => 1,
            ],
        ]);

        return $this->render('category', ['dataProvider' => $dataProvider, 'category' => $category]);
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionRequest()
    {
        if (\Yii::$app->request->isAjax) {
            $return = [];

            $model = new FaqRequestForm();

            if ($model->load(\Yii::$app->request->post())) {
                if ($model->save()) {
                    $return = [
                        'replaces' => [
                            [
                                'what' => '.faq-form',
                                'data' => RequestFormWidget::widget([
                                    'model' => new FaqRequestForm(),
                                    'view' => '_form'
                                ])
                            ],
                        ],
                    ];

                    return $this->asJson($return);
                }
            }

            $return = [
                'replaces' => [
                    [
                        'what' => '.faq-form',
                        'data' => RequestFormWidget::widget(['model' => $model, 'view' => '_form'])
                    ],
                ],
            ];

            return $this->asJson($return);
        }

        throw new NotFoundHttpException();
    }
}
