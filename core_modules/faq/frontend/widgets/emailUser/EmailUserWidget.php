<?php

namespace frontend\modules\faq\widgets\emailUser;

use common\models\FaqRequest;
use yii\base\Widget;

class EmailUserWidget extends Widget
{
    /** @var FaqRequest */
    public $model;

    public function run()
    {
        return $this->render('default', ['model' => $this->model]);
    }
}
