<?php

namespace frontend\modules\faq\widgets\categoryMenu;

use Yii;
use common\models\FaqCategory;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class CategoryMenuWidget extends Widget
{
    public function run()
    {
        $models = FaqCategory::find()->isPublished()->orderBy(['position' => SORT_ASC])->all();

        $_items = ArrayHelper::map($models, 'id', function ($data) {
            /** @var FaqCategory $data */

            return [
                'label' => $data->label,
                'url' => $data->getPageUrl(),
            ];
        });

        $items = ArrayHelper::merge([
            [
                'label' => Yii::t('faq/front', 'Home'),
                'url' => FaqCategory::getPagesUrl(),
            ]
        ], $_items);

        return $this->render('default', ['items' => $items]);
    }
}
