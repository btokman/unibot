<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\FaqRequest $model
 */
?>
<h1><?= Yii::t('faq/front', 'New faq request') ?></h1>
<table>
	<tr>
		<td>ID</td>
		<td><?= $model->id ?></td>
	</tr>
	<tr>
		<td><?= Yii::t('faq/front', 'name') ?></td>
		<td><?= $model->name ?></td>
	</tr>
	<tr>
		<td><?= Yii::t('faq/front', 'email') ?></td>
		<td><?= $model->email ?></td>
	</tr>
	<tr>
		<td><?= Yii::t('faq/front', 'phone') ?></td>
		<td><?= $model->phone ?></td>
	</tr>
	<tr>
		<td><?= Yii::t('faq/front', 'question') ?></td>
		<td><?= $model->question ?></td>
	</tr>
	<tr>
		<td><?= Yii::t('faq/front', 'date') ?></td>
		<td><?= $model->request_date ?></td>
	</tr>
</table>
