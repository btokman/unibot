<?php

namespace frontend\modules\faq\models;

use common\components\model\ActiveRecord;
use common\components\model\DefaultQuery;

/**
 * Class FaqQuery
 *
 * @package frontend\modules\faq\models
 */
class FaqQuery extends DefaultQuery
{
    /**
     * @return $this
     */
    public function isPublished()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;
        $table = $model::tableName();

        $this->andWhere([$table . '.published' => 1]);

        return $this;
    }

    /**
     * @param $alias
     *
     * @return $this
     */
    public function byAlias($alias)
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;
        $table = $model::tableName();

        $this->andWhere([$table . '.alias' => $alias]);

        return $this;
    }

    /**
     * @param $alias
     *
     * @return $this
     */
    public function byCategory($alias)
    {
        $table = FaqCategory::tableName();

        $this->andWhere([$table . '.alias' => $alias]);
        $this->joinWith(['category']);

        return $this;
    }

    /**
     * @param array $order
     *
     * @return $this
     */
    public function isOrdered($order = [])
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;
        $table = $model::tableName();

        $order[$table . '.position'] = SORT_ASC;
        $this->orderBy($order);

        return $this;
    }
}
