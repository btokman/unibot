<?php

namespace frontend\modules\faq\models;

/**
 * Class FaqCategory
 *
 * @package frontend\modules\faq\models
 */
class FaqCategory extends \common\models\FaqCategory
{
    /**
     * @return FaqQuery
     */
    public static function find()
    {
        return new FaqQuery(get_called_class());
    }
}
