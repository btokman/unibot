<?php

namespace frontend\modules\faq\models;

use common\models\FaqRequest;
use frontend\modules\faq\widgets\emailAdmin\EmailAdminWidget;
use frontend\modules\faq\widgets\emailUser\EmailUserWidget;
use yii\base\Model;

class FaqRequestForm extends Model
{
    public $category_id;

    public $name;

    public $email;

    public $phone;

    public $question;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => \Yii::t('faq/front', 'Enter name')],
            [['email'], 'required', 'message' => \Yii::t('faq/front', 'Enter email')],
            [['email'], 'email', 'message' => \Yii::t('faq/front', 'Email not correct')],
            [['phone'], 'required', 'message' => \Yii::t('faq/front', 'Enter phone')],
            [['question'], 'required', 'message' => \Yii::t('faq/front', 'Enter question')],
            [['question'], 'safe'],
            [['category_id'], 'integer'],
            [
                ['category_id'],
                'exist',
                'targetClass' => \common\models\FaqCategory::className(),
                'targetAttribute' => 'id'
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'category_id' => \Yii::t('front/faq', 'Category'),
            'name' => \Yii::t('front/faq', 'Name'),
            'email' => \Yii::t('front/faq', 'Email'),
            'phone' => \Yii::t('front/faq', 'Phone'),
            'question' => \Yii::t('front/faq', 'Question'),
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {
            $model = new FaqRequest();
            $attributes = $this->getAttributes();
            foreach ($attributes as $attribute => $value) {
                if ($model->hasAttribute($attribute)) {
                    $model->$attribute = $value;
                }
            }

            $date = new \DateTime();
            $model->request_date = $date->format('Y-m-d H:i:s');

            if ($model->validate() && $model->save()) {
                if ($this->sendEmail($model)) {
                    return true;
                }
            }

            $this->addError('question', \Yii::t('front/faq', 'Error save or validate')); // Паранойя но мне нравится
        }

        return false;
    }

    /**
     * @param FaqRequest $model
     *
     * @return bool
     */
    protected function sendEmail(FaqRequest $model)
    {
        if ($this->email) { // Send email to user
            $letter = (new \rmrevin\yii\postman\ViewLetter)
                ->setSubject(\Yii::t('front/faq', 'Thanks for your question'))
                ->setBody(EmailUserWidget::widget(['model' => new FaqRequest()]))
                ->addAddress([$this->email]);

            if (!$letter->send()) {
                $this->addError('question', $letter->getLastError());
            }
        }

        // Send email to admin
        $emails = \Yii::$app->config->get('faq_email');
        $emails = explode(',', $emails);

        $letter = (new \rmrevin\yii\postman\ViewLetter)
            ->setSubject(\Yii::t('front/faq', 'New faq request'))
            ->setBody(EmailAdminWidget::widget(['model' => new FaqRequest()]));

        foreach ($emails as $email) {
            $letter->addAddress($email);
        }

        if (!$letter->send()) {
            $this->addError('question', $letter->getLastError());

            return false;
        }

        return true;
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public static function getPagesUrl($params = [])
    {
        return \common\components\model\ActiveRecord::createUrl('/faq/default/request', $params);
    }
}
