<?php

namespace backend\modules\event\models;

use Yii;
use yii\helpers\ArrayHelper;
use creocoder\translateable\TranslateableBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\Configuration;
use common\models\EventPageSettings as CommonEventPageSettings;
use backend\modules\configuration\components\ConfigurationModel;

/**
 * Class EventPageSettings
 *
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package backend\modules\event\models
 */
class EventPageSettings extends ConfigurationModel
{
    /**
     * @inheritdoc
     */
    public $showAsConfig = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'translateable' => [
                    'class' => TranslateableBehavior::className(),
                    'translationAttributes' => static::getTranslationAttributes(),
                ],
                'seo' => [
                    'class' => MetaTagBehavior::className()
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        return Yii::t('back/event/page', 'Event Page Settings');
    }

    /**
     * @inheritdoc
     */
    public function getFormRules()
    {
        return [
            [CommonEventPageSettings::EVENT_INDEX_PAGE_TITLE, 'required'],
            [CommonEventPageSettings::EVENT_INDEX_PAGE_SIZE, 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFormTypes()
    {
        return [
            CommonEventPageSettings::EVENT_INDEX_PAGE_TITLE => Configuration::TYPE_STRING,
            CommonEventPageSettings::EVENT_INDEX_PAGE_SIZE => Configuration::TYPE_INTEGER,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFormDescriptions()
    {
        return [
            CommonEventPageSettings::EVENT_INDEX_PAGE_TITLE => Yii::t('back/event/page', 'Page title'),
            CommonEventPageSettings::EVENT_INDEX_PAGE_SIZE => Yii::t('back/event/page', 'List page size'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFormHints()
    {
        return [
            CommonEventPageSettings::EVENT_INDEX_PAGE_TITLE => Yii::t('back/event/page', 'Title on the Event list page'),
            CommonEventPageSettings::EVENT_INDEX_PAGE_SIZE => Yii::t('back/event/page', 'Size of the Event list page'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getTranslationAttributes()
    {
        return [
            CommonEventPageSettings::EVENT_INDEX_PAGE_TITLE
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
                    CommonEventPageSettings::EVENT_INDEX_PAGE_TITLE,
                    CommonEventPageSettings::EVENT_INDEX_PAGE_SIZE,
                ],
            ]
        ];

        return $config;
    }
}
