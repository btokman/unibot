<?php

namespace backend\modules\event\models;

use backend\modules\event\validators\DateTimeCompareValidator;
use Yii;
use yii\behaviors\TimestampBehavior;
use creocoder\translateable\TranslateableBehavior;
use metalguardian\formBuilder\ActiveFormBuilder;
use metalguardian\dateTimePicker\Widget as DateTimePicker;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\base\EntityToFile;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\models\Event as CommonEvent;
use common\models\EventPageSettings as CommonEventPageSettings;
use backend\components\BackendModel;
use backend\components\TranslateableTrait;
use backend\components\ImperaviContent;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;

/**
 * This is the model class for table "{{%event}}".
 *
 * @property int $id
 * @property string $label
 * @property string $alias
 * @property string $description
 * @property string $content
 * @property string $type
 * @property string $place_label
 * @property string $place_coords
 * @property string $datetime_start
 * @property string $datetime_end
 * @property double $price
 * @property string $price_currency
 * @property string $publish_at
 * @property int $created_at
 * @property int $updated_at
 * @property int $published
 * @property int $edit_lock
 *
 * @property EventTranslation[] $translations
 */
class Event extends ActiveRecord implements BackendModel, Translateable
{
    use TranslateableTrait;
    /**
     * Attribute for imageUploader
     */
    public $image;

    /**
     * Temporary sign which used for saving images before model save
     *
     * @var string
     */
    public $sign;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%event}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'alias', 'type', 'publish_at', 'edit_lock'], 'required'],
            [['description', 'content'], 'string'],
            [['datetime_start', 'datetime_end', 'publish_at'], 'datetime', 'format' => 'y-MM-dd HH:mm:ss'], // ICU Date/Time Format Syntax
            [['datetime_start'], DateTimeCompareValidator::className(), 'compareAttribute' => 'datetime_end', 'operator' => '<'],
            [['datetime_end'], DateTimeCompareValidator::className(), 'compareAttribute' => 'datetime_start', 'operator' => '>'],
            [['created_at', 'updated_at', 'published'], 'integer'],
            [['place_label'], 'default', 'value' => ''],
            [['published'], 'default', 'value' => 1],
            [['price'], 'default', 'value' => 0.00],
            [['label', 'alias', 'type', 'place_label', 'price_currency'], 'string', 'max' => 255],
            // Expect only '<lat>, <lang>' format, with space between.
            [['place_coords'], 'match', 'pattern' => '/^(\-?\d+(\.\d+)?),\s{1}(\-?\d+(\.\d+)?)$/i'],
            [['price_currency'], 'in', 'range' => array_keys(CommonEvent::getCurrencies())],
            [['price'], 'number', 'max' => 99999999999.99],
            [['alias'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/event', 'ID'),
            'label' => Yii::t('back/event', 'Label'),
            'alias' => Yii::t('back/event', 'Alias'),
            'description' => Yii::t('back/event', 'Description'),
            'content' => Yii::t('back/event', 'Content'),
            'type' => Yii::t('back/event', 'Type'),
            'place_label' => Yii::t('back/event', 'Place label'),
            'place_coords' => Yii::t('back/event', 'Place map coordinates'),
            'datetime_start' => Yii::t('back/event', 'Event start date and time'),
            'datetime_end' => Yii::t('back/event', 'Event end date and time'),
            'price' => Yii::t('back/event', 'Price'),
            'price_currency' => Yii::t('back/event', 'Price currency'),
            'publish_at' => Yii::t('back/event', 'Publish at'),
            'created_at' => Yii::t('back/event', 'Created at'),
            'updated_at' => Yii::t('back/event', 'Updated at'),
            'published' => Yii::t('back/event', 'Published'),
            'edit_lock' => Yii::t('back/event', 'Edit lock'),
            'image' => Yii::t('back/event', 'Image'),
        ];
    }

    /**
     * Returns the name of the column that stores the lock version for implementing optimistic locking.
     *
     * @return string
     */
    public function optimisticLock()
    {
        return 'edit_lock';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'content',
            'description',
            'place_label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(EventTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/event', 'Events');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    // 'content:ntext',
                    'published:boolean',
                    'publish_at',
                    'alias',
                    // 'description:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'publish_at',
                    'alias',
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new EventSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_name form-control'
                ],
            ],
            'alias' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_alias form-control'
                ],
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'description',
                ]
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'content',
                ]
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'publish_at' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DateTimePicker::className(),
                'options' => [
                    'options' => [
                        'class' => 'form-control',
                    ],
                ],
            ],
            'type' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CommonEvent::getTypes(),
            ],
            'place_label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ]
            ],
            'place_coords' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                /* @link https://support.google.com/maps/answer/18539 */
                'hint' => Yii::t('back/event', 'The format of coordinates is decimal degrees: latitude, longitude'),
                'options' => [
                    'maxlength' => true,
                ]
            ],
            'datetime_start' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DateTimePicker::className(),
                'options' => [
                    'options' => [
                        'class' => 'form-control',
                    ],
                ],
            ],
            'datetime_end' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DateTimePicker::className(),
                'options' => [
                    'options' => [
                        'class' => 'form-control',
                    ],
                ],
            ],
            'price' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ]
            ],
            'price_currency' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => CommonEvent::getCurrencies(),
            ],
            'image' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'image',
                    'saveAttribute' => CommonEvent::SAVE_ATTRIBUTE_IMAGE,
                    'aspectRatio' => 2 / 1,
                    'multiple' => false,
                ])
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
            'edit_lock' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
