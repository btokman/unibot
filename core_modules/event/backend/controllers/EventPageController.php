<?php

namespace backend\modules\event\controllers;

use backend\modules\event\models\EventPageSettings;
use backend\modules\configuration\components\ConfigurationController;

/**
 * EventPageController implements the CRUD actions for EventPageSettings model.
 */
class EventPageController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return EventPageSettings::className();
    }
}
