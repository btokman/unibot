<?php

namespace frontend\modules\event\models;

use Yii;
use metalguardian\fileProcessor\helpers\FPM;
use common\models\Event as BaseEvent;

/**
 * This is the model class for table "{{%event}}".
 *
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package frontend\modules\event\models
 */
class Event extends BaseEvent
{
    /**
     * Get URL string for listing Event models.
     *
     * @param array $params index route params.
     * @param bool|string $scheme the URI scheme to use in the generated URL.
     * @return string
     */
    public function getIndexUrl($params = [], $scheme = false)
    {
        return static::createUrl('/event/event/index', $params, $scheme);
    }

    /**
     * Get URL string for viewing Blog model.
     *
     * @param bool|string $scheme the URI scheme to use in the generated URL.
     * @return string
     */
    public function getViewUrl($scheme = false)
    {
        return static::createUrl('/event/event/view', ['alias' => $this->alias], $scheme);
    }

    /**
     * Get image for Blog list(index) page.
     *
     * @param string $module module image size definitions for fileProcessor module.
     * @param string $size module image size definition for fileProcessor module.
     * @param string $method name of FPM method.
     * @param array $options image tag options.
     * @return null|string
     */
    public function getIndexImage($module = 'event', $size = 'image', $method = 'image', $options = [])
    {
        return isset($this->image->file_id)
            ? FPM::$method($this->image->file_id, $module, $size, $options)
            : null;
    }

    /**
     * Get Open Graph image, with absolute URL.
     *
     * @param string $module module image size definitions for fileProcessor module.
     * @param string $size module image size definition for fileProcessor module.
     * @return string
     */
    public function getOpenGraphImage($module = 'event', $size = 'image')
    {
        return Yii::$app->getRequest()->getHostInfo() . $this->getIndexImage($module, $size, 'src');
    }

    /**
     * Get Open Graph description meta tag content.
     *
     * @return string
     */
    public function getOpenGraphDescription()
    {
        return strip_tags($this->description);
    }
}
