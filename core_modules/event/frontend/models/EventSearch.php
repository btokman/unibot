<?php

namespace frontend\modules\event\models;

use yii\data\ActiveDataProvider;

/**
 * EventSearch represents the model behind the search form about `Event`.
 */
class EventSearch extends Event
{
    /**
     * @var int index page size.
     */
    public $pageSize = 10;

    /**
     * @var \yii\db\ActiveQuery|null
     */
    private $_query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'label', 'alias', 'type', 'description', 'content', 'place_label', 'price_currency', 'place_coords'], 'safe'],
            [['datetime_start', 'datetime_end', 'publish_at'], 'datetime', 'format' => 'y-MM-dd HH:mm:ss'], // ICU Date/Time Format Syntax
            [['price'], 'integer'],
        ];
    }

    /**
     * Query getter.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuery()
    {
        if ($this->_query === null) {
            $this->_query = self::find()
                ->joinWith(['image'])
                ->andWhere(['event.published' => 1])
                ->andWhere(['<=', 'event.publish_at', date('Y-m-d H:i')]);
        }

        return $this->_query;
    }

    /**
     * Search for events.
     *
     * @param array $params query params.
     * @return ActiveDataProvider
     */
    public function searchAll($params)
    {
        $query = $this->getQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize,
                'pageSizeParam' => false,
            ],
            'sort' => [
                'defaultOrder' => [
                    'publish_at' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params, strtolower($this->formName()));

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'published' => $this->published,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'place_label', $this->place_label]);

        return $dataProvider;
    }
}
