<?php

use yii\helpers\Html;
use frontend\widgets\openGraphMetaTags\Widget as OpenGraphWidget;

/**
 * @var $this yii\web\View
 * @var $model \frontend\modules\event\models\Event
 */

OpenGraphWidget::widget([
    'title' => $model->label,
    'description' => $model->getOpenGraphDescription(),
    'url' => $model->getViewUrl(true),
    'image' => $model->getOpenGraphImage('admin', 'file'),
]);

$this->title = $model->label;
?>

<p><?= Html::a('Back to list', $model->getIndexUrl()) ?></p>

<article>

    <h1><?= $model->label ?></h1>

    <div class="well">
        Event is held:
        <strong><?= Yii::$app->getFormatter()->asDatetime(strtotime($model->datetime_start), 'short') ?></strong> -
        <strong><?= Yii::$app->getFormatter()->asDatetime(strtotime($model->datetime_end), 'short') ?></strong>
    </div>

    <div class="thumbnail">
        <?= $model->getIndexImage('admin', 'file', 'image', ['class' => 'img-responsive']) ?>
    </div>

    <ul class="list-inline">
        <li>Type: <span class="label label-info"><?= $model->getDefinedType() ?></span></li>
        <li>Price: <span class="label label-warning"><?= $model->price ?> <?= $model->getDefinedCurrency() ?></span></li>
        <li>Published at: <span class="label label-default"><?= $model->publish_at ?></span></li>
        <li>Place: <span class="label label-danger"><?= $model->place_label ?> (<?= $model->place_coords ?>)</span></li>
    </ul>

    <section><?= $model->description ?></section>
    <section><?= $model->content ?></section>

</article>
