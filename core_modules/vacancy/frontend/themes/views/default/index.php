<?php
/**
 * Created by anatolii
 *
 * @var $categories VacancyCategory[]
 * @var $staticContent VacancyStaticContent
 */
use common\models\VacancyCategory;
use common\models\VacancyStaticContent;

?>
<h1>
    <?= $staticContent->title ?>
</h1>
<p><?= $staticContent->content ?></p>
<?php foreach ($categories as $category): ?>
    <h3>Category <?= $category->label ?></h3>
    <ul>
        <?php foreach ($category->vacancies as $vacancy): ?>
            <li>
                <h5><a href="<?= $vacancy->getVacancyPageUrl() ?>">Vacancy <?= $vacancy->label ?></a></h5>
                <p><?= $vacancy->description ?></p>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endforeach; ?>
