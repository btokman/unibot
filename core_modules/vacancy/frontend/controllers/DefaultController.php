<?php

namespace frontend\modules\vacancy\controllers;

use common\models\Vacancy;
use common\models\VacancyCategory;
use common\models\VacancyStaticContent;
use frontend\components\FrontendController;
use frontend\modules\vacancy\models\VacancyRequestForm;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `vacancy` module
 */
class DefaultController extends FrontendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $categories = VacancyCategory::find()
            ->alias('t')
            ->joinWith('vacancies', true, 'RIGHT JOIN')
            ->andWhere(['t.published' => 1])
            ->orderBy('position DESC')
            ->all();

        $staticContent = (new VacancyStaticContent())->get();

        return $this->render('index', [
            'categories' => $categories,
            'staticContent' => $staticContent,
        ]);
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionVacancyPage($id)
    {
        /** @var Vacancy $model */
        $model = Vacancy::find()
            ->alias('t')
            ->where(['t.id' => $id, 't.published' => 1])
            ->joinWith('images')
            ->one();
        if (!$model) {
            throw new NotFoundHttpException();
        }
        MetaTagRegister::register($model);
        $requestForm = new VacancyRequestForm();
        $requestForm->vacancy_id = $model->id;

        return $this->render('vacancy_page', [
            'model' => $model,
            'requestForm' => $requestForm,
        ]);
    }

    /**
     * @return array
     * @throws ForbiddenHttpException
     */
    public function actionVacancyRequest()
    {
        if (!Yii::$app->request->isAjax) {
            throw new ForbiddenHttpException();
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new VacancyRequestForm();
        $model->vacancy_id = $request->get('id');
        if ($model->load($request->post())) {
            if ($model->save()) {
                return [
                    'replaces' => [
                        [
                            'what' => '#vacancy-request-form',
                            'data' => $this->renderAjax('_thanks'),
                        ]
                    ],
                ];
            }
        }

        return [
            'replaces' => [
                [
                    'what' => '#vacancy-request-form',
                    'data' => $this->renderAjax('_request_form', ['model' => $model])
                ]
            ]
        ];
    }
}
