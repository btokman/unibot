<?php

use console\components\Migration;

/**
 * Class m170124_091956_create_vacancy_table migration
 */
class m170124_091956_create_vacancy_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%vacancy}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'description' => $this->text()->null()->comment('Description'),
                'responsibilities' => $this->text()->null()->comment('Responsibilities'),
                'qualifications' => $this->text()->null()->comment('Qualifications'),
                'category_id' => $this->integer()->null()->comment('Category'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-vacancy-category_id-vacancy_category-id',
            $this->tableName,
            'category_id',
            '{{%vacancy_category}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-vacancy-category_id-vacancy_category-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
