<?php

use console\components\Migration;

/**
 * Class m170124_091553_create_vacancy_category_table_translation migration
 */
class m170124_091553_create_vacancy_category_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%vacancy_category_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%vacancy_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-vacancy_category_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-vacancy_category_translation-model_id-vacancy_category-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

