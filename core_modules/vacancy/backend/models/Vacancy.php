<?php

namespace backend\modules\vacancy\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\base\EntityToFile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%vacancy}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property string $responsibilities
 * @property string $qualifications
 * @property integer $category_id
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property VacancyCategory $category
 * @property VacancyTranslation[] $translations
 */
class Vacancy extends ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    /**
    * Attribute for imageUploader
    */
    public $images;

    /**
    * Temporary sign which used for saving images before model save
    * @var string
    */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vacancy}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['description', 'responsibilities', 'qualifications'], 'string'],
            [['category_id', 'published', 'position'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'targetClass' => \common\models\VacancyCategory::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/vacancy', 'ID'),
            'label' => Yii::t('back/vacancy', 'Label'),
            'description' => Yii::t('back/vacancy', 'Description'),
            'responsibilities' => Yii::t('back/vacancy', 'Responsibilities'),
            'qualifications' => Yii::t('back/vacancy', 'Qualifications'),
            'category_id' => Yii::t('back/vacancy', 'Category'),
            'published' => Yii::t('back/vacancy', 'Published'),
            'position' => Yii::t('back/vacancy', 'Position'),
            'created_at' => Yii::t('back/vacancy', 'Created At'),
            'updated_at' => Yii::t('back/vacancy', 'Updated At'),
            'images' => Yii::t('back/vacancy', 'Images'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
            'responsibilities',
            'qualifications',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(VacancyCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(VacancyTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/vacancy', 'Vacancy');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    // 'description:ntext',
                    // 'responsibilities:ntext',
                    // 'qualifications:ntext',
                    [
                        'attribute' => 'category_id',
                        'filter' => VacancyCategory::getItems(),
                        'format' => 'raw',
                        'value' => function ($data) {
                            return ArrayHelper::getValue($data->category, 'label', Yii::t('back/vacancy', 'Category not selected'));
                        }
                    ],
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'responsibilities',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'qualifications',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'category_id',
                        'format' => 'raw',
                        'value' => ArrayHelper::getValue($this->category, 'label', Yii::t('back/vacancy', 'Category not selected'))
                    ],
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new VacancySearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'description',
                ]
            ],
            'responsibilities' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'responsibilities',
                ]
            ],
            'qualifications' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'qualifications',
                ]
            ],
            'category_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\VacancyCategory::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'images' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'images',
                    'saveAttribute' => \common\models\Vacancy::SAVE_ATTRIBUTE_IMAGES,
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => true, //Вкл/выкл множественную загрузку
                ])
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }


    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
