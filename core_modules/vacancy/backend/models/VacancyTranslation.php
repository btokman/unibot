<?php

namespace backend\modules\vacancy\models;

use Yii;

/**
* This is the model class for table "{{%vacancy_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
* @property string $responsibilities
* @property string $qualifications
*/
class VacancyTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%vacancy_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/vacancy', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/vacancy', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/vacancy', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('back/vacancy', 'Description') . ' [' . $this->language . ']',
            'responsibilities' => Yii::t('back/vacancy', 'Responsibilities') . ' [' . $this->language . ']',
            'qualifications' => Yii::t('back/vacancy', 'Qualifications') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['description', 'responsibilities', 'qualifications'], 'string'],
            [['label'], 'string', 'max' => 255],
         ];
    }
}
