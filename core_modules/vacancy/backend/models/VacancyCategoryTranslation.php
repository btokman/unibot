<?php

namespace backend\modules\vacancy\models;

use Yii;

/**
* This is the model class for table "{{%vacancy_category_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class VacancyCategoryTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%vacancy_category_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Related model id' . ' [' . $this->language . ']',
            'language' => 'Language' . ' [' . $this->language . ']',
            'label' => 'Label' . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
         ];
    }
}
