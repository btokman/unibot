<?php

namespace backend\modules\vacancy\controllers;

use backend\components\BackendController;
use backend\modules\vacancy\models\VacancyRequest;

/**
 * VacancyRequestController implements the CRUD actions for VacancyRequest model.
 */
class VacancyRequestController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return VacancyRequest::className();
    }
}
