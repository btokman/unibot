<?php

namespace backend\modules\vacancy\controllers;

use backend\components\BackendController;
use backend\modules\vacancy\models\Vacancy;

/**
 * VacancyController implements the CRUD actions for Vacancy model.
 */
class VacancyController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Vacancy::className();
    }
}
