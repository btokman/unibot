<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%vacancy_category_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 */
class VacancyCategoryTranslation extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vacancy_category_translation}}';
    }
}
