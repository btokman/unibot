Модуль "Акции"
=

1. **Таблицы в БД**:
- *offer_category* - категории акций:
```
id              INT         
label           STRING      NOT NULL    lang 
alias           STRING      NOT NULL
published       BOOLEAN     NOT NULL
position        INT         NOT NULL
created_at      INT         NOT NULL
updated_at      INT         NOT NULL
```
- *offer* - акции:
```
id              INT         
label           STRING      NOT NULL    lang
alias           STRING      NOT NULL
description     TEXT                    lang
content         TEXT                    lang
published       BOOLEAN     NOT NULL
position        INT         NOT NULL
begin_date      INT         NOT NULL
end_date        INT         NOT NULL
created_at      INT         NOT NULL
updated_at      INT         NOT NULL
```
- *offer_to_offer_category* - таблица для связи акций и категориям:
```
offer_id            INT     NOT NULL    
offer_category_id   INT     NOT NULL
```

2. **Backend**
- модель OfferCategory для создания категорий акций;
- модель Offer для создания акций. Возможна привязка ко многим категориям через таблицу *offer_to_offer_category*.

3. **Frontend**
- модель OfferCategory содержит методы:
```
- _getSearchQuery()           - возврщает базовый ActiveQuery с условием published = true и с сортировкой по позиции; используется во всех нижестоящих методах для получения категорий акций;
+ getCategoryByAlias($alias)  - возвращает модель категории по заданому алиасу и полем published = true;

+ getCategories()             - возвращает массив всех категорий, у которых published = true по возрастанию поля position;
+ getCategoryUrl()            - возвращает адресс страницы просмотра данной категории;
+ getCategoriesDataProvider() - возвращает ActiveDataProvider со всеми категорими, у которых published = true по возрастанию поля position;
```
- модель Offer содержит методы:
```
- _getSearchQuery()                                                          - возврщает базовый ActiveQuery с условием published = true и с сортировкой по дате создания; используется во всех нижестоящих методах для получения акций;
+ getOfferByAlias($alias, $active = false)                                   - возвращает опубликованую акцию с заданым алиасом; второй параметр служит для указания того, что нужно получить активную в данный момент акцию;
+ getOffersDataProvider($categoryId = null, $active = false, $pageSize = 10) - возвращает ActiveDataProvider со всеми опубликоваными акциями; параметром $categoryId можно указать необходимую категорию акций; вторым параметром $active можно указать, что необходимо получить только активные в данный момент акции; третий параметр - количество элементов на одной странице;

+ getOfferUrl()                                  - возвращает адресс для текущей акции;
+ getCategories()                                - реляция для связи с категориями акций;
+ getCreatedDate($format = 'dd.MM.yyyy HH:mm')   - возвращает дату создания акции в указаном формате;
+ getBeginDate($format = 'dd.MM.yyyy HH:mm')     - возвращает дату начала акции в указаном формате;
+ getEndDate($format = 'dd.MM.yyyy HH:mm')       - возвращает дату окончания акции в указаном формате;
- _convertTimestampToString($timestamp, $format) - производит конвертацию даты из метки времени в стороковый вид указаного формата.
```
- хелпер OfferUrlHelper содержить методы:
```
+ getOfferCategoryUrl(array $params) - возвращает адресс страницы просмотра категории решений; в параметрах нужно передать алиас нужной категории под ключем "categoryAlias"; этот метод используется моделью OfferCategory для генерации урла методом +getCategoryUrl();
+ getOfferCategoryRoute()            - возвращает путь к экшину отображения категории акций; используется методом +getOfferCategoryUrl(array $params);
+ getOfferUrl(array $params)         - возвращает адресс страницы просмотра акции; в параметрах нужно передать алиас нужной акции под ключем "offerAlias"; этот метод используется моделью Offer для генерации урла методом +getOfferUrl();
+ getOfferRoute()                    - возвращает путь к экшину отображения акций; используется методом +getOfferUrl(array $params);
```
- в контроллере OfferController два метода:
```
+ actionViewCategory($categoryAlias)    - служит для отображения категории акций с заданым алиасом; бросает 404 если не находит опубликованую категорию с заданым алиасом;
+ actionViewOffer($offerAlias)          - служит для отображения акции с заданым алиасом и бросает 404 если не находит опубликованую акцию с заданым алиасом.
```
Для этих экшинов прописаны адресса в настройках.