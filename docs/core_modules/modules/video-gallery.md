Модуль "Видео галлерея"
=

1. **Таблицы в БД**:
- *video_album* - видео альбом:
```
id              INT         
parent_id       INT         
label           STRING      NOT NULL    lang 
alias           STRING      NOT NULL
description     TEXT                    lang
content         TEXT                    lang
author          STRING                  lang
published       BOOLEAN     NOT NULL
position        INT         NOT NULL
created_at      INT         NOT NULL
updated_at      INT         NOT NULL
```
- *video* - видео:
```
id              INT         
album_id        INT
label           STRING      NOT NULL    lang
alias           STRING      NOT NULL
description     TEXT                    lang
content         TEXT                    lang
video_link      TEXT                    lang
published       BOOLEAN     NOT NULL
position        INT         NOT NULL
created_at      INT         NOT NULL
updated_at      INT         NOT NULL
```

2. **Backend**
- модель VideoAlbum для создания альбомов видео;
- модель Video для создания видео.

3. **Frontend**

На фронте два контроллера:
```
frontend\modules\videoGallery\controllers\VideoAlbumController
frontend\modules\videoGallery\controllers\VideoController
```

В каждом из них по два экшина для рендера списка сущностей и для вывода конкретной сущности по алиасу.