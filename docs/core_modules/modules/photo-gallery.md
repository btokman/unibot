Модуль "Фото галерея"
=

1. **Таблицы в БД**:
- *photo_gallery_album* - фото альбомы:
```
id              INT         
parent_id       INT         
label           STRING      NOT NULL    lang 
alias           STRING      NOT NULL
description     TEXT                    lang
content         TEXT                    lang
author          STRING                  lang
published       BOOLEAN     NOT NULL
position        INT         NOT NULL
created_at      INT         NOT NULL
updated_at      INT         NOT NULL
```
- *photo_gallery_entity* - метаданные для фото:
```
id              INT         
file_id         INT
label           STRING                  lang
alt             STRING                  lang
description     TEXT                    lang
published       BOOLEAN     NOT NULL
position        INT         NOT NULL
created_at      INT         NOT NULL
updated_at      INT         NOT NULL
```

2. **Backend**
- модель `backend\modules\photoGallery\models\PhotoGalleryAlbum` для создания фото альбомов;
- модель `backend\modules\photoGallery\models\PhotoGalleryEntity` хранения метаданных фотографий;
- хелпер `backend\modules\photoGallery\helpers\PhotoGalleryUrlHelper` для генерации урлов для сохранения и получения форм метаданных для фотографий;
- контроллер `backend\modules\photoGallery\controllers\PhotoGalleryAlbumController` для создания альбомов фотографий;
- контроллер `backend\modules\photoGallery\controllers\PhotoGalleryEntityController` для генерации и сохранения форм с метаданными для фотографий;
- в виджет `backend\modules\imagesUpload\widgets\ImageUpload` внесены изменения, которые описаны в основной доке.

3. **Frontend**

- На фронте контроллер:
```
frontend\modules\photoGallery\controllers\PhotoGalleryController.
```
В нем есть два экшина:
```
+ actionList(string $albumAlias = null): string - служит для вывода списка альбомов (рутовых или же детей);
+ actionView(string $albumAlias): string        - служит для рендера альбома.
```
- Хелпер `frontend\modules\photoGallery\helpers\PhotoGalleryUrlHelper` служит для генерации роутов и урлов.
- Три модели:
```
frontend\modules\photoGallery\models\EntityToFile       - нужна для связи файла с методанными через реляцию + getMetaData(): ActiveQuery;
frontend\modules\photoGallery\models\PhotoGalleryAlbum  - в ней находятся основные методы для поиска сущностей
frontend\modules\photoGallery\models\PhotoGalleryEntity - модель метаданных для картинок.
```