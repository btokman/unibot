Конфигурация и конвенции
========================

Для обеспечения взаимодействия подключаемых модулей с приложением, есть следующие файлы:
1. backend/config/core_modules/modules.php - хранит перечень модулей для кофигурации backend-приложения.
2. backend/config/core_modules/menu-items.php - хранит конфигурацию пунктов главного меню для модулей в виде includes 
файлов menu-items.php из модулей (см. ниже в соглашениях п.5).
3. console/config/core_modules/migration-paths.php - хранит пути к миграциям в модулях.
4. frontend/config/core_modules/modules.php - хранит перечень модулей для кофигурации frontend-приложения.
5. frontend/config/core_modules/url-rules.php - хранит urlRules для модулей в виде includes файлов url-rules.php из 
модулей (см. ниже в соглашениях п.6).

ВАЖНО!!! Для избежания конфликтов и корректной работы конфигуратора, используем соглашения:
1. В названиях всех таблиц и моделей модуля указываем вначале название модуля. Пример, таблицы в одном модуле vacancy, 
vacancy_category, vacancy_request. Модели в одном модуле Vacancy, VacancyCategory, VacancyRequest. Консольные контроллеры
называть по такому же принципу (VacancyController).
2. В правилах урлов вначале также название модуля. Пример, ['vacancy/all' => 'vacancy/default/index',  
'vacancy/<id:\d+>' => 'vacancy/default/vacancy-page', 'vacancy/vacancy-request' => 'vacancy/default/vacancy-request']
3. Не допускается название модуля, которое идентично с началом названия другого модуля. Пример, при наличии модуля vacancy,
название vac или vaca или vacan ... не допускаются.
4. Миграции для модуля храним в самом же модуле в папке migrations.
5. Если у модуля в бэке есть ссылки в главном меню, то создаем в модуле файл menu-items/menu-items.php, где содержится
привычная структура меню для бэка в виде массива.
6. Если у модуля на фронте есть url-rules, то создаем в модуле файл url-rules/url-rules.php, где содержится привычный
массив правил.
7. В классе модуля на бэке, имплементим CoreModuleBackendInterface. 
8. В классе модуля на фронте, имплементим CoreModuleFrontendInterface. 
