Готовые модули
==============

1. [vacancy](modules/vacancy.md) - Модуль вакансий.
2. [comment](modules/comment.md) - Модуль комментариев.
3. [currency](modules/currency.md) - Модуль валют.
4. [product](modules/product.md) - Модуль e-commerce. 
5. [socialShareContent](modules/social-share-content.md) - Модуль для сохранения и отображение контента для шейринга в соцсетях.
6. [import-export](modules/import-export.md) - Модуль импорта / экспорта excel. 
