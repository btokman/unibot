<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%where_it_works_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 * @property string $description
 *
 * @property WhereItWorks $model
 */
class WhereItWorksTranslation extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%where_it_works_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('app', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('app', 'Description') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(WhereItWorks::className(), ['id' => 'model_id']);
    }
}
