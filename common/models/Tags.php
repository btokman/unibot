<?php

namespace common\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%tags}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property integer $position
 *
 * @property TagsTranslation[] $translations
 */
class Tags extends \common\components\model\ActiveRecord implements Translateable
{

    use TranslateableTrait, Translate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tags}}';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(TagsTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return $this
     */
    public function getNews($offset)
    {
         $this
            ->hasMany(News::className(), ['id' => 'news_id'])
            ->viaTable(NewsToTags::tableName(), ['tag_id' => 'id'])->offset($offset)->all();
    }

    public function getUrl()
    {
        return Url::toRoute(['/news/default/index', 'tag' => $this->alias]);
    }
}
