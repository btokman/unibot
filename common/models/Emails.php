<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%emails}}".
 *
 * @property integer $id
 * @property string $email
 * @property integer $position
 */
class Emails extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%emails}}';
    }


    /**
     * @return array
     */
    public static function getEmails()
    {
        $returnData = [];
        $data = self::find()->orderBy(['position' => SORT_ASC])->all();

        foreach ($data as $item) {
            $returnData[] = $item->email;
        }

        return $returnData;
    }
}
