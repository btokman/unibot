<?php

namespace common\models;

use common\components\ConfigurationComponent;
use common\models\base\StaticPage;
use Yii;
use yii\helpers\Json;

/**
 * This is class for static page model.
 */
class HomePage extends StaticPage
{
    const PAGE_LABEL = 'homePagePageLabel';
    const PAGE_DESCRIPTION = 'homePagePageDescription';
    const BOT_BUTTON_LABEL = 'homePageBotButtonLabel';
    const HOW_IT_WORK_LABEL = 'homePageHowItWorkLabel';
    const HOW_IT_WORK_DESCRIPTION = 'homePageHowItWorkDescription';
    const WHERE_IT_WORK_LABEL = 'homePageWhereItWorkLabel';
    const WHERE_IT_WORK_BUTTON_LABEL = 'homePageWhereItWorkButtonLabel';
    const BENEFITS_LABEL = 'homePageBenefitsLabel';
    const CASES_LABEL = 'homePageCasesLabel';
    const CASES_LINK_LABEL = 'homePageCasesLinkLabel';
    const CASES_TELEGRAM_LINK = 'homePageCasesTelegramLink';
    const CASES_MESSANGER_LINK = 'homePageCasesMessangerLink';
    const NEWS_LABEL = 'homePageNewsLabel';
    const NEWS_BUTTON_LABEL = 'homePageNewsButtonLabel';
    const NEWS = 'homePageNews';
    const CASES_BUTTON_LABEL = 'casesButtonLabel';

    const PAGE_NAME = 'pageNameHome';


    public $pageLabel;
    public $pageDescription;
    public $botButtonLabel;
    public $howItWorkLabel;
    public $howItWorkDescription;
    public $whereItWorkLabel;
    public $whereItWorkButtonLabel;
    public $benefitsLabel;
    public $casesLabel;
    public $casesLinkLabel;
    public $casesTelegramLink;
    public $casesMessangerLink;
    public $newsLabel;
    public $newsButtonLabel;
    public $news;
    public $casesBtn;
    public $pageName;

    /** @var HowItWorks[] */
    public $howItWorksItems;
    /** @var WhereItWorks[] */
    public $whereItWorksItems;
    /** @var Benefits[] */
    public $advantegesItems;
    /** @var Cases[] */
    public $cases;

    /**
     * @return array
     */
    public function getKeys()
    {
        return [
            self::PAGE_LABEL,
            self::PAGE_DESCRIPTION,
            self::BOT_BUTTON_LABEL,
            self::HOW_IT_WORK_LABEL,
            self::HOW_IT_WORK_DESCRIPTION,
            self::WHERE_IT_WORK_LABEL,
            self::WHERE_IT_WORK_BUTTON_LABEL,
            self::BENEFITS_LABEL,
            self::CASES_LABEL,
            self::CASES_LINK_LABEL,
            self::CASES_TELEGRAM_LINK,
            self::CASES_MESSANGER_LINK,
            self::NEWS_LABEL,
            self::NEWS_BUTTON_LABEL,
            self::NEWS,
            self::CASES_BUTTON_LABEL,
            self::PAGE_NAME
        ];
    }

    /**
     * @return $this
     */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->pageLabel = $config->get(self::PAGE_LABEL);
        $this->pageDescription = $config->get(self::PAGE_DESCRIPTION);
        $this->botButtonLabel = $config->get(self::BOT_BUTTON_LABEL);
        $this->howItWorkLabel = $config->get(self::HOW_IT_WORK_LABEL);
        $this->howItWorkDescription = $config->get(self::HOW_IT_WORK_DESCRIPTION);
        $this->whereItWorkLabel = $config->get(self::WHERE_IT_WORK_LABEL);
        $this->whereItWorkButtonLabel = $config->get(self::WHERE_IT_WORK_BUTTON_LABEL);
        $this->benefitsLabel = $config->get(self::BENEFITS_LABEL);
        $this->casesLabel = $config->get(self::CASES_LABEL);
        $this->casesLinkLabel = $config->get(self::CASES_LINK_LABEL);
        $this->casesTelegramLink = $config->get(self::CASES_TELEGRAM_LINK);
        $this->casesMessangerLink = $config->get(self::CASES_MESSANGER_LINK);
        $this->newsLabel = $config->get(self::NEWS_LABEL);
        $this->newsButtonLabel = $config->get(self::NEWS_BUTTON_LABEL);
        $this->news = $config->get(self::NEWS);
        $this->casesBtn = $config->get(self::CASES_BUTTON_LABEL);
        $this->pageName = $config->get(self::PAGE_NAME);

        $this->howItWorksItems = HowItWorks::getElements();
        $this->whereItWorksItems = WhereItWorks::getElements();
        $this->advantegesItems = Benefits::getElements();
        $this->cases = Cases::getElements();

        $this->news = Json::decode($config->get(self::NEWS));

        return $this;
    }

    /**
     * @return array|News[]|\yii\db\ActiveRecord[]
     */
    public function getNewsItems()
    {
        return News::getElements(3, 0 , $this->news);
    }
}
