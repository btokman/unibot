<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%bot_answer_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 *
 * @property BotAnswer $model
 */
class BotAnswerTranslation extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bot_answer_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('app', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(BotAnswer::className(), ['id' => 'model_id']);
    }
}
