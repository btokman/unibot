<?php

namespace common\models;

use common\components\ConfigurationComponent;
use common\helpers\LanguageHelper;
use common\models\base\StaticPage;
use Yii;

/**
 * This is class for static page model.
 */
class FooterSettings extends StaticPage
{
    const BOT_LABEL = 'headerBotLabel';
    const COPYRIGHT = 'footerCopirightLabel';
    const CONTACTLS_LABEL = 'footerSettingsContactlsLabel';
    const ADRESS = 'footerSettingsAdress';
    const WORK_TIME = 'footerSettingsWorkTime';
    const PHONE_LABEL = 'footerSettingsPhoneLabel';
    const PHONE = 'footerSettingsPhone';
    const EMAIL_LABEL = 'footerSettingsEmailLabel';
    const EMAIL = 'footerSettingsEmail';
    const SOCIAL_LABEL = 'footerSettingsSocialLabel';
    const TELEGRAM_SOCIAL_LINK = 'footerSettingsTelegramSocialLink';
    const FACEBOOCK_SOCIAL_LINK = 'footerSettingsFaceboockSocialLink';
    const INSTAGRAM_SOCIAL_LINK = 'footerSettingsInstagramSocialLink';
    const HEADER_LINK = 'footerSettingsHeaderLink';

    const TERMS_LABEL = 'footerSettingsTermsLabel';
    const PRIVACY_LABEL = 'footerSettingsPrivacyLabel';

    const TERMS_FILE = 'termsFileFooter';
    const PRIVACY_FILE = 'termsPrivacyFile';

    public $contactlsLabel;
    public $adress;
    public $workTime;
    public $phoneLabel;
    public $phone;
    public $emailLabel;
    public $email;
    public $socialLabel;
    public $telegramSocialLink;
    public $faceboockSocialLink;
    public $instagramSocialLink;
    public $headerLink;
    public $privacyLabel;
    public $termsLabel;

    public $botLabel;
    public $copyrightLabel;

    /**
     * @return array
     */
    public function getKeys()
    {
        return [
            self::CONTACTLS_LABEL,
            self::ADRESS,
            self::WORK_TIME,
            self::PHONE_LABEL,
            self::PHONE,
            self::EMAIL_LABEL,
            self::EMAIL,
            self::SOCIAL_LABEL,
            self::TELEGRAM_SOCIAL_LINK,
            self::FACEBOOCK_SOCIAL_LINK,
            self::INSTAGRAM_SOCIAL_LINK,
            self::BOT_LABEL,
            self::COPYRIGHT,
            self::HEADER_LINK,
            self::PRIVACY_LABEL,
            self::TERMS_LABEL
        ];
    }

    /**
     * @return $this
     */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->contactlsLabel = $config->get(self::CONTACTLS_LABEL);
        $this->adress = $config->get(self::ADRESS);
        $this->workTime = $config->get(self::WORK_TIME);
        $this->phoneLabel = $config->get(self::PHONE_LABEL);
        $this->phone = $config->get(self::PHONE);
        $this->emailLabel = $config->get(self::EMAIL_LABEL);
        $this->email = $config->get(self::EMAIL);
        $this->socialLabel = $config->get(self::SOCIAL_LABEL);
        $this->telegramSocialLink = $config->get(self::TELEGRAM_SOCIAL_LINK);
        $this->faceboockSocialLink = $config->get(self::FACEBOOCK_SOCIAL_LINK);
        $this->instagramSocialLink = $config->get(self::INSTAGRAM_SOCIAL_LINK);
        $this->botLabel = $config->get(self::BOT_LABEL);
        $this->copyrightLabel = $config->get(self::COPYRIGHT);
        $this->headerLink = $config->get(self::HEADER_LINK);
        $this->privacyLabel = $config->get(self::PRIVACY_LABEL);
        $this->termsLabel = $config->get(self::TERMS_LABEL);

        return $this;
    }

    public static function getAddress()
    {
        $config = Yii::$app->config;

        return $config->get(self::ADRESS);
    }

    public static function getWorkTime()
    {
        $config = Yii::$app->config;

        return $config->get(self::WORK_TIME);
    }

    public static function getPhoneLabel()
    {
        $config = Yii::$app->config;

        return $config->get(self::PHONE_LABEL);
    }

    public static function getPhone()
    {
        $config = Yii::$app->config;

        return $config->get(self::PHONE);
    }

    public static function getEmailLabel()
    {
        $config = Yii::$app->config;

        return $config->get(self::EMAIL_LABEL);
    }

    public static function getEmail()
    {
        $config = Yii::$app->config;

        return $config->get(self::EMAIL);
    }

    /**
     * @return array|EntityToFile|null|\yii\db\ActiveRecord
     */
    public function getPrivacyFile()
    {
        return EntityToFile::find()->where(['entity_model_name' => self::formName()])
            ->andWhere(['attribute' => self::getLangFileEntity(self::PRIVACY_FILE)])
            ->one();
    }

    /**
     * @return array|EntityToFile|null|\yii\db\ActiveRecord
     */
    public function getTermsFile()
    {
        return EntityToFile::find()->where(['entity_model_name' => self::formName()])
            ->andWhere(['attribute' => self::getLangFileEntity(self::TERMS_FILE)])
            ->one();
    }

    /**
     * Help function for multilang files
     * @return string
     */

    private static function getLangFileEntity($saveAttribute)
    {
        $language = Yii::$app->language;
        $defaultLang = LanguageHelper::getCurrent()->label;
        $language === $defaultLang ? $attribute = $saveAttribute :
            $attribute = $saveAttribute . '_' . $language;
        return $attribute;
    }

}
