<?php

namespace common\models;

use common\components\ConfigurationComponent;
use common\models\base\StaticPage;
use Yii;

/**
 * This is class for static page model.
 */
class ContactsPage extends StaticPage
{
    const PAGE_LABEL = 'contactsPagePageLabel';
    const LAT = 'contactsPageLat';
    const LNG = 'contactsPageLng';
    const FILLED_VALUE = 'contactsPageFilledValue';
    const FORM_LABEL = 'contactsPageFormLabel';
    const THANKS_MESSAGE = 'contactsPageThanksMessage';


    public $pageLabel;
    public $lat;
    public $lng;
    public $filledValue;
    public $thanksMessage;
    public $formLabel;

    /**
     * @return array
     */
    public function getKeys()
    {
        return [
            self::PAGE_LABEL,
            self::LAT,
            self::LNG,
            self::FILLED_VALUE,
            self::FORM_LABEL,
            self::THANKS_MESSAGE
        ];
    }

    /**
     * @return $this
     */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->pageLabel = $config->get(self::PAGE_LABEL);
        $this->lat = $config->get(self::LAT);
        $this->lng = $config->get(self::LNG);
        $this->filledValue = $config->get(self::FILLED_VALUE);

        return $this;
    }

    /**
     * @return mixed
     */
    public static function getFormLabel()
    {
        $config = Yii::$app->config;

        return $config->get(self::FORM_LABEL);
    }

    /**
     * @return mixed
     */
    public static function getFormThanksMessage()
    {
        $config = Yii::$app->config;

        return $config->get(self::THANKS_MESSAGE);
    }
}
