<?php

namespace common\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use Yii;

/**
 * This is the model class for table "{{%menu_elements}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $route
 * @property integer $position
 * @property integer $published
 *
 * @property MenuElementsTranslation[] $translations
 */
class MenuElements extends \common\components\model\ActiveRecord implements Translateable
{
	use TranslateableTrait, Translate;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%menu_elements}}';
	}

	/**
	 * @return array
	 */
	public static function getTranslationAttributes()
	{
		return [
			'label',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'translateable' => [
				'class' => \creocoder\translateable\TranslateableBehavior::className(),
				'translationAttributes' => static::getTranslationAttributes(),
			],
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTranslations()
	{
		return $this->hasMany(MenuElementsTranslation::className(), ['model_id' => 'id']);
	}

	/**
	 * @return array|MenuElements[]|\yii\db\ActiveRecord[]
	 */
	public static function getElements()
	{
		return self::find()->isPublished()->orderBy(['position' => SORT_ASC])->all();
	}
}
