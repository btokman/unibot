<?php

namespace common\models;

use common\components\ConfigurationComponent;
use common\models\base\StaticPage;
use Yii;

/**
 * This is class for static page model.
 */
class NotFoundPage extends StaticPage
{
    const PAGE_LABEL = 'notFoundPagePageLabel';
    const BUTTON_LABEL = 'notFoundPageButtonLabel';

    const IMAGE = 'pageNotFoundImage';
    const MOBILE_IMAGE = 'pageNotFoundMobileImage';


    public $pageLabel;
    public $buttonLabel;

    /**
     * @return array
     */
    public function getKeys()
    {
        return [
            self::PAGE_LABEL,
            self::BUTTON_LABEL,
        ];
    }

    /**
     * @return $this
     */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->pageLabel = $config->get(self::PAGE_LABEL);
        $this->buttonLabel = $config->get(self::BUTTON_LABEL);

        return $this;
    }


    /**
     * @return array|EntityToFile|null|\yii\db\ActiveRecord
     */
    public function getImage()
    {
        return EntityToFile::find()->where(['entity_model_name' => self::formName()])
            ->andWhere(['attribute' => self::IMAGE])
            ->one();
    }

    /**
     * @return array|EntityToFile|null|\yii\db\ActiveRecord
     */
    public function getMobileImage()
    {
        return EntityToFile::find()->where(['entity_model_name' => self::formName()])
            ->andWhere(['attribute' => self::MOBILE_IMAGE])
            ->one();
    }
}
