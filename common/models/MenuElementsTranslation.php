<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%menu_elements_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 *
 * @property MenuElements $model
 */
class MenuElementsTranslation extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menu_elements_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('app', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(MenuElements::className(), ['id' => 'model_id']);
    }
}
