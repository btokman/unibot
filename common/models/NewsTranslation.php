<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%news_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 * @property string $short_description
 *
 * @property News $model
 */
class NewsTranslation extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('app', 'Label') . ' [' . $this->language . ']',
            'short_description' => Yii::t('app', 'Short Description') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(News::className(), ['id' => 'model_id']);
    }
}
