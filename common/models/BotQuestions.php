<?php

namespace common\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use Yii;

/**
 * This is the model class for table "{{%bot_questions}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $position
 *
 * @property BotQuestionsTranslation[] $translations
 */
class BotQuestions extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait, Translate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bot_questions}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BotQuestionsTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return array|Benefits[]|BotQuestions[]|\yii\db\ActiveRecord[]
     */
    public static function getElements()
    {
        return self::find()->orderBy(['position' => SORT_ASC])->all();
    }

    /**
     * @return $this
     */
    public function getAnswers()
    {
        return $this
            ->hasMany(BotAnswer::className(), ['id' => 'answer_id'])
            ->viaTable(AnswerToQuestion::tableName(), ['question_id' => 'id']);
    }
}
