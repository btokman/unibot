<?php

namespace common\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;


/**
 * This is the model class for table "{{%where_it_works}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property integer $position
 *
 * @property WhereItWorksTranslation[] $translations
 */
class WhereItWorks extends \common\components\model\ActiveRecord implements Translateable
{
	const IMAGE = 'whereItWorksImage';

	use TranslateableTrait, Translate;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%where_it_works}}';
	}


	/**
	 * @return array
	 */
	public static function getTranslationAttributes()
	{
		return [
			'label',
			'description',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'translateable' => [
				'class' => \creocoder\translateable\TranslateableBehavior::className(),
				'translationAttributes' => static::getTranslationAttributes(),
			],
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTranslations()
	{
		return $this->hasMany(WhereItWorksTranslation::className(), ['model_id' => 'id']);
	}

    /**
     * @return array|HowItWorks[]|\yii\db\ActiveRecord[]
     */
    public static function getElements()
    {
        return self::find()->orderBy(['position' => SORT_ASC])->all();
    }

    /**
     * @return $this
     */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])->andOnCondition([
            'AND',
            ['attribute' => self::IMAGE],
            ['entity_model_name' => self::formName()]
        ]);
    }
}
