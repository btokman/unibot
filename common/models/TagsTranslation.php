<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tags_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 *
 * @property Tags $model
 */
class TagsTranslation extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tags_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('requests', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Tags::className(), ['id' => 'model_id']);
    }
}
