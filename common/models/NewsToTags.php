<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%news_to_tags}}".
 *
 * @property integer $id
 * @property integer $news_id
 * @property integer $tag_id
 */
class NewsToTags extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_to_tags}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('requests', 'ID'),
            'news_id' => Yii::t('requests', 'News ID'),
            'tag_id' => Yii::t('requests', 'Tag ID'),
        ];
    }

    /**
     * @param $id
     * @param int $offset
     * @return array|News[] / []
     */

    public static function getNewsByTagId($id, $offset = 0)
    {
        $data = self::findAll(['tag_id' => $id]);

        $newsIds = [];

        foreach ($data as $item) {
            $newsIds[] = $item->news_id;
        }

        return News::findWithCheckForTranslations()->isPublished()
            ->andWhere(['id' => $newsIds])
            ->orderBy(['id' => SORT_DESC])->offset($offset)->all();
    }
}
