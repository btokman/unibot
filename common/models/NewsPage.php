<?php

namespace common\models;

use common\components\ConfigurationComponent;
use common\models\base\StaticPage;
use Yii;
use yii\helpers\Url;

/**
 * This is class for static page model.
 */
class NewsPage extends StaticPage
{
    const PAGE_LABEL = 'newsPagePageLabel';
    const PAGE_SUB_LABEL = 'newsPagePageSubLabel';
    const READ_MORE_BUTTON = 'newsPageReadMoreButton';
    const SHARE_BUTTON = 'newsPageShareBtn';


    public $pageLabel;
    public $pageSubLabel;
    public $readMoreButton;
    public $shareBtn;

    /**
     * @return array
     */
    public function getKeys()
    {
        return [
            self::PAGE_LABEL,
            self::PAGE_SUB_LABEL,
            self::READ_MORE_BUTTON,
            self::SHARE_BUTTON
        ];
    }

    /**
     * @return $this
     */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->pageLabel = $config->get(self::PAGE_LABEL);
        $this->pageSubLabel = $config->get(self::PAGE_SUB_LABEL);
        $this->readMoreButton = $config->get(self::READ_MORE_BUTTON);
        $this->shareBtn = $config->get(self::SHARE_BUTTON);

        return $this;
    }

    /**
     * @return string
     */
    public static function getUrl()
    {
        return Url::toRoute(['/news/default/index']);
    }
}
