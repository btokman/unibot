<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%bot_questions_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 *
 * @property BotQuestions $model
 */
class BotQuestionsTranslation extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bot_questions_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('app', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(BotQuestions::className(), ['id' => 'model_id']);
    }
}
