<?php

namespace common\models;

use common\components\ConfigurationComponent;
use common\models\base\StaticPage;
use Yii;
use yii\helpers\Url;

/**
 * This is class for static page model.
 */
class FormSettings extends StaticPage
{
    const FORM_LABEL = 'formSettingsFormLabel';
    const NAME_PLACEHOLDER = 'formSettingsNamePlaceholder';
    const PHONE_PLACEHOLDER = 'formSettingsPhonePlaceholder';
    const EMAIL_PLACEHOLDER = 'formSettingsEmailPlaceholder';
    const MESSAGE_PLACEHOLDER = 'formSettingsMessagePlaceholder';
    const BUTTON_LABEL = 'formSettingsButtonLabel';
    const SUCCESS_MESSAGE = 'formSettingsSuccessMessage';

    const NAME_ERROR = 'formNameError';
    const EMAIL_ERROR = 'formEmailError';
    const PHONE_EROROR = 'phoneError';

    const PHONE_VALIDATION_ERROR = 'phoneVaildationError';
    const EMAIL_VALIDATION_ERROR = 'emailValdiateionError';


    public $formLabel;
    public $namePlaceholder;
    public $phonePlaceholder;
    public $emailPlaceholder;
    public $messagePlaceholder;
    public $buttonLabel;
    public $successMessage;

    /**
     * @return array
     */
    public function getKeys()
    {
        return [
            self::FORM_LABEL,
            self::NAME_PLACEHOLDER,
            self::PHONE_PLACEHOLDER,
            self::EMAIL_PLACEHOLDER,
            self::MESSAGE_PLACEHOLDER,
            self::BUTTON_LABEL,
            self::SUCCESS_MESSAGE,
            self::EMAIL_ERROR,
            self::NAME_ERROR,
            self::PHONE_EROROR,
            self::PHONE_VALIDATION_ERROR,
            self::EMAIL_VALIDATION_ERROR
        ];
    }

    /**
     * @return $this
     */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->formLabel = $config->get(self::FORM_LABEL);
        $this->namePlaceholder = $config->get(self::NAME_PLACEHOLDER);
        $this->phonePlaceholder = $config->get(self::PHONE_PLACEHOLDER);
        $this->emailPlaceholder = $config->get(self::EMAIL_PLACEHOLDER);
        $this->messagePlaceholder = $config->get(self::MESSAGE_PLACEHOLDER);
        $this->buttonLabel = $config->get(self::BUTTON_LABEL);
        $this->successMessage = $config->get(self::SUCCESS_MESSAGE);

        return $this;
    }

    public static function getFormUrl()
    {
        return Url::toRoute(['/site/form']);
    }

    public static function getNameError()
    {
        $config = Yii::$app->config;

        return $config->get(self::NAME_ERROR);
    }

    public static function getEmailError()
    {
        $config = Yii::$app->config;

        return $config->get(self::EMAIL_ERROR);
    }

    public static function getEmailValidationError()
    {
        $config = Yii::$app->config;

        return $config->get(self::EMAIL_VALIDATION_ERROR);
    }

    public static function getPhoneError()
    {
        $config = Yii::$app->config;

        return $config->get(self::PHONE_EROROR);
    }

    public static function getPhoneValidationError()
    {
        $config = Yii::$app->config;

        return $config->get(self::PHONE_VALIDATION_ERROR);
    }
}
