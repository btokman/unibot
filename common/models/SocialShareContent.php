<?php

namespace common\models;

use backend\components\TranslateableTrait;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use creocoder\translateable\TranslateableBehavior;


/**
 * @inheritdoc
 */
class SocialShareContent extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'model_id',
            'title',
            'description',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(SocialShareContentTranslation::className(), ['model_id' => 'id']);
    }
}
