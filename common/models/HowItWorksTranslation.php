<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%how_it_works_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 * @property string $description
 *
 * @property HowItWorks $model
 */
class HowItWorksTranslation extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%how_it_works_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('app', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('app', 'Description') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(HowItWorks::className(), ['id' => 'model_id']);
    }
}
