<?php

namespace common\models;

use common\components\ConfigurationComponent;
use common\models\base\StaticPage;
use Yii;

/**
 * This is class for static page model.
 */
class BotSettings extends StaticPage
{
    const LAS_MESSAGE = 'botSettingsLasMessage';
    const SUCCESS_MESSAGE = 'botSettingsSuccessMessage';
    const EMAIL_PLACEHOLDER = 'botEmailPlaceholder';
    const EMAIL_ERROR_MESSAGE = 'botEmailErrorMessage';
    const STOP_BTN = 'botStopBtn';
    const MAIL_BTN = 'botMailBtn';
    const STOP_MESSAGE = 'botStopMessage';


    public $lasMessage;
    public $successMessage;
    public $botPlaceholder;
    public $errorMessage;
    public $stopMessage;

    public $stomBtn;
    public $mailBtn;

    /**
     * @return array
     */
    public function getKeys()
    {
        return [
            self::LAS_MESSAGE,
            self::SUCCESS_MESSAGE,
            self::EMAIL_PLACEHOLDER,
            self::EMAIL_ERROR_MESSAGE,
            self::STOP_BTN,
            self::MAIL_BTN,
            self::STOP_MESSAGE
        ];
    }

    /**
     * @return $this
     */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->lasMessage = $config->get(self::LAS_MESSAGE);
        $this->successMessage = $config->get(self::SUCCESS_MESSAGE);
        $this->botPlaceholder = $config->get(self::EMAIL_PLACEHOLDER);
        $this->errorMessage = $config->get(self::EMAIL_ERROR_MESSAGE);
        $this->stomBtn = $config->get(self::STOP_BTN);
        $this->mailBtn = $config->get(self::MAIL_BTN);
        $this->stopMessage = $config->get(self::STOP_MESSAGE);

        return $this;
    }
}
