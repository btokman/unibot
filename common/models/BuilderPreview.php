<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%builder_preview}}".
 *
 * @property integer $id
 * @property string $hash
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 */
class BuilderPreview extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%builder_preview}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }
}
