<?php

namespace common\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use Yii;

/**
 * This is the model class for table "{{%benefits}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property integer $position
 *
 * @property BenefitsTranslation[] $translations
 */
class Benefits extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait, Translate;

    const IMAGE = 'benefitsImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%benefits}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BenefitsTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return array|HowItWorks[]|\yii\db\ActiveRecord[]
     */
    public static function getElements()
    {
        return self::find()->orderBy(['position' => SORT_ASC])->all();
    }

    /**
     * @return $this
     */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])->andOnCondition([
            'AND',
            ['attribute' => self::IMAGE],
            ['entity_model_name' => self::formName()]
        ]);
    }
}
