<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%cases_links}}".
 *
 * @property integer $id
 * @property string $link
 * @property integer $position
 */
class CasesLinks extends \common\components\model\ActiveRecord
{
    const IMAGE = 'casesIcon';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cases_links}}';
    }

    /**
     * @return $this
     */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])->andOnCondition([
            'AND',
            ['attribute' => self::IMAGE],
            ['entity_model_name' => self::formName()]
        ]);
    }
}
