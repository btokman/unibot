<?php
namespace common\models;

use backend\components\TranslateableTrait;
use creocoder\translateable\TranslateableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * @inheritdoc
 *
 * @property ConfigurationTranslation[] $translations
 */
class Configuration extends \common\models\base\Configuration implements \common\components\model\Translateable
{
    use TranslateableTrait;

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'value',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ConfigurationTranslation::className(), ['model_id' => 'id']);
    }

}
