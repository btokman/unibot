<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%answer_to_question}}".
 *
 * @property integer $id
 * @property integer $question_id
 * @property integer $answer_id
 */
class AnswerToQuestion extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%answer_to_question}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'question_id' => Yii::t('app', 'Question ID'),
            'answer_id' => Yii::t('app', 'Answer ID'),
        ];
    }
}
