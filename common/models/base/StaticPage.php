<?php
/**
 * Created by anatolii
 */

namespace common\models\base;


use common\components\ConfigurationComponent;
use common\components\model\Model;
use Yii;

/**
 * Class StaticPage is the base class of static page models
 *
 * @package common\models\base
 */
abstract class StaticPage extends Model
{
    public function init()
    {
        parent::init();

        /** @var ConfigurationComponent $config */
        Yii::$app->config->loadConfigsByKeys($this->getKeys());
    }

    /**
     * @return int
     */
    public function getId()
    {
        return 1;
    }

    /**
     * @return array
     */
    public function getKeys()
    {
        return [];
    }

    /**
     * For preparing static page model attributes
     */
    abstract function get();
}
