<?php

namespace common\models\builder;

use backend\modules\builder\components\BuilderImageUpload;
use backend\widgets\FileUploader;
use common\components\BuilderModel;
use common\helpers\LanguageHelper;
use common\helpers\Site;
use common\models\EntityToFile;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;

/**
 * Class ImageBuilderModel
 *
 * @package backend\modules\builder\models\builder
 */
class ImageBuilderModel extends BuilderModel
{
    const SAVE_BUILDER_IMAGE = 'BuilderImage';
    public $image;

    public function getName()
    {
        return 'Image';
    }

    public function rules()
    {
        $rules = ArrayHelper::merge(parent::rules(), [
            [['image'], 'safe'],
        ]);

        return $this->prepareRules($rules);
    }

    public function getAttributeLabels()
    {
        $labels = [
            'image' => 'Image',
        ];

        return $this->prepareLabels($labels);
    }

    public function getConfig()
    {
        return ArrayHelper::merge(parent::getDefaultConfig(), [
            'image' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => FileUploader::className(),
                'label' => false,
                'options' => [
                    'model' => $this,
                    'attributeLabel' => \Yii::t('back/developersPage', 'Image'),
                    'saveAttribute' => self::SAVE_BUILDER_IMAGE,
                    'multiLang' => true,
                    'allowedFileExtensions' => ['png', 'jpg']
                ],
            ],
        ]);
    }

    public function getLocalized()
    {
        return [];
    }

    public function getLangImage()
    {
        return EntityToFile::find()->where([
            'entity_model_name' => static::formName(),
            'entity_model_id' => $this->id,
            'attribute' => self::getLangFileEntity(self::SAVE_BUILDER_IMAGE)
        ])->orderBy(['position' => SORT_ASC])->one();
    }


    /**
     * Help function for multilang files
     * @return string
     */

    private static function getLangFileEntity($saveAttribute)
    {
        $language = \Yii::$app->language;
        $defaultLang = LanguageHelper::getCurrent()->label;
        $language === $defaultLang ? $attribute = $saveAttribute :
            $attribute = $saveAttribute . '_' . $language;
        return $attribute;
    }
}
