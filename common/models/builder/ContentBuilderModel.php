<?php

namespace common\models\builder;

use common\components\BuilderModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;

/**
 * Class ContentBuilderModel
 *
 * @package backend\modules\builder\models\builder
 */
class ContentBuilderModel extends BuilderModel
{
    public $text;

    public function getName()
    {
        return \Yii::t('builder', 'Content');
    }

    public function rules()
    {
        $rules = ArrayHelper::merge(parent::rules(), [
            [['text'], 'safe'],
        ]);

        return $this->prepareRules($rules);
    }

    public function getAttributeLabels()
    {
        $labels = [
            'content' => 'Content',
        ];

        return $this->prepareLabels($labels);
    }

    public function getConfig()
    {
        return ArrayHelper::merge(parent::getDefaultConfig(), [
            'text' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\TextWidget::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'text',
                ],
                'label' => false
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ]);
    }

    public function getLocalized()
    {
        return ['text'];
    }

    public function attributeLabels()
    {
        return [
            'text' => \Yii::t('builder', 'Text')
        ];
    }
}
