<?php

namespace common\models;

use common\components\CommonBuilderBehavior;
use common\components\model\DefaultQuery;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property string $short_description
 * @property integer $position
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property NewsTranslation[] $translations
 */
class News extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait, Translate;

    public $content = [];

    const PER_PAGE = 7;

    const IMAGE = 'newsPreviewImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }


    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'short_description',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'content' => [
                'class' => CommonBuilderBehavior::className(),
                'attributes' => ['content'],
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var DefaultQuery $model */
                    $model->select(['published', 'alias', 'updated_at']);
                    $model->andWhere(['published' => 1]);
                },
                'dataClosure' => function ($model) {
                    /** @var  $model self */
                    $result = [];

                    $result = [
                        'loc' => $model->getUrl(true),
                        'lastmod' => $model->updated_at,
                        'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];

                    return $result;
                }
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(NewsTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @param $limit
     * @return array|News[]|\yii\db\ActiveRecord[]
     */
    public static function getElements($limit, $offset, $ids = [])
    {
        $data = self::findWithCheckForTranslations()->isPublished()
            ->orderBy(['id' => SORT_DESC]);

        if (count($ids) > 0) $data->andWhere(['id' => $ids]);

        return $data->limit($limit)->offset($offset)->all();
    }

    /**
     * @return $this
     */
    public function getPreviewImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])->andOnCondition([
            'AND',
            ['attribute' => self::IMAGE],
            ['entity_model_name' => self::formName()]
        ]);
    }

    /**
     * @param bool $absolute
     * @return string
     */
    public function getUrl($absolute = false)
    {
        return Url::toRoute(['/news/default/item', 'alias' => $this->alias], $absolute);
    }

    /**
     * @return $this
     */
    public function getTags()
    {
        return $this
            ->hasMany(Tags::className(), ['id' => 'tag_id'])
            ->viaTable(NewsToTags::tableName(), ['news_id' => 'id']);
    }

    /**
     * @param $string
     * @return string
     */
    public static function toCdata($string)
    {
        return '<![CDATA[' . $string . ']]>';
    }
}
