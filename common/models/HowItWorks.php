<?php

namespace common\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use Yii;

/**
 * This is the model class for table "{{%how_it_works}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property integer $position
 * @property integer $page_id
 *
 * @property HowItWorksTranslation[] $translations
 */
class HowItWorks extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait, Translate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%how_it_works}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(HowItWorksTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return array|HowItWorks[]|\yii\db\ActiveRecord[]
     */
    public static function getElements()
    {
	    return self::find()->orderBy(['position' => SORT_ASC])->all();
    }
}
