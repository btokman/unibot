<?php

namespace common\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use Yii;

/**
 * This is the model class for table "{{%bot_answer}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $position
 * @property integer $next_question
 * @property integer $is_last
 *
 * @property BotAnswerTranslation[] $translations
 */
class BotAnswer extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait, Translate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bot_answer}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BotAnswerTranslation::className(), ['model_id' => 'id']);
    }

    public function getQuestion()
    {
        return BotQuestions::findOne(['id' => $this->next_question]);
    }
}
