<?php
return [
    [
        'model_id'  => 'test-1',
        'language'  => 'uk',
        'value'     => 'test translation data 1'
    ],
    [
        'model_id'  => 'test-2',
        'language'  => 'uk',
        'value'     => 'test translation data 2'
    ],
    [
        'model_id'  => 'test-3',
        'language'  => 'uk',
        'value'     => 'test translation data 3'
    ],
];
