PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: test_table
DROP TABLE IF EXISTS test_table;
CREATE TABLE test_table (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	test_field STRING (255) NOT NULL
);

-- Table: test_relation_table
DROP TABLE IF EXISTS test_relation_table;
CREATE TABLE test_relation_table(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  rel_table_id INTEGER NOT NULL,
  test_data_field STRING (255),
  language STRING
);

-- Table: language
DROP TABLE IF EXISTS language;
CREATE TABLE language(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  label STRING (20) NOT NULL,
  code STRING (5) NOT NULL UNIQUE,
  locale STRING (5) NOT NULL,
  published INTEGER (1) NOT NULL DEFAULT 1,
  position INTEGER (1) NOT NULL DEFAULT 0,
  is_default INTEGER (1) NOT NULL DEFAULT 0,
  created_at INTEGER NOT NULL DEFAULT 0000000000,
  updated_at INTEGER NOT NULL DEFAULT 0000000000
);

-- Table: configuration
DROP TABLE IF EXISTS configuration;
CREATE TABLE configuration(
  id STRING (100) PRIMARY KEY,
  value TEXT DEFAULT NULL,
  type INTEGER NOT NULL,
  description STRING DEFAULT NULL,
  preload INTEGER (1) NOT NULL DEFAULT 1,
  published INTEGER (1) NOT NULL DEFAULT 1,
  create_at INTEGER NOT NULL DEFAULT 0000000000,
  updated_at INTEGER NOT NULL DEFAULT 0000000000,
  show INTEGER NOT NULL DEFAULT 1
);

-- Table: configuration_translation
DROP TABLE IF EXISTS configuration_translation;
CREATE TABLE configuration_translation(
  model_id STRING (100) NOT NULL PRIMARY KEY,
  language STRING (16) NOT NULL,
  value TEXT DEFAULT NULL
);

-- Table: user
DROP TABLE IF EXISTS user;
CREATE TABLE user(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username STRING (255) NOT NULL,
  auth_key STRING (32) NOT NULL,
  password_hash STRING (255) NOT NULL,
  password_reset_token STRING (255) NULL,
  email STRING (255) NOT NULL,
  status INTEGER (6) NOT NULL,
  created_at INTEGER NOT NULL DEFAULT 0000000000,
  updated_at INTEGER NOT NULL DEFAULT 0000000000,
  block_at INTEGER NULL DEFAULT 0000000000
);

-- Table: ip_block
DROP TABLE IF EXISTS ip_block;
CREATE TABLE ip_block(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  date INTEGER NOT NULL,
  ip STRING (255) NOT NULL,
  host STRING (255) NOT NULL
);

-- Table: user_auth_log
DROP TABLE IF EXISTS user_auth_log;
CREATE TABLE user_auth_log(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  userId INTEGER NULL,
  date INTEGER NULL,
  cookieBased INTEGER (1) NULL,
  duration INTEGER NULL,
  error STRING (255) NULL,
  ip STRING (255) NULL,
  host STRING (255) NULL,
  url STRING (255) NULL,
  userAgent STRING (255) NULL
);

-- Table: entity_to_file
DROP TABLE IF EXISTS entity_to_file;
CREATE TABLE entity_to_file(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  entity_model_name STRING (255) NOT NULL,
  entity_model_id INTEGER NOT NULL,
  file_id INTEGER NOT NULL,
  temp_sign STRING (255) NOT NULL DEFAULT '',
  position INTEGER NOT NULL DEFAULT 0,
  attribute STRING (255) NULL
);

-- Table: fpm_file
DROP TABLE IF EXISTS fpm_file;
CREATE TABLE fpm_file(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  extension STRING (10) NOT NULL,
  base_name STRING (255) NULL,
  alt_tag STRING (255) NULL,
  created_at INTEGER NOT NULL DEFAULT 0000000000
);

-- Table: robots
DROP TABLE IF EXISTS robots;
CREATE TABLE robots(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  text TEXT NULL
);

-- Table: test_migration_generator
DROP TABLE IF EXISTS test_migration_generator;
CREATE TABLE test_migration_generator(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  label STRING NOT NULL,
  content TEXT NOT NULL,
  published INTEGER (1) NOT NULL DEFAULT 1,
  position INTEGER NOT NULL DEFAULT 0,
  created_at INTEGER NOT NULL DEFAULT 0000000000,
  updated_at INTEGER NOT NULL DEFAULT 0000000000
);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;