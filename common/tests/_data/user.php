<?php

return [
    [
        'id'                => 1,
        'username'          => 'tester',
        'auth_key'          => Yii::$app->params['authKey'],
        'password_hash'     => Yii::$app->getSecurity()->generatePasswordHash('test'),
        'email'             => 'tester@test.test',
        'status'            => \common\models\User::STATUS_ACTIVE,
    ]
];
