<?php
namespace common\tests\base\unit;

use yii\test\FixtureTrait;

/**
 * Base db test case for unit tests.
 * Provides work with fixtures.
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
abstract class DbTestCase extends TestCase
{
    use FixtureTrait;

    /**
     * @inheritdoc
     */
    protected function init()
    {
        $this->loadFixtures();
    }

    /**
     * @inheritdoc
     */
    protected function tearDown()
    {
        $this->unloadFixtures();
        parent::tearDown();
    }

    /**
     * @inheritdoc
     */
    abstract public function fixtures();
}
