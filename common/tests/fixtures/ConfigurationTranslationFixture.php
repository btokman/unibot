<?php
namespace common\tests\fixtures;

use yii\test\ActiveFixture;

/**
 * Fixture for [[\common\models\ConfigurationTranslation]]
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class ConfigurationTranslationFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\ConfigurationTranslation';
    /**
     * @inheritdoc
     */
    public $dataFile = '@data/configuration-translation.php';
}
