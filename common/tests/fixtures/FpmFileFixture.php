<?php
namespace common\tests\fixtures;

use yii\test\ActiveFixture;

/**
 * Fixture for [[\common\models\FpmFile]] model
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class FpmFileFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\FpmFile';
    /**
     * @inheritdoc
     */
    public $dataFile = '@common/tests/_data/fpm-file.php';


    /**
     * Get files list
     *
     * @return array
     */
    public function getFiles()
    {
        return $this->getData();
    }
}
