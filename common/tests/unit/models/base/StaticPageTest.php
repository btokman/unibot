<?php
namespace common\tests\unit\models\base;

use common\tests\base\unit\DbTestCase;
use common\tests\fake\models\StaticPage;
use common\tests\fixtures\ConfigurationFixture;

/**
 * Test case for [[\common\models\base\StaticPage]]
 * @see \common\models\base\StaticPage
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class StaticPageTest extends DbTestCase
{
    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'configuration' => ['class' => ConfigurationFixture::className()]
        ];
    }

    public function testGet()
    {
        /* @var StaticPage $model */
        $model = (new StaticPage())->get();

        $this->assertEquals($model->first, 'test data 3');
        $this->assertEquals($model->second, 'test data 4');
    }
}
