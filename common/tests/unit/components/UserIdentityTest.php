<?php
namespace common\tests\unit\components;

use Yii;
use common\components\UserIdentity;
use common\models\User;
use common\tests\base\unit\DbTestCase;
use common\tests\fixtures\UserFixture;

/**
 * Test case for [[\common\components\UserIdentity]]
 * @see \common\components\UserIdentity
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class UserIdentityTest extends DbTestCase
{
    /**
     * @var UserIdentity
     */
    private $userComponent;
    /**
     * @var User
     */
    private $user;


    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'user' => ['class' => UserFixture::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();
        $this->user = User::findOne(1);
        $this->userComponent = new UserIdentity($this->user);
    }

    public function testInstanceOf()
    {
        $this->assertInstanceOf('yii\web\IdentityInterface', $this->userComponent);
    }

    public function testFindIdentity()
    {
        $this->specify(
            'find exists user',
            function () {
                $expected = new UserIdentity(User::findOne($this->user->id));
                $actual = UserIdentity::findIdentity($this->user->id);
                $this->assertEquals($expected, $actual);
            }
        );

        $this->specify(
            'find not exists user',
            function () {
                $this->assertNull(UserIdentity::findIdentity('not exists id'));
            }
        );
    }

    public function testFindByEmail()
    {
        $this->specify(
            'find by exists email',
            function () {
                $expected = new UserIdentity(User::findOne(['email' => $this->user->email]));
                $actual = UserIdentity::findByEmail($this->user->email);
                $this->assertEquals($expected, $actual);
            }
        );

        $this->specify(
            'find by not exists email',
            function () {
                $this->assertNull(UserIdentity::findByEmail('not exists email'));
            }
        );
    }

    public function testFindIdentityByAccessToken()
    {
        $this->expectException('yii\base\NotSupportedException');
        UserIdentity::findIdentityByAccessToken('token');
    }

    public function testGetId()
    {
        $this->assertEquals($this->user->id, $this->userComponent->getId());
    }

    public function testGetAuthKey()
    {
        $this->assertEquals(Yii::$app->params['authKey'], $this->userComponent->getAuthKey());
    }

    public function testValidateAuthKey()
    {
        $actual = $this->userComponent->validateAuthKey(Yii::$app->params['authKey']);
        $this->assertTrue($actual);

        $actual = $this->userComponent->validateAuthKey('wrong auth key');
        $this->assertFalse($actual);
    }

    public function testValidatePassword()
    {
        $actual = $this->userComponent->validatePassword('test');
        $this->assertTrue($actual);

        $actual = $this->userComponent->validatePassword('wrong password');
        $this->assertFalse($actual);
    }

    public function testGetUser()
    {
        $this->assertEquals($this->user, $this->userComponent->getUser());
    }

    public function testGetUsername()
    {
        $this->assertEquals($this->user->username, $this->userComponent->getUsername());
    }
}
