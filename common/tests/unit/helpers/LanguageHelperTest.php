<?php
namespace common\tests\unit\helpers;

use common\helpers\LanguageHelper;
use common\models\Language;
use common\tests\base\unit\DbTestCase;
use common\tests\fixtures\LanguageFixture;

/**
 * Test case for [[\common\helpers\LanguageHelper]]
 * @see \common\helpers\LanguageHelper
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class LanguageHelperTest extends DbTestCase
{
    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'language' => ['class' => LanguageFixture::className()],
        ];
    }

    public function testGetDefaultLanguage()
    {
        $expected = Language::findOne(['is_default' => true]);
        $actual = LanguageHelper::getDefaultLanguage();

        $this->assertEquals($expected, $actual);
    }

    public function testGetCurrent()
    {
        $expected = Language::findOne(['is_default' => true]);
        $actual = LanguageHelper::getCurrent();
        $this->assertEquals($expected, $actual);

        LanguageHelper::setCurrent('uk');
        $expected = Language::findOne(['code' => 'uk']);
        $actual = LanguageHelper::getCurrent();
        $this->assertEquals($expected, $actual);
    }

    public function testIsCurrentDefault()
    {
        LanguageHelper::setCurrent('en');
        $this->assertTrue(LanguageHelper::isCurrentDefault());

        LanguageHelper::setCurrent('uk');
        $this->assertFalse(LanguageHelper::isCurrentDefault());
    }

    public function testGetLanguageModels()
    {
        $expected = Language::find()
            ->where(['published' => true])
            ->orderBy(['position' => SORT_DESC])
            ->all();
        $actual = LanguageHelper::getLanguageModels();

        $this->assertEquals($expected, $actual);
    }

    public function testSetCurrent()
    {
        $code = 'uk';

        $this->assertTrue(LanguageHelper::setCurrent($code));
        $this->assertEquals(
            Language::findOne(['code' => $code]),
            LanguageHelper::getCurrent()
        );
    }

    public function testGetApplicationLanguages()
    {
        $expected = Language::find()
            ->select('locale')
            ->where(['published' => true])
            ->orderBy(['position' => SORT_DESC])
            ->column();
        $actual = LanguageHelper::getApplicationLanguages();

        $this->assertEquals($expected, $actual);
    }
}
