<?php
return [
    'db' => require(__DIR__ . '/db.php'),
    'config' => [
        'class' => \common\components\ConfigurationComponent::className(),
    ],
];
