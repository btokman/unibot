<?php

namespace common\components\model;
use common\helpers\LanguageHelper;

/**
 * Class ActiveRecord
 * @package common\components\model
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    use Helper;
    use RelatedFormHelpTrait;

    /**
     * @inheritdoc
     * @return DefaultQuery
     */
    public static function find()
    {
        return new DefaultQuery(get_called_class());
    }

    /**
     * @return mixed
     */
    public function getLabelForUpdateAction()
    {
        switch (true) {
            case $this->hasAttribute('label') && $this->label:
                return $this->label;
            case $this->hasAttribute('name') && $this->name:
                return $this->name;
        }

        return $this->id;
    }


    /**
     * @param array $translationColumnsToCheck
     * @return DefaultQuery
     */
    public static function findWithCheckForTranslations($translationColumnsToCheck = ['label'])
    {
        $query = self::find();

        return self::addCheckForTranslations($query, $translationColumnsToCheck);
    }

    /**
     * @param \yii\db\ActiveQuery $query
     * @param array $translationColumnsToCheck
     * @return DefaultQuery|\yii\db\ActiveQuery
     */
    public static function addCheckForTranslations(\yii\db\ActiveQuery $query, $translationColumnsToCheck = ['label'])
    {
        $defaultLang = LanguageHelper::getDefaultLanguage()->locale ?? '';
        $currentLanguage = \Yii::$app->language;

        if ($currentLanguage != $defaultLang) {
            $query->joinWith([
                'translations' => function (\yii\db\ActiveQuery $query) use ($currentLanguage) {
                    return $query->alias('tr')->andOnCondition(['tr.language' => $currentLanguage]);
                }
            ], false);

            foreach ($translationColumnsToCheck as $column) {
                $query->andWhere(['<>', "tr.$column", '']);
            }
        }

        return $query;
    }
}
