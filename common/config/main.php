<?php
use \metalguardian\fileProcessor\helpers\FPM;

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'fileProcessor' => [
            'class' => '\metalguardian\fileProcessor\Module',
            'imageSections' => [
                /*'module' => [
                    'size' => [
                        'action' => 'frame',
                        'width' => 400,
                        'height' => 200,
                        'startX' => 100,
                        'startY' => 100,
                    ],
                ],*/
                'admin' => [
                    'file' => [
                        'action' => FPM::ACTION_THUMBNAIL,
                        'width' => 100,
                        'height' => 100,
                    ],
                ],
                'builder' => [
                    'preview' => [
                        'action' => FPM::ACTION_THUMBNAIL,
                        'width' => 400,
                        'height' => 300,
                    ],
                ],
            ],
        ],
    ],
    'components' => [
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'formatter' => [
            'timeFormat' => 'H:i:s',
            'dateFormat' => 'Y-m-d',
        ],
        'i18n' => [
            'class'=> vintage\i18n\components\I18N::className(),
            'enableCaching' => YII_ENV_PROD,
            'cache' => 'i18nCache',
        ],
        'i18nCache' => [
            'class' => \yii\caching\FileCache::className(),
            'cachePath' => '@frontend/runtime/cache',
        ],
        'urlManager' => [
            'class' => '\common\components\UrlManager',
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'enableLanguageDetection' => false,
            'rules' => [],
        ],
        'postman' => [
            'class' => 'rmrevin\yii\postman\Component',
            'driver' => 'smtp',
            'default_from' => ['unibot@gmail.com', 'Unibot'],
            'subject_prefix' => '',
            'subject_suffix' => null,
            'table' => '{{%postman_letter}}',
            'view_path' => '/email',
            'smtp_config' => [
                'host' => 'smtp.gmail.com',
                'port' => 465,
                'auth' => true,
                'user' => 'vintagedevmail@gmail.com',
                'password' => 'alypew68',
                'secure' => 'ssl',
                'debug' => false,
            ]
        ],
    ],
    'sourceLanguage' => 'en',
    'language' => 'en',
];
