<?php
return [
    '' => 'home/default/index',
    'robots.txt' => 'site/robots',
    'sitemap.xml' => 'sitemap/default/index',

    'contacts' => 'contacts/default/index',

    'request' => 'site/form',
    'email' => 'site/email',

    'blog/<alias>' => 'news/default/item',


    'blog' => 'news/default/index',

];
